10	67834151	6783415	1	DoubleEG
10	83821723	8382172	1	DoubleMuon
10	69844103	6984410	1	MET
10	118905015	11890501	1	SingleElectron
10	546841468	54684146	1	SingleMuon
10	1535429153	153542915	1	DYBJetsToLL_M-50_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8
10	2526763208	252676320	1	DYBJetsToLL_M-50_Zpt-200toInf_TuneCP5_13TeV-madgraphMLM-pythia8
10	552776873	55277687	1	DYJetsToLL_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	983363467	98336346	1	DYJetsToLL_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	1266161516	126616151	1	DYJetsToLL_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	1656248828	165624882	1	DYJetsToLL_LHEFilterPtZ-0To50_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	2929692669	292969266	1	DYJetsToLL_LHEFilterPtZ-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	4743453530	474345353	1	DYJetsToLL_LHEFilterPtZ-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	4910029642	491002964	1	DYJetsToLL_LHEFilterPtZ-400To650_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	2384184186	238418418	1	DYJetsToLL_LHEFilterPtZ-50To100_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	5240806677	524080667	1	DYJetsToLL_LHEFilterPtZ-650ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	47992387	4799238	1	DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8
10	1330412789	133041278	1	DYJetsToLL_M-50_HT-100to200_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8
10	2869893828	286989382	1	DYJetsToLL_M-50_HT-1200to2500_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8
10	1715099105	171509910	1	DYJetsToLL_M-50_HT-200to400_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8
10	3239393665	323939366	1	DYJetsToLL_M-50_HT-2500toInf_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8
10	2106497307	210649730	1	DYJetsToLL_M-50_HT-400to600_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8
10	2265349551	226534955	1	DYJetsToLL_M-50_HT-600to800_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8
10	1144674647	114467464	1	DYJetsToLL_M-50_HT-70to100_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8
10	2554033961	255403396	1	DYJetsToLL_M-50_HT-800to1200_TuneCP5_PSweights_13TeV-madgraphMLM-pythia8
10	745996502	74599650	1	DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	1545889431	154588943	1	DYJetsToLL_M-50_Zpt-100to200_BPSFilter_TuneCP5_13TeV-madgraphMLM-pythia8
10	2596675673	259667567	1	DYJetsToLL_M-50_Zpt-200toInf_BPSFilter_TuneCP5_13TeV-madgraphMLM-pythia8
10	1579683234	157968323	1	DYJetsToLL_Pt-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	2491495129	249149512	1	DYJetsToLL_Pt-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	2846666068	284666606	1	DYJetsToLL_Pt-400To650_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	1177344765	117734476	1	DYJetsToLL_Pt-50To100_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	2424389095	242438909	1	DYJetsToLL_Pt-650ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	499920274	49992027	1	QCD_HT1000to1500_TuneCP5_PSWeights_13TeV-madgraph-pythia8
10	35362546	3536254	1	QCD_HT100to200_TuneCP5_PSWeights_13TeV-madgraph-pythia8
10	899765413	89976541	1	QCD_HT1500to2000_TuneCP5_PSWeights_13TeV-madgraph-pythia8
10	1293548104	129354810	1	QCD_HT2000toInf_TuneCP5_PSWeights_13TeV-madgraph-pythia8
10	41907574	4190757	1	QCD_HT200to300_TuneCP5_PSWeights_13TeV-madgraph-pythia8
10	64964803	6496480	1	QCD_HT300to500_TuneCP5_PSWeights_13TeV-madgraph-pythia8
10	132396655	13239665	1	QCD_HT500to700_TuneCP5_PSWeights_13TeV-madgraph-pythia8
10	29243707	2924370	1	QCD_HT50to100_TuneCP5_PSWeights_13TeV-madgraph-pythia8
10	245477274	24547727	1	QCD_HT700to1000_TuneCP5_PSWeights_13TeV-madgraph-pythia8
10	932722852	93272285	1	ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8
10	458527322	45852732	1	ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8
10	420864468	42086446	1	ST_t-channel_antitop_5f_InclusiveDecays_TuneCP5_13TeV-powheg-pythia8
10	452451337	45245133	1	ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8
10	413424535	41342453	1	ST_t-channel_top_5f_InclusiveDecays_TuneCP5_13TeV-powheg-pythia8
10	1531992201	153199220	1	ST_tW_antitop_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8
10	745210258	74521025	1	ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8
10	1533346032	153334603	1	ST_tW_top_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8
10	932573553	93257355	1	ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8
10	2090681368	209068136	1	TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8
10	12682219810	1268221981	1	TTToHadronic_TuneCP5_13TeV-powheg-pythia8
10	3258377201	325837720	1	TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8
10	1147144655	114714465	1	WBJetsToLNu_Wpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8
10	2083638501	208363850	1	WBJetsToLNu_Wpt-200toInf_TuneCP5_13TeV-madgraphMLM-pythia8
10	1022248072	102224807	1	WHJet_HToBB_WToLNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
10	170384502	17038450	1	WJetsToLNu_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	556956646	55695664	1	WJetsToLNu_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	819824787	81982478	1	WJetsToLNu_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	829282701	82928270	1	WJetsToLNu_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8
10	2202502067	220250206	1	WJetsToLNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8
10	1197794222	119779422	1	WJetsToLNu_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8
10	2728654140	272865414	1	WJetsToLNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8
10	1692490765	169249076	1	WJetsToLNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8
10	1931000914	193100091	1	WJetsToLNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8
10	676155432	67615543	1	WJetsToLNu_HT-70To100_TuneCP5_13TeV-madgraphMLM-pythia8
10	2172829429	217282942	1	WJetsToLNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8
10	96046601638	9604660163	1	WJetsToLNu_Pt-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	2359229980	235922998	1	WJetsToLNu_Pt-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	2530577429	253057742	1	WJetsToLNu_Pt-400To600_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	2572822440	257282244	1	WJetsToLNu_Pt-600ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	310009401	31000940	1	WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	37530720358	3753072035	1	WJetsToLNu_Wpt-100to200_BPSFilter_TuneCP5_13TeV-madgraphMLM-pythia8
10	2384483830	238448383	1	WJetsToLNu_Wpt-200toInf_BPSFilter_TuneCP5_13TeV-madgraphMLM-pythia8
10	1026458094	102645809	1	WWTo2L2Nu_TuneCP5_13TeV-powheg-pythia8
10	477489594	47748959	1	WW_TuneCP5_13TeV-pythia8
10	1012026674	101202667	1	WZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	728738624	72873862	1	WZTo3LNu_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	573731974	57373197	1	WZTo3LNu_mllmin4p0_TuneCP5_13TeV-powheg-pythia8
10	403697876	40369787	1	WZ_TuneCP5_13TeV-pythia8
10	1655181202	165518120	1	ZHJet_HToBB_ZToLL_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
10	690675024	69067502	1	ZZTo2L2Nu_TuneCP5_13TeV_powheg_pythia8
10	1065001983	106500198	1	ZZTo2Q2L_mllmin4p0_TuneCP5_13TeV-amcatnloFXFX-pythia8
10	711582204	71158220	1	ZZTo4L_TuneCP5_13TeV_powheg_pythia8
10	339757346	33975734	1	ZZ_TuneCP5_13TeV-pythia8
10	40212800903	4021280090	1	split_part1
10	39280585620	3928058562	1	split_part2
10	16531355935	1653135593	1	split_part3
10	24806784	2480678	1	split_part4
10	8393237270	839323727	1	split_part_1
10	4241839097	424183909	1	split_part_2
10	24792428	2479242	1	split_part_3
10	24792428	2479242	1	split_part_4
