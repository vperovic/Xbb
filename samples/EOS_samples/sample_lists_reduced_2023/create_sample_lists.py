import os
import sys
import math

def generate_root_tree_list(path_to_sample):
    total_size = 0
    file_count = 0
    root_tree_list = []
    for path, currentDirectory, files in os.walk(path_to_sample):
        for file in files:
            path_to_file = os.path.join(path, file)
            if (".root" in path_to_file) and ("FAIL" not in path_to_file) and ("Fail" not in path_to_file) and ("Junk" not in path_to_file) and (".root.sw" not in path_to_file) and (file_count < 10):
                #print(path_to_file)
                root_tree_list.append(path_to_file)
                file_count += 1
                total_size += os.path.getsize(path_to_file)
    return root_tree_list, file_count, total_size


def create_text_files(parent_directory, textdirectory, year):
    children = next(os.walk(parent_directory))[1]
    if not os.path.exists(textdirectory):
         os.mkdir(textdirectory)

    list_file = open(year + "_SAMPLE_LIST_SIZES.TXT", "a")
    submission_file = open(year + "_SUBMISSION_FILE.TXT", "a")

    for sample_name in children:
        sample_path = parent_directory + sample_name
        text_name = textdirectory + "/" + sample_name + ".txt"
        print(sample_path)
        tree_list, file_count, total_size = generate_root_tree_list(sample_path)
        textfile = open(text_name, "w")
        for element in tree_list:
            textfile.write(element + "\n")
        textfile.close()

        bigmem = False
        if file_count == 0 or total_size == 0:
            print(tree_list)
            list_file.write(str(file_count) + "\t" + str(total_size) + "\t" + str("NaN") + "\t" + str("NaN") + "\t" + str(sample_name) + "\n")
            continue

        number_of_files_per_job = int(math.ceil(file_count*500000000.0/total_size))
        if number_of_files_per_job == 1:
            bigmem = True
        while (((file_count/number_of_files_per_job)<1000) and (number_of_files_per_job > 1)):
            number_of_files_per_job = number_of_files_per_job/2

        list_file.write(str(file_count) + "\t" + str(total_size) + "\t" + str(total_size/file_count) + "\t" + str(number_of_files_per_job) + "\t" + str(sample_name) + "\n")
        submission_file.write("./submit.py -T Zll" + year + " -J run --modules=Prep.Step1 -F prep/" + sample_name + " --force --input PREPin --output PREPout --set='Directories.samplefiles:=<!Directories|samplefiles_split!>' -S " + sample_name + " -N "+ str(number_of_files_per_job) + " %s\n" %("--queue=bigmem.q" if bigmem else ""))
    list_file.close()
    submission_file.close()


create_text_files("/eos/cms/store/group/phys_higgs/ec/hbb/ntuples/VHbbPostNano/UL/EFT/2016_preVFP/V1/", "2016preVFP_ec", "2016preVFP")
create_text_files("/eos/cms/store/group/phys_higgs/hbb/ntuples/VHbbPostNano/UL/EFT/2016_preVFP/V1/", "2016preVFP_ph", "2016preVFP")

create_text_files("/eos/cms/store/group/phys_higgs/ec/hbb/ntuples/VHbbPostNano/UL/EFT/2016/V1/", "2016_ec", "2016")
create_text_files("/eos/cms/store/group/phys_higgs/hbb/ntuples/VHbbPostNano/UL/EFT/2016/V1/", "2016_ph", "2016")

create_text_files("/eos/cms/store/group/phys_higgs/ec/hbb/ntuples/VHbbPostNano/UL/EFT/2017/V1/", "2017_ec", "2017")
create_text_files("/eos/cms/store/group/phys_higgs/hbb/ntuples/VHbbPostNano/UL/EFT/2017/V1/", "2017_ph", "2017")

create_text_files("/eos/cms/store/group/phys_higgs/ec/hbb/ntuples/VHbbPostNano/UL/EFT/2018/V1/", "2018_ec", "2018")
create_text_files("/eos/cms/store/group/phys_higgs/hbb/ntuples/VHbbPostNano/UL/EFT/2018/V1/", "2018_ph", "2018")


#create_text_files("/eos/cms/store/group/phys_higgs/ec/hbb/ntuples/VHbbPostNano/UL/EFT/2017/V1/SingleMuon/", "2017_SingleMuon")
#create_text_files("/eos/cms/store/group/phys_higgs/ec/hbb/ntuples/VHbbPostNano/UL/EFT/2017/V1/DoubleMuon/", "2017_DoubleMuon")
#create_text_files("/eos/cms/store/group/phys_higgs/ec/hbb/ntuples/VHbbPostNano/UL/EFT/2017/V1/EGamma/", "2017_EGamma")





