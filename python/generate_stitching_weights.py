#!/usr/bin/env python
import ROOT
import os
import sys
ROOT.gROOT.SetBatch(True)
from myutils import BetterConfigParser, ParseInfo
import glob
from multiprocessing import Process
from myutils.sampleTree import SampleTree as SampleTree
import argparse
import itertools
from myutils.BranchList import BranchList
from myutils.XbbConfig import XbbConfigReader, XbbConfigTools

parser = argparse.ArgumentParser(description='run scripts that compute stitching coefficients for samples with overlap')
parser.add_argument('--output', dest='output', help='Output destination', default = "stitching.ini")
parser.add_argument('--from', dest='fromFolder', help='folder name to use, e.g. PREPout', default='SYSout')
parser.add_argument('--year', dest='year', help='year', default = "2018")
parser.add_argument('--channel', dest='channel', help='channel', default = "Zll")
args = parser.parse_args()


fromFolder = str(args.fromFolder)
year = str(args.year)
channel = str(args.channel)
output = str(args.output)


Tag = channel + year 



"""
This script generates the stitching weight config according to the dictionaries defined below.
The usage is the following:


python generate_stitching_weights.py --output test --from BOOSTout --year 2016 --channel Zll


The cuts are either on LHE_Vpt, on LHE_HT or on LHE_NpNLO.
The script getExtWeightsNew.py which is called does a tensor product of the cuts in NpNLO and Vpt.
The output is written in the config folder of the channel.
The naming will generate the corresponding tags for stitching weights
"""




DY_samples = { "DYJetsToLL_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_NpNLO==0", 
               "DYJetsToLL_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_NpNLO==1",
               "DYJetsToLL_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_NpNLO==2",
               "DYJetsToLL_LHEFilterPtZ-0To50_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_Vpt<50",
               "DYJetsToLL_LHEFilterPtZ-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_Vpt>=100&&LHE_Vpt<250",
               "DYJetsToLL_LHEFilterPtZ-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_Vpt>=250&&LHE_Vpt<400",
               "DYJetsToLL_LHEFilterPtZ-400To650_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8": "LHE_Vpt>=400&&LHE_Vpt<650",
               "DYJetsToLL_LHEFilterPtZ-50To100_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8":"LHE_Vpt>=50&&LHE_Vpt<100",
               "DYJetsToLL_LHEFilterPtZ-650ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8":"LHE_Vpt>=650",
               #LO sample, need to add it also
               "DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8":"",
               "DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8" : ""
                }

WJ_samples = { "WJetsToLNu_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_NpNLO==0",
               "WJetsToLNu_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_NpNLO==1",
               "WJetsToLNu_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_NpNLO==2",
               "WJetsToLNu_Pt-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_Vpt>=100&&LHE_Vpt<250",
               "WJetsToLNu_Pt-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_Vpt>=250&&LHE_Vpt<400",
               "WJetsToLNu_Pt-400To600_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"LHE_Vpt>=400&&LHE_Vpt<600",
               "WJetsToLNu_Pt-600ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" : "LHE_Vpt>=600",
               "WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8" : "" 
            }

ZJ_samples = { "ZJetsToNuNu_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8" :"LHE_HT>=100&&LHE_HT<200",
               "ZJetsToNuNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8" :"LHE_HT>=1200&&LHE_HT<2500",
               "ZJetsToNuNu_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8" :"LHE_HT>=200&&LHE_HT<400",
               "ZJetsToNuNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8" :"LHE_HT>=2500",
               "ZJetsToNuNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8" :"LHE_HT>=400&&LHE_HT<600",
               "ZJetsToNuNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8" :"LHE_HT>=600&&LHE_HT<800",
               "ZJetsToNuNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8" :"LHE_HT>=800&&LHE_HT<1200"
            }








#Allsamples = [DY_samples, WJ_samples, ZJ_samples]
Allsamples = [DY_samples]
#Allsamples = [WJ_samples,ZJ_samples]

for s, samples in enumerate(Allsamples):
    sample_names = ""
    sample_cuts_Jets = ""
    sample_cuts_Pt = ""
    for sample_name,sample_cut in samples.iteritems():
        sample_names+=sample_name 
        sample_names+=","

        if len(sample_cut)>0: 
            
            if "NpNLO" in sample_cut:
                sample_cuts_Jets+=sample_cut
                sample_cuts_Jets+=","
            else:
                sample_cuts_Pt+=sample_cut
                sample_cuts_Pt+=","

    sample_names = sample_names[:-1]
    sample_cuts = (len(sample_cuts_Jets)>0)*(sample_cuts_Jets[:-1] + ";") + sample_cuts_Pt[:-1]

    print("the samples")
    print(sample_names)
    print("the cuts")
    print(sample_cuts)

    
    
    f = open("stitching_weights/" + "test" + ".sh", "w")    
    #f.write("cd ..\n")
    f.write("python getExtWeightsNew.py --samples '" + sample_names + "' --fc '" + sample_cuts + "' --from "  + fromFolder + " -T "  + Tag + " --prune 0 --output " + output)
   
    f.close()

    os.system(("stitching_weights/test.sh" %vars() ))

 
    #os.system(("echo getExtWeightsNew.py --samples %(sample_names)s --fc %(sample_cuts)s --from %(fromFolder)s -T %(Tag)s --prune 0" %vars() ))  
        























