import os
import time


def submit_list(filename):
    user = os.getlogin()
    countstring = "squeue -u " + user + " | wc -l"
    myfile = open(filename, "r")
    completed = False
    
    myline = myfile.readline()
    while myline:
        hold = True
        while hold:
           num = int(os.popen(countstring).read())
           print(num)
           if num > 300:
               time.sleep(60) #60 for normal run
           else: hold = False
    
        os.system(myline)
        myline = myfile.readline()
    myfile.close()



#-----------------------------------------------------------------------------
#------------------EFT--------------------------------------------------------
#-----------------------------------------------------------------------------

#submit_list("submission_files/EFT_2017.TXT")




#-----------------------------------------------------------------------------
#------------------BOOST------------------------------------------------------
#-----------------------------------------------------------------------------

#submit_list("submission_files/BOOST_2018.TXT")
#submit_list("submission_files/BOOST_2017.TXT")
#submit_list("submission_files/BOOST_2016.TXT")
#submit_list("submission_files/BOOST_2016preVFP.TXT")




#-----------------------------------------------------------------------------
#------------------KINFIT-----------------------------------------------------
#-----------------------------------------------------------------------------

#submit_list("submission_files/KINFIT_2018.TXT")
#submit_list("submission_files/KINFIT_2017.TXT")
#submit_list("submission_files/KINFIT_2016.TXT")
#submit_list("submission_files/KINFIT_2016preVFP.TXT")




#-----------------------------------------------------------------------------
#------------------SYS--------------------------------------------------------
#-----------------------------------------------------------------------------

#submit_list("submission_files/SYS_2018.TXT")
#submit_list("submission_files/SYS_2017.TXT")
#submit_list("submission_files/SYS_2016.TXT")
#submit_list("submission_files/SYS_2016preVFP.TXT")




#-----------------------------------------------------------------------------
#------------------HADD------------------------------------------------------
#-----------------------------------------------------------------------------

#submit_list("submission_files/HADD.TXT")




#-----------------------------------------------------------------------------
#------------------COUNT ===== RUN MANUALLY!!!--------------------------------
#-----------------------------------------------------------------------------





#-----------------------------------------------------------------------------
#------------------PREP3------------------------------------------------------
#-----------------------------------------------------------------------------

#submit_list("submission_files/PREP3_2018_Wlv_SUBMISSION.TXT")

#submit_list("submission_files/PREP3_2017_Wlv_SUBMISSION.TXT")

#submit_list("submission_files/PREP3_2016_Wlv_SUBMISSION.TXT")

#submit_list("submission_files/PREP3_2016preVFP_Wlv_SUBMISSION.TXT")
#-----------------------------------------------------------------------------



#-----------------------------------------------------------------------------
#------------------PREP2------------------------------------------------------
#-----------------------------------------------------------------------------

#submit_list("submission_files/PREP2_2018_Zll_SUBMISSION.TXT")
#submit_list("submission_files/PREP2_2018_Wlv_SUBMISSION.TXT")
#submit_list("submission_files/PREP2_2018_Zvv_SUBMISSION.TXT")

#submit_list("submission_files/PREP2_2017_Zll_SUBMISSION.TXT")
#submit_list("submission_files/PREP2_2017_Wlv_SUBMISSION.TXT")
#submit_list("submission_files/PREP2_2017_Zvv_SUBMISSION.TXT")

#submit_list("submission_files/PREP2_2016_Zll_SUBMISSION.TXT")
#submit_list("submission_files/PREP2_2016_Wlv_SUBMISSION.TXT")
#submit_list("submission_files/PREP2_2016_Zvv_SUBMISSION.TXT")

#submit_list("submission_files/PREP2_2016preVFP_Zll_SUBMISSION.TXT")
#submit_list("submission_files/PREP2_2016preVFP_Wlv_SUBMISSION.TXT")
#submit_list("submission_files/PREP2_2016preVFP_Zvv_SUBMISSION.TXT")
#-----------------------------------------------------------------------------





#-----------------------------------------------------------------------------
#------------------PREP1------------------------------------------------------
#-----------------------------------------------------------------------------

#submit_list("submission_files/PREP1_2018_ZLL_SUBMISSION.TXT")
#submit_list("submission_files/PREP1_2018_WLV_SUBMISSION.TXT")
#submit_list("submission_files/PREP1_2018_ZVV_SUBMISSION.TXT")

#submit_list("submission_files/PREP1_2017_ZLL_SUBMISSION.TXT")
#submit_list("submission_files/PREP1_2017_WLV_SUBMISSION.TXT")
#submit_list("submission_files/PREP1_2017_ZVV_SUBMISSION.TXT")

#submit_list("submission_files/PREP1_2016_ZLL_SUBMISSION.TXT")
#submit_list("submission_files/PREP1_2016_WLV_SUBMISSION.TXT")
#submit_list("submission_files/PREP1_2016_ZVV_SUBMISSION.TXT")

#submit_list("submission_files/PREP1_2016preVFP_ZLL_SUBMISSION.TXT")
#submit_list("submission_files/PREP1_2016preVFP_WLV_SUBMISSION.TXT")
#submit_list("submission_files/PREP1_2016preVFP_ZVV_SUBMISSION.TXT")
#-----------------------------------------------------------------------------



