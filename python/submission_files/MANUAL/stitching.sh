python generate_stitching_weights.py --from BOOSTout --year 2016preVFP --channel Zvv >> submission_files/MANUAL/stitching_log/stitching_Zvv2016preVFP.out 
python generate_stitching_weights.py --from BOOSTout --year 2016preVFP --channel Zll >> submission_files/MANUAL/stitching_log/stitching_Zll2016preVFP.out 
python generate_stitching_weights.py --from BOOSTout --year 2016preVFP --channel Wlv >> submission_files/MANUAL/stitching_log/stitching_Wlv2016preVFP.out 
python generate_stitching_weights.py --from BOOSTout --year 2017 --channel Zvv >> submission_files/MANUAL/stitching_log/stitching_Zvv2017.out 
python generate_stitching_weights.py --from BOOSTout --year 2017 --channel Zll >> submission_files/MANUAL/stitching_log/stitching_Zll2017.out 
python generate_stitching_weights.py --from BOOSTout --year 2017 --channel Wlv >> submission_files/MANUAL/stitching_log/stitching_Wlv2017.out 


