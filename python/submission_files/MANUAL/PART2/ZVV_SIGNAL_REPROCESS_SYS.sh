./submit.py -T Zll2016preVFP -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
./submit.py -T Wlv2016preVFP -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
./submit.py -T Zvv2016preVFP -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8

#./submit.py -T Zll2016 -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
#./submit.py -T Wlv2016 -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
#./submit.py -T Zvv2016 -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8

./submit.py -T Zll2017 -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
./submit.py -T Wlv2017 -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
./submit.py -T Zvv2017 -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8

#./submit.py -T Zll2018 -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
#./submit.py -T Wlv2018 -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
#./submit.py -T Zvv2018 -F sysnew-sys -J sysnew --addCollections Sys.all --force --input SYSin --output SYSout -S ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
