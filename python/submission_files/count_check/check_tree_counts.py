import os
import sys
import math

def generate_root_tree_list(path_to_sample):
    total_size = 0
    file_count = 0
    root_tree_list = []
    for path, currentDirectory, files in os.walk(path_to_sample):
        for file in files:
            path_to_file = os.path.join(path, file)
            if (".root" in path_to_file) and ("FAIL" not in path_to_file) and ("Fail" not in path_to_file) and ("Junk" not in path_to_file) and (".root.sw" not in path_to_file):
                if os.path.getsize(path_to_file) > 1:
                    file_count += 1
                else:
                   print(path_to_file)
    return file_count


def count_lines(textfile):
    with open(textfile, 'r') as fp:
        for count, line in enumerate(fp):
            pass
        return count+1


def check_prep_files(parent_directory):
    year = parent_directory.split("/")[9][12:-3]
    channel = parent_directory.split("/")[11]
    sample_lists_dir = "/work/vperovic/CMSSW_10_1_0/src/Xbb/samples/VHbbPostNano"+ year + "_UL_V1/"
    processed_samples = next(os.walk(parent_directory))[1]
    for sample_name in processed_samples:
        sample_path = parent_directory + sample_name
        print(sample_path)
        tree_count = generate_root_tree_list(sample_path)
        line_count = count_lines(sample_lists_dir + sample_name + ".txt")
        if not (tree_count == line_count): print("-------------------------------------------------MISMATCH------------------------------------------")
        print(line_count, tree_count)
        print("\n\n\n")
    

check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/fglessge/VHbb/VHbbPostNano2018_UL/V1/Zll/prep2/")
check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/fglessge/VHbb/VHbbPostNano2018_UL/V1/Wlv/prep3/")
check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/fglessge/VHbb/VHbbPostNano2018_UL/V1/Zvv/prep2/")
#check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/vperovic/VHbb/VHbbPostNano2017_UL/V1/Zll/prep2/")
#check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/vperovic/VHbb/VHbbPostNano2017_UL/V1/Wlv/prep3/")
#check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/vperovic/VHbb/VHbbPostNano2017_UL/V1/Zvv/prep2/")
#check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/acalandr/VHbb/VHbbPostNano2016_UL/V1/Zll/prep2/")
#check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/acalandr/VHbb/VHbbPostNano2016_UL/V1/Wlv/prep3/")
#check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/acalandr/VHbb/VHbbPostNano2016_UL/V1/Zvv/prep2/")
#check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/chatterj/VHbb/VHbbPostNano2016preVFP_UL/V1/Zll/prep2/")
#check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/chatterj/VHbb/VHbbPostNano2016preVFP_UL/V1/Wlv/prep3/")
#check_prep_files("/pnfs/psi.ch/cms/trivcat/store/user/chatterj/VHbb/VHbbPostNano2016preVFP_UL/V1/Zvv/prep2/")






