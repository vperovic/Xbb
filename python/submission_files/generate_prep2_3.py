import os
import sys
import math

def generate_root_tree_list(path_to_sample):
    total_size = 0
    file_count = 0
    root_tree_list = []
    for path, currentDirectory, files in os.walk(path_to_sample):
        for file in files:
            path_to_file = os.path.join(path, file)
            if (".root" in path_to_file) and ("FAIL" not in path_to_file) and ("Fail" not in path_to_file) and ("Junk" not in path_to_file) and (".root.sw" not in path_to_file):
                #print(path_to_file)
                root_tree_list.append(path_to_file)
                file_count += 1
                total_size += os.path.getsize(path_to_file)
    return root_tree_list, file_count, total_size


#def create_prep_file(parent_directory, year, channel):
def create_prep_file(parent_directory):
    year = parent_directory.split("/")[9][12:-3]
    channel = parent_directory.split("/")[11]
    children = next(os.walk(parent_directory))[1]
    list_file = open(year + "_" + channel + "_SIZES_PREP2.TXT", "w")
    submission_file = open("PREP2_" + year + "_" + channel + "_SUBMISSION.TXT", "w")
    if channel == "Wlv": 
        list_file_step3 = open(year + "_" + channel + "_SIZES_PREP3.TXT", "w")
        submission_file_step3 = open("PREP3_" + year + "_" + channel + "_SUBMISSION.TXT", "w")

    for sample_name in children:
        sample_path = parent_directory + sample_name
        print(sample_path)
        tree_list, file_count, total_size = generate_root_tree_list(sample_path)

        bigmem = False
        veryshort = False
        if file_count == 0 or total_size == 0:
            print(tree_list)
            list_file.write(str(file_count) + "\t" + str(total_size) + "\t" + str("NaN") + "\t" + str("NaN") + "\t" + str(sample_name) + "\n")
            continue

        number_of_files_per_job = int(math.ceil(file_count*500000000.0/total_size))
        if number_of_files_per_job > 20:
            veryshort = True
        if number_of_files_per_job == 1:
            bigmem = True
        while (((file_count/number_of_files_per_job)<1000) and (number_of_files_per_job > 1)):
            number_of_files_per_job = number_of_files_per_job//2

        list_file.write(str(file_count) + "\t" + str(total_size) + "\t" + str(total_size/file_count) + "\t" + str(number_of_files_per_job) + "\t" + str(sample_name) + "\n")
        submission_file.write("./submit.py -T "+ channel + year + " -J run --modules=Prep.Step2 -F prep2/" + sample_name + " --force --input PREP2in --output PREP2out --set='Directories.samplefiles:=<!Directories|samplefiles_split!>' -S " + sample_name + " -N "+ str(number_of_files_per_job) + "%s" %(" --queue=bigmem.q" if bigmem else "") + " %s\n" %("--queue=veryshort.q" if veryshort else ""))
        if channel == "Wlv":
            list_file_step3.write(str(file_count) + "\t" + str(total_size) + "\t" + str(total_size/file_count) + "\t" + str(number_of_files_per_job) + "\t" + str(sample_name) + "\n")
            submission_file_step3.write("./submit.py -T "+ channel + year + " -J run --modules=Prep.Step3 -F prep3/" + sample_name + " --force --input PREP3in --output PREP3out --set='Directories.samplefiles:=<!Directories|samplefiles_split!>' -S " + sample_name + " -N "+ str(number_of_files_per_job) + "%s" %(" --queue=bigmem.q" if bigmem else "") + " %s\n" %("--queue=veryshort.q" if veryshort else ""))
        
    list_file.close()
    submission_file.close()
    if channel == "Wlv":
        list_file_step3.close()
        submission_file_step3.close()
    

create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/fglessge/VHbb/VHbbPostNano2018_UL/V1/Zll/prep/")
create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/fglessge/VHbb/VHbbPostNano2018_UL/V1/Wlv/prep/")
create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/fglessge/VHbb/VHbbPostNano2018_UL/V1/Zvv/prep/")


create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/vperovic/VHbb/VHbbPostNano2017_UL/V1/Zll/prep/")
create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/vperovic/VHbb/VHbbPostNano2017_UL/V1/Wlv/prep/")
create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/vperovic/VHbb/VHbbPostNano2017_UL/V1/Zvv/prep/")


create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/acalandr/VHbb/VHbbPostNano2016_UL/V1/Zll/prep/")
create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/acalandr/VHbb/VHbbPostNano2016_UL/V1/Wlv/prep/")
create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/acalandr/VHbb/VHbbPostNano2016_UL/V1/Zvv/prep/")


create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/chatterj/VHbb/VHbbPostNano2016preVFP_UL/V1/Zll/prep/")
create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/chatterj/VHbb/VHbbPostNano2016preVFP_UL/V1/Wlv/prep/")
create_prep_file("/pnfs/psi.ch/cms/trivcat/store/user/chatterj/VHbb/VHbbPostNano2016preVFP_UL/V1/Zvv/prep/")






