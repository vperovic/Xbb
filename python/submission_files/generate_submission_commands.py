import os
import time


samples_to_process = [
"EGamma",
"DoubleEG",
"SingleElectron",
"DoubleMuon",
"SingleMuon",
"MET",
"DYJetsToLL_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"DYJetsToLL_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"DYJetsToLL_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"DYJetsToLL_LHEFilterPtZ-0To50_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"DYJetsToLL_LHEFilterPtZ-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"DYJetsToLL_LHEFilterPtZ-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"DYJetsToLL_LHEFilterPtZ-400To650_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"DYJetsToLL_LHEFilterPtZ-50To100_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"DYJetsToLL_LHEFilterPtZ-650ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8",
"DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"QCD_HT1000to1500_TuneCP5_PSWeights_13TeV-madgraph-pythia8",
"QCD_HT100to200_TuneCP5_PSWeights_13TeV-madgraph-pythia8",
"QCD_HT1500to2000_TuneCP5_PSWeights_13TeV-madgraph-pythia8",
"QCD_HT2000toInf_TuneCP5_PSWeights_13TeV-madgraph-pythia8",
"QCD_HT200to300_TuneCP5_PSWeights_13TeV-madgraph-pythia8",
"QCD_HT300to500_TuneCP5_PSWeights_13TeV-madgraph-pythia8",
"QCD_HT500to700_TuneCP5_PSWeights_13TeV-madgraph-pythia8",
"QCD_HT50to100_TuneCP5_PSWeights_13TeV-madgraph-pythia8",
"QCD_HT700to1000_TuneCP5_PSWeights_13TeV-madgraph-pythia8",
"ST_s-channel_4f_leptonDecays_TuneCP5_erdON_13TeV-amcatnlo-pythia8",
"ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8",
"ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8",
"ST_t-channel_antitop_5f_InclusiveDecays_TuneCP5_13TeV-powheg-pythia8",
"ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8",
"ST_t-channel_top_5f_InclusiveDecays_TuneCP5_13TeV-powheg-pythia8",
"ST_tW_antitop_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8",
"ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8",
"ST_tW_top_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8",
"ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8",
"TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8",
"TTToHadronic_TuneCP5_13TeV-powheg-pythia8",
"TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8",
"WHJet_HToBB_WToLNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8",
"WJetsToLNu_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"WJetsToLNu_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"WJetsToLNu_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"WJetsToLNu_Pt-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"WJetsToLNu_Pt-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"WJetsToLNu_Pt-400To600_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"WJetsToLNu_Pt-600ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8",
"WW_TuneCP5_13TeV-pythia8",
"WZ_TuneCP5_13TeV-pythia8",
"ZHJet_HToBB_ZToLL_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8",
"ZHJet_HToBB_ZToNuNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8",
"ZJetsToNuNu_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8",
"ZJetsToNuNu_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8",
"ZJetsToNuNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8",
"ZJetsToNuNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8",
"ZJetsToNuNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8",
"ZJetsToNuNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8",
"ZJetsToNuNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8",
"ZZ_TuneCP5_13TeV-pythia8"]

##samples_to_ignore_2018 = ["DoubleEG", "SingleElectron", "ST_t-channel_top_5f_InclusiveDecays_TuneCP5_13TeV-powheg-pythia8", "ZH-ZtoNuNu-1j-SMEFTsim-topU3l"]
##samples_to_ignore_2017 = ["EGamma", "ZH-ZtoNuNu-1j-SMEFTsim-topU3l"]
##samples_to_ignore_2016 = ["EGamma", "ZH-ZtoNuNu-1j-SMEFTsim-topU3l"]
##samples_to_ignore_2016preVFP = ["EGamma", "ZH-ZtoNuNu-1j-SMEFTsim-topU3l", "WHJet_HToBB_WToLNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8", "ZHJet_HToBB_ZToLL_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8"]


def generate_prep1_files(inputfile, outputfile, year, channel):
    channelyear = channel + year
    Zllyear = "Zll" + year
    myfile = open(inputfile, "r")
    outputtxt = open(outputfile, "w")
    myline = myfile.readline()
    while myline:
        record = False
        for sample in samples_to_process:
            if sample in myline:
                record = True
        #prepare_line
        myline = myline.replace(Zllyear, channelyear)
        myline = myline.replace("PREPin", "PREP1in")
        myline = myline.replace("PREPout", "PREP1out")
        if not record: myline = "#" + myline
        #print(myline)
        #outputtxt.write(myline)
        if record: outputtxt.write(myline)
        myline = myfile.readline()
    myfile.close()
    outputtxt.close()



    
generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2018_SUBMISSION_FILE.TXT", "PREP1_2018_ZLL_SUBMISSION.TXT", "2018", "Zll")
generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2018_SUBMISSION_FILE.TXT", "PREP1_2018_WLV_SUBMISSION.TXT", "2018", "Wlv")
generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2018_SUBMISSION_FILE.TXT", "PREP1_2018_ZVV_SUBMISSION.TXT", "2018", "Zvv")


generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2017_SUBMISSION_FILE.TXT", "PREP1_2017_ZLL_SUBMISSION.TXT", "2017", "Zll")
generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2017_SUBMISSION_FILE.TXT", "PREP1_2017_WLV_SUBMISSION.TXT", "2017", "Wlv")
generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2017_SUBMISSION_FILE.TXT", "PREP1_2017_ZVV_SUBMISSION.TXT", "2017", "Zvv")


generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2016_SUBMISSION_FILE.TXT", "PREP1_2016_ZLL_SUBMISSION.TXT", "2016", "Zll")
generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2016_SUBMISSION_FILE.TXT", "PREP1_2016_WLV_SUBMISSION.TXT", "2016", "Wlv")
generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2016_SUBMISSION_FILE.TXT", "PREP1_2016_ZVV_SUBMISSION.TXT", "2016", "Zvv")


generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2016preVFP_SUBMISSION_FILE.TXT", "PREP1_2016preVFP_ZLL_SUBMISSION.TXT", "2016preVFP", "Zll")
generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2016preVFP_SUBMISSION_FILE.TXT", "PREP1_2016preVFP_WLV_SUBMISSION.TXT", "2016preVFP", "Wlv")
generate_prep1_files("../../samples/EOS_samples/sample_lists_2023/2016preVFP_SUBMISSION_FILE.TXT", "PREP1_2016preVFP_ZVV_SUBMISSION.TXT", "2016preVFP", "Zvv")




