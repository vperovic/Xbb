4390	20750718147	4726815	3	DYJetsToLL_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8
2435	30788702650	12644231	2	DYJetsToLL_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8
5056	13366121754	2643615	5	DYJetsToLL_LHEFilterPtZ-0To50_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
2026	197555718136	97510226	1	DYJetsToLL_LHEFilterPtZ-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
117	15811287649	135139210	1	DYJetsToLL_LHEFilterPtZ-400To650_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
9988	31324034721	3136166	5	DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8
2774	315435019	113711	2	QCD_HT100to200_TuneCP5_PSWeights_13TeV-madgraph-pythia8
402	137612796	342320	1	QCD_HT1500to2000_TuneCP5_PSWeights_13TeV-madgraph-pythia8
233	81473773	349672	1	QCD_HT2000toInf_TuneCP5_PSWeights_13TeV-madgraph-pythia8
2158	288692349	133777	1	QCD_HT200to300_TuneCP5_PSWeights_13TeV-madgraph-pythia8
1853	412155215	222425	1	QCD_HT500to700_TuneCP5_PSWeights_13TeV-madgraph-pythia8
1348	149491884	110899	1	QCD_HT50to100_TuneCP5_PSWeights_13TeV-madgraph-pythia8
683	10278371311	15048859	1	ST_s-channel_4f_leptonDecays_TuneCP5_13TeV-amcatnlo-pythia8
3491	21785626808	6240511	2	ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8
3519	18997884617	5398660	2	ST_t-channel_antitop_5f_InclusiveDecays_TuneCP5_13TeV-powheg-pythia8
6535	41461838693	6344581	4	ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8
302	6918386930	22908565	1	ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8
292	6891441135	23600825	1	ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8
4836	68025716347	14066525	4	WJetsToLNu_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8
5103	778125635073	152483957	4	WJetsToLNu_Pt-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
195	36578656926	187582856	1	WJetsToLNu_Pt-600ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
1363	3808461740	2794175	1	WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8
786	5624096684	7155339	1	WW_TuneCP5_13TeV-pythia8
404	2086533538	5164686	1	WZ_TuneCP5_13TeV-pythia8
204	3462195993	16971548	1	ZHJet_HToBB_ZToLL_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
1018	137547764	135115	1	ZJetsToNuNu_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8
21	6366987	303189	1	ZJetsToNuNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8
230	76880817	334264	1	ZJetsToNuNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8
80	28315272	353940	1	ZJetsToNuNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8
136	332475191	2444670	1	ZZ_TuneCP5_13TeV-pythia8
268	1490400508978	5561195929	1	WJetsToLNu_Pt-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
3106	52798937033	16999013	3	DYJetsToLL_LHEFilterPtZ-50To100_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
2203	390184852	177115	2	QCD_HT300to500_TuneCP5_PSWeights_13TeV-madgraph-pythia8
906	189710633	209393	1	ZJetsToNuNu_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8
5337	220095415824	41239538	3	TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8
3475	1442355494	415066	2	DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8
13633	24206739412	1775598	8	SingleMuon
199	382108013	1920140	1	TTToHadronic_TuneCP5_13TeV-powheg-pythia8
1664	437155482	262713	1	QCD_HT700to1000_TuneCP5_PSWeights_13TeV-madgraph-pythia8
439	19126707709	43568810	1	ST_tW_top_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8
611	105214333659	172200218	1	WJetsToLNu_Pt-400To600_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
521	18116872316	34773267	1	WHJet_HToBB_WToLNu_M-125_TuneCP5_SMEFTsim_topU3l_13TeV-madgraphMLM-pythia8
10	4359568	435956	1	ZJetsToNuNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8
730	204757539	280489	1	ZJetsToNuNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8
8656	797538934515	92137122	6	TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8
13627	17678232373	1297294	12	SingleElectron
431	18892720232	43834617	1	ST_tW_antitop_5f_NoFullyHadronicDecays_TuneCP5_13TeV-powheg-pythia8
7038	38481456134	5467669	5	ST_t-channel_top_5f_InclusiveDecays_TuneCP5_13TeV-powheg-pythia8
8574	4754355363	554508	7	WJetsToLNu_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8
530	159537101	301013	1	QCD_HT1000to1500_TuneCP5_PSWeights_13TeV-madgraph-pythia8
4059	3055551620	752784	2	DYJetsToLL_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8
123	9395268471	76384296	1	DYJetsToLL_LHEFilterPtZ-650ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
8967	39528854298	4408258	7	WJetsToLNu_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8
629	102521442095	162991163	1	DYJetsToLL_LHEFilterPtZ-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8
