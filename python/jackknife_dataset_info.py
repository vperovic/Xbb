#!/usr/bin/env python 
from __future__ import division
import ROOT
import pickle
import array
import copy
import glob
from myutils.sampleTree import SampleTree
import sys
import os
from myutils.XbbConfig import XbbConfigReader, XbbConfigTools
from myutils import ParseInfo
from myutils.BranchList import BranchList
from myutils.FileLocator import FileLocator
import csv
ROOT.gROOT.SetBatch(True) 
ROOT.gSystem.Load("../interface/VHbbNameSpace_h.so")
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
import argparse

#python jackknife_dataset_info_copy.py --channel Znn --year 2018 --input_files="/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Zvv/eval/18oct20_all_isBoostedmjj250cut/MET/tree_*.root"   --output_file="test"

#Use this script only for data. For MC, we need to add extra evalutaion cut.
parser = argparse.ArgumentParser() 
parser.add_argument('-c', '--channel',metavar='channel', required=True, help='choose one of the Zee,Zmm,Wen,Wmn,Znn')
parser.add_argument('-i', '--input_files',metavar='input_files', required=True, help='example:/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Zvv/eval/18oct20_all_isBoostedmjj250cut/MET/tree_*.root') 
parser.add_argument('-o', '--output_file',metavar='output_file', default="jacknife_info", required=True, help='output_file_name') 
parser.add_argument('-y', '--year',metavar='year', required=True, help='year') 
#args_dict = vars(parser.parse_args())
args = parser.parse_args() 

channel = str(args.channel)
year    = str(args.year)
input_files = str(args.input_files)
output_file = str(args.output_file)
config = {'Znn':'Zvv'+year,'Zee':'Zll'+year,'Zmm':'Zll'+year,'Wen':'Wlv'+year,'Wmn':'Wlv'+year}
config = XbbConfigTools(config=XbbConfigReader.read(config[channel]))
sample_path = glob.glob(input_files)
#sample_path = glob.glob('/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Zvv/eval/18oct20_all_isBoostedmjj250cut/MET/tree_*.root')

sampleTree1 = SampleTree(sample_path, treeName='Events', xrootdRedirector='root://t3dcachedb03.psi.ch:1094/')

#print sample_path
region_cat = {
  'Zee' : {
    'SR_low_Zee': '1', 'Zlf_low_Zee': '2', 'Zhf_low_Zee': '3', 'ttbar_low_Zee': '4',
    'SR_med_Zee_0j': '5', 'Zlf_med_Zee': '6', 'Zhf_med_Zee': '7', 'ttbar_med_Zee': '8',
    'SR_med_Zee_ge1j': '9', 
    'Zlf_high_Zee': '14', 'Zhf_high_Zee': '15', 'ttbar_high_Zee': '16',
    'Zlf_high_Zee_BOOST': '18', 'Zhf_high_Zee_BOOST': '19', 'ttbar_high_Zee_BOOST': '20',
    'SR_high1_Zee': '21', 'SR_high1_Zee_BOOST': '22', 'SR_high2_Zee': '23', 'SR_high2_Zee_BOOST': '24',
  },
  'Zmm' : {
    'SR_low_Zmm': '1', 'Zlf_low_Zmm': '2', 'Zhf_low_Zmm': '3', 'ttbar_low_Zmm': '4',
    'SR_med_Zmm_0j': '5', 'Zlf_med_Zmm': '6', 'Zhf_med_Zmm': '7', 'ttbar_med_Zmm': '8',
    'SR_med_Zmm_ge1j': '9', 
    'Zlf_high_Zmm': '14', 'Zhf_high_Zmm': '15', 'ttbar_high_Zmm': '16',
    'Zlf_high_Zmm_BOOST': '18', 'Zhf_high_Zmm_BOOST': '19', 'ttbar_high_Zmm_BOOST': '20',
    'SR_high1_Zmm': '21', 'SR_high1_Zmm_BOOST': '22', 'SR_high2_Zmm': '23', 'SR_high2_Zmm_BOOST': '24',
  },
  'Znn' : {
    'SR_med_Znn_0j': '5', 'Zlf_med_Znn': '6', 'Zhf_med_Znn': '7', 'ttbar_med_Znn': '8',
    'SR_med_Znn_ge1j': '9',
    'Zlf_high_Znn': '14', 'Zhf_high_Znn': '15', 'ttbar_high_Znn': '16',
    'Zlf_high_Znn_BOOST': '18', 'Zhf_high_Znn_BOOST': '19', 'ttbar_high_Znn_BOOST': '20',
    'SR_high1_Znn': '21', 'SR_high1_Znn_BOOST': '22', 'SR_high2_Znn': '23', 'SR_high2_Znn_BOOST': '24',
  },
 'Wen' : {
    'SR_med_Wen': '5', 'Wlf_med_Wen': '6', 'Whf_med_Wen': '7', 'ttbar_med_Wen': '8',
    'Wlf_high_Wen': '14', 'Whf_high_Wen': '15', 'ttbar_high_Wen': '15',
    'Wlf_high_Wen_BOOST': '18', 'Whf_high_Wen_BOOST': '19', 'ttbar_high_Wen_BOOST': '20',
    'SR_high1_Wen': '21', 'SR_high1_Wen_BOOST': '22', 'SR_high2_Wen': '23', 'SR_high2_Wen_BOOST': '24',
  },
 'Wmn' : {
    'SR_med_Wmn': '5', 'Wlf_med_Wmn': '6', 'Whf_med_Wmn': '7', 'ttbar_med_Wmn': '8',
    'Wlf_high_Wmn': '14', 'Whf_high_Wmn': '15', 'ttbar_high_Wmn': '15',
    'Wlf_high_Wmn_BOOST': '18', 'Whf_high_Wmn_BOOST': '19', 'ttbar_high_Wmn_BOOST': '20',
    'SR_high1_Wmn': '21', 'SR_high1_Wmn_BOOST': '22', 'SR_high2_Wmn': '23', 'SR_high2_Wmn_BOOST': '24',
  },
}
            
regions = {
            'Zee': region_cat['Zee'].keys(),
            'Zmm': region_cat['Zmm'].keys(),
            'Znn': region_cat['Znn'].keys(),
            'Wmn': region_cat['Wmn'].keys(),
            'Wen': region_cat['Wen'].keys(),
          }

region_def = {
            'Zee':{i: None for i in region_cat['Zee']},
            'Zmm':{i: None for i in region_cat['Zmm']},
            'Znn':{i: None for i in region_cat['Znn']},
            'Wmn':{i: None for i in region_cat['Wmn']},
            'Wen':{i: None for i in region_cat['Wen']},             
          }

region_var = copy.deepcopy(region_def)
region_plot = copy.deepcopy(region_def)

region_sum = {
            'Zee':{i: 0 for i in region_cat['Zee']},
            'Zmm':{i: 0 for i in region_cat['Zmm']},
            'Znn':{i: 0 for i in region_cat['Znn']},
            'Wmn':{i: 0 for i in region_cat['Wmn']},
            'Wen':{i: 0 for i in region_cat['Wen']},             
          }



existingBranches = {}


for i in regions[channel]:
    region_def[channel][i] = str(config.get("Cuts",i))
    sampleTree1.addFormula(i,region_def[channel][i])
    region_var[channel][i] = str(config.get("dc:"+i,"var")) 

print("var ",region_var)

variables = []

for var in region_var[channel]:
    if region_var[channel][var].replace('.Nominal','') in variables: 
        continue
    if region_var[channel][var] =='FatJet_deepTagMD_bbvsLight[Hbb_fjidx]':
        existingBranches['FatJet_deepTagMD_bbvsLight'] = array.array('f', [0.0]*50)
        sampleTree1.tree.SetBranchAddress('FatJet_deepTagMD_bbvsLight', existingBranches['FatJet_deepTagMD_bbvsLight'])
        existingBranches['Hbb_fjidx'] = array.array('i', [0]*10)
        sampleTree1.tree.SetBranchAddress('Hbb_fjidx',existingBranches['Hbb_fjidx'])
    elif region_var[channel][var] =='2*(Jet_btagDeepB[hJidx[0]]>0.7527)+(Jet_btagDeepB[hJidx[1]]>0.4184)+(Jet_btagDeepB[hJidx[1]]>0.7527)':
        existingBranches['Jet_btagDeepB'] = array.array('f', [0.0]*50)
        sampleTree1.tree.SetBranchAddress('Jet_btagDeepB', existingBranches['Jet_btagDeepB'])
        existingBranches['hJidx'] = array.array('i', [0]*50)
        sampleTree1.tree.SetBranchAddress('hJidx',existingBranches['hJidx'])        
    else:    
        existingBranches[region_var[channel][var].replace('.Nominal','')] = array.array('f', [0.0]*50)
        sampleTree1.tree.SetBranchAddress(region_var[channel][var].replace('.Nominal',''), existingBranches[region_var[channel][var].replace('.Nominal','')])
    variables.append(region_var[channel][var].replace('.Nominal',''))
   

for i in ['run','event']:
    existingBranches[i] = array.array('i', [0])
    sampleTree1.tree.SetBranchAddress(i,existingBranches[i])
#print(sampleTree1.getFormulas())

k=0
dictionary={}

for event in sampleTree1:
    k=k+1
    for i in region_def[channel]:
        passed = sampleTree1.evaluate(i)
        if passed: 
            if region_var[channel][i] =='FatJet_deepTagMD_bbvsLight[Hbb_fjidx]':
                dictionary[existingBranches['run'][0],existingBranches['event'][0]] = [region_cat[channel][i],existingBranches['FatJet_deepTagMD_bbvsLight'][existingBranches['Hbb_fjidx'][0]]]
                region_sum[channel][i] = region_sum[channel][i] + 1
                #print("existingBranches['FatJet_deepTagMD_bbvsLight']",existingBranches['FatJet_deepTagMD_bbvsLight'][existingBranches['Hbb_fjidx'][0]])

            elif region_var[channel][i] =='2*(Jet_btagDeepB[hJidx[0]]>0.7527)+(Jet_btagDeepB[hJidx[1]]>0.4184)+(Jet_btagDeepB[hJidx[1]]>0.7527)':
                btag0=existingBranches['Jet_btagDeepB'][existingBranches['hJidx'][0]]
                btag1=existingBranches['Jet_btagDeepB'][existingBranches['hJidx'][1]]
                score=(2*(btag0>0.7527)+(btag1>0.4184)+(btag1>0.7527))
                dictionary[existingBranches['run'][0],existingBranches['event'][0]] = [region_cat[channel][i],score]
                region_sum[channel][i] = region_sum[channel][i] + 1
            else:    
                dictionary[existingBranches['run'][0],existingBranches['event'][0]] = [region_cat[channel][i],existingBranches[region_var[channel][i].replace('.Nominal','')][0]]    
                region_sum[channel][i] = region_sum[channel][i] + 1 
                #print("regions splotted ", i)
                #print("sum is: ",region_sum[channel][i])
            break            
    #if k==500:break      
    if k % 5000 == 0:
        print("events read: ",i, 'region+channel ',region_sum[channel])

#print(dictionary)
print(region_sum[channel])

with open(output_file+'.pickle', 'wb') as handle:
    pickle.dump(dictionary, handle, protocol=pickle.HIGHEST_PROTOCOL)

#with open('filename.pickle', 'rb') as handle:
#    b = pickle.load(handle)

#print dictionary == b
