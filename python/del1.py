
cats = {
  'Zee' : {
    'SR_low_Zee': '1', 'Zlf_low_Zee': '2', 'Zhf_low_Zee': '3', 'ttbar_low_Zee': '4',
    'SR_med_Zee_0j': '5', 'Zlf_med_Zee': '6', 'Zhf_med_Zee': '7', 'ttbar_med_Zee': '8',
    'SR_med_Zee_ge1j': '9', 
    'Zlf_high_Zee': '14', 'Zhf_high_Zee': '15', 'ttbar_high_Zee': '16',
    'Zlf_high_Zee_BOOST': '18', 'Zhf_high_Zee_BOOST': '19', 'ttbar_high_Zee_BOOST': '20',
    'SR_high1_Zee': '21', 'SR_high1_Zee_BOOST': '22', 'SR_high2_Zee': '23', 'SR_high2_Zee_BOOST': '24',
  },
  'Zmm' : {
    'SR_low_Zmm': '1', 'Zlf_low_Zmm': '2', 'Zhf_low_Zmm': '3', 'ttbar_low_Zmm': '4',
    'SR_med_Zmm_0j': '5', 'Zlf_med_Zmm': '6', 'Zhf_med_Zmm': '7', 'ttbar_med_Zmm': '8',
    'SR_med_Zmm_ge1j': '9', 
    'Zlf_high_Zmm': '14', 'Zhf_high_Zmm': '15', 'ttbar_high_Zmm': '16',
    'Zlf_high_Zmm_BOOST': '18', 'Zhf_high_Zmm_BOOST': '19', 'ttbar_high_Zmm_BOOST': '20',
    'SR_high1_Zmm': '21', 'SR_high1_Zmm_BOOST': '22', 'SR_high2_Zmm': '23', 'SR_high2_Zmm_BOOST': '24',
  },
  'Znn' : {
    'SR_med_Znn_0j': '5', 'Zlf_med_Znn': '6', 'Zhf_med_Znn': '7', 'ttbar_med_Znn': '8',
    'SR_med_Znn_ge1j': '9',
    'Zlf_high_Znn': '14', 'Zhf_high_Znn': '15', 'ttbar_high_Znn': '16',
    'Zlf_high_Znn_BOOST': '18', 'Zhf_high_Znn_BOOST': '19', 'ttbar_high_Znn_BOOST': '20',
    'SR_high1_Znn': '21', 'SR_high1_Znn_BOOST': '22', 'SR_high2_Znn': '23', 'SR_high2_Znn_BOOST': '24',
  },
 'Wen' : {
    'SR_med_Wen': '5', 'Wlf_med_Wen': '6', 'Whf_med_Wen': '7', 'ttbar_med_Wen': '8',
    'Wlf_high_Wen': '14', 'Whf_high_Wen': '15', 'ttbar_high_Wen': '15',
    'Wlf_high_Wen_BOOST': '18', 'Whf_high_Wen_BOOST': '19', 'ttbar_high_Wen_BOOST': '20',
    'SR_high1_Wen': '21', 'SR_high1_Wen_BOOST': '22', 'SR_high2_Wen': '23', 'SR_high2_Wen_BOOST': '24',
  },
 'Wmn' : {
    'SR_med_Wmn': '5', 'Wlf_med_Wmn': '6', 'Whf_med_Wmn': '7', 'ttbar_med_Wmn': '8',
    'Wlf_high_Wmn': '14', 'Whf_high_Wmn': '15', 'ttbar_high_Wmn': '15',
    'Wlf_high_Wmn_BOOST': '18', 'Whf_high_Wmn_BOOST': '19', 'ttbar_high_Wmn_BOOST': '20',
    'SR_high1_Wmn': '21', 'SR_high1_Wmn_BOOST': '22', 'SR_high2_Wmn': '23', 'SR_high2_Wmn_BOOST': '24',
  },
}

print(cats['Znn'].keys())

a={i: None for i in cats['Znn']}
print(a)
