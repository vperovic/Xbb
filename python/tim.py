import time

start1 = time.time()
import XRootD.client
XRootD.client.FileSystem("root://t3dcachedb03.psi.ch:1094").stat("/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Wlv/VHbbPostNano2018/tmp/28may21_")

start2 = time.time()
import os
os.path.exists("/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Wlv/VHbbPostNano2018/tmp/28may21_")

start3 = time.time()
import subprocess
with open(os.devnull, 'w') as fp:
    subprocess.call(["xrdfs t3dcachedb03.psi.ch stat /pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Wlv/VHbbPostNano2018/tmp/28may21_"], shell=True, stdout=fp, stderr=fp) 
start4 = time.time()

print("XRootD: ",start2-start1)
print("os: ",start3-start2)
print("xrdfs: ",start4-start3)

import ROOT
start5 = time.time()
ROOT.TFile.Open("root://t3dcachedb03.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Wlv/VHbbPostNano2018/tmp/28may21_/tmp_bf3ccaeef0b29ab8d8b67516c1d342b7497ffa5be37c6e4955fddaa0_1of28.root", 'read')

start6 = time.time()
ROOT.TFile.Open("/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Wlv/VHbbPostNano2018/tmp/28may21_/tmp_bf3ccaeef0b29ab8d8b67516c1d342b7497ffa5be37c6e4955fddaa0_1of28.root", 'read') 
start7 = time.time()
print("file with xroot: ",start6-start5)
print("file wo xroot: ",start7-start6)

#('XRootD: ', 0.3823390007019043)                                                 ('os: ', 0.012408971786499023)                                                   ('xrdfs: ', 0.31931614875793457) 


#time()
#print("--- %s seconds ---" % (time() - start_time))
