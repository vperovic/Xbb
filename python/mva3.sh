#!/bin/bash

# example: VHbb2018commonconfig/mva_bins.sh "arctans(15)"
# arctan shape binning with S(number of signal events) bins, max 15

REBINMETHOD=$1

echo "bin boundaries for method ${REBINMETHOD}"
echo ""

echo "dc:SR_med_Wmn.rebin_list="`./submit.py -J runplot -T Wlv2018 -F tmp --regions SR_med_Wmn --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Wmn|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_med_Wmn" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_med_Wen.rebin_list="`./submit.py -J runplot -T Wlv2018 -F tmp --regions SR_med_Wen --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Wen|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_med_Wen" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high_Wmn.rebin_list="`./submit.py -J runplot -T Wlv2018 -F tmp --regions SR_high_Wmn --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Wmn|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_high_Wmn" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high_Wen.rebin_list="`./submit.py -J runplot -T Wlv2018 -F tmp --regions SR_high_Wen --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Wen|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_high_Wen" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_high_Wmn_BOOST.rebin_list="`./submit.py -J runplot -T Wlv2018 -F tmp --regions SR_high_Wmn_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:BDT.relPath=<!Eval|VH_BDT_branchName!>.Nominal;plotDef:BDT.xAxis=DNN_SR_high_Wmn_BOOST" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_high_Wen_BOOST.rebin_list="`./submit.py -J runplot -T Wlv2018 -F tmp --regions SR_high_Wen_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:BDT.relPath=<!Eval|VH_BDT_branchName!>.Nominal;plotDef:BDT.xAxis=DNN_SR_high_Wen_BOOST" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`

