#!/usr/bin/env python
import ROOT
import numpy as np
import array
import os
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import cPickle

class FlagOverlapFromFile(AddCollectionsModule):

    def __init__(self, fileNameSR, fileNameCR, NormOverlap, SRonly=True, branchName='overlapEvent'):
        super(FlagOverlapFromFile, self).__init__()
        self.branchName = branchName
        self.fileNameSR = fileNameSR
        self.fileNameCR = fileNameCR
        self.version = 20211019

        self.SRonly = SRonly
        self.NormOverlap = NormOverlap

    def customInit(self, initVars):
        self.sample = initVars['sample']
        self.config = initVars['config'] 

        self.addBranch(self.branchName)
        self.addBranch(self.branchName + "_MCWeight")

        self.sampleTree = initVars['sampleTree']

        self.cuts = self.config.get('LimitGeneral','List').split(",")

        if self.SRonly:
            self.cuts = [c for c in self.cuts if c.startswith('SR')]

        for c in self.cuts:
            self.selection = self.config.get('Cuts',c)
            self.sampleTree.addFormula(self.selection) 

        self.File = {}
        with open(self.fileNameSR, "rb") as f: 
            eventList = cPickle.load(f)
        with open(self.fileNameCR, "rb") as f:
            eventList_CR = cPickle.load(f)

        if not self.SRonly:
            eventList.update(eventList_CR)

 
        self.OverlapMCWeight = {}
        with open(self.NormOverlap) as f:
            for line in f:
                (key, val) = line.split()
                val = val.split("/")
                self.OverlapMCWeight[key[:-1]] = float(val[0])/float(val[1]) 
    
        self.Runs = [x[0] for x in eventList.keys()]
        self.EventNumbers = [x[1] for x in eventList.keys()]
        self.EventList = eventList

    def processEvent(self, tree):

        flag = 0.0
        mcweight = 1.0

        if not self.sample.isData():
            flag           = 1.0

            for c in self.cuts:
                selection = self.config.get('Cuts',c)
                if self.sampleTree.evaluate(selection):
                    mcweight = self.OverlapMCWeight[c]

        if self.sample.isData():
            run = tree.run
            event = tree.event

            if run in self.Runs:
                if event in self.EventNumbers:
                    if (run,event) in self.EventList.keys():
                        flag           = 1.0

        self._b(self.branchName)[0]           = flag
        self._b(self.branchName + "_MCWeight")[0] = mcweight

