#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class PtjjWeights(AddCollectionsModule):

    def __init__(self, year):
        super(PtjjWeights, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(PtjjWeights, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(PtjjWeights, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(PtjjWeights, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']
        print(">>>>>>>>>>>> year", self.year)


        if self.year == "2018":

            self.PtjjWeight_VPt = { 
                "isDY":
                    {
                    "isZee":
                        {
                            '75150':{'bins':[0,25.0,50.0,75.0,100.0], 'central':np.array([1.2623, 1.2699, 1.1397, 1.0102])/0.904,'uncertainty':[0.0444, 0.0266, 0.0151, 0.0101]                        },
                            '150250':{'bins':[0,25.0,50.0,75.0,100.0], 'central':np.array([1.0747, 1.1207, 1.0991, 1.1386])/1.0097,'uncertainty':[0.1061, 0.069, 0.0585, 0.0535]
                            },
                            '250Inf':{'bins':[0,25.0,50.0,75.0,100.0],'central':np.array([1.1554, 1.1996, 1.0568, 0.986])/1.0686,'uncertainty':[0.2349, 0.1553, 0.1319, 0.1332]
                            },
                        },

                    "isZmm":
                        {
                            '75150':{'bins':[0,25.0,50.0,75.0,100.0], 'central':np.array([1.229, 1.2061, 1.1382, 0.9913])/0.9152,'uncertainty':[0.0373, 0.0218, 0.013, 0.0087]                        },
                            '150250':{'bins':[0,25.0,50.0,75.0,100.0], 'central':np.array([1.449, 1.0508, 1.116, 1.1152])/0.9881,'uncertainty':[0.1153, 0.0567, 0.0507, 0.0478]
                            },
                            '250Inf':{'bins':[0,25.0,50.0,75.0,100.0],'central':np.array([1.1308, 1.1642, 1.1414, 1.0205])/1.0616,'uncertainty':[0.2071, 0.1494, 0.1266, 0.1186]
                            },
                        },
                                 },
                    }


        elif self.year == "2017":

            self.PtjjWeight_VPt = { 
                "isDY":
                    {
                    "isZee":
                        {
                            '75150':{'bins':[0,25.0,50.0,75.0,100.0], 'central': np.array([1.2246, 1.267, 1.072, 0.9544])/0.954    ,'uncertainty': [0.048, 0.0288, 0.0157, 0.0107]       },
                            '150250':{'bins':[0,25.0,50.0,75.0,100.0], 'central': np.array([1.3208, 1.0656, 1.2808, 1.0936])/0.9652   ,'uncertainty': [0.1338, 0.0761, 0.0728, 0.0623]     },
                            '250Inf':{'bins':[0,25.0,50.0,75.0,100.0],'central': np.array([1.0597, 1.1594, 0.698, 0.7796])/1.0452   ,'uncertainty': [0.245, 0.1857, 0.1295, 0.1388]      },
                        },

                    "isZmm":
                        {
                            '75150':{'bins':[0,25.0,50.0,75.0,100.0], 'central':  np.array([1.2535, 1.2447, 1.0735, 0.9407])/0.9617    ,'uncertainty': [0.0401, 0.024, 0.0135, 0.009]         },
                            '150250':{'bins':[0,25.0,50.0,75.0,100.0], 'central': np.array( [1.0541, 1.0978, 1.1203, 1.0686])/0.9738     ,'uncertainty': [0.0977, 0.079, 0.0613, 0.0517]        },
                            '250Inf':{'bins':[0,25.0,50.0,75.0,100.0],'central': np.array([0.9854, 1.226, 0.8458, 0.9259])/1.0555    ,'uncertainty': [0.2111, 0.1593, 0.1233, 0.1321]     },
                        },
                                 },
                    }

#central values are:  [1.2246, 1.267, 1.072, 0.9544, 0.954, 0.9183]
#uncertainties are:  [0.048, 0.0288, 0.0157, 0.0107, 0.0086, 0.0485]
#
#central values are:  [1.3208, 1.0656, 1.2808, 1.0936, 0.9652, 0.894]
#uncertainties are:  [0.1338, 0.0761, 0.0728, 0.0623, 0.0156, 0.0227]
#
#central values are:  [1.0597, 1.1594, 0.698, 0.7796, 1.0452, 0.889]
#uncertainties are:  [0.245, 0.1857, 0.1295, 0.1388, 0.0812, 0.0297]
#
#central values are:  [1.2535, 1.2447, 1.0735, 0.9407, 0.9617, 0.9582]
#uncertainties are:  [0.0401, 0.024, 0.0135, 0.009, 0.0074, 0.0429]
#
#central values are:  [1.0541, 1.0978, 1.1203, 1.0686, 0.9738, 0.9079]
#uncertainties are:  [0.0977, 0.079, 0.0613, 0.0517, 0.0139, 0.0203]
#
#central values are:  [0.9854, 1.226, 0.8458, 0.9259, 1.0555, 0.876]
#uncertainties are:  [0.2111, 0.1593, 0.1233, 0.1321, 0.0708, 0.0266]
#



        elif self.year == "2016":

            self.PtjjWeight_VPt = { 
                "isDY":
                    {
                    "isZee":
                        {
                            '75150':{'bins':[0,25.0,50.0,75.0,100.0], 'central': [1.4179, 1.2412, 1.0522, 0.9346] ,'uncertainty':[0.0859, 0.0462, 0.0247, 0.0167]
                             },
                            '150250':{'bins':[0,25.0,50.0,75.0,100.0], 'central':[1.4139, 1.3592, 1.146, 1.1908] ,'uncertainty':[0.2387, 0.1494, 0.1162, 0.0998]
                            },
                            '250Inf':{'bins':[0,100.0],'central':[1.2189]  ,'uncertainty':[0.1529]  
                            },
                        },

                    "isZmm":
                        {
                            '75150':{'bins':[0,25.0,50.0,75.0,100.0], 'central':[1.1931, 1.2583, 1.0444, 0.929] ,'uncertainty':[0.0708, 0.0377, 0.02, 0.0138]                 },
                            '150250':{'bins':[0,25.0,50.0,75.0,100.0], 'central':[1.122, 1.3218, 1.148, 1.0469] ,'uncertainty':[0.1691, 0.1235, 0.0929, 0.0989] 
                            },
                            '250Inf':{'bins':[0,100.0],'central':  [1.3706]  ,'uncertainty':  [0.1377] 
                            },
                        },
                                 },
                    }



        elif self.year == "2016preVFP":

            self.PtjjWeight_VPt = { 
                "isDY":
                    {
                    "isZee":
                        {
                            '75150':{'bins':[0,25.0,50.0,75.0,100.0], 'central': np.array([1.3078, 1.2449, 1.083, 0.9293])/0.9637 ,'uncertainty':[0.0728, 0.043, 0.0236, 0.0156]
                             },
                            '150250':{'bins':[0,25.0,50.0,75.0,100.0], 'central': np.array([1.6309, 1.103, 1.1972, 1.2019])/0.9419 ,'uncertainty':[0.2278, 0.1206, 0.105, 0.0961]
                            },
                            '250Inf':{'bins':[0,25.0,50.0,75.0,100.0],'central': np.array([0.9405, 1.522, 1.3932, 0.8476])/1.1788  ,'uncertainty':   [0.3914, 0.3441, 0.2894, 0.2274]
                            },
                        },

                    "isZmm":
                        {
                            '75150':{'bins':[0,25.0,50.0,75.0,100.0], 'central': np.array([1.3776, 1.3, 1.063, 0.935])/0.9575  ,'uncertainty': [0.0641, 0.0363, 0.019, 0.013]              },
                            '150250':{'bins':[0,25.0,50.0,75.0,100.0], 'central': np.array([1.1279, 1.3137, 1.1607, 1.1142])/0.957  ,'uncertainty': [0.1659, 0.115, 0.09, 0.0819]
                            },
                            '250Inf':{'bins':[0,25.0,50.0,75.0,100.0],'central': np.array([1.1521, 1.3864, 1.3334, 1.0832])/0.9837  ,'uncertainty':  [0.3338, 0.2857, 0.2397, 0.2307]
                            },
                        },
                                 },
                    }


#central values are:  [1.3078, 1.2449, 1.083, 0.9293, 0.9637, 1.0009]
#uncertainties are:  [0.0728, 0.043, 0.0236, 0.0156, 0.0133, 0.0835]
#
#central values are:  [1.6309, 1.103, 1.1972, 1.2019, 0.9419, 0.9712]
#uncertainties are:  [0.2278, 0.1206, 0.105, 0.0961, 0.0231, 0.0382]
#
#central values are:  [0.9405, 1.522, 1.3932, 0.8476, 1.1788, 0.863]
#uncertainties are:  [0.3914, 0.3441, 0.2894, 0.2274, 0.1331, 0.0419]
#
#central values are:  [1.3776, 1.3, 1.063, 0.935, 0.9575, 0.98]
#uncertainties are:  [0.0641, 0.0363, 0.019, 0.013, 0.011, 0.0692]
#
#central values are:  [1.1279, 1.3137, 1.1607, 1.1142, 0.957, 0.951]
#uncertainties are:  [0.1659, 0.115, 0.09, 0.0819, 0.0205, 0.0337]
#
#central values are:  [1.1521, 1.3864, 1.3334, 1.0832, 0.9837, 0.873]
#uncertainties are:  [0.3338, 0.2857, 0.2397, 0.2307, 0.1071, 0.038]






        else:
            raise Exception("Wrong year")


        self.get_Ptjjdict = {'75':'75150', '150':'150250','250':'250Inf'}
        self.Ptjj_branches = ["DY_ptjj_weight"]

        if self.sample.isMC():
            self.maxNjet   = 256


            for var in ["H_pt","V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
        
            for var in ["isZmm","isZee"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
        
            
            
            #Ptjj
            for var in self.Ptjj_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)



    def applies_ptjj(self, attr):
        isPtjj_lt200 = False

 
        if (any([s in self.sample.identifier for s in ['amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if (attr["H_pt"][0] < 100.0):
                if (attr["channel"] == "isZee" or attr["channel"] == "isZmm"):
                    if (any([x in self.sample.identifier for x in ['DYJets']])):
                        isPtjj_lt200 = True

                #elif (attr["channel"] == "isWenu" or attr["channel"] == "isWmunu"):
                #    if (any([x in self.sample.identifier for x in ['DYJets','WJetsToLNu']])):
                #        raise Exception("Did you check the btag cuts ? ")
                #        isNLO_dRbb_lt1 = True
                #elif (attr["channel"] == "isZnn"):
                #    if (any([x in self.sample.identifier for x in ['WJetsToLNu','ZJetsToNuNu']])):
                #        isNLO_dRbb_lt1 = True
                #        raise Exception("Did you check the btag cuts ? ")
                #else:
                #    isNLO_dRbb_lt1 = False
                        
        return isPtjj_lt200


    def get_lowerBound(self,value,bins):
        j = 0
        while not (value<bins[j] or value>=250):
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])


    def get_eventWeight_Ptjj(self, attr):
        
        
        PtjjWeight_Vpt = []
 
        Ptjj_event     = attr["H_pt"][0]
        sample = attr["NLOsample"]
        channel = attr["channel"]
        
        ### Vpt split
        PtjjWeight_bin_Vpt= self.get_Ptjjdict[self.get_lowerBound(attr["V_pt"][0],[75,150,250])]
        j=0
        while not (Ptjj_event < self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["bins"][j]):
            j+=1
        PtjjWeight_Vpt = [self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["central"][j-1], self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["central"][j-1] + self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["uncertainty"][j-1], self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["central"][j-1] - self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["uncertainty"][j-1]]
        

        return  PtjjWeight_Vpt



    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)

            attr = {}
            for var in ["V_pt","hJidx","isZee","isZmm","H_pt"]: 
                attr[var] = self.existingBranches[var]

            if any([x in self.sample.identifier for x in ['DYJets']]):
                attr["NLOsample"] = "isDY"
            elif any([x in self.sample.identifier for x in ['WJetsToLNu']]):
                attr["NLOsample"] = "isWJets" 
            elif any([x in self.sample.identifier for x in ['ZJetsToNuNu']]):
                attr["NLOsample"] = "isZJets" 
            attr["channel"] = None   
            

            if attr["isZee"][0] == 1: attr["channel"] = "isZee"
            if attr["isZmm"][0] == 1: attr["channel"] = "isZmm"
            #if attr["isWenu"][0] == 1: attr["channel"] = "isWenu"
            #if attr["isWmunu"][0] == 1: attr["channel"] = "isWmunu"
            #if attr["isZnn"][0] == 1: attr["channel"] = "isZnn"

            if self.applies_ptjj(attr):
                
                PtjjWeight_Vpt = self.get_eventWeight_Ptjj(attr)
                #print("Checking pass boundary conditions")
                #print("Features", attr["H_pt"],attr["hJidx"][0],attr["hJidx"][1],attr["V_pt"][0])  
                #print("channel", attr["channel"])
                #print(PtjjWeight_Vpt)
                self._b("DY_ptjj_weight")[0]        = PtjjWeight_Vpt[0]
                self._b("DY_ptjj_weight"+"Up")[0]   = PtjjWeight_Vpt[1]
                self._b("DY_ptjj_weight"+"Down")[0] = PtjjWeight_Vpt[2]
            else:

                self._b("DY_ptjj_weight")[0]        = 1.0
                self._b("DY_ptjj_weight"+"Up")[0]   = 1.0
                self._b("DY_ptjj_weight"+"Down")[0] = 1.0




if __name__=='__main__':

    print("This module creates branches with ptjj weights")

