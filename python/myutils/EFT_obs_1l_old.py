#!/usr/bin/env python
import ROOT
from ROOT import TVector3, TLorentzVector
from math import pi, sqrt, cos, sin, sinh, log, cosh, acos, acosh
import numpy as np
import array
import os
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import random

class EFT_obs_1l(AddCollectionsModule):

    def __init__(self, branchName='EFT_obs'):
        super(EFT_obs_1l, self).__init__()
        self.branchName = branchName


    def customInit(self, initVars):
        self.sample = initVars['sample']
        self.config = initVars['config']
        
        #6 values:
        #[0] random neutrino - charged lepton reference
        #[1] neutrino 1 - charged lepton reference
        #[2] neutrino 2 - charged lepton reference
        #[3] random neutrino - positive lepton reference
        #[4] neutrino 1 - positive lepton reference
        #[5] neutrino 2 - positive lepton reference
   
    
        #Positions 3,4 and 5 are not physical, the angle goes from the V boson to the right handed lepton
        #The right handed lepton is the positively charged lepton or the anti neutrino


 
        self.addVectorBranch(self.branchName + '_Theta_e', length = 3)
        self.addVectorBranch(self.branchName + '_Theta_m', length = 3)
        self.addVectorBranch(self.branchName + '_Theta_l', length = 3)
        self.addVectorBranch(self.branchName + '_theta_e', length = 3)
        self.addVectorBranch(self.branchName + '_theta_m', length = 3)
        self.addVectorBranch(self.branchName + '_theta_l', length = 3)
        self.addVectorBranch(self.branchName + '_phi_e', length = 3)
        self.addVectorBranch(self.branchName + '_phi_m', length = 3)
        self.addVectorBranch(self.branchName + '_phi_l', length = 3)
        self.addVectorBranch(self.branchName + '_phi_weight', length = 3)


        self.addVectorBranch(self.branchName + '_LHE_Theta_e', length = 2)
        self.addVectorBranch(self.branchName + '_LHE_Theta_m', length = 2)
        self.addVectorBranch(self.branchName + '_LHE_Theta_l', length = 2)
        self.addVectorBranch(self.branchName + '_LHE_theta_e', length = 2)
        self.addVectorBranch(self.branchName + '_LHE_theta_m', length = 2)
        self.addVectorBranch(self.branchName + '_LHE_theta_l', length = 2)
        self.addVectorBranch(self.branchName + '_LHE_phi_e', length = 2)
        self.addVectorBranch(self.branchName + '_LHE_phi_m', length = 2)
        self.addVectorBranch(self.branchName + '_LHE_phi_l', length = 2)
        self.addVectorBranch(self.branchName + '_LHE_phi_weight', length = 2)


        #debugging_only
        self.addBranch(self.branchName + '_LHE_H_mass_from_bb')
        self.addBranch(self.branchName + '_LHE_H_pt_from_bb')
        self.addBranch(self.branchName + '_LHE_H_eta_from_bb')
        self.addBranch(self.branchName + '_LHE_H_phi_from_bb')
        self.addBranch(self.branchName + '_LHE_Vtype')

        self.addVectorBranch(self.branchName + '_VH_mass', length = 3)
        self.addBranch(self.branchName + '_LHE_VH_mass_from_llbb')

        #Additional features 
        self.addBranch(self.branchName + '_thrust')
        self.addBranch(self.branchName + '_Vh_beam_angle')

        self.sampleTree = initVars['sampleTree']

    def processEvent(self, tree):
        # if current entry has not been processed yet
        if not self.hasBeenProcessed(tree):
            self.markProcessed(tree)
            for i in range(3):
                self._b(self.branchName + '_Theta_e')[i] = -99997.0
                self._b(self.branchName + '_Theta_m')[i] = -99997.0
                self._b(self.branchName + '_Theta_l')[i] = -99997.0
                self._b(self.branchName + '_theta_e')[i] = -99997.0
                self._b(self.branchName + '_theta_m')[i] = -99997.0
                self._b(self.branchName + '_theta_l')[i] = -99997.0
                self._b(self.branchName + '_phi_e')[i]   = -99997.0
                self._b(self.branchName + '_phi_m')[i]   = -99997.0
                self._b(self.branchName + '_phi_l')[i]   = -99997.0
                self._b(self.branchName + '_phi_weight')[i]   = -99997.0


            for i in range(2):
                self._b(self.branchName + '_LHE_Theta_e')[i] = -99997.0
                self._b(self.branchName + '_LHE_Theta_m')[i] = -99997.0
                self._b(self.branchName + '_LHE_Theta_l')[i] = -99997.0
                self._b(self.branchName + '_LHE_theta_e')[i] = -99997.0
                self._b(self.branchName + '_LHE_theta_m')[i] = -99997.0
                self._b(self.branchName + '_LHE_theta_l')[i] = -99997.0
                self._b(self.branchName + '_LHE_phi_e')[i]   = -99997.0
                self._b(self.branchName + '_LHE_phi_m')[i]   = -99997.0
                self._b(self.branchName + '_LHE_phi_l')[i]   = -99997.0
                self._b(self.branchName + '_LHE_phi_weight')[i]   = -99997.0

            self._b(self.branchName + '_LHE_H_pt_from_bb')[0]   = -99997.0
            self._b(self.branchName + '_LHE_H_eta_from_bb')[0]   = -99997.0
            self._b(self.branchName + '_LHE_H_phi_from_bb')[0]   = -99997.0
            self._b(self.branchName + '_LHE_H_mass_from_bb')[0]   = -99997.0
            self._b(self.branchName + '_LHE_Vtype')[0]   = -99997.0
            
            for i in range(3): self._b(self.branchName + '_VH_mass')[i]   = -99997.0
            self._b(self.branchName + '_LHE_VH_mass_from_llbb')[0]   = -99997.0


            self._b(self.branchName + '_LHE_H_pt_from_bb')[0],self._b(self.branchName + '_LHE_H_eta_from_bb')[0],self._b(self.branchName + '_LHE_H_phi_from_bb')[0],self._b(self.branchName + '_LHE_H_mass_from_bb')[0]  = self.getHiggsPtEtaPhiMFrombb(tree)
            self._b(self.branchName + '_LHE_Vtype')[0] = self.getLHELeptons(tree,True)[2] #positive reference does not matter for Vtype 

            #getAngle(tree, LHElevel, strict_reference, neutrino choice (0-random, 1,2))

            for i in range(3):
                #if i < 3: 
                #    refn = False
                #    cnt = i
                #else:
                #    refn = True
                #    cnt = i-3
                
                #Set the reference to true !!!! If not, the theta observable will not be computed correctly
                refn = True
                cnt = i
                self._b(self.branchName + '_Theta_e')[i], self._b(self.branchName + '_Theta_m')[i], self._b(self.branchName + '_Theta_l')[i] = self.getTheta(tree,False,refn,cnt)
                self._b(self.branchName + '_theta_e')[i], self._b(self.branchName + '_theta_m')[i], self._b(self.branchName + '_theta_l')[i] = self.gettheta(tree,False,refn,cnt)
                self._b(self.branchName + '_phi_e')[i], self._b(self.branchName + '_phi_m')[i], self._b(self.branchName + '_phi_l')[i] = self.getphi(tree,False,refn,cnt)
                self._b(self.branchName + '_phi_weight')[i] = self.getphiweight(tree,False,refn,cnt)
                
            
            for i in range(2):
                if i < 1: refn = False
                else: refn = True
                self._b(self.branchName + '_LHE_Theta_e')[i], self._b(self.branchName + '_LHE_Theta_m')[i], self._b(self.branchName + '_LHE_Theta_l')[i] = self.getTheta(tree,True,refn,-1)
                self._b(self.branchName + '_LHE_theta_e')[i], self._b(self.branchName + '_LHE_theta_m')[i], self._b(self.branchName + '_LHE_theta_l')[i] = self.gettheta(tree,True,refn,-1)
                self._b(self.branchName + '_LHE_phi_e')[i], self._b(self.branchName + '_LHE_phi_m')[i], self._b(self.branchName + '_LHE_phi_l')[i] = self.getphi(tree,True,refn,-1)
                self._b(self.branchName + '_LHE_phi_weight')[i] = self.getphiweight(tree,True,refn,-1)


            for i in range(3): self._b(self.branchName + '_VH_mass')[i] = self.getVH(tree,i)[3]
            self._b(self.branchName + '_LHE_VH_mass_from_llbb')[0] = self.getLHEVH(tree,True)[3] #positive reference does not matter here

            self._b(self.branchName + '_Vh_beam_angle')[0] = self.getWHbeamAngle(tree)
            self._b(self.branchName + '_thrust')[0] = self.getThrust(tree)


    def getLeptons(self, tree, reference, neutrino_choice):
        isWenu = tree.isWenu
        isWmunu = tree.isWmunu
        vLidx = tree.vLidx
        Vtype = tree.Vtype

        #basic check for consistency
        #if (isZee+isZmm != 1): print "isZee and isZmm inconsistent"
        if (isWenu == 1 and Vtype != 3): print "isZee and Vtype inconsistent"
        if (isWmunu == 1 and Vtype != 2): print "isZmm and Vtype inconsistent"

        lep1, lep2 = TLorentzVector(), TLorentzVector()
        lep_chg, lep_nu = TLorentzVector(), TLorentzVector()
      
        Electron_pt = tree.Electron_pt
        Electron_eta = tree.Electron_eta
        Electron_phi = tree.Electron_phi
        Electron_mass = tree.Electron_mass
        Electron_charge = tree.Electron_charge

        Muon_pt = tree.Muon_pt
        Muon_eta = tree.Muon_eta
        Muon_phi = tree.Muon_phi
        Muon_mass = tree.Muon_mass
        Muon_charge = tree.Muon_charge


        if (Vtype == 2): #Wmn
            try:
                lep_chg.SetPtEtaPhiM(Muon_pt[vLidx[0]],Muon_eta[vLidx[0]],Muon_phi[vLidx[0]],Muon_mass[vLidx[0]])
            except:
                lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)                  
            
            try:
                lep_nu = self.getNeutrino(tree,lep_chg,neutrino_choice)
            except:
                lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            
            chg_lep_positive = (Muon_charge[vLidx[0]] > 0)
            

            if reference: #strict theory reference to the positively charged lepton
                if chg_lep_positive:
                    lep1 = lep_chg
                    lep2 = lep_nu
                else:
                    lep1 = lep_nu
                    lep2 = lep_chg
            else: #old default to charged lepton
               
                raise Exception("Not the right way to compute the angle theta") 
                #This is not physical, the angle goes from the V boson to the right handed lepton
                #The right handed lepton is the positively charged lepton or the anti neutrino
                lep1 = lep_chg
                lep2 = lep_nu
                  


        if (Vtype == 3): #Wenee
            try:
                lep_chg.SetPtEtaPhiM(Electron_pt[vLidx[0]],Electron_eta[vLidx[0]],Electron_phi[vLidx[0]],Electron_mass[vLidx[0]])
            except:
                lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)                  
            try:
                lep_nu = self.getNeutrino(tree,lep_chg,neutrino_choice)
            except:
                lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            
            chg_lep_positive = (Electron_charge[vLidx[0]] > 0)
            

            if reference: #strict theory reference to the positively charged lepton
                if chg_lep_positive:
                    lep1 = lep_chg
                    lep2 = lep_nu
                else:
                    lep1 = lep_nu
                    lep2 = lep_chg
            else: #old default to charged lepton
                lep1 = lep_chg
                lep2 = lep_nu

        return lep1, lep2, Vtype






    def getLHELeptons(self,tree,reference):
        if tree.isData == 1 or not hasattr(tree, 'LHEPart_pdgId'): #ZZ, WZ, WW pythia8 have no LHE branches
            lep1,lep2 = TLorentzVector(),TLorentzVector()
            lep1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            lep2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            LHE_Vtype = -1
            return lep1, lep2, LHE_Vtype


        LHE_pdgId = list(tree.LHEPart_pdgId)
        LHE_pt = list(tree.LHEPart_pt)
        LHE_eta = list(tree.LHEPart_eta) 
        LHE_phi = list(tree.LHEPart_phi)
        LHE_mass = list(tree.LHEPart_mass)
        LHE_isZee = False
        LHE_isZmm = False
        LHE_isZtautau = False
        LHE_isZnn = False
        LHE_isWen = False
        LHE_isWmn = False
        LHE_isWtaun = False
        LHE_Vtype = -99

        lep1, lep2 = TLorentzVector(), TLorentzVector()
        lep_chg, lep_nu = TLorentzVector(), TLorentzVector()


        if ((11 in LHE_pdgId) and (-11 in LHE_pdgId)): LHE_isZee = True
        if ((13 in LHE_pdgId) and (-13 in LHE_pdgId)): LHE_isZmm = True
        if ((15 in LHE_pdgId) and (-15 in LHE_pdgId)): LHE_isZtautau = True
        if (((12 in LHE_pdgId) and (-12 in LHE_pdgId)) or ((14 in LHE_pdgId) and (-14 in LHE_pdgId)) or ((16 in LHE_pdgId) and (-16 in LHE_pdgId))): LHE_isZnn = True

        if (((11 in LHE_pdgId) and (-12 in LHE_pdgId)) or ((-11 in LHE_pdgId) and (12 in LHE_pdgId))): LHE_isWen = True
        if (((13 in LHE_pdgId) and (-14 in LHE_pdgId)) or ((-13 in LHE_pdgId) and (14 in LHE_pdgId))): LHE_isWmn = True
        if (((15 in LHE_pdgId) and (-16 in LHE_pdgId)) or ((-15 in LHE_pdgId) and (16 in LHE_pdgId))): LHE_isWtaun = True


	if (LHE_isWen or LHE_isWmn):
            if LHE_isWen: 
                LHE_Vtype = 3
            else:
                pass
            if LHE_isWmn: 
                LHE_Vtype = 2
            else:
                pass
        elif (LHE_isZee and LHE_isZmm): 
            LHE_Vtype = -9 
        else:
            LHE_Vtype = -99


        if (LHE_Vtype == 2):
            if (13 in LHE_pdgId):            
                try:
                    lep_chg.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(13)],LHE_eta[LHE_pdgId.index(13)],LHE_phi[LHE_pdgId.index(13)],LHE_mass[LHE_pdgId.index(13)])
                except:
                    lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                try:
                    lep_nu.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-14)],LHE_eta[LHE_pdgId.index(-14)],LHE_phi[LHE_pdgId.index(-14)],LHE_mass[LHE_pdgId.index(-14)])
                except:
                    lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

            else:
                try: 
                    lep_chg.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-13)],LHE_eta[LHE_pdgId.index(-13)],LHE_phi[LHE_pdgId.index(-13)],LHE_mass[LHE_pdgId.index(-13)])
                except:
                    lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                try: 
                    lep_nu.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(14)],LHE_eta[LHE_pdgId.index(14)],LHE_phi[LHE_pdgId.index(14)],LHE_mass[LHE_pdgId.index(14)])
                except:
                    lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)


        if (LHE_Vtype == 3):
            if (11 in LHE_pdgId):            
                try:
                    lep_chg.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(11)],LHE_eta[LHE_pdgId.index(11)],LHE_phi[LHE_pdgId.index(11)],LHE_mass[LHE_pdgId.index(11)])
                except:
                    lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                try:
                    lep_nu.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-12)],LHE_eta[LHE_pdgId.index(-12)],LHE_phi[LHE_pdgId.index(-12)],LHE_mass[LHE_pdgId.index(-12)])
                except:
                    lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

            else:
                try: 
                    lep_chg.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-11)],LHE_eta[LHE_pdgId.index(-11)],LHE_phi[LHE_pdgId.index(-11)],LHE_mass[LHE_pdgId.index(-11)])
                except:
                    lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                try: 
                    lep_nu.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(12)],LHE_eta[LHE_pdgId.index(12)],LHE_phi[LHE_pdgId.index(12)],LHE_mass[LHE_pdgId.index(12)])
                except:
                    lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

	chg_lep_positive = (-11 in LHE_pdgId) or (-13 in LHE_pdgId) or (-15 in LHE_pdgId)
        if reference: #strict theory reference to the positively charged lepton
            if chg_lep_positive:
                lep1 = lep_chg
                lep2 = lep_nu
            else:
                lep1 = lep_nu
                lep2 = lep_chg
        else: #old default to charged lepton
            lep1 = lep_chg
            lep2 = lep_nu


	return lep1, lep2, LHE_Vtype


    




    def getH(self, tree):
        H_pt = tree.H_pt
        H_eta = tree.H_eta
        H_phi = tree.H_phi
        H_mass = tree.H_mass
        H = TLorentzVector()
        H.SetPtEtaPhiM(H_pt, H_eta, H_phi, H_mass)
        return H


    def getLHEH(self,tree): 
        if tree.isData == 1 or not hasattr(tree, 'LHEPart_pdgId'): #ZZ, WZ, WW pythia8 have no LHE branches
            bb = TLorentzVector()
            bb.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            return bb
            
        LHE_pdgId = list(tree.LHEPart_pdgId)
        LHE_pt = list(tree.LHEPart_pt)
        LHE_eta = list(tree.LHEPart_eta)
        LHE_phi = list(tree.LHEPart_phi)
        LHE_mass = list(tree.LHEPart_mass)

	b1, b2, bb = TLorentzVector(), TLorentzVector(), TLorentzVector()
        bb.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

	try:
            b1.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-5)],LHE_eta[LHE_pdgId.index(-5)],LHE_phi[LHE_pdgId.index(-5)],LHE_mass[LHE_pdgId.index(-5)])
        except:
            b1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
        try:
            b2.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(5)],LHE_eta[LHE_pdgId.index(5)],LHE_phi[LHE_pdgId.index(5)],LHE_mass[LHE_pdgId.index(5)])
        except:
            b2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

        bb = b1 + b2
        return bb


    def getLHEVH(self,tree,reference):
        H = self.getLHEH(tree)
        lep1, lep2, LHE_Vtype = self.getLHELeptons(tree,reference)
        V, VH = TLorentzVector(), TLorentzVector()
        V = lep1 + lep2
        VH = V + H
        return VH.Pt(), VH.Eta(), VH.Phi(), VH.M()


    def getVH(self,tree,neutrino_choice):
        H = self.getH(tree)
        V_pt = tree.V_pt 
        V_eta = tree.V_eta
        V_phi = tree.V_phi
        V_mass = tree.V_mass
        V, VH = TLorentzVector(), TLorentzVector()
        try:
            V.SetPtEtaPhiM(V_pt, V_eta, V_phi, V_mass)
        except:
            V.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
        VH = V + H 
        return VH.Pt(), VH.Eta(), VH.Phi(), VH.M()


    def getHiggsPtEtaPhiMFrombb(self,tree):
        H = self.getLHEH(tree)
        H_mass = H.M()
        H_eta = H.Eta()
        H_phi = H.Phi()
        H_pt = H.Pt()
        return H_pt, H_eta, H_phi, H_mass

    def getTheta(self, tree, LHE_level,reference,neutrino_choice):
        if LHE_level:
            lep1, lep2, Vtype = self.getLHELeptons(tree,reference)
            H = self.getLHEH(tree)
        else:
            lep1, lep2, Vtype = self.getLeptons(tree,reference,neutrino_choice)
            H = self.getH(tree)
        
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100, -100, -100

        beam.SetPxPyPzE(0,0,6500,6500)

        V_mom, bVH = TLorentzVector(), TVector3()
        V_mom = tmp_lep1+tmp_lep2
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()

        V_mom.Boost(-bVH)

        Theta = float('nan')

        try:
            #Theta  = acos((V_mom.Vect().Unit()).Dot(beam.Vect().Unit()))
            Theta = (V_mom.Vect().Unit()).Angle(beam.Vect().Unit())
        except Exception:
            #pass
            Theta = -100

        if (Vtype == 2):
            Theta_l = Theta
            Theta_m = Theta
            Theta_e = -99999

        elif (Vtype == 3):
            Theta_l = Theta
            Theta_e = Theta
            Theta_m = -99999

        else:
            Theta_l = -9999
            Theta_m = -9999
            Theta_e = -9999

        return Theta_e, Theta_m, Theta_l


    def gettheta(self, tree, LHE_level,reference,neutrino_choice):
        if LHE_level:
            lep1, lep2, Vtype = self.getLHELeptons(tree,reference)
            H = self.getLHEH(tree)
        else:
            lep1, lep2, Vtype = self.getLeptons(tree,reference,neutrino_choice)
            H = self.getH(tree)

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100, -100, -100

        V_mom, bVH, bV = TLorentzVector(), TVector3(), TVector3()

        bVH = (tmp_lep1 + tmp_lep2 + tmp_H).BoostVector()
        V_mom = (tmp_lep1 + tmp_lep2)

        V_mom.Boost(-bVH)
        tmp_lep1.Boost(-bVH)

        bV = V_mom.BoostVector()
        tmp_lep1.Boost(-bV)

        theta = float('nan')
        try:
            theta = (V_mom).Angle(tmp_lep1.Vect())
        except Exception:
            #pass
            theta = -100

        if (Vtype == 2):
            theta_l = theta
            theta_m = theta
            theta_e = -99999

        elif (Vtype == 3):
            theta_l = theta
            theta_e = theta
            theta_m = -99999

        else:
            theta_l = -9999
            theta_m = -9999
            theta_e = -9999

        #print(tmp_lep1.Pt(), tmp_lep1.Eta(), tmp_lep1.Phi(), tmp_lep1.M(),tmp_lep2.Pt(), tmp_lep2.Eta(), tmp_lep2.Phi(), tmp_lep2.M())
        return theta_e, theta_m, theta_l


    def getphi(self, tree, LHE_level,reference,neutrino_choice):
        if LHE_level:
            lep1, lep2, Vtype = self.getLHELeptons(tree,reference)
            H = self.getLHEH(tree)
        else:
            lep1, lep2, Vtype = self.getLeptons(tree,reference,neutrino_choice)
            H = self.getH(tree)


        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100, -100, -100

        beam.SetPxPyPzE(0,0,6500,6500)

        V_mom, bVH, n_scatter, n_decay = TLorentzVector(), TVector3(), TVector3(), TVector3()
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()
        V_mom = tmp_lep1+tmp_lep2

        tmp_lep1.Boost(-bVH)
        tmp_lep2.Boost(-bVH)
        V_mom.Boost(-bVH)
        #beam.Boost(-bVH)

        n_scatter = ((beam.Vect().Unit()).Cross(V_mom.Vect())).Unit()
        n_decay   = (tmp_lep1.Vect().Cross(tmp_lep2.Vect())).Unit()

        sign_flip =  -1 if ( ((n_scatter.Cross(n_decay))*(V_mom.Vect())) < 0 ) else +1

        try:
            phi = sign_flip*acos(n_scatter.Dot(n_decay))
        except Exception:
            #pass
            phi = -100

        if (Vtype == 2):
            phi_l = phi
            phi_m = phi
            phi_e = -99999

        elif (Vtype == 3):
            phi_l = phi
            phi_e = phi
            phi_m = -99999

        else:
            phi_l = -9999
            phi_m = -9999
            phi_e = -9999

        return phi_e, phi_m, phi_l


    def getphiweight(self, tree, LHE_level,reference,neutrino_choice):
        Theta = self.getTheta(tree, LHE_level,reference,neutrino_choice)[2]
        theta = self.gettheta(tree, LHE_level,reference,neutrino_choice)[2]
        try:
            weight = np.sin(2*theta) * np.sin(2*Theta)
        except Exception:
            #pass 
            weight = -999999
        
        return weight


    def getNeutrino(self,tree,vec_lep,neutrino_choice): # Only used for reco, LHE handled like a lepton
        W_mass = 80.4
        pnu_random = TLorentzVector()
        pnu_1 = TLorentzVector()
        pnu_2 = TLorentzVector()
        nueta = [-100,-100]

        #Corrected MET 
        MET_pt = tree.MET_Pt
        MET_phi = tree.MET_Phi

        #print(vec_lep.Pt(), vec_lep.Eta(), vec_lep.Phi(), vec_lep.M(), MET_pt, MET_phi)

        if vec_lep.E()<0:
            pnu_random.SetPtEtaPhiM(0,-100,-100,0)
            pnu_1.SetPtEtaPhiM(0,-100,-100,0)
            pnu_2.SetPtEtaPhiM(0,-100,-100,0)
        else:
            mT2 = 2*vec_lep.Pt()*MET_pt*(1-cos(self.deltaPhi(vec_lep.Phi(),MET_phi)))
            Delta2 = (W_mass*W_mass - mT2)*1./(2*MET_pt*vec_lep.Pt())
            if (Delta2>=0):
                try:
                    nueta[0] = (vec_lep.Eta() + abs(acosh(1+(Delta2))))
                    nueta[1] = (vec_lep.Eta() - abs(acosh(1+(Delta2))))
                except Exception:
                    nueta[0] = -200
                    nueta[1] = -200
                    #pass
                    #pass
            else:
                nueta[0] = vec_lep.Eta()
                nueta[1] = vec_lep.Eta()        

       
        pnu_1.SetPtEtaPhiM(MET_pt,nueta[0],MET_phi,0)
        pnu_2.SetPtEtaPhiM(MET_pt,nueta[1],MET_phi,0)
        #print(pnu_1.Pt(), pnu_1.Eta(), pnu_1.Phi(), pnu_1.M())
        if (random.random()>=0.5): pnu_random = pnu_1
        else: pnu_random = pnu_2
        if (neutrino_choice == 0): return pnu_random
        if (neutrino_choice == 1): return pnu_1
        if (neutrino_choice == 2): return pnu_2

    def deltaPhi(self,phi1,phi2):
        dphi = phi1 - phi2
        while (dphi >= pi): dphi-=2*pi
        while (dphi < -pi): dphi+=2*pi
        return dphi



    def getThrust(self, tree):

        #Grab the random neutrino choice
        lep1, lep2, Vtype = self.getLeptons(tree,True,0)
        
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_b1, tmp_b2 = TLorentzVector(), TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
      
        
        idx0 = tree.hJidx[0]       
        idx1 = tree.hJidx[1]       

        if idx0 < 0 or idx1 < 0:
            return -1
 
 
        tmp_b1.SetPtEtaPhiM(tree.Jet_pt[idx0],tree.Jet_eta[idx0],tree.Jet_phi[idx0],tree.Jet_mass[idx0])
        tmp_b2.SetPtEtaPhiM(tree.Jet_pt[idx1],tree.Jet_eta[idx1],tree.Jet_phi[idx1],tree.Jet_mass[idx1])


        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tree.Jet_eta[idx0]<-10 or tree.Jet_eta[idx1]<-10 ):
            return -1

        v_l1 = tmp_lep1.Vect()
        v_l2 = tmp_lep2.Vect()
        v_b1 = tmp_b1.Vect()
        v_b2 = tmp_b2.Vect()

        v_array = [v_l1, v_l2, v_b1, v_b2]

        
        thrust = float('nan')


        directions = []

        for theta in np.arange(0, np.pi, 0.2):
            for phi in np.arange(0, 2*np.pi, 0.2):
                unit_vec = TVector3(0,0,1)  
                unit_vec.SetMag(1.0)
                unit_vec.SetTheta(theta)
                unit_vec.SetPhi(phi)
                directions.append(unit_vec)

        #directions = np.array(directions) 

        scalar_product = [sum([v.Dot(direction)*np.heaviside(v.Dot(direction), 1.0) for v in v_array]) for direction in directions ]  


        d = max(scalar_product)
        
        pt_tot = sum([v.Mag() for v in v_array])        
        
        if pt_tot > 0:
            d = d/pt_tot
        else:
            return -1

        return d



    def getWHbeamAngle(self, tree):
        lep1, lep2, Vtype = self.getLeptons(tree, True, 0)
        H = self.getH(tree)
        
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100

        beam.SetPxPyPzE(0,0,6500,6500)

        VH = TLorentzVector()
        VH = tmp_lep1+tmp_lep2+tmp_H

        WHbeamAngle = float('nan')

        try:
            #Theta  = acos((V_mom.Vect().Unit()).Dot(beam.Vect().Unit()))
            WHbeamAngle = (VH.Vect().Unit()).Dot(beam.Vect().Unit())
        except Exception:
            #pass
            WHbeamAngle = -100

        return WHbeamAngle







"""

def neutrino_mom(vec_lep, MET_pt, MET_phi, random):

        W_mass = 80.4

        pnu = TLorentzVector()

        if vec_lep.E()<0:
                pnu.etPtEtaPhiM(0,-100,-100,0)
        else:
                mT2 = 2*vec_lep.Pt()*MET_pt*(1-cos(deltaPhi(vec_lep.Phi(),MET_phi)))
                Delta2 = (W_mass*W_mass - mT2)*1./(2*MET_pt*vec_lep.Pt())
                if (Delta2>=0):
                        try:
                                nueta = (vec_lep.Eta() + abs(acosh(1+(Delta2)))) if (random>=0.5) else (vec_lep.Eta() - abs(acosh(1+(Delta2))))
                        except Exception:
                                pass
                                nueta = -100
                        pnu.SetPtEtaPhiM(MET_pt,nueta,MET_phi,0)
                else:
                        #pnu.SetPtEtaPhiM(0,-100,-100,0)
                        nueta = vec_lep.Eta()
                        pnu.SetPtEtaPhiM(MET_pt,nueta,MET_phi,0)

        return pnu

def neutrino_mom_both(vec_lep, MET_pt, MET_phi, W_mass=80.4):

        #W_mass = 80.4
        pnu = [TLorentzVector(), TLorentzVector()]
        nueta = [-100,-100]

        if vec_lep.E()<0:
                pnu[0].SetPtEtaPhiM(0,-100,-100,0)
                pnu[1].SetPtEtaPhiM(0,-100,-100,0)
        else:
                mT2 = 2*vec_lep.Pt()*MET_pt*(1-cos(deltaPhi(vec_lep.Phi(),MET_phi)))
                Delta2 = (W_mass*W_mass - mT2)*1./(2*MET_pt*vec_lep.Pt())
                if (Delta2>=0):
                        try:
                                nueta[0] = (vec_lep.Eta() + abs(acosh(1+(Delta2))))
                                nueta[1] = (vec_lep.Eta() - abs(acosh(1+(Delta2))))
                        except Exception:
                                pass
                                pass
                else:
                        nueta[0] = vec_lep.Eta()
                        nueta[1] = vec_lep.Eta()

        pnu[0].SetPtEtaPhiM(MET_pt,nueta[0],MET_phi,0)
        pnu[1].SetPtEtaPhiM(MET_pt,nueta[1],MET_phi,0)

        return pnu

"""
