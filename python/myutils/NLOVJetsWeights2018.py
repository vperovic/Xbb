#!/usr/bin/env python
import ROOT
import numpy as np
import array
import os
from BranchTools import Collection
from BranchTools import AddCollectionsModule
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from sampleTree import SampleTree
import re

class NLOVJetsWeights2018(AddCollectionsModule):

    def __init__(self, fileNameWJets, fileNameDYJets, branchName='weightNLOVJets', year=2018):
        super(NLOVJetsWeights2018, self).__init__()
        self.fileNameWJets = fileNameWJets
        self.fileNameDYJets = fileNameDYJets
        self.version = 1
        self.branchName = branchName
        self.year = int(year)

        self.WJets = ['WJetsToLNu_Pt-100To250_TuneCP5_13TeV-amcatnloFXFX-pythia8','WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8','WJetsToLNu_Pt-400To600_TuneCP5_13TeV-amcatnloFXFX-pythia8','WJetsToLNu_Pt-600ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8']
        self.DYJets = ['DYJetsToLL_Pt-50To100_TuneCP5_13TeV-amcatnloFXFX-pythia8','DYJetsToLL_Pt-100To250_TuneCP5_13TeV-amcatnloFXFX-pythia8','DYJetsToLL_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8','DYJetsToLL_Pt-400To650_TuneCP5_13TeV-amcatnloFXFX-pythia8','DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8']
        self.NLOVptbins  = self.WJets + self.DYJets
        self.baseName = "CMS_VJetsNLO2018_PtBinnedRwt_"

    def customInit(self, initVars):
        self.sample = initVars['sample']
        print(self.sample.identifier)
        self.config = initVars['config']
        if not self.sample.isData():
            self.addBranch(self.branchName)
            self.addBranch(self.branchName + '_Up')
            self.addBranch(self.branchName + '_Down')
            for s in self.NLOVptbins:
                m=re.search('(.*)(Jets)(\w+Pt-)(.*)(_Tune.*)',s)
                for NpNLO in ["1","2"]:
                    self.addBranch(self.baseName + m.groups()[0] + m.groups()[3] + '_' + NpNLO + 'J' + 'Up') 
                    self.addBranch(self.baseName + m.groups()[0] + m.groups()[3] + '_' + NpNLO + 'J' + 'Down')
            
            self.rootFileWJets = ROOT.TFile.Open(self.fileNameWJets,"READ")
            self.rootFileDYJets = ROOT.TFile.Open(self.fileNameDYJets,"READ")
            self.fit = {}
            self.up = {}
            self.down = {}
            for njet in [1,2]:
                self.fit[njet] = {}
                self.up[njet] = {}
                self.down[njet] = {}
                for sample in self.WJets:
                    self.fit[njet][sample]      = self.rootFileWJets.Get(sample+"_"+str(njet))
                    self.up[njet][sample]    = self.rootFileWJets.Get(sample+"_"+str(njet)+"Up")
                    self.down[njet][sample]    = self.rootFileWJets.Get(sample+"_"+str(njet)+"Down")
                for sample in self.DYJets:
                    self.fit[njet][sample]      = self.rootFileDYJets.Get(sample+"_"+str(njet))
                    self.up[njet][sample]    = self.rootFileDYJets.Get(sample+"_"+str(njet)+"Up")
                    self.down[njet][sample]    = self.rootFileDYJets.Get(sample+"_"+str(njet)+"Down")


    def processEvent(self, tree):
        # if current entry has not been processed yet
        if not self.sample.isData() and not self.hasBeenProcessed(tree) and self.year==2018:
            self.markProcessed(tree)
            self._b(self.branchName)[0]                        = 1.0
            self._b(self.branchName + '_Up')[0]              = 1.0
            self._b(self.branchName + '_Down')[0]         = 1.0

            for s in self.NLOVptbins:
                m=re.search('(.*)(Jets)(\w+Pt-)(.*)(_Tune.*)',s)
                for NpNLO in ["1","2"]:
                    self._b(self.baseName + m.groups()[0] + m.groups()[3] + '_' + NpNLO + 'J' + 'Up')[0]   = 1.0
                    self._b(self.baseName + m.groups()[0] + m.groups()[3] + '_' + NpNLO + 'J' + 'Down')[0] = 1.0
            if self.applies(tree):

                vpt         = tree.LHE_Vpt
                njet        = ord(tree.LHE_NpNLO)
                

                #print(vpt,njet)
                
                #if(tree.event==157545900):print("self.fit is",self.fit,vpt,njet)
                #if(tree.event==157545900):print("self.fit is",self.fit[njet])
    
                self._b(self.branchName)[0]         = self.getWeight(vpt,njet,"nom") 
                self._b(self.branchName + '_Up')[0]         = self.getWeight(vpt,njet,"down") 
                self._b(self.branchName + '_Down')[0]         = self.getWeight(vpt,njet,"up") 

            #print(self._b(self.branchName + '_Down')[0], self._b(self.branchName)[0], self._b(self.branchName + '_Up')[0]) 
                for s in self.NLOVptbins:
                    m=re.search('(.*)(Jets)(\w+Pt-)(.*)(_Tune.*)',s)
                    if (s==self.sample.identifier):
                        for NpNLO in str(ord(tree.LHE_NpNLO)):
                            self._b(self.baseName + m.groups()[0] + m.groups()[3] + '_' + NpNLO + 'J' + 'Up')[0]   = self._b(self.branchName + '_Up')[0] 
                            self._b(self.baseName + m.groups()[0] + m.groups()[3] + '_' + NpNLO + 'J' + 'Down')[0] = self._b(self.branchName + '_Down')[0]
                    else:
                        for NpNLO in str(ord(tree.LHE_NpNLO)):
                            self._b(self.baseName + m.groups()[0] + m.groups()[3] + '_' + NpNLO + 'J' + 'Up')[0]   = self._b(self.branchName)[0]
                            self._b(self.baseName + m.groups()[0] + m.groups()[3] + '_' + NpNLO + 'J' + 'Down')[0] = self._b(self.branchName)[0]
                    

    def applies(self, tree):
        applyNLOWeights = False

        applyNLOWeights = any([x in self.sample.identifier for x in self.NLOVptbins]) and ord(tree.LHE_NpNLO) in [1,2]

        return applyNLOWeights

    def getWeight(self, vpt, njet, var):
        
        if var == "nom":
            histo = self.fit[njet][self.sample.identifier]
        else: 
            histo = getattr(self,var)[njet][self.sample.identifier]

        nbin = histo.FindBin(vpt)
        weight = histo.GetBinContent(nbin)
        if weight >0:
            return 1./weight
        else:
            return 0.0

if __name__=='__main__':

    config = XbbConfigReader.read('Wlv2018')
    info = ParseInfo(config=config)
    #sample = [x for x in info if x.identifier == 'DYJetsToLL_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8'][0]
    sample = [x for x in info if x.identifier == 'WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8'][0]
    #sample = [x for x in info if x.identifier == 'WJetsToLNu_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8'][0]

    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/13may22_vjets_isboostedagain/DYJetsToLL_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_2443d8e3a58f20de3e6b7c6498e28d2125d3243eea8a08ab8ce6a72b_000000_000000_0000_122_2454530b1634010aec910ee42f09938e8e0ddaa0e9f2d416e5cf7428.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch:1094//")
    sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Wlv/mva/13may22_vjets_/WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_123f72d4ac18713e4f1ce37939f9d9331dfd76f0a75e29fc308d6eee_000000_000000_0000_386_9c8ad434e20704911fcd5c545adfbc8be4dc1f37b0ac75ff8bf7041d.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch:1094//")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Wlv/mva/13may22_vjets_/WJetsToLNu_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_f7984367a7de281f38de9180c4cdb14b60d5a24d49e0599ea9bfab0c_000000_000000_0000_3_2784534fea2924d038b41d433f40e899b5878d306de01140e95905f2.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch:1094//")
    w = NLOVJetsWeights2018(fileNameWJets="/work/creissel/VHbb/CMSSW_10_1_0/CMSSW_10_1_0/src/Xbb/python/NLOVJetsWeights2018/Wln2018_NLO_ptBin_reweights.root",fileNameDYJets="/work/creissel/VHbb/CMSSW_10_1_0/CMSSW_10_1_0/src/Xbb/python/NLOVJetsWeights2018/Zll2018_NLO_ptBin_reweights.root")
    w.customInit({'sampleTree': sampleTree, 'sample': sample, 'config': config})
    sampleTree.addOutputBranches(w.getBranches())

    n=0
    for event in sampleTree:
        n=n+1
        #event,run,true_attr, attr = w.processEvent(event)
        w.processEvent(event)
        if n == 200: break
        #print(event.weightNLOVJets, event.weightNLOVJets_Up, event.weightNLOVJets_Down)

