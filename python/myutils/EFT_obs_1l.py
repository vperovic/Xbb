#!/usr/bin/env python
import ROOT
from ROOT import TVector3, TLorentzVector
from math import pi, sqrt, cos, sin, sinh, log, cosh, acos, acosh
import numpy as np
import array
import os
from BranchTools import Collection
from BranchTools import AddCollectionsModule
from XbbConfig import XbbConfigTools
import random

class EFT_obs_1l(AddCollectionsModule):

    def __init__(self, branchName='EFT_obs'):
        super(EFT_obs_1l, self).__init__()
        self.branchName = branchName


    def customInit(self, initVars):
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.isData     = initVars['sample'].isData()
        self.sampleTree = initVars['sampleTree']
        self.xbbConfig  = XbbConfigTools(self.config)

        self.Nevent = 0           


       
        # Include boost syst for boosted events
        if not self.isData:
            self.systematics = ["Nominal"] + self.xbbConfig.getJECuncertainties(step='VReco') + ['jms','jmr']

            #self.systematics = ["Nominal"]
            #NEED TO ADD UNCLUST FOR MET (1 lep): actually not, it was dropped
            #,'unclustEn']

        else: 

            self.systematics = ["Nominal"]
 
        #self.addBranch(self.branchName + '_Theta_e')
        #self.addBranch(self.branchName + '_Theta_m')
        #self.addBranch(self.branchName + '_Theta_l')
        #self.addBranch(self.branchName + '_theta_e')
        #self.addBranch(self.branchName + '_theta_m')
        #self.addBranch(self.branchName + '_theta_l')
        #self.addBranch(self.branchName + '_phi_e')
        #self.addBranch(self.branchName + '_phi_m')
        #self.addBranch(self.branchName + '_phi_l')
        #self.addBranch(self.branchName + '_phi_weight')


        #debugging_only
        #self.addBranch(self.branchName + '_LHE_Theta_e')
        #self.addBranch(self.branchName + '_LHE_Theta_m')
        #self.addBranch(self.branchName + '_LHE_Theta_l')
        #self.addBranch(self.branchName + '_LHE_theta_e')
        #self.addBranch(self.branchName + '_LHE_theta_m')
        #self.addBranch(self.branchName + '_LHE_theta_l')
        #self.addBranch(self.branchName + '_LHE_phi_e')
        #self.addBranch(self.branchName + '_LHE_phi_m')
        #self.addBranch(self.branchName + '_LHE_phi_l')
        #self.addBranch(self.branchName + '_LHE_phi_weight')


        #self.addBranch(self.branchName + '_LHE_H_mass_from_bb')
        #self.addBranch(self.branchName + '_LHE_H_pt_from_bb')
        #self.addBranch(self.branchName + '_LHE_H_eta_from_bb')
        #self.addBranch(self.branchName + '_LHE_H_phi_from_bb')
        #self.addBranch(self.branchName + '_LHE_Vtype')

        ##self.addBranch(self.branchName + '_VH_mass')
        #self.addBranch(self.branchName + '_LHE_VH_mass_from_llbb')


        #Additional features 
        #self.addBranch(self.branchName + '_thrust')
        #self.addBranch(self.branchName + '_Vh_beam_angle')

        #Gen level Eta quantities
        self.addBranch(self.branchName + '_LHE_V_mass')
        self.addBranch(self.branchName + '_LHE_V_eta')
        self.addBranch(self.branchName + '_LHE_Neutrino_eta')

        self.EFT_features = ["Theta_e","Theta_m","Theta_l","theta_e","theta_m","theta_l","phi_e","phi_m","phi_l","phi_weight","VH_mass","thrust","Vh_beam_angle","V_eta_Wmass", "V_mass_Wmass","NeutrinoEta_random","NeutrinoEta_sol1","NeutrinoEta_sol2"]  
        self.EFTProperties = [self.branchName + '_' + x for x in self.EFT_features]


        for EFTProperty in self.EFTProperties: 
            for syst in self.systematics:
                for Q in self._variations(syst):
                    self.addBranch(self._v(EFTProperty, syst, Q))




    def processEvent(self, tree):
        # if current entry has not been processed yet
        if not self.hasBeenProcessed(tree):
            self.markProcessed(tree)

            #print("Nevent ", self.Nevent, " out of ", tree.GetEntries())
            self.Nevent+=1


            #No syst affecting LHE
            #self._b(self.branchName + '_LHE_H_pt_from_bb')[0],self._b(self.branchName + '_LHE_H_eta_from_bb')[0],self._b(self.branchName + '_LHE_H_phi_from_bb')[0],self._b(self.branchName + '_LHE_H_mass_from_bb')[0]  = self.getHiggsPtEtaPhiMFrombb(tree)
            #self._b(self.branchName + '_LHE_Vtype')[0] = self.getLHELeptons(tree)[2] 

            #self._b(self.branchName + '_LHE_Theta_e')[0], self._b(self.branchName + '_LHE_Theta_m')[0], self._b(self.branchName + '_LHE_Theta_l')[0] = self.getTheta(tree,True)
            #self._b(self.branchName + '_LHE_theta_e')[0], self._b(self.branchName + '_LHE_theta_m')[0], self._b(self.branchName + '_LHE_theta_l')[0] = self.gettheta(tree,True)
            #self._b(self.branchName + '_LHE_phi_e')[0], self._b(self.branchName + '_LHE_phi_m')[0], self._b(self.branchName + '_LHE_phi_l')[0] = self.getphi(tree,True)

            #self._b(self.branchName + '_LHE_VH_mass_from_llbb')[0] = self.getLHEVH(tree)[3]
            #self._b(self.branchName + '_LHE_phi_weight')[0] = self.getphiweight(tree,True)

            V_eta_gen, V_mass_gen, Neutrino_eta_gen = self.get_LHE_EFTangles(tree)
            
            self._b(self.branchName + '_LHE_V_eta')[0] = V_eta_gen
            self._b(self.branchName + '_LHE_V_mass')[0] = V_mass_gen
            self._b(self.branchName + '_LHE_Neutrino_eta')[0] = Neutrino_eta_gen



            for syst in self.systematics:
                for Q in self._variations(syst):

                    Theta, theta, phi, weight, ZHbeamAngle, VHmass, V_mass, V_eta, Neutrino_eta_rand, Neutrino_eta_1, Neutrino_eta_2 = self.getEFTangles(tree, syst, Q)


                    #self._b(self._v(self.branchName + '_Theta_e', syst, Q))[0], self._b(self._v(self.branchName + '_Theta_m', syst, Q))[0], self._b(self._v(self.branchName + '_Theta_l', syst, Q))[0] = self.getTheta(tree,False, syst, Q)
                    #self._b(self._v(self.branchName + '_theta_e', syst, Q))[0], self._b(self._v(self.branchName + '_theta_m', syst, Q))[0], self._b(self._v(self.branchName + '_theta_l', syst, Q))[0] = self.gettheta(tree,False, syst, Q)
                    #self._b(self._v(self.branchName + '_phi_e', syst, Q))[0], self._b(self._v(self.branchName + '_phi_m', syst, Q))[0], self._b(self._v(self.branchName + '_phi_l', syst, Q))[0] = self.getphi(tree,False, syst, Q)

                    #self._b(self._v(self.branchName + '_VH_mass', syst, Q))[0] = self.getVH(tree, syst, Q)[3]

                    #self._b(self._v(self.branchName + '_phi_weight', syst, Q))[0] = self.getphiweight(tree,False, syst, Q)
                    #
                    #self._b(self._v(self.branchName + '_Vh_beam_angle', syst, Q))[0] = self.getZHbeamAngle(tree, syst, Q)
                    #self._b(self._v(self.branchName + '_thrust', syst, Q))[0] = self.getThrust(tree, syst, Q)

                    self._b(self._v(self.branchName + '_Theta_l', syst, Q))[0] = Theta 
                    self._b(self._v(self.branchName + '_theta_l', syst, Q))[0] = theta
                    self._b(self._v(self.branchName + '_phi_l', syst, Q))[0] = phi
                    self._b(self._v(self.branchName + '_phi_weight', syst, Q))[0] = weight                     
                    self._b(self._v(self.branchName + '_Vh_beam_angle', syst, Q))[0] = ZHbeamAngle
                    self._b(self._v(self.branchName + '_VH_mass', syst, Q))[0] = VHmass
                    self._b(self._v(self.branchName + '_V_mass_Wmass', syst, Q))[0] = V_mass
                    self._b(self._v(self.branchName + '_V_eta_Wmass', syst, Q))[0] = V_eta
                    self._b(self._v(self.branchName + '_NeutrinoEta_random', syst, Q))[0] = Neutrino_eta_rand
                    self._b(self._v(self.branchName + '_NeutrinoEta_sol1', syst, Q))[0] = Neutrino_eta_1
                    self._b(self._v(self.branchName + '_NeutrinoEta_sol2', syst, Q))[0] = Neutrino_eta_2




    def getLeptons(self, tree, reference = True, neutrino_choice = 0):
        isWenu = tree.isWenu
        isWmunu = tree.isWmunu
        vLidx = tree.vLidx
        Vtype = tree.Vtype

        #basic check for consistency
        #if (isZee+isZmm != 1): print "isZee and isZmm inconsistent"
        if (isWenu == 1 and Vtype != 3): print "isZee and Vtype inconsistent"
        if (isWmunu == 1 and Vtype != 2): print "isZmm and Vtype inconsistent"

        lep1, lep2 = TLorentzVector(), TLorentzVector()
        lep_chg, lep_nu = TLorentzVector(), TLorentzVector()
      
        Electron_pt = tree.Electron_pt
        Electron_eta = tree.Electron_eta
        Electron_phi = tree.Electron_phi
        Electron_mass = tree.Electron_mass
        Electron_charge = tree.Electron_charge

        Muon_pt = tree.Muon_pt
        Muon_eta = tree.Muon_eta
        Muon_phi = tree.Muon_phi
        Muon_mass = tree.Muon_mass
        Muon_charge = tree.Muon_charge


        if (Vtype == 2): #Wmn
            try:
                lep_chg.SetPtEtaPhiM(Muon_pt[vLidx[0]],Muon_eta[vLidx[0]],Muon_phi[vLidx[0]],Muon_mass[vLidx[0]])
            except:
                lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)                  
            
            try:
                lep_nu = self.getNeutrino(tree,lep_chg,neutrino_choice)
            except:
                lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            
            chg_lep_positive = (Muon_charge[vLidx[0]] > 0)
            

            if reference: #strict theory reference to the positively charged lepton
                if chg_lep_positive:
                    lep1 = lep_chg
                    lep2 = lep_nu
                else:
                    lep1 = lep_nu
                    lep2 = lep_chg
            else: #old default to charged lepton
               
                raise Exception("Not the right way to compute the angle theta") 
                #This is not physical, the angle goes from the V boson to the right handed lepton
                #The right handed lepton is the positively charged lepton or the anti neutrino
                lep1 = lep_chg
                lep2 = lep_nu
                  


        if (Vtype == 3): #Wenee
            try:
                lep_chg.SetPtEtaPhiM(Electron_pt[vLidx[0]],Electron_eta[vLidx[0]],Electron_phi[vLidx[0]],Electron_mass[vLidx[0]])
            except:
                lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)                  
            try:
                lep_nu = self.getNeutrino(tree,lep_chg,neutrino_choice)
            except:
                lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            
            chg_lep_positive = (Electron_charge[vLidx[0]] > 0)
            

            if reference: #strict theory reference to the positively charged lepton
                if chg_lep_positive:
                    lep1 = lep_chg
                    lep2 = lep_nu
                else:
                    lep1 = lep_nu
                    lep2 = lep_chg
            else: #old default to charged lepton
                lep1 = lep_chg
                lep2 = lep_nu

        return lep1, lep2, Vtype, chg_lep_positive




    def getNeutrino(self,tree,vec_lep,neutrino_choice = 0): # Only used for reco, LHE handled like a lepton
        W_mass = 80.4
        pnu_random = TLorentzVector()
        pnu_1 = TLorentzVector()
        pnu_2 = TLorentzVector()
        nueta = [-100,-100]

        #Corrected MET 
        MET_pt = tree.MET_Pt
        MET_phi = tree.MET_Phi

        #print(vec_lep.Pt(), vec_lep.Eta(), vec_lep.Phi(), vec_lep.M(), MET_pt, MET_phi)

        if vec_lep.E()<0:
            pnu_random.SetPtEtaPhiM(0,-100,-100,0)
            pnu_1.SetPtEtaPhiM(0,-100,-100,0)
            pnu_2.SetPtEtaPhiM(0,-100,-100,0)
        else:
            mT2 = 2*vec_lep.Pt()*MET_pt*(1-cos(self.deltaPhi(vec_lep.Phi(),MET_phi)))
            Delta2 = (W_mass*W_mass - mT2)*1./(2*MET_pt*vec_lep.Pt())
            

            if (Delta2>=0):
                try:
                    nueta[0] = (vec_lep.Eta() + abs(acosh(1+(Delta2))))
                    nueta[1] = (vec_lep.Eta() - abs(acosh(1+(Delta2))))
                except Exception:
                    nueta[0] = -200
                    nueta[1] = -200
                    #pass
                    #pass
            else:

                nueta[0] = vec_lep.Eta()
                nueta[1] = vec_lep.Eta()        

       
        pnu_1.SetPtEtaPhiM(MET_pt,nueta[0],MET_phi,0)
        pnu_2.SetPtEtaPhiM(MET_pt,nueta[1],MET_phi,0)
        #print(pnu_1.Pt(), pnu_1.Eta(), pnu_1.Phi(), pnu_1.M())


        if (random.random()>=0.5): 
            pnu_random = pnu_1
        else: 
            pnu_random = pnu_2

        if (neutrino_choice == 0): return pnu_random
        if (neutrino_choice == 1): return pnu_1
        if (neutrino_choice == 2): return pnu_2





    def getH(self, tree):
        H_pt = tree.H_pt
        H_eta = tree.H_eta
        H_phi = tree.H_phi
        H_mass = tree.H_mass
        H = TLorentzVector()
        H.SetPtEtaPhiM(H_pt, H_eta, H_phi, H_mass)
        return H


    def getHsyst(self, tree, syst, Q):
        
        Hbb_fjidx = tree.Hbb_fjidx 
        hJidx = tree.hJidx
        
        idx0 = hJidx[0]       
        idx1 = hJidx[1]       
        H = TLorentzVector()

        if Hbb_fjidx > -1:
           
            H_eta = tree.FatJet_eta[Hbb_fjidx]
            H_phi = tree.FatJet_phi[Hbb_fjidx]


            if self._isnominal(syst) or "Reg" in syst:
                H_mass = tree.FatJet_Msoftdrop[Hbb_fjidx]
                H_pt = tree.FatJet_pt[Hbb_fjidx]

            #H mass specific syst
            elif "jms" in syst or "jmr" in syst:
                H_pt = getattr(tree, 'FatJet_pt_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
                H_mass = getattr(tree, 'FatJet_msoftdrop_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
            else: 
                H_pt = getattr(tree, 'FatJet_pt_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
                H_mass = getattr(tree, 'FatJet_msoftdrop_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]

        elif idx0 > -1 and idx1 > -1:


            if "jms" in syst or "jmr" in syst or syst == "jer" or self._isnominal(syst):
                H_pt = tree.H_pt
                H_eta = tree.H_eta
                H_phi = tree.H_phi
                H_mass = tree.H_mass
            else: 
                H_pt = getattr(tree, 'H_pt_{s}_{d}'.format(s=syst, d=Q))
                H_eta = tree.H_eta
                H_phi = tree.H_phi
                H_mass = getattr(tree, 'H_mass_{s}_{d}'.format(s=syst, d=Q))


        else:
             
            H.SetPtEtaPhiM(-1.0, -1.0, -1.0, -1.0)
            return H

        H.SetPtEtaPhiM(H_pt, H_eta, H_phi, H_mass)
        return H



    def getEFTangles(self, tree, syst, Q):
        
        lep1, lep2, Vtype, chg_lep_positive = self.getLeptons(tree)
        H = self.getHsyst(tree,syst,Q)
    
        if H.M() < 0:
            return -100,-100, -100,-100, -100,-100, -100,-100, -100,-100, -100

    
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100,-100, -100,-100, -100,-100, -100,-100, -100,-100, -100


        #Theta
        beam.SetPxPyPzE(0,0,6500,6500)

        V_mom, bVH = TLorentzVector(), TVector3()
        V_mom = tmp_lep1+tmp_lep2
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()

        V_mom.Boost(-bVH)

        Theta = float('nan')

        try:
            Theta = (V_mom.Vect().Unit()).Angle(beam.Vect().Unit())
        except Exception:
            Theta = -100

        #theta
        V_mom, bVH, bV = TLorentzVector(), TVector3(), TVector3()

        bVH = (tmp_lep1 + tmp_lep2 + tmp_H).BoostVector()
        V_mom = (tmp_lep1 + tmp_lep2)

        V_mom.Boost(-bVH)
        tmp_lep1.Boost(-bVH)

        bV = V_mom.BoostVector()
        tmp_lep1.Boost(-bV)

        theta = float('nan')
        try:
            theta = (V_mom).Angle(tmp_lep1.Vect())

        except Exception:
            #pass
            theta = -100


        #phi
        V_mom, bVH, n_scatter, n_decay = TLorentzVector(), TVector3(), TVector3(), TVector3()
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()
        V_mom = tmp_lep1+tmp_lep2

        tmp_lep1.Boost(-bVH)
        tmp_lep2.Boost(-bVH)
        V_mom.Boost(-bVH)
        #beam.Boost(-bVH)

        n_scatter = ((beam.Vect().Unit()).Cross(V_mom.Vect())).Unit()
        n_decay   = (tmp_lep1.Vect().Cross(tmp_lep2.Vect())).Unit()

        sign_flip =  -1 if ( ((n_scatter.Cross(n_decay))*(V_mom.Vect())) < 0 ) else +1

        try:
            phi = sign_flip*acos(n_scatter.Dot(n_decay))
        except Exception:
            #pass
            phi = -100


        #phiweight
        try:
            weight = np.sin(2*theta) * np.sin(2*Theta)
        except Exception:
            #pass 
            weight = -100


        #Helicity
        VH = TLorentzVector()
        VH = tmp_lep1+tmp_lep2+tmp_H

        ZHbeamAngle = float('nan')

        try:
            ZHbeamAngle = (VH.Vect().Unit()).Dot(beam.Vect().Unit())
        except Exception:
            ZHbeamAngle = -100


        try:
            VHmass = VH.M()
        except Exception:
            VHmass = -100

        #V quantities with W mass constraint (from neutrino)
        V_mom = tmp_lep1+tmp_lep2

        V_eta = V_mom.Eta()
        V_mass = V_mom.M()

        #Neutrino Eta

        if chg_lep_positive:
            charged_lepton = tmp_lep1
            neutrino = tmp_lep2
        else:
            charged_lepton = tmp_lep2
            neutrino = tmp_lep1

        Neutrino_mom_rand = self.getNeutrino(tree,charged_lepton,0) 
        Neutrino_mom_1 = self.getNeutrino(tree,charged_lepton,1) 
        Neutrino_mom_2 = self.getNeutrino(tree,charged_lepton,2) 


        Neutrino_eta_rand = Neutrino_mom_rand.Eta()
        Neutrino_eta_1 = Neutrino_mom_1.Eta()
        Neutrino_eta_2 = Neutrino_mom_2.Eta()



        return Theta, theta, phi, weight, ZHbeamAngle, VHmass, V_mass, V_eta, Neutrino_eta_rand, Neutrino_eta_1, Neutrino_eta_2


    def get_LHE_EFTangles(self, tree):

        #Gen level Neutrino
        lep_chg, lep_nu, LHE_Vtype = self.getLHELeptons(tree)

        V_mom = lep_chg + lep_nu
        
        V_eta_gen = V_mom.Eta()
        V_mass_gen = V_mom.M()
        Neutrino_eta_gen = lep_nu.Eta()

        return V_eta_gen, V_mass_gen, Neutrino_eta_gen


    def getVH(self,tree, syst, Q):
        H = self.getHsyst(tree, syst, Q)
        V_pt = tree.V_pt 
        V_eta = tree.V_eta
        V_phi = tree.V_phi
        V_mass = tree.V_mass
        V, VH = TLorentzVector(), TLorentzVector()
        try:
            V.SetPtEtaPhiM(V_pt, V_eta, V_phi, V_mass)
        except:
            V.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
        VH = V + H 
        return VH.Pt(), VH.Eta(), VH.Phi(), VH.M()



    def getTheta(self, tree, LHE_level,syst = None, Q = None):
        if LHE_level:
            lep1, lep2, Vtype = self.getLHELeptons(tree)
            H = self.getLHEH(tree)
        else:
            lep1, lep2, Vtype, _= self.getLeptons(tree)
            H = self.getHsyst(tree,syst,Q)
    
        if H.M() < 0:
            return [-100,-100, -100]

    
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100, -100, -100

        beam.SetPxPyPzE(0,0,6500,6500)

        V_mom, bVH = TLorentzVector(), TVector3()
        V_mom = tmp_lep1+tmp_lep2
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()

        V_mom.Boost(-bVH)

        Theta = float('nan')

        try:
            #Theta  = acos((V_mom.Vect().Unit()).Dot(beam.Vect().Unit()))
            Theta = (V_mom.Vect().Unit()).Angle(beam.Vect().Unit())
        except Exception:
            #pass
            Theta = -100

        if (Vtype == 2):
            Theta_l = Theta
            Theta_m = Theta
            Theta_e = -99999

        elif (Vtype == 3):
            Theta_l = Theta
            Theta_e = Theta
            Theta_m = -99999

        else:
            Theta_l = -9999
            Theta_m = -9999
            Theta_e = -9999


        return Theta_e, Theta_m, Theta_l


    def gettheta(self, tree, LHE_level, syst = None, Q = None):
        if LHE_level:
            lep1, lep2, Vtype = self.getLHELeptons(tree)
            H = self.getLHEH(tree)
        else:
            lep1, lep2, Vtype, _ = self.getLeptons(tree)
            H = self.getHsyst(tree, syst, Q)


        if H.M() < 0:
            return [-100,-100, -100]


        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100, -100, -100

        V_mom, bVH, bV = TLorentzVector(), TVector3(), TVector3()

        bVH = (tmp_lep1 + tmp_lep2 + tmp_H).BoostVector()
        V_mom = (tmp_lep1 + tmp_lep2)

        V_mom.Boost(-bVH)
        tmp_lep1.Boost(-bVH)

        bV = V_mom.BoostVector()
        tmp_lep1.Boost(-bV)

        theta = float('nan')
        try:
            theta = (V_mom).Angle(tmp_lep1.Vect())

        except Exception:
            #pass
            theta = -100

        if (Vtype == 2):
            theta_l = theta
            theta_m = theta
            theta_e = -99999

        elif (Vtype == 3):
            theta_l = theta
            theta_e = theta
            theta_m = -99999

        else:
            theta_l = -9999
            theta_m = -9999
            theta_e = -9999

        return theta_e, theta_m, theta_l


    def getphi(self, tree, LHE_level, syst = None, Q = None):
        if LHE_level:
            lep1, lep2, Vtype = self.getLHELeptons(tree)
            H = self.getLHEH(tree)
        else:
            lep1, lep2, Vtype, _= self.getLeptons(tree)
            H = self.getHsyst(tree, syst, Q)

        if H.M() < 0:
            return [-100,-100,-100]

        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100, -100, -100

        beam.SetPxPyPzE(0,0,6500,6500)

        V_mom, bVH, n_scatter, n_decay = TLorentzVector(), TVector3(), TVector3(), TVector3()
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()
        V_mom = tmp_lep1+tmp_lep2

        tmp_lep1.Boost(-bVH)
        tmp_lep2.Boost(-bVH)
        V_mom.Boost(-bVH)
        #beam.Boost(-bVH)

        n_scatter = ((beam.Vect().Unit()).Cross(V_mom.Vect())).Unit()
        n_decay   = (tmp_lep1.Vect().Cross(tmp_lep2.Vect())).Unit()

        sign_flip =  -1 if ( ((n_scatter.Cross(n_decay))*(V_mom.Vect())) < 0 ) else +1

        try:
            phi = sign_flip*acos(n_scatter.Dot(n_decay))
        except Exception:
            #pass
            phi = -100

        if (Vtype == 2):
            phi_l = phi
            phi_m = phi
            phi_e = -99999

        elif (Vtype == 3):
            phi_l = phi
            phi_e = phi
            phi_m = -99999

        else:
            phi_l = -9999
            phi_m = -9999
            phi_e = -9999

        return phi_e, phi_m, phi_l


    def getphiweight(self, tree, LHE_level, syst = None, Q = None):
        Theta = self.getTheta(tree, LHE_level, syst, Q)[2]
        theta = self.gettheta(tree, LHE_level, syst, Q)[2]
        try:
            weight = np.sin(2*theta) * np.sin(2*Theta)
        except Exception:
            #pass 
            weight = -999999
        
        return weight

    def getThrust(self, tree, syst, Q):
        lep1, lep2, Vtype, _ = self.getLeptons(tree)
        
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_b1, tmp_b2 = TLorentzVector(), TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
      

        idx0 = tree.hJidx[0]       
        idx1 = tree.hJidx[1]       
        Hbb_fjidx = tree.Hbb_fjidx


        if idx0 > -1 and idx1 > -1:
        
            if self._isnominal(syst) or "jmr" in syst or "jms" in syst or 'jerReg' in syst:
                Jet_Pt      = tree.Jet_pt
                Jet_Mass    = tree.Jet_mass
            else:
                Jet_Pt      = getattr(tree, 'Jet_pt_{s}{d}'.format(s=syst, d=Q))
                Jet_Mass    = getattr(tree, 'Jet_mass_{s}{d}'.format(s=syst, d=Q))

     
            tmp_b1.SetPtEtaPhiM(Jet_Pt[idx0],tree.Jet_eta[idx0],tree.Jet_phi[idx0],Jet_Mass[idx0])
            tmp_b2.SetPtEtaPhiM(Jet_Pt[idx1],tree.Jet_eta[idx1],tree.Jet_phi[idx1],Jet_Mass[idx1])


        elif Hbb_fjidx > -1:
            
            if self._isnominal(syst) or "Reg" in syst:
                Jet_Pt      = tree.FatJet_pt[Hbb_fjidx]
                Jet_Mass    = tree.FatJet_Msoftdrop[Hbb_fjidx]

            elif "jmr" in syst or "jms" in syst:
                Jet_Pt      = getattr(tree, 'FatJet_pt_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
                Jet_Mass    = getattr(tree, 'FatJet_msoftdrop_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
            else:
                Jet_Pt      = getattr(tree, 'FatJet_pt_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
                Jet_Mass    = getattr(tree, 'FatJet_msoftdrop_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]

     
            tmp_b1.SetPtEtaPhiM(Jet_Pt,tree.FatJet_eta[Hbb_fjidx],tree.FatJet_phi[Hbb_fjidx],Jet_Mass)


        else:
            return -1


        #if(lep1.Eta()<-10 or lep2.Eta()<-10 or tree.Jet_eta[idx0]<-10 or tree.Jet_eta[idx1]<-10 ):
        #    return -1

        v_l1 = tmp_lep1.Vect()
        v_l2 = tmp_lep2.Vect()
        v_b1 = tmp_b1.Vect()


        if idx0 > -1 and idx1 > -1:
            v_b2 = tmp_b2.Vect()

            v_array = [v_l1, v_l2, v_b1, v_b2]

        else:
       
            v_array = [v_l1, v_l2, v_b1]

 
        thrust = float('nan')


        directions = []

        for theta in np.arange(0, np.pi, 0.2):
            for phi in np.arange(0, 2*np.pi, 0.2):
                unit_vec = TVector3(0,0,1)  
                unit_vec.SetMag(1.0)
                unit_vec.SetTheta(theta)
                unit_vec.SetPhi(phi)
                directions.append(unit_vec)

        #directions = np.array(directions) 

        scalar_product = [sum([v.Dot(direction)*np.heaviside(v.Dot(direction), 1.0) for v in v_array]) for direction in directions ]  


        d = max(scalar_product)
        
        pt_tot = sum([v.Mag() for v in v_array])        
        
        if pt_tot > 0:
            d = d/pt_tot
        else:
            return -1

        return d



    def deltaPhi(self,phi1,phi2):
        dphi = phi1 - phi2
        while (dphi >= pi): dphi-=2*pi
        while (dphi < -pi): dphi+=2*pi
        return dphi



    def getZHbeamAngle(self, tree, syst, Q):
        lep1, lep2, Vtype, _= self.getLeptons(tree)
        H = self.getHsyst(tree, syst, Q)
       

        if H.M() < 0:
            return -100

 
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100

        beam.SetPxPyPzE(0,0,6500,6500)

        VH = TLorentzVector()
        VH = tmp_lep1+tmp_lep2+tmp_H

        ZHbeamAngle = float('nan')

        try:
            #Theta  = acos((V_mom.Vect().Unit()).Dot(beam.Vect().Unit()))
            ZHbeamAngle = (VH.Vect().Unit()).Dot(beam.Vect().Unit())
        except Exception:
            #pass
            ZHbeamAngle = -100

        return ZHbeamAngle

    
    #######################
    ######## LHE #########
    #####################

    def getLHELeptons(self,tree,reference = True):
        if tree.isData == 1 or not hasattr(tree, 'LHEPart_pdgId'): #ZZ, WZ, WW pythia8 have no LHE branches
            lep1,lep2 = TLorentzVector(),TLorentzVector()
            lep1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            lep2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            LHE_Vtype = -1
            return lep1, lep2, LHE_Vtype


        LHE_pdgId = list(tree.LHEPart_pdgId)
        LHE_pt = list(tree.LHEPart_pt)
        LHE_eta = list(tree.LHEPart_eta) 
        LHE_phi = list(tree.LHEPart_phi)
        LHE_mass = list(tree.LHEPart_mass)
        LHE_isZee = False
        LHE_isZmm = False
        LHE_isZtautau = False
        LHE_isZnn = False
        LHE_isWen = False
        LHE_isWmn = False
        LHE_isWtaun = False
        LHE_Vtype = -99

        lep1, lep2 = TLorentzVector(), TLorentzVector()
        lep_chg, lep_nu = TLorentzVector(), TLorentzVector()


        if ((11 in LHE_pdgId) and (-11 in LHE_pdgId)): LHE_isZee = True
        if ((13 in LHE_pdgId) and (-13 in LHE_pdgId)): LHE_isZmm = True
        if ((15 in LHE_pdgId) and (-15 in LHE_pdgId)): LHE_isZtautau = True
        if (((12 in LHE_pdgId) and (-12 in LHE_pdgId)) or ((14 in LHE_pdgId) and (-14 in LHE_pdgId)) or ((16 in LHE_pdgId) and (-16 in LHE_pdgId))): LHE_isZnn = True

        if (((11 in LHE_pdgId) and (-12 in LHE_pdgId)) or ((-11 in LHE_pdgId) and (12 in LHE_pdgId))): LHE_isWen = True
        if (((13 in LHE_pdgId) and (-14 in LHE_pdgId)) or ((-13 in LHE_pdgId) and (14 in LHE_pdgId))): LHE_isWmn = True
        if (((15 in LHE_pdgId) and (-16 in LHE_pdgId)) or ((-15 in LHE_pdgId) and (16 in LHE_pdgId))): LHE_isWtaun = True


	if (LHE_isWen or LHE_isWmn):
            if LHE_isWen: 
                LHE_Vtype = 3
            else:
                pass
            if LHE_isWmn: 
                LHE_Vtype = 2
            else:
                pass
        elif (LHE_isZee and LHE_isZmm): 
            LHE_Vtype = -9 
        else:
            LHE_Vtype = -99


        if (LHE_Vtype == 2):
            if (13 in LHE_pdgId):            
                try:
                    lep_chg.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(13)],LHE_eta[LHE_pdgId.index(13)],LHE_phi[LHE_pdgId.index(13)],LHE_mass[LHE_pdgId.index(13)])
                except:
                    lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                try:
                    lep_nu.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-14)],LHE_eta[LHE_pdgId.index(-14)],LHE_phi[LHE_pdgId.index(-14)],LHE_mass[LHE_pdgId.index(-14)])
                except:
                    lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

            else:
                try: 
                    lep_chg.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-13)],LHE_eta[LHE_pdgId.index(-13)],LHE_phi[LHE_pdgId.index(-13)],LHE_mass[LHE_pdgId.index(-13)])
                except:
                    lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                try: 
                    lep_nu.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(14)],LHE_eta[LHE_pdgId.index(14)],LHE_phi[LHE_pdgId.index(14)],LHE_mass[LHE_pdgId.index(14)])
                except:
                    lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)


        if (LHE_Vtype == 3):
            if (11 in LHE_pdgId):            
                try:
                    lep_chg.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(11)],LHE_eta[LHE_pdgId.index(11)],LHE_phi[LHE_pdgId.index(11)],LHE_mass[LHE_pdgId.index(11)])
                except:
                    lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                try:
                    lep_nu.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-12)],LHE_eta[LHE_pdgId.index(-12)],LHE_phi[LHE_pdgId.index(-12)],LHE_mass[LHE_pdgId.index(-12)])
                except:
                    lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

            else:
                try: 
                    lep_chg.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-11)],LHE_eta[LHE_pdgId.index(-11)],LHE_phi[LHE_pdgId.index(-11)],LHE_mass[LHE_pdgId.index(-11)])
                except:
                    lep_chg.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                try: 
                    lep_nu.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(12)],LHE_eta[LHE_pdgId.index(12)],LHE_phi[LHE_pdgId.index(12)],LHE_mass[LHE_pdgId.index(12)])
                except:
                    lep_nu.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

	chg_lep_positive = (-11 in LHE_pdgId) or (-13 in LHE_pdgId) or (-15 in LHE_pdgId)
        if reference: #strict theory reference to the positively charged lepton
            if chg_lep_positive:
                lep1 = lep_chg
                lep2 = lep_nu
            else:
                lep1 = lep_nu
                lep2 = lep_chg
        else: #old default to charged lepton
            lep1 = lep_chg
            lep2 = lep_nu


	return lep_chg, lep_nu, LHE_Vtype

    def getLHEH(self,tree): 
        if tree.isData == 1 or not hasattr(tree, 'LHEPart_pdgId'): #ZZ, WZ, WW pythia8 have no LHE branches
            bb = TLorentzVector()
            bb.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            return bb
            
        LHE_pdgId = list(tree.LHEPart_pdgId)
        LHE_pt = list(tree.LHEPart_pt)
        LHE_eta = list(tree.LHEPart_eta)
        LHE_phi = list(tree.LHEPart_phi)
        LHE_mass = list(tree.LHEPart_mass)

        b1, b2, bb = TLorentzVector(), TLorentzVector(), TLorentzVector()
        bb.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

        try:
            b1.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-5)],LHE_eta[LHE_pdgId.index(-5)],LHE_phi[LHE_pdgId.index(-5)],LHE_mass[LHE_pdgId.index(-5)])
        except:
            b1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
        try:
            b2.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(5)],LHE_eta[LHE_pdgId.index(5)],LHE_phi[LHE_pdgId.index(5)],LHE_mass[LHE_pdgId.index(5)])
        except:
            b2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

        bb = b1 + b2
        return bb
    


    def getLHEVH(self,tree):
        H = self.getLHEH(tree)
        lep1, lep2, LHE_Vtype = self.getLHELeptons(tree)
        V, VH = TLorentzVector(), TLorentzVector()
        V = lep1 + lep2
        VH = V + H
        return VH.Pt(), VH.Eta(), VH.Phi(), VH.M()
    

    def getHiggsPtEtaPhiMFrombb(self,tree):
        H = self.getLHEH(tree)
        H_mass = H.M()
        H_eta = H.Eta()
        H_phi = H.Phi()
        H_pt = H.Pt()
        return H_pt, H_eta, H_phi, H_mass
