#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class BTagWeights1lepLFCR_2017(AddCollectionsModule):

    def __init__(self, year):
        super(BTagWeights1lepLFCR_2017, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(BTagWeights1lepLFCR_2017, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(BTagWeights1lepLFCR_2017, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(BTagWeights1lepLFCR_2017, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']


        self.BTagWeights_Vpt = { 
                "isWenu":
                    {
                        '150250':{'bins1':[0.1522, 0.237675, 0.32315 , 0.408625, 0.4941], 'bins2':[0.0, 0.05, 0.10, 0.15, 0.20], 'central':[[1.1736211776733398, 0.9927641153335571, 0.9897501468658447, 0.9518734216690063], [1.0857696533203125, 1.1115121841430664, 1.1207304000854492, 1.1642554998397827], [1.138763427734375, 1.215050220489502, 0.9925808906555176, 1.318078875541687], [1.0419847965240479, 1.107140302658081, 1.1008220911026, 1.1294221878051758]] ,'uncertainty':[[0.2794101270594341, 0.02507854930908239, 0.03184804407098039, 0.038930979078859784], [0.030342443073165627, 0.045255764591751776, 0.057605906364965376, 0.08285783275543075], [0.05014929107839662, 0.08169896454644478, 0.07644402485147565, 0.3884094424557342], [0.0810372503753126, 0.09477212058699838, 0.11377446607246706, 0.14907737507176608]]
                        },
                        '250Inf':{'bins1':[0.1522, 0.26616667, 0.38013333, 0.4941],'bins2':[0.0, 0.05, 0.10, 0.15],'central':[[0.9961340427398682, 1.136314034461975, 0.9398121237754822], [1.1437879800796509, 1.1566728353500366, 1.1187039613723755], [1.2065081596374512, 1.393152117729187, 1.1961569786071777]],'uncertainty':[[0.029743781324731728, 0.05489559983189688, 0.06568301866098668], [0.045484805783519516, 0.07658792695577174, 0.10550966417375826], [0.07372478614955905, 0.12539530751075045, 0.18105637485860276]]
                        },
                    },
                  "isWmunu":
                    {
                        '150250':{'bins1':[0.1522  , 0.237675, 0.32315 , 0.408625, 0.4941  ], 'bins2':[0.0, 0.05, 0.10, 0.15, 0.20],'central':[[1.0278621912002563, 0.808647632598877, 1.0361902713775635, 1.2025080919265747], [1.1010618209838867, 1.1330838203430176, 1.1669220924377441, 1.0661277770996094], [1.1805343627929688, 1.095475673675537, 1.265771746635437, 1.2845360040664673], [1.2009007930755615, 0.8763824105262756, 1.259667158126831, 1.2309513092041016]] ,'uncertainty': [[0.016536712710205993, 0.18678689947524527, 0.02979166395876972, 0.04753236056095041], [0.0266649923022901, 0.03872436427521865, 0.0523152079432168, 0.1105592869472324], [0.04453661454140271, 0.05733583032933459, 0.08609204919761249, 0.11929384516408331], [0.07219303007916786, 0.1628940999607197, 0.12639774199550238, 0.14575735810720178]]
                        },
                        '250Inf':{'bins1':[0.1522, 0.26616667, 0.38013333, 0.4941],'bins2':[0.0, 0.05, 0.10, 0.15],'central':[[1.0730159282684326, 0.988750696182251, 1.0435762405395508], [1.2090599536895752, 1.2852609157562256, 1.4730981588363647], [1.213042140007019, 1.340933084487915, 1.603660225868225]],'uncertainty':[[0.028584502719928274, 0.039750834679736, 0.06055395169593947], [0.04216841098893964, 0.07011797871931767, 0.11304601266040316], [0.06296342171032596, 0.108865915435778, 0.19468103130133077]]
                        },
                    },                  
                }

        self.BTagWeights_Vpt_set2 = {
            "isWenu":{
                    '150250':{'central':[1.0476595163345337],'uncertainty':[0.05548471341255057]},
                    '250Inf':{'central':[1.1367871761322021],'uncertainty':[0.061996711609727674]},
                    },
            "isWmunu":{
                    '150250':{'central':[1.219683289527893],'uncertainty':[0.05504745215889187]},
                    '250Inf':{'central':[1.277798056602478],'uncertainty':[0.0602362543376478]},
                    }
            }
        
        self.get_BTagWeights_Vptdict = {'150':'150250','250':'250Inf'}
        self.BTagWeights_Vpt_branches = ["WJet_btagDeepBVHbb2D"]

        if self.sample.isMC():
            self.maxNjet   = 256

            for var in ["Jet_btagDeepB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isWmunu","isWenu"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["nGenBpt25eta2p6","nGenDpt25eta2p6"]:
                self.existingBranches[var] = array.array('i', [100])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.BTagWeights_Vpt_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                    
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies(self, attr):
        isWJbtag_udsgc = False
        #print(attr["hJidx"][0])
        #print(attr["hJidx"][1])
        #print(attr["Jet_btagDeepB"][attr["hJidx"][0]])
        #print(attr["Jet_btagDeepB"][attr["hJidx"][1]])
        #print(attr["nGenBpt25eta2p6"][0])
        #print(attr["nGenDpt25eta2p6"][0])
        if (any([x in self.sample.identifier for x in ['WJetsToLNu','W1JetsToLNu','W2JetsToLNu']]) and any([s in self.sample.identifier for s in ['amcnloFXFX','amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if ((attr["Jet_btagDeepB"][attr["hJidx"][0]] > 0.1522 and attr["Jet_btagDeepB"][attr["hJidx"][0]] < 0.4941  and attr["Jet_btagDeepB"][attr["hJidx"][1]] < 0.4941) and ((attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1) or (attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]>0))):
                isWJbtag_udsgc = True
                #print(attr["hJidx"][0])
                #print(attr["hJidx"][1])
                #print(attr["Jet_btagDeepB"][attr["hJidx"][0]])
                #print(attr["Jet_btagDeepB"][attr["hJidx"][1]])
                #print(attr["nGenBpt25eta2p6"][0])
                #print(attr["nGenDpt25eta2p6"][0])
 
        #print("final: ",isWJbtag_udsgc)
        #print("----------------------------------------------------------------")
        return isWJbtag_udsgc

    def get_lowerBound(self,value,bins):
        j = 0
        #print("value is",value)
        #print("bins are ",bins)
        while not (value<bins[j] or value>=250):
            #print("value is ", value, "and bins are ",bins[j])
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        dRbbWeight_Vpt  = []
        dRbbWeight_Incl = []
 
        btag_hJidx0        = attr["Jet_btagDeepB"][attr["hJidx"][0]]
        btag_hJidx1        = attr["Jet_btagDeepB"][attr["hJidx"][1]]
        BTagWeights_Vptbin = self.get_BTagWeights_Vptdict[self.get_lowerBound(attr["V_pt"][0],[150,250])]

        #print(attr["V_pt"])
       
 
        j=0
        channel = None
        if attr["isWenu"][0] == 1: channel = "isWenu"
        if attr["isWmunu"][0] == 1: channel = "isWmunu"

        if (btag_hJidx0 < 0.0 or btag_hJidx1 < 0.0):
            return 1.0,1.0,1.0

        if (btag_hJidx1 >= self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins2"][-1]):
            nom = self.BTagWeights_Vpt_set2[channel][BTagWeights_Vptbin]['central'][0]
            unc = self.BTagWeights_Vpt_set2[channel][BTagWeights_Vptbin]['uncertainty'][0]
            #print("set2 ",nom,nom+unc,nom-unc)
            return nom,nom+unc,nom-unc

        while not (btag_hJidx0 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins1"][j]):
            #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][j])
            #print(btag_hJidx0)
            j+=1
            #print("j is now amining for ",j)
        k=0
        while not (btag_hJidx1 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins2"][k]):
            k+=1
        #print(j-1)
        #print(k-1)
        #print("channel ",channel," BTagWeights_Vptbin ",BTagWeights_Vptbin," j-1 ",j-1," k-1 ",k-1)
        #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1])
        nom = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1]
        up = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] + self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        down = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] - self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        #print("set1 ",nom,up,down)        
        return  nom,up,down 

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)
            attr = {}
            #for var in ["V_pt","hJidx","isWenu","isWmunu","Jet_btagDeepB","nGenBpt25eta2p6","nGenDpt25eta2p6","isZnn"]: 
            for var in ["V_pt","hJidx","isWenu","isWmunu","Jet_btagDeepB","nGenBpt25eta2p6","nGenDpt25eta2p6"]: 
                attr[var] = self.existingBranches[var]
            #print(attr["isWmunu"])        
            if self.applies(attr):
                nom,up,down = self.get_eventWeight(attr)
                self._b("WJet_btagDeepBVHbb2D")[0]         = nom
                self._b("WJet_btagDeepBVHbb2D"+"Up")[0]    = up
                self._b("WJet_btagDeepBVHbb2D"+"Down")[0]  = down
                #print("-------------------------------------------------")
            else:
                self._b("WJet_btagDeepBVHbb2D")[0]         = 1.0
                self._b("WJet_btagDeepBVHbb2D"+"Up")[0]    = 1.0
                self._b("WJet_btagDeepBVHbb2D"+"Down")[0]  = 1.0
                #print("-------------------------------------------------")


if __name__=='__main__':

    config = XbbConfigReader.read('Wlv2018')
    info = ParseInfo(config=config)
    sample = [x for x in info if x.identifier == 'WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8'][0]
    print(sample)

    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_94b42e05db46cb9fd44285b60e0ba8b57d34232afdeef2661e673e5f_000000_000000_0000_0_5c5fb21d0e20a7f021911ce80109ccd1fa39014fff8bca8fc463de97.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Wlv/mva/18oct20_all_2DbtagdRbbDYWeightsfromLFCR/WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_bb21dac9dc44e46adf956d61d5b485c39341b737ba249606a80eb2f6_000000_000000_0000_0_09ffe41113d97125a40885075110dac51c0f173e112396ca28a14ce2.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYBJetsToLL_M-50_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8/tree_a6b49dbb202048bd17cdf3f392ef3c2813bf6388d3275d8708169f7b_000000_000000_0000_0_f11fc63b7542561838a2fc3df60d8f8f1f67dd0c6fd24dd343324e76.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    w = BTagWeights1lepLFCR("2018")
    w.customInit({'sampleTree': sampleTree, 'sample': sample, 'config': config})
    sampleTree.addOutputBranches(w.getBranches())
    #histograms={}
    #for jec in w.JEC_reduced:
        
    #for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
    #    histograms[var] = {}
    #    for syst in w.JEC_reduced:
    #        histograms[var][syst] = {}
    #        for Q in ['Up','Down']:
    #            histograms[var][syst][Q]=ROOT.TH1F(var+syst+Q, var+syst+Q, 400, -2.0, 2.0 )

    n=0 
    #var = "MET_phi"
    for event in sampleTree:
        n=n+1
        w.processEvent(event)
        if n==30: break

   # with open('jec_validate_'+var+'.csv', 'w') as file:
   #     writer = csv.writer(file)
   #     writer.writerow(["event","run","Q",'true_jesAbsolute','jesAbsolute','AT','diff','true_jesAbsolute_2018','jesAbsolute_2018','AT','diff','true_jesBBEC1','jesBBEC1','AT','diff','true_jesBBEC1_2018','jesBBEC1_2018','AT','diff','true_jesEC2','jesEC2','AT','diff','true_jesEC2_2018','jesEC2_2018','AT','diff','true_jesHF','jesHF','AT','diff','true_jesHF_2018','jesHF_2018','AT','diff','true_jesRelativeSample_2018','jesRelativeSample_2018','AT','diff'])
   #     for event in sampleTree:
   #         n=n+1
   #         event,run,true_attr, attr = w.processEvent(event)
   #         for Q in ['Up','Down']:
   #             njet = len(true_attr[var][w.JEC_reduced[0]][Q])
   #             if njet>1:
   #                 for i in range(njet):
   #                     writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][i],attr[var]['jesAbsolute'][Q][i],'','',true_attr[var]['jesAbsolute_2018'][Q][i],attr[var]['jesAbsolute_2018'][Q][i],'','',true_attr[var]['jesBBEC1'][Q][i],attr[var]['jesBBEC1'][Q][i],'','',true_attr[var]['jesBBEC1_2018'][Q][i],attr[var]['jesBBEC1_2018'][Q][i],'','',true_attr[var]['jesEC2'][Q][i],attr[var]['jesEC2'][Q][i],'','',true_attr[var]['jesEC2_2018'][Q][i],attr[var]['jesEC2_2018'][Q][i],'','',true_attr[var]['jesHF'][Q][i],attr[var]['jesHF'][Q][i],'','',true_attr[var]['jesHF_2018'][Q][i],attr[var]['jesHF_2018'][Q][i],'','',true_attr[var]['jesRelativeSample_2018'][Q][i],attr[var]['jesRelativeSample_2018'][Q][i],'',''])
   #             else:
   #                 writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][0],attr[var]['jesAbsolute'][Q][0],'','',true_attr[var]['jesAbsolute_2018'][Q][0],attr[var]['jesAbsolute_2018'][Q][0],'','',true_attr[var]['jesBBEC1'][Q][0],attr[var]['jesBBEC1'][Q][0],'','',true_attr[var]['jesBBEC1_2018'][Q][0],attr[var]['jesBBEC1_2018'][Q][0],'','',true_attr[var]['jesEC2'][Q][0],attr[var]['jesEC2'][Q][0],'','',true_attr[var]['jesEC2_2018'][Q][0],attr[var]['jesEC2_2018'][Q][0],'','',true_attr[var]['jesHF'][Q][0],attr[var]['jesHF'][Q][0],'','',true_attr[var]['jesHF_2018'][Q][0],attr[var]['jesHF_2018'][Q][0],'','',true_attr[var]['jesRelativeSample_2018'][Q][0],attr[var]['jesRelativeSample_2018'][Q][0],'',''])

   #         #print("----------events over------------")
   #         if n==5: break

   # f = TFile("delete.root","RECREATE")

   # for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
   #     for syst in w.JEC_reduced:
   #         for Q in ['Up','Down']:
   #             histograms[var][syst][Q].Write()
   # f.Close()
