#!/usr/bin/env python
import ROOT
from ROOT import TVector3, TLorentzVector
from math import pi, sqrt, cos, sin, sinh, log, cosh, acos, acosh
import numpy as np
import array
import os
from BranchTools import Collection
from BranchTools import AddCollectionsModule
from XbbConfig import XbbConfigTools

class EFT_obs_2l(AddCollectionsModule):

    def __init__(self, branchName='EFT_obs'):
        super(EFT_obs_2l, self).__init__()
        self.branchName = branchName


    def customInit(self, initVars):
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.isData     = initVars['sample'].isData()
        self.sampleTree = initVars['sampleTree']
        self.xbbConfig  = XbbConfigTools(self.config)

        self.Nevent = 0           


       
        # Include boost syst for boosted events
        if not self.isData:
            self.systematics = ["Nominal"] + self.xbbConfig.getJECuncertainties(step='VReco') + ['jms','jmr']

            #self.systematics = ["Nominal"]
            #NEED TO ADD UNCLUST FOR MET (1 lep)
            #,'unclustEn']

        else: 

            self.systematics = ["Nominal"]
 

        ##debugging_only
        #self.addBranch(self.branchName + '_LHE_Theta_e')
        #self.addBranch(self.branchName + '_LHE_Theta_m')
        #self.addBranch(self.branchName + '_LHE_Theta_l')
        #self.addBranch(self.branchName + '_LHE_theta_e')
        #self.addBranch(self.branchName + '_LHE_theta_m')
        #self.addBranch(self.branchName + '_LHE_theta_l')
        #self.addBranch(self.branchName + '_LHE_phi_e')
        #self.addBranch(self.branchName + '_LHE_phi_m')
        #self.addBranch(self.branchName + '_LHE_phi_l')
        #self.addBranch(self.branchName + '_LHE_phi_weight')


        #self.addBranch(self.branchName + '_LHE_H_mass_from_bb')
        #self.addBranch(self.branchName + '_LHE_H_pt_from_bb')
        #self.addBranch(self.branchName + '_LHE_H_eta_from_bb')
        #self.addBranch(self.branchName + '_LHE_H_phi_from_bb')
        #self.addBranch(self.branchName + '_LHE_Vtype')

        ##self.addBranch(self.branchName + '_VH_mass')
        #self.addBranch(self.branchName + '_LHE_VH_mass_from_llbb')


        #Additional features 
        #self.addBranch(self.branchName + '_thrust')
        #self.addBranch(self.branchName + '_Vh_beam_angle')


        self.EFT_features = ["Theta_e","Theta_m","Theta_l","theta_e","theta_m","theta_l","phi_e","phi_m","phi_l","phi_weight","VH_mass","thrust","Vh_beam_angle"]  
        self.EFTProperties = [self.branchName + '_' + x for x in self.EFT_features]


        for EFTProperty in self.EFTProperties: 
            for syst in self.systematics:
                for Q in self._variations(syst):
                    self.addBranch(self._v(EFTProperty, syst, Q))




    def processEvent(self, tree):
        # if current entry has not been processed yet
        if not self.hasBeenProcessed(tree):
            self.markProcessed(tree)

            #print("Nevent ", self.Nevent, " out of ", tree.GetEntries())
            self.Nevent+=1


            #No syst affecting LHE
            #self._b(self.branchName + '_LHE_H_pt_from_bb')[0],self._b(self.branchName + '_LHE_H_eta_from_bb')[0],self._b(self.branchName + '_LHE_H_phi_from_bb')[0],self._b(self.branchName + '_LHE_H_mass_from_bb')[0]  = self.getHiggsPtEtaPhiMFrombb(tree)
            #self._b(self.branchName + '_LHE_Vtype')[0] = self.getLHELeptons(tree)[2] 

            #self._b(self.branchName + '_LHE_Theta_e')[0], self._b(self.branchName + '_LHE_Theta_m')[0], self._b(self.branchName + '_LHE_Theta_l')[0] = self.getTheta(tree,True)
            #self._b(self.branchName + '_LHE_theta_e')[0], self._b(self.branchName + '_LHE_theta_m')[0], self._b(self.branchName + '_LHE_theta_l')[0] = self.gettheta(tree,True)
            #self._b(self.branchName + '_LHE_phi_e')[0], self._b(self.branchName + '_LHE_phi_m')[0], self._b(self.branchName + '_LHE_phi_l')[0] = self.getphi(tree,True)

            #self._b(self.branchName + '_LHE_VH_mass_from_llbb')[0] = self.getLHEVH(tree)[3]
            #self._b(self.branchName + '_LHE_phi_weight')[0] = self.getphiweight(tree,True)


            for syst in self.systematics:
                for Q in self._variations(syst):


                    Theta, theta, phi, weight, ZHbeamAngle, VHmass = self.getEFTangles(tree, syst, Q)

                    self._b(self._v(self.branchName + '_Theta_l', syst, Q))[0] = Theta 
                    self._b(self._v(self.branchName + '_theta_l', syst, Q))[0] = theta
                    self._b(self._v(self.branchName + '_phi_l', syst, Q))[0] = phi
                    self._b(self._v(self.branchName + '_phi_weight', syst, Q))[0] = weight                     
                    self._b(self._v(self.branchName + '_Vh_beam_angle', syst, Q))[0] = ZHbeamAngle
                    self._b(self._v(self.branchName + '_VH_mass', syst, Q))[0] = VHmass
                    

                    #self._b(self._v(self.branchName + '_thrust', syst, Q))[0] = self.getThrust(tree, syst, Q)


                    #self._b(self._v(self.branchName + '_Theta_e', syst, Q))[0], self._b(self._v(self.branchName + '_Theta_m', syst, Q))[0], self._b(self._v(self.branchName + '_Theta_l', syst, Q))[0] = self.getTheta(tree,False, syst, Q)
                    #self._b(self._v(self.branchName + '_theta_e', syst, Q))[0], self._b(self._v(self.branchName + '_theta_m', syst, Q))[0], self._b(self._v(self.branchName + '_theta_l', syst, Q))[0] = self.gettheta(tree,False, syst, Q)
                    #self._b(self._v(self.branchName + '_phi_e', syst, Q))[0], self._b(self._v(self.branchName + '_phi_m', syst, Q))[0], self._b(self._v(self.branchName + '_phi_l', syst, Q))[0] = self.getphi(tree,False, syst, Q)


                    #self._b(self._v(self.branchName + '_VH_mass', syst, Q))[0] = self.getVH(tree, syst, Q)[3]

                    #self._b(self._v(self.branchName + '_phi_weight', syst, Q))[0] = self.getphiweight(tree,False, syst, Q)
                    #
                    #self._b(self._v(self.branchName + '_Vh_beam_angle', syst, Q))[0] = self.getZHbeamAngle(tree, syst, Q)
                    #self._b(self._v(self.branchName + '_thrust', syst, Q))[0] = self.getThrust(tree, syst, Q)




    def getEFTangles(self, tree, syst, Q):
        
        lep1, lep2, Vtype = self.getLeptons(tree)
        H = self.getHsyst(tree,syst,Q)
    
        if H.M() < 0:
            return -100,-100, -100,-100, -100,-100

    
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100,-100, -100,-100, -100,-100


        #Theta
        beam.SetPxPyPzE(0,0,6500,6500)

        V_mom, bVH = TLorentzVector(), TVector3()
        V_mom = tmp_lep1+tmp_lep2
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()

        V_mom.Boost(-bVH)

        Theta = float('nan')

        try:
            Theta = (V_mom.Vect().Unit()).Angle(beam.Vect().Unit())
        except Exception:
            Theta = -100

        #theta
        V_mom, bVH, bV = TLorentzVector(), TVector3(), TVector3()

        bVH = (tmp_lep1 + tmp_lep2 + tmp_H).BoostVector()
        V_mom = (tmp_lep1 + tmp_lep2)

        V_mom.Boost(-bVH)
        tmp_lep1.Boost(-bVH)

        bV = V_mom.BoostVector()
        tmp_lep1.Boost(-bV)

        theta = float('nan')
        try:
            theta = (V_mom).Angle(tmp_lep1.Vect())

        except Exception:
            #pass
            theta = -100


        #phi
        V_mom, bVH, n_scatter, n_decay = TLorentzVector(), TVector3(), TVector3(), TVector3()
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()
        V_mom = tmp_lep1+tmp_lep2

        tmp_lep1.Boost(-bVH)
        tmp_lep2.Boost(-bVH)
        V_mom.Boost(-bVH)
        #beam.Boost(-bVH)

        n_scatter = ((beam.Vect().Unit()).Cross(V_mom.Vect())).Unit()
        n_decay   = (tmp_lep1.Vect().Cross(tmp_lep2.Vect())).Unit()

        sign_flip =  -1 if ( ((n_scatter.Cross(n_decay))*(V_mom.Vect())) < 0 ) else +1

        try:
            phi = sign_flip*acos(n_scatter.Dot(n_decay))
        except Exception:
            #pass
            phi = -100


        #phiweight
        try:
            weight = np.sin(2*theta) * np.sin(2*Theta)
        except Exception:
            #pass 
            weight = -100


        #Helicity
        VH = TLorentzVector()
        VH = tmp_lep1+tmp_lep2+tmp_H

        ZHbeamAngle = float('nan')

        try:
            ZHbeamAngle = (VH.Vect().Unit()).Dot(beam.Vect().Unit())
        except Exception:
            ZHbeamAngle = -100


        try:
            VHmass = VH.M()
        except Exception:
            VHmass = -100



        return Theta, theta, phi, weight, ZHbeamAngle, VHmass



    def getLeptons(self, tree):
        isZee = tree.isZee
        isZmm = tree.isZmm
        vLidx = tree.vLidx
        Vtype = tree.Vtype

        #basic check for consistency
        #if (isZee+isZmm != 1): print "isZee and isZmm inconsistent"
        if (isZee == 1 and Vtype != 1): print("isZee and Vtype inconsistent")
        if (isZmm == 1 and Vtype != 0): print("isZmm and Vtype inconsistent")

        Electron_pt = tree.Electron_pt
        Electron_eta = tree.Electron_eta
        Electron_phi = tree.Electron_phi
        Electron_mass = tree.Electron_mass
        Electron_charge = tree.Electron_charge

        Muon_pt = tree.Muon_pt
        Muon_eta = tree.Muon_eta
        Muon_phi = tree.Muon_phi
        Muon_mass = tree.Muon_mass
        Muon_charge = tree.Muon_charge

        lep1, lep2 = TLorentzVector(), TLorentzVector()

        if (Vtype == 0): #Zmm
            order = Muon_charge[vLidx[0]] > Muon_charge[vLidx[1]]
            try:
                if order:
                    lep1.SetPtEtaPhiM(Muon_pt[vLidx[0]],Muon_eta[vLidx[0]],Muon_phi[vLidx[0]],Muon_mass[vLidx[0]])
                else:
                    lep2.SetPtEtaPhiM(Muon_pt[vLidx[0]],Muon_eta[vLidx[0]],Muon_phi[vLidx[0]],Muon_mass[vLidx[0]])
            except:
                if order:
                    lep1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                else:
                    lep2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)                  
            try:
                if order:
                    lep2.SetPtEtaPhiM(Muon_pt[vLidx[1]],Muon_eta[vLidx[1]],Muon_phi[vLidx[1]],Muon_mass[vLidx[1]])
                else:
                    lep1.SetPtEtaPhiM(Muon_pt[vLidx[1]],Muon_eta[vLidx[1]],Muon_phi[vLidx[1]],Muon_mass[vLidx[1]])
            except:
                if order:
                    lep2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                else:
                    lep1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)



        if (Vtype == 1): #Zee
            order = Electron_charge[vLidx[0]] > Electron_charge[vLidx[1]]
            try:
                if order:
                    lep1.SetPtEtaPhiM(Electron_pt[vLidx[0]],Electron_eta[vLidx[0]],Electron_phi[vLidx[0]],Electron_mass[vLidx[0]])
                else:
                    lep2.SetPtEtaPhiM(Electron_pt[vLidx[0]],Electron_eta[vLidx[0]],Electron_phi[vLidx[0]],Electron_mass[vLidx[0]])
            except:
                if order:
                    lep1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                else:
                    lep2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            try:
                if order:
                    lep2.SetPtEtaPhiM(Electron_pt[vLidx[1]],Electron_eta[vLidx[1]],Electron_phi[vLidx[1]],Electron_mass[vLidx[1]])
                else:
                    lep1.SetPtEtaPhiM(Electron_pt[vLidx[1]],Electron_eta[vLidx[1]],Electron_phi[vLidx[1]],Electron_mass[vLidx[1]])
            except:
                if order:
                    lep2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
                else:
                    lep1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

        return lep1, lep2, Vtype






    def getLHELeptons(self,tree):
        if tree.isData == 1 or not hasattr(tree, 'LHEPart_pdgId'): #ZZ, WZ, WW pythia8 have no LHE branches
            lep1,lep2 = TLorentzVector(),TLorentzVector()
            lep1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            lep2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            LHE_Vtype = -1
            return lep1, lep2, LHE_Vtype


        LHE_pdgId = list(tree.LHEPart_pdgId)
        LHE_pt = list(tree.LHEPart_pt)
        LHE_eta = list(tree.LHEPart_eta) 
        LHE_phi = list(tree.LHEPart_phi)
        LHE_mass = list(tree.LHEPart_mass)
        LHE_isZee = False
        LHE_isZmm = False
        LHE_isZtautau = False
        LHE_isZnn = False
        LHE_isWen = False
        LHE_isWmn = False
        LHE_isWtaun = False
        LHE_Vtype = -99

        lep1, lep2 = TLorentzVector(), TLorentzVector()


        if ((11 in LHE_pdgId) and (-11 in LHE_pdgId)): LHE_isZee = True
        if ((13 in LHE_pdgId) and (-13 in LHE_pdgId)): LHE_isZmm = True
        if ((15 in LHE_pdgId) and (-15 in LHE_pdgId)): LHE_isZtautau = True
        if (((12 in LHE_pdgId) and (-12 in LHE_pdgId)) or ((14 in LHE_pdgId) and (-14 in LHE_pdgId)) or ((16 in LHE_pdgId) and (-16 in LHE_pdgId))): LHE_isZnn = True

        if (((11 in LHE_pdgId) and (-12 in LHE_pdgId)) or ((-11 in LHE_pdgId) and (12 in LHE_pdgId))): LHE_isWen = True
        if (((13 in LHE_pdgId) and (-14 in LHE_pdgId)) or ((-13 in LHE_pdgId) and (14 in LHE_pdgId))): LHE_isWmn = True
        if (((15 in LHE_pdgId) and (-16 in LHE_pdgId)) or ((-15 in LHE_pdgId) and (16 in LHE_pdgId))): LHE_isWtaun = True
        
        if (LHE_isZee or LHE_isZmm):
            if LHE_isZee: 
                LHE_Vtype = 1
            else:
                pass
            if LHE_isZmm: 
                LHE_Vtype = 0
            else:
                pass
        elif (LHE_isZee and LHE_isZmm): 
            LHE_Vtype = -9 
        else:
            LHE_Vtype = -99

        if (LHE_Vtype == 1):
            try:
                    lep1.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-11)],LHE_eta[LHE_pdgId.index(-11)],LHE_phi[LHE_pdgId.index(-11)],LHE_mass[LHE_pdgId.index(-11)])
            except:
                    lep1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

            try: 
                    lep2.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(11)],LHE_eta[LHE_pdgId.index(11)],LHE_phi[LHE_pdgId.index(11)],LHE_mass[LHE_pdgId.index(11)])
            except:
                    lep2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

        if (LHE_Vtype == 0):

            try:
                    lep1.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-13)],LHE_eta[LHE_pdgId.index(-13)],LHE_phi[LHE_pdgId.index(-13)],LHE_mass[LHE_pdgId.index(-13)])
            except:
                    lep1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

            try:
                    lep2.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(13)],LHE_eta[LHE_pdgId.index(13)],LHE_phi[LHE_pdgId.index(13)],LHE_mass[LHE_pdgId.index(13)])
            except:
                    lep2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

        return lep1, lep2, LHE_Vtype


    




    def getH(self, tree):
        H_pt = tree.H_pt
        H_eta = tree.H_eta
        H_phi = tree.H_phi
        H_mass = tree.H_mass
        H = TLorentzVector()
        H.SetPtEtaPhiM(H_pt, H_eta, H_phi, H_mass)
        return H


    def getHsyst(self, tree, syst, Q):
        
        Hbb_fjidx = tree.Hbb_fjidx 
        hJidx = tree.hJidx
        
        idx0 = hJidx[0]       
        idx1 = hJidx[1]       
        H = TLorentzVector()

        if Hbb_fjidx > -1:
           
            H_eta = tree.FatJet_eta[Hbb_fjidx]
            H_phi = tree.FatJet_phi[Hbb_fjidx]


            if self._isnominal(syst) or "Reg" in syst:
                H_mass = tree.FatJet_Msoftdrop[Hbb_fjidx]
                H_pt = tree.FatJet_pt[Hbb_fjidx]

            #H mass specific syst
            elif "jms" in syst or "jmr" in syst:
                H_pt = getattr(tree, 'FatJet_pt_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
                #tree.FatJet_pt[Hbb_fjidx]
                H_mass = getattr(tree, 'FatJet_msoftdrop_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
            else: 
                H_pt = getattr(tree, 'FatJet_pt_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
                H_mass = getattr(tree, 'FatJet_msoftdrop_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]

        elif idx0 > -1 and idx1 > -1:


            if "jms" in syst or "jmr" in syst or syst == "jer" or self._isnominal(syst):
                H_pt = tree.H_pt
                H_eta = tree.H_eta
                H_phi = tree.H_phi
                H_mass = tree.H_mass
            else: 
                H_pt = getattr(tree, 'H_pt_{s}_{d}'.format(s=syst, d=Q))
                H_eta = tree.H_eta
                H_phi = tree.H_phi
                H_mass = getattr(tree, 'H_mass_{s}_{d}'.format(s=syst, d=Q))


        else:
             
            H.SetPtEtaPhiM(-1.0, -1.0, -1.0, -1.0)
            return H

        H.SetPtEtaPhiM(H_pt, H_eta, H_phi, H_mass)
        return H



    def getLHEH(self,tree): 
        if tree.isData == 1 or not hasattr(tree, 'LHEPart_pdgId'): #ZZ, WZ, WW pythia8 have no LHE branches
            bb = TLorentzVector()
            bb.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
            return bb
            
        LHE_pdgId = list(tree.LHEPart_pdgId)
        LHE_pt = list(tree.LHEPart_pt)
        LHE_eta = list(tree.LHEPart_eta)
        LHE_phi = list(tree.LHEPart_phi)
        LHE_mass = list(tree.LHEPart_mass)

        b1, b2, bb = TLorentzVector(), TLorentzVector(), TLorentzVector()
        bb.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

        try:
            b1.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(-5)],LHE_eta[LHE_pdgId.index(-5)],LHE_phi[LHE_pdgId.index(-5)],LHE_mass[LHE_pdgId.index(-5)])
        except:
            b1.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
        try:
            b2.SetPtEtaPhiM(LHE_pt[LHE_pdgId.index(5)],LHE_eta[LHE_pdgId.index(5)],LHE_phi[LHE_pdgId.index(5)],LHE_mass[LHE_pdgId.index(5)])
        except:
            b2.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)

        bb = b1 + b2
        return bb


    def getLHEVH(self,tree):
        H = self.getLHEH(tree)
        lep1, lep2, LHE_Vtype = self.getLHELeptons(tree)
        V, VH = TLorentzVector(), TLorentzVector()
        V = lep1 + lep2
        VH = V + H
        return VH.Pt(), VH.Eta(), VH.Phi(), VH.M()


    def getVH(self,tree, syst, Q):
        H = self.getHsyst(tree, syst, Q)
        V_pt = tree.V_pt 
        V_eta = tree.V_eta
        V_phi = tree.V_phi
        V_mass = tree.V_mass
        V, VH = TLorentzVector(), TLorentzVector()
        try:
            V.SetPtEtaPhiM(V_pt, V_eta, V_phi, V_mass)
        except:
            V.SetPtEtaPhiM(-1.0,-1.0,-1.0,-1.0)
        VH = V + H 
        return VH.Pt(), VH.Eta(), VH.Phi(), VH.M()


    def getHiggsPtEtaPhiMFrombb(self,tree):
        H = self.getLHEH(tree)
        H_mass = H.M()
        H_eta = H.Eta()
        H_phi = H.Phi()
        H_pt = H.Pt()
        return H_pt, H_eta, H_phi, H_mass

    def getTheta(self, tree, LHE_level,syst = None, Q = None):
        if LHE_level:
            lep1, lep2, Vtype = self.getLHELeptons(tree)
            H = self.getLHEH(tree)
        else:
            lep1, lep2, Vtype = self.getLeptons(tree)
            H = self.getHsyst(tree,syst,Q)
    
        if H.M() < 0:
            return [-100,-100, -100]

    
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100, -100, -100

        beam.SetPxPyPzE(0,0,6500,6500)

        V_mom, bVH = TLorentzVector(), TVector3()
        V_mom = tmp_lep1+tmp_lep2
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()

        V_mom.Boost(-bVH)

        Theta = float('nan')

        try:
            #Theta  = acos((V_mom.Vect().Unit()).Dot(beam.Vect().Unit()))
            Theta = (V_mom.Vect().Unit()).Angle(beam.Vect().Unit())
        except Exception:
            #pass
            Theta = -100

        if (Vtype == 0):
            Theta_l = Theta
            Theta_m = Theta
            Theta_e = -99999

        elif (Vtype == 1):
            Theta_l = Theta
            Theta_e = Theta
            Theta_m = -99999

        else:
            Theta_l = -9999
            Theta_m = -9999
            Theta_e = -9999


        return Theta_e, Theta_m, Theta_l


    def gettheta(self, tree, LHE_level, syst = None, Q = None):
        if LHE_level:
            lep1, lep2, Vtype = self.getLHELeptons(tree)
            H = self.getLHEH(tree)
        else:
            lep1, lep2, Vtype = self.getLeptons(tree)
            H = self.getHsyst(tree, syst, Q)


        if H.M() < 0:
            return [-100,-100, -100]


        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100, -100, -100

        V_mom, bVH, bV = TLorentzVector(), TVector3(), TVector3()

        bVH = (tmp_lep1 + tmp_lep2 + tmp_H).BoostVector()
        V_mom = (tmp_lep1 + tmp_lep2)

        V_mom.Boost(-bVH)
        tmp_lep1.Boost(-bVH)

        bV = V_mom.BoostVector()
        tmp_lep1.Boost(-bV)

        theta = float('nan')
        try:
            theta = (V_mom).Angle(tmp_lep1.Vect())
        except Exception:
            #pass
            theta = -100

        if (Vtype == 0):
            theta_l = theta
            theta_m = theta
            theta_e = -99999

        elif (Vtype == 1):
            theta_l = theta
            theta_e = theta
            theta_m = -99999

        else:
            theta_l = -9999
            theta_m = -9999
            theta_e = -9999

        return theta_e, theta_m, theta_l


    def getphi(self, tree, LHE_level, syst = None, Q = None):
        if LHE_level:
            lep1, lep2, Vtype = self.getLHELeptons(tree)
            H = self.getLHEH(tree)
        else:
            lep1, lep2, Vtype = self.getLeptons(tree)
            H = self.getHsyst(tree, syst, Q)

        if H.M() < 0:
            return [-100,-100,-100]

        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100, -100, -100

        beam.SetPxPyPzE(0,0,6500,6500)

        V_mom, bVH, n_scatter, n_decay = TLorentzVector(), TVector3(), TVector3(), TVector3()
        bVH = (tmp_lep1+tmp_lep2+tmp_H).BoostVector()
        V_mom = tmp_lep1+tmp_lep2

        tmp_lep1.Boost(-bVH)
        tmp_lep2.Boost(-bVH)
        V_mom.Boost(-bVH)
        #beam.Boost(-bVH)

        n_scatter = ((beam.Vect().Unit()).Cross(V_mom.Vect())).Unit()
        n_decay   = (tmp_lep1.Vect().Cross(tmp_lep2.Vect())).Unit()

        sign_flip =  -1 if ( ((n_scatter.Cross(n_decay))*(V_mom.Vect())) < 0 ) else +1

        try:
            phi = sign_flip*acos(n_scatter.Dot(n_decay))
        except Exception:
            #pass
            phi = -100

        if (Vtype == 0):
            phi_l = phi
            phi_m = phi
            phi_e = -99999

        elif (Vtype == 1):
            phi_l = phi
            phi_e = phi
            phi_m = -99999

        else:
            phi_l = -9999
            phi_m = -9999
            phi_e = -9999

        return phi_e, phi_m, phi_l


    def getphiweight(self, tree, LHE_level, syst = None, Q = None):
        Theta = self.getTheta(tree, LHE_level, syst, Q)[2]
        theta = self.gettheta(tree, LHE_level, syst, Q)[2]
        try:
            weight = np.sin(2*theta) * np.sin(2*Theta)
        except Exception:
            #pass 
            weight = -999999
        
        return weight

    def getThrust(self, tree, syst, Q):
        lep1, lep2, Vtype = self.getLeptons(tree)
        
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_b1, tmp_b2 = TLorentzVector(), TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
      

        idx0 = tree.hJidx[0]       
        idx1 = tree.hJidx[1]       
        Hbb_fjidx = tree.Hbb_fjidx


        if idx0 > -1 and idx1 > -1:
        
            if self._isnominal(syst) or "jmr" in syst or "jms" in syst or 'jerReg' in syst:
                Jet_Pt      = tree.Jet_pt
                Jet_Mass    = tree.Jet_mass
            else:
                Jet_Pt      = getattr(tree, 'Jet_pt_{s}{d}'.format(s=syst, d=Q))
                Jet_Mass    = getattr(tree, 'Jet_mass_{s}{d}'.format(s=syst, d=Q))

     
            tmp_b1.SetPtEtaPhiM(Jet_Pt[idx0],tree.Jet_eta[idx0],tree.Jet_phi[idx0],Jet_Mass[idx0])
            tmp_b2.SetPtEtaPhiM(Jet_Pt[idx1],tree.Jet_eta[idx1],tree.Jet_phi[idx1],Jet_Mass[idx1])


        elif Hbb_fjidx > -1:
            
            if self._isnominal(syst) or "Reg" in syst:
                Jet_Pt      = tree.FatJet_pt[Hbb_fjidx]
                Jet_Mass    = tree.FatJet_Msoftdrop[Hbb_fjidx]

            elif "jmr" in syst or "jms" in syst:
                Jet_Pt      = getattr(tree, 'FatJet_pt_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
                Jet_Mass    = getattr(tree, 'FatJet_msoftdrop_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
            else:
                Jet_Pt      = getattr(tree, 'FatJet_pt_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]
                Jet_Mass    = getattr(tree, 'FatJet_msoftdrop_{s}{d}'.format(s=syst, d=Q))[Hbb_fjidx]

     
            tmp_b1.SetPtEtaPhiM(Jet_Pt,tree.FatJet_eta[Hbb_fjidx],tree.FatJet_phi[Hbb_fjidx],Jet_Mass)


        else:
            return -1


        #if(lep1.Eta()<-10 or lep2.Eta()<-10 or tree.Jet_eta[idx0]<-10 or tree.Jet_eta[idx1]<-10 ):
        #    return -1

        v_l1 = tmp_lep1.Vect()
        v_l2 = tmp_lep2.Vect()
        v_b1 = tmp_b1.Vect()


        if idx0 > -1 and idx1 > -1:
            v_b2 = tmp_b2.Vect()

            v_array = [v_l1, v_l2, v_b1, v_b2]

        else:
       
            v_array = [v_l1, v_l2, v_b1]

 
        thrust = float('nan')


        directions = []

        for theta in np.arange(0, np.pi, 0.2):
            for phi in np.arange(0, 2*np.pi, 0.2):
                unit_vec = TVector3(0,0,1)  
                unit_vec.SetMag(1.0)
                unit_vec.SetTheta(theta)
                unit_vec.SetPhi(phi)
                directions.append(unit_vec)

        #directions = np.array(directions) 

        scalar_product = [sum([v.Dot(direction)*np.heaviside(v.Dot(direction), 1.0) for v in v_array]) for direction in directions ]  


        d = max(scalar_product)
        
        pt_tot = sum([v.Mag() for v in v_array])        
        
        if pt_tot > 0:
            d = d/pt_tot
        else:
            return -1

        return d


    def getZHbeamAngle(self, tree, syst, Q):
        lep1, lep2, Vtype = self.getLeptons(tree)
        H = self.getHsyst(tree, syst, Q)
       

        if H.M() < 0:
            return -100

 
        beam = TLorentzVector()

        tmp_lep1, tmp_lep2, tmp_H = TLorentzVector(), TLorentzVector(), TLorentzVector()

        tmp_lep1.SetPtEtaPhiM(lep1.Pt(),lep1.Eta(),lep1.Phi(),lep1.M())
        tmp_lep2.SetPtEtaPhiM(lep2.Pt(),lep2.Eta(),lep2.Phi(),lep2.M())
        tmp_H.SetPtEtaPhiM(H.Pt(),H.Eta(),H.Phi(),H.M())

        if(lep1.Eta()<-10 or lep2.Eta()<-10 or tmp_H.Eta()<-10):
            return -100

        beam.SetPxPyPzE(0,0,6500,6500)

        VH = TLorentzVector()
        VH = tmp_lep1+tmp_lep2+tmp_H

        ZHbeamAngle = float('nan')

        try:
            #Theta  = acos((V_mom.Vect().Unit()).Dot(beam.Vect().Unit()))
            ZHbeamAngle = (VH.Vect().Unit()).Dot(beam.Vect().Unit())
        except Exception:
            #pass
            ZHbeamAngle = -100

        return ZHbeamAngle

