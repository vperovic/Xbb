#!/usr/bin/env python
from __future__ import print_function
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator

from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class JECcorrelator(AddCollectionsModule):

    def __init__(self, year):
        super(JECcorrelator, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False

        self.year = year if type(year) == str else str(year)
    
    def customInit(self, initVars):
        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']

        #self.correlation_scheme = {
        #    "jesAbsolute" : ["jesAbsoluteMPFBias","jesAbsoluteScale","jesFragmentation","jesPileUpDataMC","jesPileUpPtRef","jesRelativeFSR","jesSinglePionECAL","jesSinglePionHCAL"],
        #    "jesAbsolute_"+self.year : ["jesAbsoluteStat","jesRelativeStatFSR","jesTimePtEta"],
        #    "jesBBEC1": ["jesPileUpPtBB","jesPileUpPtEC1","jesRelativePtBB"],
        #    "jesBBEC1_"+self.year: ["jesRelativeJEREC1","jesRelativePtEC1","jesRelativeStatEC"],
        #    "jesEC2": ["jesPileUpPtEC2"],
        #    "jesEC2_"+self.year: ["jesRelativeJEREC2","jesRelativePtEC2"],
        #    "jesFlavorQCD": ["jesFlavorQCD"],
        #    "jesHF": ["jesPileUpPtHF","jesRelativeJERHF","jesRelativePtHF"],
        #    "jesHF_"+self.year: ["jesRelativeStatHF"],
        #    "jesRelativeBal": ["jesRelativeBal"],
        #    "jesRelativeSample_"+self.year: ["jesRelativeSample"],
        #}

        self.correlation_scheme = {
            "jesRelativeBal": ["jesRelativeBal"],
        }



        if self.sample.isMC():

            self.xbbConfig  = XbbConfigTools(self.config)
            #self.JEC_reduced = self.xbbConfig.getJECuncertainties(step='reduced')
            self.JEC_reduced = ["jesRelativeBal"]
            # remove JER uncertainties
            self.JEC_reduced = [x for x in self.JEC_reduced if not x.startswith("jer")]
            #self.JEC_reduced.remove("jesRelativeSample_"+self.year)
            print(self.JEC_reduced)

            self.maxNjet   = 256
            self.maxNfatjet = 50
            self.nEvent = 0
            for jec in self.JEC_reduced:
                for ud in ["Up","Down"]:
                    self.addVectorBranch("Jet_pt_"+jec+ud, default=0.0, branchType='f', length=self.maxNjet, leaflist="Jet_pt_"+jec+ud+"[nJet]/F")
                    self.addVectorBranch("Jet_mass_"+jec+ud, default=0.0, branchType='f', length=self.maxNjet, leaflist="Jet_mass_"+jec+ud+"[nJet]/F")
                    self.addBranch("MET_pt_"+jec+ud, default=0.0)
                    self.addBranch("MET_phi_"+jec+ud, default=0.0)
                    self.addVectorBranch("FatJet_pt_"+jec+ud, default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_pt_"+jec+ud+"[nFatJet]/F")
                    self.addVectorBranch("FatJet_mass_"+jec+ud, default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_mass_"+jec+ud+"[nFatJet]/F")
                    self.addVectorBranch("FatJet_msoftdrop_"+jec+ud, default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_msoftdrop_"+jec+ud+"[nFatJet]/F")

                    
    def correlator(self,jec,tree,iJet,ud,nom,var):

        # RelativeSample uncertainty missing in 2016 datasets
        if jec == "jesRelativeSample_2016":
            return nom

        jec_to_correlate = self.correlation_scheme[jec]
        squared_sum = 0
        for j in jec_to_correlate:
            print("j to loop over is ", j)
            if iJet >= 0:
                squared_sum += (getattr(tree,var+"_"+j+ud)[iJet] - nom)**2
                print("sqaured sum is: ",squared_sum, "getattr(tree,var+_+j+ud)[iJet] ", getattr(tree,var+'_'+j+ud)[iJet], " nom ",nom)
            else:
                squared_sum += (getattr(tree,var+"_"+j+ud) - nom)**2

        if ud == "Up":
            print("nominal in nuple is:  ",nom)
            print("Up in tuple was: ",getattr(tree,"Jet_pt"+"_"+jec+ud)[iJet])
            print("Up computed is ",nom+np.sqrt(squared_sum))
            return nom+np.sqrt(squared_sum)
            
        if ud == "Down":
            print("down in tuple was: ",getattr(tree,"Jet_pt"+"_"+jec+ud)[iJet])
            print("Down computed is ",nom-np.sqrt(squared_sum))
            return nom-np.sqrt(squared_sum)

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC():
            self.markProcessed(tree)
           
            nJet = tree.nJet
            nFatJet = tree.nFatJet

            for jec in self.JEC_reduced:
                print("JEC studied is ", jec)
                
                for ud in ["Up","Down"]:
                    print(" variation is ",ud)
                     
                    Jet_pt_sys_UD = getattr(tree, 'Jet_pt_{s}{d}'.format(s=jec, d=ud))  
                    # update Jet_pt and Jet_mass variations 
                    for i in range(nJet):
                        print("jet is ",i)
                        Jet_pt_nom = tree.Jet_Pt[i]
                        Jet_mass_nom = tree.Jet_mass[i]
                        ntuple_value = getattr(tree,"Jet_pt"+"_"+jec+ud)[i]
                        print("in ProcessEventL ntuple_value : ",ntuple_value,"  for ud in ",ud)
                        self._b("Jet_pt_"+jec+ud)[i] = self.correlator(jec,tree,i,ud,Jet_pt_nom,"Jet_pt")
                        print("in ProcessEvent, computed valye",self._b("Jet_pt_"+jec+ud)[i])
                        histograms[jec][ud].Fill(ntuple_value-self._b("Jet_pt_"+jec+ud)[i])
                        #self._b("Jet_mass_"+jec+ud)[i] = self.correlator(jec,tree,i,ud,Jet_pt_nom,"Jet_mass")
                        print("jet over****************************")
                    #print(histograms[jec][ud].GetEntries())

                    # update MET_pt and MET_phi variations
                    MET_pt_nom = tree.MET_Pt
                    MET_phi_nom = tree.MET_Phi
                    self._b("MET_pt_"+jec+ud)[0] = self.correlator(jec,tree,-1,ud,MET_pt_nom,"MET_pt")
                    self._b("MET_phi_"+jec+ud)[0] = self.correlator(jec,tree,-1,ud,MET_phi_nom,"MET_phi")

                    for i in range(nFatJet):
                        FatJet_pt_nom = tree.FatJet_Pt[i]
                        FatJet_mass_nom = tree.FatJet_mass[i]
                        FatJet_msoftdrop_nom = tree.FatJet_msoftdrop[i]
                        self._b("FatJet_pt_"+jec+ud)[i] = self.correlator(jec,tree,i,ud,FatJet_pt_nom,"FatJet_pt")
                        self._b("FatJet_mass_"+jec+ud)[i] = self.correlator(jec,tree,i,ud,FatJet_mass_nom,"FatJet_mass")
                        self._b("FatJet_msoftdrop_"+jec+ud)[i] = self.correlator(jec,tree,i,ud,FatJet_msoftdrop_nom,"FatJet_msoftdrop")
            self.nEvent += 1
            if self.nEvent % 1000 == 0:
                print(self.nEvent, " events processed!")

if __name__=='__main__':

    config = XbbConfigReader.read('Zvv2018')
    info = ParseInfo(config=config)
    sample = [x for x in info if x.identifier == 'ZH_HToBB_ZToNuNu_M125_13TeV_powheg_pythia8'][0]

    #sampleTree = SampleTree(['/store/group/phys_higgs/hbb/ntuples/VHbbPostNano/2018/V12/ZH_HToBB_ZToNuNu_M125_13TeV_powheg_pythia8/RunIIAutumn18NanoAODv6-Nano25O133/200221_205457/0000/tree_1.root'], treeName='Events', xrootdRedirector="root://eoscms.cern.ch/")
    sampleTree = SampleTree(['/store/group/phys_higgs/hbb/ntuples/VHbbPostNano/2018/V13/ZH_HToBB_ZToNuNu_M125_13TeV_powheg_pythia8/RunIIAutumn18NanoAODv7-Nano02A85/200519_095652/0000/tree_1.root'], treeName='Events', xrootdRedirector="root://eoscms.cern.ch/")
    w = JECcorrelator("2018")
    w.customInit({'sampleTree': sampleTree, 'sample': sample, 'config': config})
    sampleTree.addOutputBranches(w.getBranches())
    histograms={}
    for jec in w.JEC_reduced:                                                                                                                                               histograms[jec] = {}

    for syst in histograms:
        for Q in ['Up','Down']:
            histograms[syst][Q]=ROOT.TH1F('Jet_pt'+syst+Q, 'Jet_pt'+syst+Q, 400, -2.0, 2.0 )
    #sampleTree.process()
    n=0
    for event in sampleTree:
        w.processEvent(event)
        n=n+1
        #if n%1000==0: print('{i} events are processed!'.format(i=n))
        print("----------events over------------")
        if n==3: break

    #l = TList()

    #f = TFile("histlist.root","RECREATE")

    #for syst in histograms:
    #    print("inside")
    #    for Q in ['Up','Down']:
    #       print("insside")
    #       #h1    = histograms[syst][Q]
           #histograms[syst][Q].Write()#histograms[syst][Q])

    #f.Close()
    #f = TFile("histlist.root","RECREATE")
    #l.Write("histlist", TObject.kSingleKey)
    #f.ls()
           #print(histograms[syst][Q])
           #c1    = TCanvas( 'Jet_pt'+syst+Q, 'Jet_pt'+syst+Q, 200, 10, 700, 500 )
           #print(c1)
           #c1.SetLogy()
           #histograms[syst][Q].Draw()
           #print("done")
           #c1.Modified()
           #c1.Update()
           #print("here")
           #c1.SaveAs('jecv12v13/Jet_pt_'+syst+Q+'.png')
           #print('done: ','Jet_pt'+syst+Q)

        
        #if (n%1000==0): print('{i} events processed'.format(i=n))
 

           # h1    = TH1F( 'Jet_pt'+syst+Q, 'Jet_pt'+syst+Q, 400, -2.0, 2.0 )
           # for i in w.diff[syst][Q]:
           #      
           #     #print(i)
           #     h1.Fill(i)
           # #c1.SetLogy()
           # #c1.GetFrame().SetBorderSize( 6 )
           # #c1.GetFrame().SetBorderMode( -1 )

    #for syst in histograms:
    #    print("inside")
    #    for Q in ['Up','Down']:
    #        c1    = TCanvas( 'Jet_pt'+syst+Q, 'Jet_pt'+syst+Q, 200, 10, 700, 500 )
    #        c1.SetLogy() 
    #        c1.GetFrame().SetBorderSize( 6 )
    #        c1.GetFrame().SetBorderMode( -1 )
    #        print(histograms[syst][Q].GetEntries())
    #        histograms[syst][Q].Draw()
    #        c1.Modified()
    #        c1.Update()
    #           # #print('making: ','Jet_pt'+syst+Q)
    #        c1.SaveAs('jecv12v13/Jet_pt'+sy0st+Q+'.png')#h1.Divide(h2)
           # print('done: ','Jet_pt'+syst+Q_ 
