#!/usr/bin/env python
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import numpy as np

# apply x/y/phi corrections to data
class metphi(object):
    def __init__(self,MET_Pt,MET_Phi,run,year,True,PV_npvs):

        self.MET_Pt = MET_Pt
        self.MET_Phi = MET_Phi
        self.run = run
        self.PV_npvs = PV_npvs
        self.year = 2018

        self.corr = {
         2018: {
            "2018MC": {"METxcorr":"-(0.296713*self.PV_npvs -0.141506)", "METycorr":"-(0.115685*self.PV_npvs +0.0128193)"},
            "2018A": {"METxcorr":"-(0.362865*self.PV_npvs -1.94505)", "METycorr":"-(0.0709085*self.PV_npvs -0.307365)"},
            "2018B": {"METxcorr":"-(0.492083*self.PV_npvs -2.93552)", "METycorr":"-(0.17874*self.PV_npvs -0.786844)"},
            "2018C": {"METxcorr":"-(0.521349*self.PV_npvs -1.44544)", "METycorr":"-(0.118956*self.PV_npvs -1.96434)"},
            "2018D": {"METxcorr":"-(0.531151*self.PV_npvs -1.37568)", "METycorr":"-(0.0884639*self.PV_npvs -1.57089)"}
            }
        }

    def processEvent(self):

        runnb = self.run 
        runera = self.get_era(runnb)
        npv = self.PV_npvs
        if(npv>100): npv=100;

        MET_p4 = ROOT.TLorentzVector()
        MET_p4.SetPtEtaPhiM(self.MET_Pt, 0.0, self.MET_Phi, 0.0)
        MET = MET_p4.E() 
        MET_x = MET_p4.X()
        MET_y = MET_p4.Y()
        MET_phi = MET_p4.Phi()

        if (runera != -1):

            METxcorr = eval(self.corr[self.year][runera]["METxcorr"])
            METycorr = eval(self.corr[self.year][runera]["METycorr"])

            METcorr_x = MET * np.cos(MET_phi) + METxcorr 
            METcorr_y = MET * np.sin(MET_phi) + METycorr
            METcorr_pt = np.sqrt(METcorr_x * METcorr_x + METcorr_y * METcorr_y)
            #print("METcorr_x ",METcorr_x)
            #print("METcorr_y ",METcorr_y)
            #print("METcorr_pt ",METcorr_pt)

            if (METcorr_x == 0) and (METcorr_y > 0): METcorr_phi = np.pi 
            elif (METcorr_x == 0) and (METcorr_y < 0): METcorr_phi = -np.pi 
            elif (METcorr_x > 0): METcorr_phi = np.arctan(METcorr_y/METcorr_x) 
            elif (METcorr_x < 0) and (METcorr_y > 0): METcorr_phi = np.arctan(METcorr_y/METcorr_x)+ np.pi 
            elif (METcorr_x < 0) and (METcorr_y < 0): METcorr_phi = np.arctan(METcorr_y/METcorr_x)- np.pi 
            else: METcorr_phi = 0 

            return(METcorr_phi,METcorr_pt)


    def get_era(self, runnb):

        runera =-1

        if (self.year == 2018):
            if (runnb >=315252 and runnb<=316995): runera = "2018A"
            if (runnb >=316998 and runnb<=319312): runera = "2018B"
            if (runnb >=319313 and runnb<=320393): runera = "2018C"
            if (runnb >=320394 and runnb<=325273): runera = "2018D"
                            
        return runera
