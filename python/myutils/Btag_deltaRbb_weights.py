#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class BTagAndDeltaRbbWeights(AddCollectionsModule):

    def __init__(self, year):
        super(BTagAndDeltaRbbWeights, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(BTagAndDeltaRbbWeights, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(BTagAndDeltaRbbWeights, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(BTagAndDeltaRbbWeights, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.btagWPloose = float(self.config.get('General','btagWP_Loose'))
        self.btagWPmedium = float(self.config.get('General','btagWP_Medium'))
        print(">>>>>>>>>>>> year", self.year)
        print("Loose BTAG ", self.btagWPloose)
        print("Medium BTAG ", self.btagWPmedium)




        if self.year == "2018":


            #self.BTagWeights_Vpt = { 
            #        "isZee":
            #            {
            #                '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.7841845750808716, 0.8271123766899109, 0.8868122100830078], [0.0, 0.9180288910865784, 0.9771317839622498], [0.0, 0.0, 1.0656260251998901]],'uncertainty':[[0.009547978456703323, 0.00835603559055333, 0.01254564666323473], [0.0, 0.015309999317676778, 0.016248294554851734], [0.0, 0.0, 0.032742352862242675]]                        },
            #                '150250':{'bins':[-0.01, 0.01, 0.05], 'central':[[0.7904078960418701, 0.8611980080604553], [0.0, 1.0102940797805786]],'uncertainty':[[0.021001359838741546, 0.01575126575150901], [0.0, 0.022058785165954005]]
            #                },
            #                '250Inf':{'bins':[-0.01, 0.05],'central':[[0.9534371495246887]],'uncertainty':[[0.030099543838993655]]
            #                },
            #            },
            #        "isZmm":
            #            {
            #                '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.7723640203475952, 0.8156688213348389, 0.8652512431144714], [0.0, 0.9080958366394043, 0.9734914898872375], [0.0, 0.0, 1.0095767974853516]],'uncertainty':[[0.008271486945230277, 0.007185019334201254, 0.01069741053722021], [0.0, 0.013081922790161158, 0.0138218949060418], [0.0, 0.0, 0.026230896537749657]]                        
            #                },
            #                '150250':{'bins':[-0.01, 0.01, 0.05], 'central':[[0.7419997453689575, 0.8116430044174194, 0.8769641518592834], [0.0, 0.9639992117881775, 0.9859392046928406], [0.0, 0.0, 1.1322089433670044]],'uncertainty':[[0.01779827211165947, 0.016295645538333826, 0.024585956923045722], [0.0, 0.02854219810840987, 0.02855009975845111], [0.0, 0.0, 0.05647763803376349]]
            #                },
            #                '250Inf':{'bins':[-0.01, 0.05],'central':[[0.7985278367996216]],'uncertainty':[[0.02436832817119183]]
            #                },
            #            },
            #        }


            self.BTagWeights_Vpt = { 
                    "isZee":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.783863365650177, 0.8261989951133728, 0.8863885402679443], [0.0, 0.9183457493782043, 0.9754696488380432], [0.0, 0.0, 1.0640263557434082]],'uncertainty':[[0.009527078875015638, 0.008335039008378342, 0.012513208813816], [0.0, 0.015254698333727942, 0.01614602488772682], [0.0, 0.0, 0.03250197586258306]]                        },
                            '150250':{'bins':[-0.01, 0.01, 0.05], 'central':[[0.813897967338562, 0.869371235370636], [0.0, 1.0210609436035156]],'uncertainty':[[0.020366554087406362, 0.015217046233661342], [0.0, 0.021158436997795047]]
                            },
                            '250Inf':{'bins':[-0.01, 0.016666, 0.05],'central':[[0.9422100782394409, 1.0096567869186401], [0.0, 0.946218729019165]],'uncertainty':[[0.03294527262397498, 0.035971881582626215], [0.0, 0.05787721259087242]]
                            },
                        },

                    "isZmm":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.7731058597564697, 0.8155791759490967, 0.8662646412849426], [0.0, 0.9091378450393677, 0.972213625907898], [0.0, 0.0, 1.0104209184646606]],'uncertainty':[[0.008261689044306561, 0.00716882667749624, 0.010688607327727211], [0.0, 0.013038662266865432, 0.013745442465082885], [0.0, 0.0, 0.02607398637018547]]                        },
                            '150250':{'bins':[-0.01, 0.01, 0.05], 'central':[[0.762007474899292, 0.8558753728866577], [0.0, 1.0075902938842773]],'uncertainty':[[0.017197517061262275, 0.013295048114380424], [0.0, 0.018265584420434754]]
                            },
                            '250Inf':{'bins':[-0.01, 0.016666, 0.05],'central':[[0.8233022689819336, 0.8736903667449951], [0.0, 1.060705542564392]],'uncertainty':[[0.027470247486494577, 0.029489850941342613], [0.0, 0.053385525545422384]]
                            },
                        },
                        }


            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isDY":
                        {
                        "isZee":
                            {
                                '75Inf':{'bins': [0.4,0.6,0.8,1.0],'central':[1.3525, 1.3845, 1.3709],'uncertainty':[0.0978, 0.0757, 0.0738]
                                },
                            },
                          "isZmm":
                            {
                                '75Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central': [1.2998, 1.2827, 1.326,],'uncertainty':[0.0863, 0.0648, 0.0631]
                                },
                            },
                        },
                    }



        else:
            raise Exception("Wrong year")


        self.get_BTagWeights_Vptdict = {'75':'75150', '150':'150250','250':'250Inf'}
        self.BTagWeights_Vpt_branches = ["DY_btagDeepFlavB_2D"]
        self.get_dRbbdict = {'75':'75150', '150':'150250','250':'250Inf'}
        self.dRbb_branches = ["dRbbWeight_Incl"]

        if self.sample.isMC():
            self.maxNjet   = 256

            for var in ["Jet_btagDeepFlavB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["nGenBpt25eta2p6","nGenDpt25eta2p6"]:
                self.existingBranches[var] = array.array('i', [100])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
        
            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isZmm","isZee","isWenu","isWmunu","isZnn"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
        
            
            #Btag 2D
            for var in self.BTagWeights_Vpt_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
            
            #DeltaRbb
            for var in self.dRbb_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)


 
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies_btag2D(self, attr):
        isDYbtag_udsg = False
        #print(">>>>>>>>>> before")
        #print(attr["hJidx"][0])
        #print(attr["hJidx"][1])
        #print("leading btag",attr["Jet_btagDeepFlavB"][attr["hJidx"][0]])
        #print("truth?",  attr["Jet_btagDeepFlavB"][attr["hJidx"][0]] < self.btagWPloose)
        #print("subleading btag",attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])
        #print(attr["nGenBpt25eta2p6"][0])
        #print(attr["nGenDpt25eta2p6"][0])
        #print(self.btagWPloose)
        if (any([x in self.sample.identifier for x in ['DYJets']]) and any([s in self.sample.identifier for s in ['amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if ((attr["Jet_btagDeepFlavB"][attr["hJidx"][0]] < self.btagWPloose and attr["Jet_btagDeepFlavB"][attr["hJidx"][1]] < self.btagWPloose) and (attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1)):
                isDYbtag_udsg = True
                #print(">>>>>>>>>> in")
                #print(attr["hJidx"][0])
                #print(attr["hJidx"][1])
                #print(attr["Jet_btagDeepFlavB"][attr["hJidx"][0]])
                #print(attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])
                #print(attr["nGenBpt25eta2p6"][0])
                #print(attr["nGenDpt25eta2p6"][0])
                #print(">>>>>>>>>> after")
 
        #print("final: ",isDYbtag_udsg)
        #print("----------------------------------------------------------------")
        return isDYbtag_udsg

    def applies_deltaRbb(self, attr):
        isNLO_dRbb_lt1 = False

 
        if (any([s in self.sample.identifier for s in ['amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if (self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0):
                if (attr["channel"] == "isZee" or attr["channel"] == "isZmm"):
                    if (any([x in self.sample.identifier for x in ['DYJets']])):
                        #Additional condition: needs to be in Zhf
                        if ((attr["Jet_btagDeepFlavB"][attr["hJidx"][0]] > self.btagWPmedium and attr["Jet_btagDeepFlavB"][attr["hJidx"][1]] > self.btagWPloose)):
                            isNLO_dRbb_lt1 = True
                            #print("dRbb:")
                            #print(self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]))

                elif (attr["channel"] == "isWenu" or attr["channel"] == "isWmunu"):
                    if (any([x in self.sample.identifier for x in ['DYJets','WJetsToLNu']])):
                        raise Exception("Did you check the btag cuts ? ")
                        isNLO_dRbb_lt1 = True
                elif (attr["channel"] == "isZnn"):
                    if (any([x in self.sample.identifier for x in ['WJetsToLNu','ZJetsToNuNu']])):
                        isNLO_dRbb_lt1 = True
                        raise Exception("Did you check the btag cuts ? ")
                else:
                    isNLO_dRbb_lt1 = False
                        
        return isNLO_dRbb_lt1





    def get_lowerBound(self,value,bins):
        j = 0
        #print("value is",value)
        #print("bins are ",bins)
        while not (value<bins[j] or value>=250):
            #print("value is ", value, "and bins are ",bins[j])
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])


    def get_eventWeight_deltaRbb(self, attr):
        
        #dRbbWeight_Vpt  = []
        
        dRbbWeight_Incl = []
 
        dRbb_event     = self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0])
        dRbbWeight_bin_incl = '75Inf'
        sample = attr["NLOsample"]
        channel = attr["channel"]
        
        ### Vpt split
        #dRbbWeight_bin_Vpt= self.get_dRbbdict[self.get_lowerBound(attr["V_pt"][0],[75,150,250])]
        #j=0
        #VptBins = [150,250]
        #if any(channel==x for x in ["isZee","isZmm"]): VptBins=[75,150,250]
        #dRbbWeight_bin_Vpt = self.get_dRbbdict[self.get_lowerBound(attr["V_pt"][0],VptBins)]
        #while not (dRbb_event < self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["bins"][j]):
        #    j+=1
        #dRbbWeight_Vpt = [self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1] + self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["uncertainty"][j-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1] - self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["uncertainty"][j-1]]
        

        ### Inclusive
        k=0
        while not (dRbb_event < self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["bins"][k]):
            k+=1
        #print("channel ",channel)
        #print("Vpt ",attr["V_pt"][0])
        dRbbWeight_Incl = [self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1] + self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["uncertainty"][k-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1] - self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["uncertainty"][k-1]]
        
        #return  dRbbWeight_Vpt
        return  dRbbWeight_Incl







    def get_eventWeight_btag2D(self, attr):
        dRbbWeight_Vpt  = []
        dRbbWeight_Incl = []
 
        btag_hJidx0        = attr["Jet_btagDeepFlavB"][attr["hJidx"][0]]
        btag_hJidx1        = attr["Jet_btagDeepFlavB"][attr["hJidx"][1]]
        BTagWeights_Vptbin = self.get_BTagWeights_Vptdict[self.get_lowerBound(attr["V_pt"][0],[75,150,250])]
 
        j=0
        channel = None
        if attr["isZee"][0] == 1: channel = "isZee"
        if attr["isZmm"][0] == 1: channel = "isZmm"

        if (btag_hJidx0 < 0.0 or btag_hJidx1 < 0.0):
            return 1.0,1.0,1.0

        while not (btag_hJidx0 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][j]):
            #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][j])
            #print(btag_hJidx0)
            j+=1
            #print("j is now amining for ",j)
        k=0
        while not (btag_hJidx1 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][k]):
            k+=1
        #print(j-1)
        #print(k-1)
        #print("channel ",channel," BTagWeights_Vptbin ",BTagWeights_Vptbin," j-1 ",j-1," k-1 ",k-1)
        #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1])
        nom = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1]
        up = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] + self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        down = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] - self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        #print(nom,up,down)        
        return  nom,up,down 



    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)

            attr = {}
            for var in ["V_pt","hJidx","isZee","isZmm","Jet_btagDeepFlavB","nGenBpt25eta2p6","nGenDpt25eta2p6","hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","isWenu","isWmunu","isZnn"]: 
                attr[var] = self.existingBranches[var]
            if self.applies_btag2D(attr):
                nom,up,down = self.get_eventWeight_btag2D(attr)
                self._b("DY_btagDeepFlavB_2D")[0]         = nom
                self._b("DY_btagDeepFlavB_2D"+"Up")[0]    = up
                self._b("DY_btagDeepFlavB_2D"+"Down")[0]  = down
            else:
                self._b("DY_btagDeepFlavB_2D")[0]         = 1.0
                self._b("DY_btagDeepFlavB_2D"+"Up")[0]    = 1.0
                self._b("DY_btagDeepFlavB_2D"+"Down")[0]  = 1.0

            if any([x in self.sample.identifier for x in ['DYJets']]):
                attr["NLOsample"] = "isDY"
            elif any([x in self.sample.identifier for x in ['WJetsToLNu']]):
                attr["NLOsample"] = "isWJets" 
            elif any([x in self.sample.identifier for x in ['ZJetsToNuNu']]):
                attr["NLOsample"] = "isZJets" 
            attr["channel"] = None   
            

            if attr["isZee"][0] == 1: attr["channel"] = "isZee"
            if attr["isZmm"][0] == 1: attr["channel"] = "isZmm"
            if attr["isWenu"][0] == 1: attr["channel"] = "isWenu"
            if attr["isWmunu"][0] == 1: attr["channel"] = "isWmunu"
            if attr["isZnn"][0] == 1: attr["channel"] = "isZnn"

            if self.applies_deltaRbb(attr):
                
                dRbbWeight_Incl = self.get_eventWeight_deltaRbb(attr)
                #print("Checking pass boundary conditions")
                #print("Leading", attr["Jet_btagDeepFlavB"][attr["hJidx"][0]])                   
                #print("Subleading", attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])                   
                #print(dRbbWeight_Incl)
                self._b("dRbbWeight_Incl")[0]        = dRbbWeight_Incl[0]
                self._b("dRbbWeight_Incl"+"Up")[0]   = dRbbWeight_Incl[1]
                self._b("dRbbWeight_Incl"+"Down")[0] = dRbbWeight_Incl[2]
            else:

                self._b("dRbbWeight_Incl")[0]        = 1.0
                self._b("dRbbWeight_Incl"+"Up")[0]   = 1.0
                self._b("dRbbWeight_Incl"+"Down")[0] = 1.0




if __name__=='__main__':

    print("This module creates branches with deltaRbb and btag 2D weights")








