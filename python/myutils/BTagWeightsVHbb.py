#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class BTagWeightsVHbb(AddCollectionsModule):

    def __init__(self, year):
        super(BTagWeightsVHbb, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(BTagWeightsVHbb, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(BTagWeightsVHbb, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(BTagWeightsVHbb, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']

        #BtagWeight_Jet_Pt_central = {2030:[0.6523, 0.7402, 0.8102, 0.8977, 0.9473, 1.0825, 0.961], 3040: [0.708, 0.7931, 0.8222, 0.8603, 0.9291, 0.9733, 1.0259], 4060: [0.737, 0.7444, 0.795, 0.8512, 0.8631, 0.8914, 1.0224], 60100:[0.7752, 0.7392, 0.7595, 0.8399, 0.9011, 0.8756, 0.9641], 100200:[0.8272, 0.7427, 0.8233, 0.8226, 0.9065, 0.8295, 0.8974], 200Inf:[0.89, 0.8043, 0.9056, 0.9924, 0.79]}
        #BtagWeight_Jet_Pt_unc = {2030:[0.0082, 0.0248, 0.0147, 0.0217, 0.0307, 0.0477, 0.0426], 3040:[0.0175, 0.0269, 0.0182, 0.0268, 0.0425, 0.0641, 0.0778], 4060:[0.0182, 0.019, 0.0142, 0.0232, 0.0342, 0.0485, 0.0718], 60100:[0.0167, 0.0142, 0.0108, 0.0183, 0.0297, 0.0395, 0.0568],100200:[0.0178, 0.0154, 0.0125, 0.0171, 0.03, 0.0399, 0.0571], 200Inf:[0.0481, 0.0331, 0.034, 0.0711, 0.0934]}
        #self.btagWeight_JetPtbins = {
        #                '2030':{'bins':[-15.0, 0.0, 0.02068333, 0.04136667, 0.06205, 0.08273333, 0.10341667, 0.1241] ,'central':[0.6523, 0.7402, 0.8102, 0.8977, 0.9473, 1.0825, 0.961],'uncertainty':[0.0082, 0.0248, 0.0147, 0.0217, 0.0307, 0.0477, 0.0426]},
        #                '3040':{'bins':[-15.0, 0.0, 0.02068333, 0.04136667, 0.06205, 0.08273333, 0.10341667, 0.1241] ,'central':[0.708, 0.7931, 0.8222, 0.8603, 0.9291, 0.9733, 1.0259],'uncertainty':[0.0175, 0.0269, 0.0182, 0.0268, 0.0425, 0.0641, 0.0778],
        #                },
        #                '4060':{'bins':[-15.0, 0.0, 0.02068333, 0.04136667, 0.06205, 0.08273333, 0.10341667, 0.1241],'central':[0.737, 0.7444, 0.795, 0.8512, 0.8631, 0.8914, 1.0224],'uncertainty':[0.0182, 0.019, 0.0142, 0.0232, 0.0342, 0.0485, 0.0718]
        #                },
        #                '60100':{'bins':[-15.0, 0.0, 0.02068333, 0.04136667, 0.06205, 0.08273333, 0.10341667, 0.1241],'central':[0.7752, 0.7392, 0.7595, 0.8399, 0.9011, 0.8756, 0.9641],'uncertainty':[0.0167, 0.0142, 0.0108, 0.0183, 0.0297, 0.0395, 0.0568]
        #                },
        #                '100200':{'bins':[-15.0, 0.0, 0.02068333, 0.04136667, 0.06205, 0.08273333, 0.10341667, 0.1241],'central':[0.8272, 0.7427, 0.8233, 0.8226, 0.9065, 0.8295, 0.8974],'uncertainty':[0.0178, 0.0154, 0.0125, 0.0171, 0.03, 0.0399, 0.0571]
        #                },
        #                '200Inf':{'bins':[-15.0, 0.0, 0.031025, 0.06205 , 0.093075, 0.1241] ,'central':[0.89, 0.8043, 0.9056, 0.9924, 0.79],'uncertainty':[0.0481, 0.0331, 0.034, 0.0711, 0.0934]
        #                }                            
        #    }
        self.btagWeight_JetPtbins = {
                        '2030':{'bins':[-15.0, 0.0, 0.031025, 0.06205 , 0.093075, 0.1241] ,'central':[1.5, 0.7854, 0.8771, 1.0436, 1.1233],'uncertainty':[0.133, 0.0164, 0.0183, 0.0384, 0.0596]
                        },
                        '3040':{'bins':[-15.0, 0.0, 0.031025, 0.06205 , 0.093075, 0.1241] ,'central':[1.5, 0.8331, 0.9173, 0.9826, 1.135],'uncertainty':[0.133, 0.0213, 0.0299, 0.0574, 0.1044]
                        },
                        '4060':{'bins':[-15.0, 0.0, 0.031025, 0.06205 , 0.093075, 0.1241],'central':[1.5, 0.7852, 0.882, 0.9022, 0.8777],'uncertainty':[0.133, 0.0162, 0.0258, 0.0487, 0.0666]
                        },
                        '60100':{'bins':[-15.0, 0.0, 0.031025, 0.06205 , 0.093075, 0.1241],'central':[1.5, 0.7584, 0.8216, 0.8787, 0.855],'uncertainty':[0.133, 0.0124, 0.02, 0.0382, 0.0515]
                        },
                        '100200':{'bins':[-15.0, 0.0, 0.031025, 0.06205 , 0.093075, 0.1241],'central':[1.5, 0.7631, 0.8066, 0.8132, 0.7868],'uncertainty':[0.133, 0.0134, 0.0209, 0.0355, 0.0499]
                        },
                        '200Inf':{'bins':[-15.0, 0.0, 0.031025, 0.06205 , 0.093075, 0.1241] ,'central':[1.5, 0.7845, 0.8312, 0.9668, 0.7558],'uncertainty':[0.133, 0.0361, 0.0494, 0.0942, 0.1071]
                        }                            
            }
        
        self.get_btagbin = {'20':'2030', '30':'3040','40':'4060','60':'60100', '100': '100200', '200': '200Inf'}
        self.btag_VHbb = ["Jet_btagDeepBVHbb"]

        if self.sample.isMC():
            self.maxNjet   = 256

            for var in ["Jet_Pt","Jet_btagDeepB","Jet_eta"]:
                self.existingBranches[var] = array.array('f', [0.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["nJet","isZee","nGenBpt25eta2p6","nGenDpt25eta2p6"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx","Jet_lepFilter","Jet_puId","Jet_jetId"]:
                self.existingBranches[var] = array.array('i', [0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in ["Jet_btagDeepBVHbb"]:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                    
    def applies(self, attr):
        isDYnlo_udsg = False
        #if (self.sample.group=="ZJets_0b_udsg"):
        #    pass
            #print("self.sample.identifier ",self.sample.identifier)
            #print("self.sample.group ",self.sample.group)  (nGenBpt25eta2p6<1&&nGenDpt25eta2p6<1)
            #print("tree.Jet_btagDeepB",self.existingBranches["Jet_btagDeepB"])
            #print("tree.Jet_btagDeepB",self.existingBranches["hJidx"])
            #print(self.existingBranches["nGenBpt25eta2p6"])
            #print(self.existingBranches["nGenDpt25eta2p6"])
        #print(attr["Jet_btagDeepB"][attr["hJidx"][1]])
        #print(attr["Jet_btagDeepB"][attr["hJidx"][0]])
        #print("nGenBpt25eta2p6 ",attr["nGenBpt25eta2p6"][0])
        #print("nGenDpt25eta2p6 ",attr["nGenDpt25eta2p6"][0])
        #print(attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1)
        #print(any([x in self.sample.identifier for x in ['DYJets','DY1Jets','DY2Jets']]))
        #print(self.sample.identifier)
        #print(any([s in self.sample.identifier for s in ['amcnloFXFX','amcatnloFXFX']]))
        #print(attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)
        #print(attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1)        
 
        if (any([x in self.sample.identifier for x in ['DYJets','DY1Jets','DY2Jets']]) and any([s in self.sample.identifier for s in ['amcnloFXFX','amcatnloFXFX']]) and  (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1) and (attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1) and attr["isZee"][0]):   
            #print("isDYnlo_udsg interm",isDYnlo_udsg)
            if ((attr["Jet_btagDeepB"][attr["hJidx"][0]] < 0.1241 and attr["Jet_btagDeepB"][attr["hJidx"][1]] < 0.1241) and (attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1)):
                isDYnlo_udsg = True
        #print("final",isDYnlo_udsg)
        return isDYnlo_udsg

    def get_upperBound(self,value,bins):
        j = 0
        #print("value is",value)
        #print("bins are ",bins)
        while not (value<bins[j] or value>=200):
            #print("value is ", value, "and bins are ",bins[j])
            j+=1
        if (value>=200):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        central = []
        up = []
        down = []
        #print("Jet_jetId  ",attr["Jet_jetId"])
        for i in range(attr["nJet"][0]):
            #print("start ",i)
            #print(attr["Jet_Pt"][i]>=20.0)
            #print(attr["Jet_btagDeepB"][i]<0.1241)
            #print(abs(attr["Jet_eta"][i]) < 2.5)
            #print(attr["Jet_lepFilter"][i] > 0)
            #print(attr["Jet_puId"][i] > 6.0 or attr["Jet_Pt"][i]>50)
            #print(attr["Jet_jetId"][i])
            #print("end ",i)
            if (attr["Jet_Pt"][i]>=20.0 and attr["Jet_btagDeepB"][i]<0.1241 and abs(attr["Jet_eta"][i]) < 2.5 and attr["Jet_lepFilter"][i] > 0 and (attr["Jet_puId"][i] > 6.0 or attr["Jet_Pt"][i]>50) and attr["Jet_jetId"][i] > 4):   
                #print(attr["Jet_Pt"][i])
                #print(self.get_upperBound(attr["Jet_Pt"][i],[20,30,40,60,100,200]))
                btagWeight_bin = self.get_btagbin[self.get_upperBound(attr["Jet_Pt"][i],[20,30,40,60,100,200])]
                #print("btagWeight_bin ", btagWeight_bin)
                j=0
                #print("deepcs for ",i," ",attr["Jet_btagDeepB"][i])
                #print(self.btagWeight_JetPtbins[btagWeight_bin]["bins"])
                while not (attr["Jet_btagDeepB"][i]<self.btagWeight_JetPtbins[btagWeight_bin]["bins"][j]):
                    #print("deepcs for ",i," ",attr["Jet_btagDeepB"][i])
                    #print("central for ",i, " ",self.btagWeight_JetPtbins[btagWeight_bin]["bins"][j])
                    j+=1
                central.append(self.btagWeight_JetPtbins[btagWeight_bin]["central"][j-1])
                up.append(self.btagWeight_JetPtbins[btagWeight_bin]["central"][j-1]+self.btagWeight_JetPtbins[btagWeight_bin]["uncertainty"][j-1])
                down.append(self.btagWeight_JetPtbins[btagWeight_bin]["central"][j-1]-self.btagWeight_JetPtbins[btagWeight_bin]["uncertainty"][j-1])
            else:
                central.append(1.0)
                up.append(1.0)
                down.append(1.0)
            
        central_event = 1.0
        up_event = 1.0
        down_event = 1.0
        for i in range(len(central)):
            central_event *= central[i]
            up_event *= up[i]
            down_event *= down[i]
        #print(central_event, up_event, down_event)   
        #print("central is ",central)
        #print("jet pt", attr["Jet_Pt"])
        #print("deppcsv ",attr["Jet_btagDeepB"])
        return  central_event, up_event, down_event

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)
            attr = {}
            for var in ["Jet_Pt","Jet_btagDeepB","hJidx","nGenBpt25eta2p6","nGenDpt25eta2p6","nJet","isZee","Jet_eta","Jet_lepFilter","Jet_puId","Jet_jetId"]: 
                attr[var] = self.existingBranches[var]
            if self.applies(attr):
                for var in ["Jet_btagDeepBVHbb"]:
                    btag_central,btag_up,btag_down = self.get_eventWeight(attr)
                    self._b(var)[0] = btag_central
                    self._b(var+"Up")[0]   = btag_up
                    self._b(var+"Down")[0] = btag_down
            else:   
                for var in ["Jet_btagDeepBVHbb"]:
                    self._b(var)[0] = 1.0
                    self._b(var+"Up")[0]   = 1.0
                    self._b(var+"Down")[0] = 1.0
                    


if __name__=='__main__':

    config = XbbConfigReader.read('Zll2018')
    info = ParseInfo(config=config)
    sample = [x for x in info if x.identifier == 'DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8'][0]
    print(sample)

    sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_94b42e05db46cb9fd44285b60e0ba8b57d34232afdeef2661e673e5f_000000_000000_0000_0_5c5fb21d0e20a7f021911ce80109ccd1fa39014fff8bca8fc463de97.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8/tree_0b9e5ec9f1d4328fc7ef364ebb5dd7364200ce37948e3753c76e6133_000000_000000_0000_5_fadbb45f88e40b3f3f44b0585861b2c4f87d08c5be7feda31913e251.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    w = BTagWeightsVHbb("2018")
    w.customInit({'sampleTree': sampleTree, 'sample': sample, 'config': config})
    sampleTree.addOutputBranches(w.getBranches())
    #histograms={}
    #for jec in w.JEC_reduced:
        
    #for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
    #    histograms[var] = {}
    #    for syst in w.JEC_reduced:
    #        histograms[var][syst] = {}
    #        for Q in ['Up','Down']:
    #            histograms[var][syst][Q]=ROOT.TH1F(var+syst+Q, var+syst+Q, 400, -2.0, 2.0 )

    n=0 
    #var = "MET_phi"
    for event in sampleTree:
        n=n+1
        w.processEvent(event)
        if n==5: break

   # with open('jec_validate_'+var+'.csv', 'w') as file:
   #     writer = csv.writer(file)
   #     writer.writerow(["event","run","Q",'true_jesAbsolute','jesAbsolute','AT','diff','true_jesAbsolute_2018','jesAbsolute_2018','AT','diff','true_jesBBEC1','jesBBEC1','AT','diff','true_jesBBEC1_2018','jesBBEC1_2018','AT','diff','true_jesEC2','jesEC2','AT','diff','true_jesEC2_2018','jesEC2_2018','AT','diff','true_jesHF','jesHF','AT','diff','true_jesHF_2018','jesHF_2018','AT','diff','true_jesRelativeSample_2018','jesRelativeSample_2018','AT','diff'])
   #     for event in sampleTree:
   #         n=n+1
   #         event,run,true_attr, attr = w.processEvent(event)
   #         for Q in ['Up','Down']:
   #             njet = len(true_attr[var][w.JEC_reduced[0]][Q])
   #             if njet>1:
   #                 for i in range(njet):
   #                     writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][i],attr[var]['jesAbsolute'][Q][i],'','',true_attr[var]['jesAbsolute_2018'][Q][i],attr[var]['jesAbsolute_2018'][Q][i],'','',true_attr[var]['jesBBEC1'][Q][i],attr[var]['jesBBEC1'][Q][i],'','',true_attr[var]['jesBBEC1_2018'][Q][i],attr[var]['jesBBEC1_2018'][Q][i],'','',true_attr[var]['jesEC2'][Q][i],attr[var]['jesEC2'][Q][i],'','',true_attr[var]['jesEC2_2018'][Q][i],attr[var]['jesEC2_2018'][Q][i],'','',true_attr[var]['jesHF'][Q][i],attr[var]['jesHF'][Q][i],'','',true_attr[var]['jesHF_2018'][Q][i],attr[var]['jesHF_2018'][Q][i],'','',true_attr[var]['jesRelativeSample_2018'][Q][i],attr[var]['jesRelativeSample_2018'][Q][i],'',''])
   #             else:
   #                 writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][0],attr[var]['jesAbsolute'][Q][0],'','',true_attr[var]['jesAbsolute_2018'][Q][0],attr[var]['jesAbsolute_2018'][Q][0],'','',true_attr[var]['jesBBEC1'][Q][0],attr[var]['jesBBEC1'][Q][0],'','',true_attr[var]['jesBBEC1_2018'][Q][0],attr[var]['jesBBEC1_2018'][Q][0],'','',true_attr[var]['jesEC2'][Q][0],attr[var]['jesEC2'][Q][0],'','',true_attr[var]['jesEC2_2018'][Q][0],attr[var]['jesEC2_2018'][Q][0],'','',true_attr[var]['jesHF'][Q][0],attr[var]['jesHF'][Q][0],'','',true_attr[var]['jesHF_2018'][Q][0],attr[var]['jesHF_2018'][Q][0],'','',true_attr[var]['jesRelativeSample_2018'][Q][0],attr[var]['jesRelativeSample_2018'][Q][0],'',''])

   #         #print("----------events over------------")
   #         if n==5: break

   # f = TFile("delete.root","RECREATE")

   # for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
   #     for syst in w.JEC_reduced:
   #         for Q in ['Up','Down']:
   #             histograms[var][syst][Q].Write()
   # f.Close()
