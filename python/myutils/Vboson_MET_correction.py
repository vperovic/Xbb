#!/usr/bin/env python
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
from math import pi, sqrt, cos, sin, sinh, log, cosh, acos, acosh
import array
import os
import numpy as np
from XbbConfig import XbbConfigTools
import random

# do jet/lepton selection and skimming
class Vboson_MET_correction(AddCollectionsModule):

    def __init__(self, debug=False):
        self.debug = debug or 'XBBDEBUG' in os.environ
        super(Vboson_MET_correction, self).__init__()
        self.version = 2

    def customInit(self, initVars):
        self.sampleTree = initVars['sampleTree']
        self.isData     = initVars['sample'].isData()
        self.sample     = initVars['sample']
        self.config     = initVars['config']
        self.xbbConfig  = XbbConfigTools(self.config)

        self.systematics = self.xbbConfig.getJECuncertainties(step='VReco') + ['unclustEn']

        self.allVariations = ['Nominal']
       
        # jet and MET systematics for MC
        if not self.isData:
            self.allVariations += self.systematics 


        self.V_pt     = array.array('f', [0.0])
        self.V_phi    = array.array('f', [0.0])
        self.V_eta     = array.array('f', [0.0])
        self.V_mass    = array.array('f', [0.0])
        self.V_mt    = array.array('f', [0.0])

        self.V_pt_syst  = {}
        self.V_phi_syst  = {}
        self.V_eta_syst  = {}
        self.V_mass_syst  = {}
        self.V_mt_syst  = {}
 
        self.sampleTree.tree.SetBranchAddress("V_pt", self.V_pt)
        self.sampleTree.tree.SetBranchAddress("V_phi", self.V_phi)
        self.sampleTree.tree.SetBranchAddress("V_eta", self.V_eta)
        self.sampleTree.tree.SetBranchAddress("V_mass", self.V_mass)
        self.sampleTree.tree.SetBranchAddress("V_mt", self.V_mt)


        for syst in self.allVariations:
            self.addVsystematics(syst)

        self.Nevent = 0


    def addVsystematics(self, syst):
        if self._isnominal(syst): 
            # replace values from post-processor / selection
            self.addBranch("V_pt_uncorrected")
            self.addBranch("V_eta_uncorrected")
            self.addBranch("V_phi_uncorrected")
            self.addBranch("V_mass_uncorrected")
            self.addBranch("V_mt_uncorrected")
        else:
            
            self.V_pt_syst[syst] = {}
            self.V_phi_syst[syst] = {}
            self.V_eta_syst[syst] = {}
            self.V_mass_syst[syst] = {}
            self.V_mt_syst[syst] = {}
            for Q in self._variations(syst):
                self.V_pt_syst[syst][Q]  = array.array('f', [0.0])
                self.V_phi_syst[syst][Q]  = array.array('f', [0.0])
                self.V_eta_syst[syst][Q]  = array.array('f', [0.0])
                self.V_mass_syst[syst][Q]  = array.array('f', [0.0])
                self.V_mt_syst[syst][Q]  = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress("V_pt_"+syst+"_"+Q, self.V_pt_syst[syst][Q])
                self.sampleTree.tree.SetBranchAddress("V_phi_"+syst+"_"+Q, self.V_phi_syst[syst][Q])
                self.sampleTree.tree.SetBranchAddress("V_eta_"+syst+"_"+Q, self.V_eta_syst[syst][Q])
                self.sampleTree.tree.SetBranchAddress("V_mass_"+syst+"_"+Q, self.V_mass_syst[syst][Q])
                self.sampleTree.tree.SetBranchAddress("V_mt_"+syst+"_"+Q, self.V_mt_syst[syst][Q])

                self.addBranch("V_pt_uncorrected_"+syst+Q)
                self.addBranch("V_phi_uncorrected_"+syst+Q)
                self.addBranch("V_eta_uncorrected_"+syst+Q)
                self.addBranch("V_mass_uncorrected_"+syst+Q)
                self.addBranch("V_mt_uncorrected_"+syst+Q)




    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree):
            self.markProcessed(tree)

 
            self._b('V_pt_uncorrected')[0]  = tree.V_pt
            self._b('V_phi_uncorrected')[0]  = tree.V_phi
            self._b('V_eta_uncorrected')[0]  = tree.V_eta
            self._b('V_mass_uncorrected')[0]  = tree.V_mass
            self._b('V_mt_uncorrected')[0]  = tree.V_mt

            print("Nevent ", self.Nevent,tree.GetEntries())
            self.Nevent+=1


            for syst in self.allVariations:
                for Q in self._variations(syst):

                    if  tree.Vtype == 2 or tree.Vtype == 3:
                       
                        if not self._isnominal(syst):
                            # backup uncorrected branches
                            self._b("V_pt_uncorrected_"+syst+Q)[0]  = self.V_pt_syst[syst][Q][0]
                            self._b("V_phi_uncorrected_"+syst+Q)[0]  = self.V_phi_syst[syst][Q][0]
                            self._b("V_eta_uncorrected_"+syst+Q)[0]  = self.V_eta_syst[syst][Q][0]
                            self._b("V_mass_uncorrected_"+syst+Q)[0]  = self.V_mass_syst[syst][Q][0]
                            self._b("V_mt_uncorrected_"+syst+Q)[0]  = self.V_mt_syst[syst][Q][0]
 

                        i1 = tree.vLidx[0]
                        if tree.Vtype==3:
                            sel_lepton_pt   = tree.Electron_pt[i1]
                            sel_lepton_eta  = tree.Electron_eta[i1]
                            sel_lepton_phi  = tree.Electron_phi[i1]
                            sel_lepton_mass = tree.Electron_mass[i1]
                        else:
                            sel_lepton_pt   = tree.Muon_pt[i1]
                            sel_lepton_eta  = tree.Muon_eta[i1]
                            sel_lepton_phi  = tree.Muon_phi[i1]
                            sel_lepton_mass = tree.Muon_mass[i1]

                        MET = ROOT.TLorentzVector()
                        Lep = ROOT.TLorentzVector()
                        
                        Lep.SetPtEtaPhiM(sel_lepton_pt, sel_lepton_eta, sel_lepton_phi, sel_lepton_mass)
                        
                        if syst.lower()=='nominal' or 'jerReg' in syst:
                            #MET.SetPtEtaPhiM(tree.MET_Pt, 0.0, tree.MET_Phi, 0.0)
                            neutrino = self.getNeutrino(tree,Lep, 0, is_nominal = True)
                            MET.SetPtEtaPhiM(neutrino.Pt(), neutrino.Eta(), neutrino.Phi(), 0.0)
                            #print("MET", tree.MET_Pt, 0.0, tree.MET_Phi, 0.0)
                            #print("neutrino", neutrino.Pt(), neutrino.Eta(), neutrino.Phi(), 0.0)


                        else:
                            neutrino = self.getNeutrino(tree,Lep, 0, is_nominal = False, syst = syst, UD = Q)
                            #MET.SetPtEtaPhiM(getattr(tree, "MET_T1_pt_{syst}{UD}".format(syst=syst, UD=Q)), 0.0, getattr(tree, "MET_T1_phi_{syst}{UD}".format(syst=syst, UD=Q)), 0.0)
                            MET.SetPtEtaPhiM(neutrino.Pt(), neutrino.Eta(), neutrino.Phi(), 0.0)
                        cosPhi12 = (Lep.Px()*MET.Px() + Lep.Py()*MET.Py()) / (Lep.Pt() * MET.Pt())
                        if not self._isnominal(syst):
                            #self._b(self._v("V_mt", syst,UD))[0] = ROOT.TMath.Sqrt(2*Lep.Pt()*MET.Pt() * (1 - cosPhi12))

                            self.V_mt_syst[syst][Q][0]  = ROOT.TMath.Sqrt(2*Lep.Pt()*MET.Pt() * (1 - cosPhi12))
                        else:
                            
                            self.V_mt[0]  = ROOT.TMath.Sqrt(2*Lep.Pt()*MET.Pt() * (1 - cosPhi12))

                        V = MET + Lep

                        #if self._isnominal(syst):
                        #   
                        #    #if abs(self.V_phi[0] - V.Phi())>0.1:
                        #    

                        #    print("Uncorr eta", self.V_eta[0]) 
                        #    print("corr eta", V.Eta()) 
                         


                    if V is not None:
                        if not self._isnominal(syst):
                            self.V_pt_syst[syst][Q][0]  = V.Pt() 
                            self.V_phi_syst[syst][Q][0]  = V.Phi() 
                            self.V_eta_syst[syst][Q][0]  = V.Eta() 
                            self.V_mass_syst[syst][Q][0]  = V.M() 
                        
                        else:
                            self.V_pt[0]  = V.Pt()
                            self.V_phi[0]  = V.Phi()
                            self.V_eta[0]  = V.Eta()
                            self.V_mass[0]  = V.M()



    def getNeutrino(self,tree,vec_lep,neutrino_choice,is_nominal, syst=None, UD=None): # Only used for reco, LHE handled like a lepton
        W_mass = 80.4
        pnu_random = ROOT.TLorentzVector()
        pnu_1 = ROOT.TLorentzVector()
        pnu_2 = ROOT.TLorentzVector()
        nueta = [-100,-100]

        if is_nominal:
        
            MET_pt = tree.MET_Pt
            MET_phi = tree.MET_Phi

        else:

            MET_pt = getattr(tree, "MET_T1_pt_{syst}{UD}".format(syst=syst, UD=UD))
            MET_phi = getattr(tree, "MET_T1_phi_{syst}{UD}".format(syst=syst, UD=UD))



        #print(vec_lep.Pt(), vec_lep.Eta(), vec_lep.Phi(), vec_lep.M(), MET_pt, MET_phi)

        if vec_lep.E()<0:
            pnu_random.SetPtEtaPhiM(0,-100,-100,0)
            pnu_1.SetPtEtaPhiM(0,-100,-100,0)
            pnu_2.SetPtEtaPhiM(0,-100,-100,0)
        else:
            mT2 = 2*vec_lep.Pt()*MET_pt*(1-cos(self.deltaPhi(vec_lep.Phi(),MET_phi)))
            Delta2 = (W_mass*W_mass - mT2)*1./(2*MET_pt*vec_lep.Pt())
            if (Delta2>=0):
                try:
                    nueta[0] = (vec_lep.Eta() + abs(acosh(1+(Delta2))))
                    nueta[1] = (vec_lep.Eta() - abs(acosh(1+(Delta2))))
                except Exception:
                    nueta[0] = -200
                    nueta[1] = -200
                    #pass
                    #pass
            else:
                nueta[0] = vec_lep.Eta()
                nueta[1] = vec_lep.Eta()        

       
        pnu_1.SetPtEtaPhiM(MET_pt,nueta[0],MET_phi,0)
        pnu_2.SetPtEtaPhiM(MET_pt,nueta[1],MET_phi,0)
        #print(pnu_1.Pt(), pnu_1.Eta(), pnu_1.Phi(), pnu_1.M())
        if (random.random()>=0.5): pnu_random = pnu_1
        else: pnu_random = pnu_2
        if (neutrino_choice == 0): return pnu_random
        if (neutrino_choice == 1): return pnu_1
        if (neutrino_choice == 2): return pnu_2


    def deltaPhi(self,phi1,phi2):
        dphi = phi1 - phi2
        while (dphi >= pi): dphi-=2*pi
        while (dphi < -pi): dphi+=2*pi
        return dphi














