import json
import os
import array
from JsonTable import JsonTable
from vLeptons import vLeptonSelector
from BranchTools import Collection
from BranchTools import AddCollectionsModule

class ElectronSFfromJSON(AddCollectionsModule):
    
    def __init__(self, jsonFiles=None, branchName="electronSF",channel='None',year=None):
        super(ElectronSFfromJSON, self).__init__()
        self.jsonFiles = jsonFiles
        self.debug = 'XBBDEBUG' in os.environ
        self.branchName = branchName

        # load JOSN files
        self.jsonTable = JsonTable(jsonFiles)
        self.channel = channel 
        self.trackerSfName = None
        if self.channel== 'Zll':
            self.triggerLegNames = ['Trig{year}passingDoubleEleLeg1'.format(year=year), 'Trig{year}passingDoubleEleLeg2'.format(year=year)]
        elif self.channel == 'Wlv':
            self.triggerLegNames = ['Trig{year}passingSingleEle'.format(year=year)]
        else: 
            print "Channel not defined!"
            raise Exception("ChannelNotDefined")

        self.systVariations = [None, 'Down', 'Up']
        self.triggerSf = [self.jsonTable.getEtaPtTable(x) for x in self.triggerLegNames]

    def customInit(self, initVars):
        sample = initVars['sample']
        self.isData = sample.type == 'DATA'
        self.config = initVars['config']

        # prepare buffers for new branches to add
        if not self.isData:
            for x in ['',  '_trigger']:
                self.addVectorBranch(self.branchName + x, default=1.0, length=3)
    
    def processEvent(self, tree):
        # if current entry has not been processed yet
        if not self.hasBeenProcessed(tree) and not self.isData:
            self.markProcessed(tree)

            #TODO check the assigned weights for events with more than 2 letpns
            Vtype = tree.Vtype
            vLidx = []
            lep_eta = []
            lep_pt = []
         
            if self.channel == "Wlv" and Vtype == 3:
                vLidx = [tree.vLidx[0]]
                lep_pt = [tree.Electron_pt[vLidx[0]]]
                lep_eta = [tree.Electron_eta[vLidx[0]]]
            elif self.channel == "Zll" and Vtype ==1:
                vLidx = [tree.vLidx[0],tree.vLidx[1]]
                lep_pt = [tree.Electron_pt[vLidx[0]],tree.Electron_pt[vLidx[1]]]
                lep_eta = [tree.Electron_eta[vLidx[0]],tree.Electron_eta[vLidx[1]]]

            self.computeSF(
                        weight_trigg=self._b(self.branchName + '_trigger'),
                        lep_eta=lep_eta,
                        lep_pt=lep_pt,
                        lep_n=len(vLidx),
                        etacut=1.566
                    )

        return True


    def computeSF(self, weight_trigg, lep_eta, lep_pt, lep_n, etacut):
        '''Computes the trigger, IdIso (including separated variations in eta) and final event SF'''
        # require two electrons

        if lep_n == 1 or lep_n == 2:
            #Calculating the trigger weights and Down/Up variations
            for i, syst in enumerate(self.systVariations):
                weight_trigg[i] = self.getTriggerSf(lep_eta, lep_pt, lep_n, syst=syst)

        #This is when an event has 0 or more than 2 lepton
        else:
            for i, syst in enumerate(self.systVariations):
                weight_trigg[i] = 1.


    
    def getTriggerSf(self, eta, pt, len_n, syst=None):
        triggSF = 1.
        for i in range(len_n):
            triggSF = triggSF * self.jsonTable.find(self.triggerSf[i], eta[i], pt[i], syst=syst)

        return triggSF
    
if __name__ == "__main__":
    sfObject = ElectronSFfromJSON([
            'data/Run2ElectronSF/Trig2017passingDoubleEleLeg1.json',
            'data/Run2ElectronSF/Trig2017passingDoubleEleLeg2.json',
            'data/Run2ElectronSF/IDs2017passingMVA94Xwp90iso.json',
        ], channel='Zll', year=2017)
    print sfObject.getIdIsoSf(0.5, 50)
    print sfObject.getIdIsoSf(-2.5, 50)
    print sfObject.getIdIsoSf(2.5, 250)
    print sfObject.getIdIsoSf(-0.5, 250)
    print sfObject.getTriggerSf([0.5, 1.1], [50, 30], 2)
