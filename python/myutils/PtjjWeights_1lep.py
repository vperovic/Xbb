#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class PtjjWeights_1lep(AddCollectionsModule):

    def __init__(self, year):
        super(PtjjWeights_1lep, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(PtjjWeights_1lep, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(PtjjWeights_1lep, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(PtjjWeights_1lep, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']
        print(">>>>>>>>>>>> year", self.year)


        if self.year == "2018":

            self.PtjjWeight_VPt = { 
                "isWJets":
                    {
                    "isWenu":
                        {
                            '150250':{'bins':[100,120,140,160,200.0], 'central':[1.3415, 1.1528, 1.028, 0.9488],'uncertainty':[0.0152, 0.011, 0.0089, 0.0063]
                            },
                            '250Inf':{'bins':[100,200],'central':[1.091],'uncertainty':[0.018]
                            },
                        },

                    "isWmunu":
                        {
                            '150250':{'bins':[100,120,140,160,200.0], 'central':[1.3133, 1.1254, 1.0051, 0.9458],'uncertainty':[0.0131, 0.0095, 0.0077, 0.0056]
                            },
                            '250Inf':{'bins':[100,200],'central':[1.1349],'uncertainty':[0.0165]
                            },
                        },
                                
                },
                                }


        elif self.year == "2017":

            self.PtjjWeight_VPt = { 
                "isWJets":
                    {
                    "isWenu":
                        {
                            '150250':{'bins':[100,120,140,160,200.0], 'central': [1.1503, 1.0781, 1.013, 0.9422] ,'uncertainty': [0.0234, 0.0186, 0.0141, 0.0084]  
                            },
                            '250Inf':{'bins':[100,200],'central': [1.0353] ,'uncertainty': [0.0199] 
                            },
                        },

                    "isWmunu":
                        {
                            '150250':{'bins':[100,120,140,160,200.0], 'central':  [1.1529, 1.0621, 0.9854, 0.9332]  ,'uncertainty':  [0.0209, 0.016, 0.0124, 0.0075] 
                            },
                            '250Inf':{'bins':[100,200],'central':[1.0427] ,'uncertainty':  [0.018] 
                            },
                        },
                                
                },
                                }


			#central values are:  [1.1503, 1.0781, 1.013, 0.9422, 0.8766]
			#uncertainties are:  [0.0234, 0.0186, 0.0141, 0.0084, 0.008]
			#
			#central values are:  [1.0353, 0.8816]
			#uncertainties are:  [0.0199, 0.0076]
			#
			#central values are:  [1.1529, 1.0621, 0.9854, 0.9332, 0.8991]
			#uncertainties are:  [0.0209, 0.016, 0.0124, 0.0075, 0.0075]
			#
			#central values are:  [1.0427, 0.8823]
			#uncertainties are:  [0.018, 0.007]


        else:
            raise Exception("Wrong year")


        self.get_Ptjjdict = {'150':'150250','250':'250Inf'}
        self.Ptjj_branches = ["WJ_ptjj_weight"]

        if self.sample.isMC():
            self.maxNjet   = 256


            for var in ["H_pt","V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
        
            for var in ["isZmm","isZee","isWenu","isWmunu","isZnn"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
        
            
            #Ptjj
            for var in self.Ptjj_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)



    def applies_ptjj(self, attr):
        isPtjj_lt200 = False

 
        if (any([s in self.sample.identifier for s in ['amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if (attr["H_pt"][0] < 200.0):
                if (attr["channel"] == "isWenu" or attr["channel"] == "isWmunu"):
                    if (any([x in self.sample.identifier for x in ['WJets']])):
                        isPtjj_lt200 = True

                #elif (attr["channel"] == "isWenu" or attr["channel"] == "isWmunu"):
                #    if (any([x in self.sample.identifier for x in ['DYJets','WJetsToLNu']])):
                #        raise Exception("Did you check the btag cuts ? ")
                #        isNLO_dRbb_lt1 = True
                #elif (attr["channel"] == "isZnn"):
                #    if (any([x in self.sample.identifier for x in ['WJetsToLNu','ZJetsToNuNu']])):
                #        isNLO_dRbb_lt1 = True
                #        raise Exception("Did you check the btag cuts ? ")
                #else:
                #    isNLO_dRbb_lt1 = False
                        
        return isPtjj_lt200


    def get_lowerBound(self,value,bins):
        j = 0
        while not (value<bins[j] or value>=250):
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])


    def get_eventWeight_Ptjj(self, attr):
        
        
        PtjjWeight_Vpt = []
 
        Ptjj_event     = attr["H_pt"][0]
        sample = attr["NLOsample"]
        channel = attr["channel"]
        
        ### Vpt split
        PtjjWeight_bin_Vpt= self.get_Ptjjdict[self.get_lowerBound(attr["V_pt"][0],[150,250])]
        j=0
        while not (Ptjj_event < self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["bins"][j]):
            j+=1
        PtjjWeight_Vpt = [self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["central"][j-1], self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["central"][j-1] + self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["uncertainty"][j-1], self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["central"][j-1] - self.PtjjWeight_VPt[sample][channel][PtjjWeight_bin_Vpt]["uncertainty"][j-1]]
        

        return  PtjjWeight_Vpt



    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)

            attr = {}
            for var in ["V_pt","hJidx","isZee","isZmm","H_pt","isWenu","isWmunu","isZnn"]: 
                attr[var] = self.existingBranches[var]

            if any([x in self.sample.identifier for x in ['DYJets']]):
                attr["NLOsample"] = "isDY"
            elif any([x in self.sample.identifier for x in ['WJetsToLNu']]):
                attr["NLOsample"] = "isWJets" 
            elif any([x in self.sample.identifier for x in ['ZJetsToNuNu']]):
                attr["NLOsample"] = "isZJets" 
            attr["channel"] = None   
            

            if attr["isZee"][0] == 1: attr["channel"] = "isZee"
            if attr["isZmm"][0] == 1: attr["channel"] = "isZmm"
            if attr["isWenu"][0] == 1: attr["channel"] = "isWenu"
            if attr["isWmunu"][0] == 1: attr["channel"] = "isWmunu"
            if attr["isZnn"][0] == 1: attr["channel"] = "isZnn"

            if self.applies_ptjj(attr):
                
                PtjjWeight_Vpt = self.get_eventWeight_Ptjj(attr)
                #print("Checking pass boundary conditions")
                #print("Features", attr["H_pt"],attr["hJidx"][0],attr["hJidx"][1],attr["V_pt"][0])  
                #print("channel", attr["channel"])
                #print(PtjjWeight_Vpt)
                self._b("WJ_ptjj_weight")[0]        = PtjjWeight_Vpt[0]
                self._b("WJ_ptjj_weight"+"Up")[0]   = PtjjWeight_Vpt[1]
                self._b("WJ_ptjj_weight"+"Down")[0] = PtjjWeight_Vpt[2]
            else:

                self._b("WJ_ptjj_weight")[0]        = 1.0
                self._b("WJ_ptjj_weight"+"Up")[0]   = 1.0
                self._b("WJ_ptjj_weight"+"Down")[0] = 1.0




if __name__=='__main__':

    print("This module creates branches with ptjj weights")

