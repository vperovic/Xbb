#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class BTagWeights_WJ_TT_ltlooseWP(AddCollectionsModule):

    def __init__(self, year):
        super(BTagWeights_WJ_TT_ltlooseWP, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(BTagWeights_WJ_TT_ltlooseWP, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(BTagWeights_WJ_TT_ltlooseWP, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(BTagWeights_WJ_TT_ltlooseWP, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.btagWPloose = float(self.config.get('General','btagWP_Loose'))
        print(">>>>>>>>>>>> year", self.year)
        print("Loose BTAG ", self.btagWPloose)



        if self.year == "2018":

            self.BTagWeights_Vpt = {
                    "isWJets":
                        {
                        "isWenu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.049],'central':[1.0335, 1.1548, 1.1685, 1.2597],'uncertainty':[0.0069, 0.0073, 0.0124, 0.0138]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.049],'central':[0.9009, 0.9753, 1.0212, 1.0775],'uncertainty':[0.0131, 0.0111, 0.0179, 0.0191]
                                },
                            },
                          "isWmunu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.049],'central':[0.9517, 1.0621, 1.1209, 1.1544],'uncertainty':[0.0056, 0.006, 0.0107, 0.0114]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.049],'central':[0.8534, 0.9214, 0.9802, 1.0275],'uncertainty':[0.0111, 0.0095, 0.0157, 0.0166]
                                },
                            },
                        },
                    "isTT":
                        {
                        "isWenu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.049],'central':[0.8286, 0.8745, 0.9608, 0.9162],'uncertainty':[0.0461, 0.0168, 0.0197, 0.0163]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.049],'central':[0.7582, 0.7934, 0.8655, 0.822],'uncertainty':[0.0854, 0.031, 0.0358, 0.0295]
                                },
                            },
                          "isWmunu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.049],'central':[0.7795, 0.863, 0.8893, 0.9063],'uncertainty':[0.0397, 0.0148, 0.0169, 0.0144]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.049],'central':[0.6815, 0.8014, 0.7804, 0.8376],'uncertainty':[0.0726, 0.028, 0.0309, 0.0269]
                                },
                            },
                        },
                    }



        elif self.year == "2017":

            self.BTagWeights_Vpt = {
                    "isWJets":
                        {
                        "isWenu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.054]  ,'central': [1.0, 1.0, 1.0, 1.0]  ,'uncertainty': [0.0, 0.0, 0.0, 0.0]   
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.054]  ,'central': [1.0, 1.0, 1.0, 1.0]  ,'uncertainty': [0.0, 0.0, 0.0, 0.0]  
                                },
                            },
                          "isWmunu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.054]  ,'central': [1.0, 1.0, 1.0, 1.0]  ,'uncertainty': [0.0, 0.0, 0.0, 0.0] 
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.054]  ,'central': [1.0, 1.0, 1.0, 1.0]  ,'uncertainty': [0.0, 0.0, 0.0, 0.0]  
                                },
                            },
                        },
                    "isTT":
                        {
                        "isWenu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.054]  ,'central': np.array([0.9472, 1.0586, 1.0127, 0.9295])/0.9499  ,'uncertainty': [0.0742, 0.0277, 0.0299, 0.0225]   
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.054]  ,'central': np.array([0.8761, 0.9475, 0.9647, 1.0179])/0.8969  ,'uncertainty': [0.131, 0.0465, 0.052, 0.0409]  
                                },
                            },
                          "isWmunu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.054]  ,'central': np.array([0.9265, 0.977, 0.9799, 1.0231])/0.9415   ,'uncertainty': [0.0673, 0.0241, 0.0266, 0.0212]   
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.054]  ,'central': np.array([0.7728, 0.8847, 0.9629, 0.9786])/0.8678   ,'uncertainty': [0.1107, 0.0418, 0.0467, 0.0364]  
                                },
                            },
                        },
                    }


#central values are:  [0.9472, 1.0586, 1.0127, 0.9295, 0.9499]
#uncertainties are:  [0.0742, 0.0277, 0.0299, 0.0225, 0.0066]
#
#central values are:  [0.8761, 0.9475, 0.9647, 1.0179, 0.8969]
#uncertainties are:  [0.131, 0.0465, 0.052, 0.0409, 0.0115]
#
#central values are:  [0.9265, 0.977, 0.9799, 1.0231, 0.9415]
#uncertainties are:  [0.0673, 0.0241, 0.0266, 0.0212, 0.0059]
#
#central values are:  [0.7728, 0.8847, 0.9629, 0.9786, 0.8678]
#uncertainties are:  [0.1107, 0.0418, 0.0467, 0.0364, 0.0103]




        elif self.year == "2016":



            #self.BTagWeights_Vpt = {
            #        "isWJets":
            #            {
            #            "isWenu":
            #                {
            #                    '150250':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.9505, 1.07, 1.1684, 1.2774])/1.403,'uncertainty':[0.0173, 0.0171, 0.0295, 0.0344]
            #                    },
            #                    '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.7681, 0.839, 0.9825, 0.9615])/1.0792,'uncertainty':[0.0266, 0.0206, 0.0354, 0.0375]
            #                    },
            #                },
            #              "isWmunu":
            #                {
            #                    '150250':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.9389, 0.9913, 1.0809, 1.299])/1.2952,'uncertainty':[0.0149, 0.0141, 0.0245, 0.0305]
            #                    },
            #                    '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.7703, 0.8204, 0.9465, 1.0226])/1.0843,'uncertainty':[0.0225, 0.0181, 0.0303, 0.0336]
            #                    },
            #                },
            #            },
            #        "isTT":
            self.BTagWeights_Vpt = {
                    "isWJets":
                        {
                        "isWenu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.7681, 0.839, 0.9825, 0.9615])/1.0792,'uncertainty':[0.0266, 0.0206, 0.0354, 0.0375]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.7681, 0.839, 0.9825, 0.9615])/1.0792,'uncertainty':[0.0266, 0.0206, 0.0354, 0.0375]
                                },
                            },
                          "isWmunu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.7703, 0.8204, 0.9465, 1.0226])/1.0843,'uncertainty':[0.0225, 0.0181, 0.0303, 0.0336]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.7703, 0.8204, 0.9465, 1.0226])/1.0843,'uncertainty':[0.0225, 0.0181, 0.0303, 0.0336]
                                },
                            },
                        },
                    "isTT":
                        {
                        "isWenu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.7121, 0.8131, 0.8032, 0.9035])/0.8856,'uncertainty':[0.1033, 0.0355, 0.038, 0.035]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.5763, 0.6437, 0.8855, 0.8216])/0.8458,'uncertainty':[0.1616, 0.0536, 0.0643, 0.0547]
                                },
                            },
                          "isWmunu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.8374, 0.7179, 0.8243, 0.8689])/0.8947,'uncertainty':[0.1024, 0.0307, 0.0344, 0.0308]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.048],'central':np.array([0.6577, 0.5979, 0.8065, 0.8139])/0.8209,'uncertainty':[0.1506, 0.0477, 0.056, 0.0493]
                                },
                            },
                        },
                    }



        elif self.year == "2016preVFP":


            self.BTagWeights_Vpt = {
                    "isWJets":
                        {
                        "isWenu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.0508],'central': [1.0, 1.0, 1.0, 1.0], 'uncertainty':[0.0, 0.0, 0.0, 0.0]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.0508],'central': [1.0, 1.0, 1.0, 1.0], 'uncertainty':[0.0, 0.0, 0.0, 0.0]
                                },
                            },
                          "isWmunu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.0508],'central': [1.0, 1.0, 1.0, 1.0], 'uncertainty':[0.0, 0.0, 0.0, 0.0]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.0508],'central': [1.0, 1.0, 1.0, 1.0], 'uncertainty':[0.0, 0.0, 0.0, 0.0]
                                },
                            },
                        },
                    "isTT":
                        {
                        "isWenu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.0508],'central': np.array([0.7424, 0.8035, 0.7445, 0.7913])/0.7946  ,'uncertainty':  [0.0995, 0.0339, 0.0347, 0.0283]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.0508],'central': np.array([0.5538, 0.7708, 0.8071, 0.723])/0.7459  ,'uncertainty': [0.1425, 0.0555, 0.0589, 0.0455]
                                },
                            },
                          "isWmunu":
                            {
                                '150250':{'bins':[-0.01,0.008,0.02,0.03,0.0508],'central': np.array([0.7901, 0.7763, 0.7524, 0.7516])/0.7954  ,'uncertainty': [0.0885, 0.0296, 0.0308, 0.0246]
                                },
                                '250Inf':{'bins':[-0.01,0.008,0.02,0.03,0.0508],'central': np.array([0.8773, 0.7019, 0.6357, 0.6627])/0.7201  ,'uncertainty': [0.16, 0.0485, 0.0474, 0.0384]
                                },
                            },
                        },
                    }

#central values are:  [0.7424, 0.8035, 0.7445, 0.7913, 0.7946]
#uncertainties are:  [0.0995, 0.0339, 0.0347, 0.0283, 0.0085]
#
#central values are:  [0.5538, 0.7708, 0.8071, 0.723, 0.7459]
#uncertainties are:  [0.1425, 0.0555, 0.0589, 0.0455, 0.0141]
#
#central values are:  [0.7901, 0.7763, 0.7524, 0.7516, 0.7954]
#uncertainties are:  [0.0885, 0.0296, 0.0308, 0.0246, 0.0076]
#
#central values are:  [0.8773, 0.7019, 0.6357, 0.6627, 0.7201]
#uncertainties are:  [0.16, 0.0485, 0.0474, 0.0384, 0.0124]


        else:
            raise Exception("Wrong year")


        self.get_BTagWeights_Vptdict = {'150':'150250','250':'250Inf'}
        self.BTagWeights_Vpt_branches = ["WJ_TT_btagDeepFlavB"]

        if self.sample.isMC():
            self.maxNjet   = 256

            for var in ["Jet_btagDeepFlavB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isZmm","isZee","isWenu","isWmunu","isZnn"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["nGenBpt25eta2p6","nGenDpt25eta2p6"]:
                self.existingBranches[var] = array.array('i', [100])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.BTagWeights_Vpt_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                


    def applies(self, attr):
        is_WJ_TT_btag_ltl = False
        

        #print(">>>>>>>>>> before")
        #print(attr["hJidx"][0])
        #print(attr["hJidx"][1])
        #print("leading btag",attr["Jet_btagDeepFlavB"][attr["hJidx"][0]])
        #print("truth?",  attr["Jet_btagDeepFlavB"][attr["hJidx"][0]] < self.btagWPloose)
        #print("subleading btag",attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])
        #print(attr["nGenBpt25eta2p6"][0])
        #print(attr["nGenDpt25eta2p6"][0])
        #print(self.btagWPloose)
        
        #If W+jets LF
        if (any([x in self.sample.identifier for x in ['WJets']]) and any([s in self.sample.identifier for s in ['amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):  
            #0b_udsg or 0b_c and lower than loose
            if ((attr["Jet_btagDeepFlavB"][attr["hJidx"][1]] < self.btagWPloose ) and (   (attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1) or (attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]>0) )):
                is_WJ_TT_btag_ltl = True
                #print(">>>>>>>>>> WJ")
                #print(attr["hJidx"][1])
                #print(attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])
                #print(attr["nGenBpt25eta2p6"][0])
                #print(attr["nGenDpt25eta2p6"][0])
                #print(attr["V_pt"][0])
                #print(attr["channel"])
                #print(attr["NLOsample"])


        #If TT
        if (any([x in self.sample.identifier for x in ['TTT']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):  
            #lower than loose
            if (attr["Jet_btagDeepFlavB"][attr["hJidx"][1]] < self.btagWPloose ):
                is_WJ_TT_btag_ltl = True
                #print(">>>>>>>>>> TT")
                #print(attr["hJidx"][1])
                #print(attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])
                #print(attr["V_pt"][0])
                #print(attr["channel"])
                #print(attr["NLOsample"])


 
        return is_WJ_TT_btag_ltl

    def get_lowerBound(self,value,bins):
        j = 0
        while not (value<bins[j] or value>=250):
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
 
        btag_hJidx1        = attr["Jet_btagDeepFlavB"][attr["hJidx"][1]]
        BTagWeights_Vptbin = self.get_BTagWeights_Vptdict[self.get_lowerBound(attr["V_pt"][0],[150,250])]

        sample = attr["NLOsample"]
        channel = attr["channel"]
        
        k=0
        while not (btag_hJidx1 < self.BTagWeights_Vpt[sample][channel][BTagWeights_Vptbin]["bins"][k]):
            k+=1
        BtagWeight_Vpt = [self.BTagWeights_Vpt[sample][channel][BTagWeights_Vptbin]["central"][k-1], self.BTagWeights_Vpt[sample][channel][BTagWeights_Vptbin]["central"][k-1] + self.BTagWeights_Vpt[sample][channel][BTagWeights_Vptbin]["uncertainty"][k-1], self.BTagWeights_Vpt[sample][channel][BTagWeights_Vptbin]["central"][k-1] - self.BTagWeights_Vpt[sample][channel][BTagWeights_Vptbin]["uncertainty"][k-1]]
        
        return  BtagWeight_Vpt




    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)

            attr = {}
            for var in ["V_pt","hJidx","isZee","isZmm","Jet_btagDeepFlavB","isWenu","isWmunu","isZnn","nGenBpt25eta2p6","nGenDpt25eta2p6"]: 
                attr[var] = self.existingBranches[var]

            attr["NLOsample"] = None   
            if any([x in self.sample.identifier for x in ['DYJets']]):
                attr["NLOsample"] = "isDY"
            elif any([x in self.sample.identifier for x in ['WJetsToLNu']]):
                attr["NLOsample"] = "isWJets" 
            elif any([x in self.sample.identifier for x in ['ZJetsToNuNu']]):
                attr["NLOsample"] = "isZJets" 
            elif any([x in self.sample.identifier for x in ['TTT']]):
                attr["NLOsample"] = "isTT" 
            
            attr["channel"] = None   
            if attr["isZee"][0] == 1: attr["channel"] = "isZee"
            if attr["isZmm"][0] == 1: attr["channel"] = "isZmm"
            if attr["isWenu"][0] == 1: attr["channel"] = "isWenu"
            if attr["isWmunu"][0] == 1: attr["channel"] = "isWmunu"
            if attr["isZnn"][0] == 1: attr["channel"] = "isZnn"



            if self.applies(attr):
                BtagWeight_Vpt = self.get_eventWeight(attr)
                #print("Weight: ", BtagWeight_Vpt)

                self._b("WJ_TT_btagDeepFlavB")[0]         = BtagWeight_Vpt[0]
                self._b("WJ_TT_btagDeepFlavB"+"Up")[0]    = BtagWeight_Vpt[1]
                self._b("WJ_TT_btagDeepFlavB"+"Down")[0]  = BtagWeight_Vpt[2]
            else:
                self._b("WJ_TT_btagDeepFlavB")[0]         = 1.0
                self._b("WJ_TT_btagDeepFlavB"+"Up")[0]    = 1.0
                self._b("WJ_TT_btagDeepFlavB"+"Down")[0]  = 1.0

if __name__=='__main__':

    print("This module computes btag weights for tt and wj samples in the lower than loose region")

