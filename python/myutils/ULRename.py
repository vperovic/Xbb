#!/usr/bin/env python
from __future__ import print_function
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools

class ULRename(AddCollectionsModule):

    def __init__(self, year):
        super(ULRename, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.year  = int(year)
        self.quickloadWarningShown = False

    def customInit(self, initVars):
        self.sampleTree  = initVars['sampleTree']
        self.sample      = initVars['sample']
        self.config      = initVars['config']
        self.xbbConfig   = XbbConfigTools(self.config)

        self.maxNfatjet = 50

        if self.sample.isMC():
            self.addVectorBranch("FatJet_pt_jerUp", default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_pt_jerUp"+"[nFatJet]/F")
            self.addVectorBranch("FatJet_pt_jerDown", default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_pt_jerDown"+"[nFatJet]/F")
            self.addVectorBranch("FatJet_mass_jerUp", default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_mass_jerUp"+"[nFatJet]/F")
            self.addVectorBranch("FatJet_mass_jerDown", default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_mass_jerDown"+"[nFatJet]/F")
            self.addVectorBranch("FatJet_msoftdrop_jerUp", default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_msoftdrop_jerUp"+"[nFatJet]/F")
            self.addVectorBranch("FatJet_msoftdrop_jerDown", default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_msoftdrop_jerDown"+"[nFatJet]/F")
            self.addVectorBranch("FatJet_msoftdrop_tau21DDT_jerUp", default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_msoftdrop_tau21DDT_jerUp"+"[nFatJet]/F")
            self.addVectorBranch("FatJet_msoftdrop_tau21DDT_jerDown", default=0.0, branchType='f', length=self.maxNfatjet, leaflist="FatJet_msoftdrop_tau21DDT_jerDown"+"[nFatJet]/F")



    def processEvent(self, tree): 
        if not self.hasBeenProcessed(tree):
            self.markProcessed(tree)
        if self.sample.isMC():
             nFatJet = tree.nFatJet
             if nFatJet > 0:
                 for i in range(nFatJet):
                     FatJet_JERsplitID = str(self.getJERsplitID(tree.FatJet_pt_nom[i], tree.FatJet_eta[i]))

                     branchName = "FatJet_pt_jer"+FatJet_JERsplitID+"Up"
                     self._b('FatJet_pt_jerUp')[i] = getattr(tree,branchName)[i]

                     branchName = "FatJet_pt_jer"+FatJet_JERsplitID+"Down"
                     self._b('FatJet_pt_jerDown')[i] = getattr(tree,branchName)[i]

                     branchName = "FatJet_mass_jer"+FatJet_JERsplitID+"Up"
                     self._b('FatJet_mass_jerUp')[i] = getattr(tree,branchName)[i]

                     branchName = "FatJet_mass_jer"+FatJet_JERsplitID+"Down"
                     self._b('FatJet_mass_jerDown')[i] = getattr(tree,branchName)[i]

                     branchName = "FatJet_msoftdrop_jer"+FatJet_JERsplitID+"Up"
                     self._b('FatJet_msoftdrop_jerUp')[i] = getattr(tree,branchName)[i]

                     branchName = "FatJet_msoftdrop_jer"+FatJet_JERsplitID+"Down"
                     self._b('FatJet_msoftdrop_jerDown')[i] = getattr(tree,branchName)[i]

                     branchName = "FatJet_msoftdrop_tau21DDT_jer"+FatJet_JERsplitID+"Up"
                     self._b('FatJet_msoftdrop_tau21DDT_jerUp')[i] = getattr(tree,branchName)[i]

                     branchName = "FatJet_msoftdrop_tau21DDT_jer"+FatJet_JERsplitID+"Down"
                     self._b('FatJet_msoftdrop_tau21DDT_jerDown')[i] = getattr(tree,branchName)[i]





    def getJERsplitID(self, pt, eta):
        #if not self.splitJER:
        #    return ""
        if abs(eta) < 1.93:
            return 0
        elif abs(eta) < 2.5:
            return 1
        elif abs(eta) < 3:
            if pt < 50:
                return 2
            else:
                return 3
        else:
            if pt < 50:
                return 4
            else:
                return 5




