#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class NLORbblt1Weights_2lep(AddCollectionsModule):

    def __init__(self, year):
        super(NLORbblt1Weights_2lep, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights_2lep, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights_2lep, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(NLORbblt1Weights_2lep, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']
        #self.btagWPloose = float(self.config.get('General','btagWP_Loose'))
        #self.btagWPmedium = float(self.config.get('General','btagWP_Medium'))
        print(">>>>>>>>>>>> year", self.year)
        #print("Loose BTAG ", self.btagWPloose)
        #print("Medium BTAG ", self.btagWPmedium)



        if self.year == "2016":

            #Extracted from light flavor, applied everywhere
            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isDY":
                        {
                        "isZee":
                            {
                                '75Inf':{'bins': [0.4,0.6,0.8,1.0],'central':[1.1342, 1.171, 1.1884],'uncertainty':[0.1447, 0.0897, 0.0678]
                                },
                        
                        },
                          "isZmm":
                            {
                                '75Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central':[1.0075, 1.2218, 1.2314],'uncertainty':[0.1214, 0.0807, 0.0598]
                                },
                            },
                        },
                    }

        elif self.year == "2018":

            #Extracted from light flavor, applied everywhere
            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isDY":
                        {
                        "isZee":
                            {
                                '75Inf':{'bins': [0.4,0.6,0.8,1.0],'central':np.array([0.8637, 1.0092, 1.0591])/0.8772,'uncertainty':[0.0652, 0.0413, 0.0331]
                                },
                        
                        },
                          "isZmm":
                            {
                                '75Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central':np.array([1.0067, 1.0649, 1.1744])/0.8739,'uncertainty':[0.0638, 0.0381, 0.0319]
                                },
                            },
                        },
                    }



        elif self.year == "2017":

            #Extracted from light flavor, applied everywhere
            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isDY":
                        {
                        "isZee":
                            {
                                '75Inf':{'bins': [0.4,0.6,0.8,1.0],'central': np.array([0.988, 1.1586, 1.2508])/0.9016  ,'uncertainty': [0.129, 0.0659, 0.0436]
                                },

                        },
                          "isZmm":
                            {
                                '75Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central': np.array([1.0919, 1.2264, 1.2221])/0.9171  ,'uncertainty': [0.1201, 0.0609, 0.0372] 
                                },
                            },
                        },
                    }


#central values are:  [0.988, 1.1586, 1.2508, 0.9016, 1.0012]
#uncertainties are:  [0.129, 0.0659, 0.0436, 0.0189, 0.0056]

#central values are:  [1.0919, 1.2264, 1.2221, 0.9171, 0.9998]
#uncertainties are:  [0.1201, 0.0609, 0.0372, 0.017, 0.0048]




        elif self.year == "2016preVFP":

            #Extracted from light flavor, applied everywhere
            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isDY":
                        {
                        "isZee":
                            {
                                '75Inf':{'bins': [0.4,0.6,0.8,1.0],'central': np.array([1.0573, 1.1043, 1.0964])/0.9289  ,'uncertainty': [0.1423, 0.0842, 0.0617] 
                                },
                        
                        },
                          "isZmm":
                            {
                                '75Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central': np.array([0.8415, 1.2274, 1.1453])/0.9477  ,'uncertainty': [0.112, 0.0796, 0.0563] 
                                },
                            },
                        },
                    }


#central values are:  [1.0573, 1.1043, 1.0964, 0.9289, 1.0006]
#uncertainties are:  [0.1423, 0.0842, 0.0617, 0.0304, 0.0084]

#central values are:  [0.8415, 1.2274, 1.1453, 0.9477, 0.9988]
#uncertainties are:  [0.112, 0.0796, 0.0563, 0.0265, 0.0071]



        else:
            raise Exception("Wrong year")


        #self.dRbbWeight_VPt["isDY"]["isWenu"] = self.dRbbWeight_VPt["isDY"]["isZee"]
        #self.dRbbWeight_VPt["isDY"]["isWmunu"] = self.dRbbWeight_VPt["isDY"]["isZmm"]

        #print("self.dRbbWeight_VPt ",self.dRbbWeight_VPt)

        


        self.get_dRbbdict = {'75':'75150', '150':'150250','250':'250Inf'}
        #self.dRbb_branches = ["dRbbWeight_Vpt","dRbbWeight_Incl"]
        #self.dRbb_branches = ["dRbbWeight_Vpt"]
        self.dRbb_branches = ["dRbbWeight_Incl"]

        if self.sample.isMC():
            self.maxNjet   = 256


            for var in ["Jet_btagDeepFlavB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isZmm","isZee","isWenu","isWmunu","isZnn"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.dRbb_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                    
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies(self, attr):
        isNLO_dRbb_lt1 = False

        #if(attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1):
        #    print("res")  
        #print(self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0,"drBBb")
        #print("---------------------")
 
        if (any([s in self.sample.identifier for s in ['amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if (self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0):
                if (attr["channel"] == "isZee" or attr["channel"] == "isZmm"):
                    if (any([x in self.sample.identifier for x in ['DYJets']])):
                        isNLO_dRbb_lt1 = True
                            #print("dRbb:")
                            #print(self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]))

                elif (attr["channel"] == "isWenu" or attr["channel"] == "isWmunu"):
                    if (any([x in self.sample.identifier for x in ['DYJets','WJetsToLNu']])):
                        raise Exception("Did you check the btag cuts ? ")
                        isNLO_dRbb_lt1 = True
                elif (attr["channel"] == "isZnn"):
                    if (any([x in self.sample.identifier for x in ['WJetsToLNu','ZJetsToNuNu']])):
                        isNLO_dRbb_lt1 = True
                        raise Exception("Did you check the btag cuts ? ")
                else:
                    isNLO_dRbb_lt1 = False
                        
        return isNLO_dRbb_lt1

    def get_lowerBound(self,value,bins):
        j = 0
        while not (value<bins[j] or value>=250):
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        
        #dRbbWeight_Vpt  = []
        
        dRbbWeight_Incl = []
 
        dRbb_event     = self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0])
        dRbbWeight_bin_incl = '75Inf'
        sample = attr["NLOsample"]
        channel = attr["channel"]
       
        #print("deltaRbb", dRbb_event)
 
        ### Vpt split
        #dRbbWeight_bin_Vpt= self.get_dRbbdict[self.get_lowerBound(attr["V_pt"][0],[75,150,250])]
        #j=0
        #VptBins = [150,250]
        #if any(channel==x for x in ["isZee","isZmm"]): VptBins=[75,150,250]
        #dRbbWeight_bin_Vpt = self.get_dRbbdict[self.get_lowerBound(attr["V_pt"][0],VptBins)]
        #while not (dRbb_event < self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["bins"][j]):
        #    j+=1
        #dRbbWeight_Vpt = [self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1] + self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["uncertainty"][j-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1] - self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["uncertainty"][j-1]]
        

        ### Inclusive
        k=0
        while not (dRbb_event < self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["bins"][k]):
            k+=1
        #print("channel ",channel)
        #print("Vpt ",attr["V_pt"][0])
        dRbbWeight_Incl = [self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1] + self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["uncertainty"][k-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1] - self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["uncertainty"][k-1]]
        
        #return  dRbbWeight_Vpt
        return  dRbbWeight_Incl

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)
            attr = {}
            attr["NLOsample"] = None
            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt","hJidx","isZee","isZmm","isWenu","isWmunu","isZnn","Jet_btagDeepFlavB"]: 
                attr[var] = self.existingBranches[var]
            if any([x in self.sample.identifier for x in ['DYJets']]):
                attr["NLOsample"] = "isDY"
            elif any([x in self.sample.identifier for x in ['WJetsToLNu']]):
                attr["NLOsample"] = "isWJets" 
            elif any([x in self.sample.identifier for x in ['ZJetsToNuNu']]):
                attr["NLOsample"] = "isZJets" 
            attr["channel"] = None   
            

            if attr["isZee"][0] == 1: attr["channel"] = "isZee"
            if attr["isZmm"][0] == 1: attr["channel"] = "isZmm"
            if attr["isWenu"][0] == 1: attr["channel"] = "isWenu"
            if attr["isWmunu"][0] == 1: attr["channel"] = "isWmunu"
            if attr["isZnn"][0] == 1: attr["channel"] = "isZnn"

            #print("channel is ",attr["channel"])

            if self.applies(attr):
                #dRbbWeight_Vpt,dRbbWeight_Incl = self.get_eventWeight(attr)
                
                dRbbWeight_Incl = self.get_eventWeight(attr)
                
                #print("Checking pass boundary conditions")
                #print("Leading", attr["Jet_btagDeepFlavB"][attr["hJidx"][0]])                   
                #print("Subleading", attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])                   
                #print(dRbbWeight_Incl)
                
 
                #self._b("dRbbWeight_Vpt")[0]         = dRbbWeight_Vpt[0]
                #self._b("dRbbWeight_Vpt"+"Up")[0]    = dRbbWeight_Vpt[1]
                #self._b("dRbbWeight_Vpt"+"Down")[0]  = dRbbWeight_Vpt[2]
                self._b("dRbbWeight_Incl")[0]        = dRbbWeight_Incl[0]
                self._b("dRbbWeight_Incl"+"Up")[0]   = dRbbWeight_Incl[1]
                self._b("dRbbWeight_Incl"+"Down")[0] = dRbbWeight_Incl[2]
            else:


                #self._b("dRbbWeight_Vpt")[0]         = 1.0
                #self._b("dRbbWeight_Vpt"+"Up")[0]    = 1.0
                #self._b("dRbbWeight_Vpt"+"Down")[0]  = 1.0
                self._b("dRbbWeight_Incl")[0]        = 1.0
                self._b("dRbbWeight_Incl"+"Up")[0]   = 1.0
                self._b("dRbbWeight_Incl"+"Down")[0] = 1.0

if __name__=='__main__':

    config = XbbConfigReader.read('Wlv2018')
    info = ParseInfo(config=config)
    sample = [x for x in info if x.identifier == 'DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8'][0]
    print(sample)
