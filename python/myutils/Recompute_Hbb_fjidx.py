#!/usr/bin/env python
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import itertools
from XbbConfig import XbbConfigTools
from vLeptons import vLeptonSelector

# do jet/lepton selection and skimming
class Recompute_Hbb_fjidx(AddCollectionsModule):

    # original 2017: puIdCut=0, jetIdCut=-1
    # now:           puIdCut=6, jetIdCut=4
    def __init__(self):
        super(Recompute_Hbb_fjidx, self).__init__()

    def customInit(self, initVars):
        self.sampleTree = initVars['sampleTree']
        self.isData = initVars['sample'].isData()
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.xbbConfig = XbbConfigTools(self.config)

        self.systematics        = self.xbbConfig.getJECuncertainties(step='Preselection') 
        self.METsystematics     = [x for x in self.systematics if 'jerReg' not in x] + ['unclustEn']
        self.systematicsBoosted = [x for x in self.systematics if 'jerReg' not in x] + ['jms', 'jmr']

        
        #Branches should already be there
        self.addIntegerBranch("Hbb_fjidx")
        if self.sample.isMC():
            for syst in self.systematicsBoosted:
                for UD in ['Up','Down']:
                    self.addIntegerBranch("Hbb_fjidx_{syst}{UD}".format(syst=syst, UD=UD))

        self.Nevent = 0

    def processEvent(self, tree):

        if not self.hasBeenProcessed(tree):
            self.markProcessed(tree)


            # VECTOR BOSON
            Vphi_syst = {}
            if tree.isZee or tree.isZmm:
                lep1 = ROOT.TLorentzVector()
                lep2 = ROOT.TLorentzVector()
                i1 = tree.vLidx[0]
                i2 = tree.vLidx[1]
                if tree.isZee:
                    lep1.SetPtEtaPhiM(tree.Electron_pt[i1], tree.Electron_eta[i1], tree.Electron_phi[i1], tree.Electron_mass[i1])
                    lep2.SetPtEtaPhiM(tree.Electron_pt[i2], tree.Electron_eta[i2], tree.Electron_phi[i2], tree.Electron_mass[i2])
                elif tree.isZmm:
                    lep1.SetPtEtaPhiM(tree.Muon_pt[i1], tree.Muon_eta[i1], tree.Muon_phi[i1], tree.Muon_mass[i1])
                    lep2.SetPtEtaPhiM(tree.Muon_pt[i2], tree.Muon_eta[i2], tree.Muon_phi[i2], tree.Muon_mass[i2])
                V = lep1 + lep2
            elif tree.isWenu or tree.isWmunu:
                i1 = tree.vLidx[0]
                if tree.isWenu:
                    sel_lepton_pt   = tree.Electron_pt[i1]
                    sel_lepton_eta  = tree.Electron_eta[i1]
                    sel_lepton_phi  = tree.Electron_phi[i1]
                    sel_lepton_mass = tree.Electron_mass[i1]
                elif tree.isWmunu:
                    sel_lepton_pt   = tree.Muon_pt[i1]
                    sel_lepton_eta  = tree.Muon_eta[i1]
                    sel_lepton_phi  = tree.Muon_phi[i1]
                    sel_lepton_mass = tree.Muon_mass[i1]

                MET = ROOT.TLorentzVector()
                Lep = ROOT.TLorentzVector()
                if self.sample.isMC(): #VP: different naming for MET in data and MC in UL
                    MET.SetPtEtaPhiM(tree.MET_Pt, 0.0, tree.MET_Phi, 0.0)
                else:
                    MET.SetPtEtaPhiM(tree.MET_T1_pt, 0.0, tree.MET_T1_phi, 0.0)
                Lep.SetPtEtaPhiM(sel_lepton_pt, sel_lepton_eta, sel_lepton_phi, sel_lepton_mass)
                cosPhi12 = (Lep.Px()*MET.Px() + Lep.Py()*MET.Py()) / (Lep.Pt() * MET.Pt())
                V = MET + Lep

                if self.sample.isMC():
                    MET_syst = ROOT.TLorentzVector()
                    for syst in self.METsystematics:
                        for UD in self._variations(syst):
                            MET_syst.SetPtEtaPhiM(self._r('MET_T1_pt_'+syst+UD), 0.0, self._r('MET_T1_phi_'+syst+UD), 0.0)
                            V_syst = MET_syst + Lep
                            Vphi_syst[syst+UD] = V_syst.Phi()

            elif tree.isZnn:
                MET = ROOT.TLorentzVector()
                if self.sample.isMC(): #VP: different naming for MET in data and MC in UL
                    MET.SetPtEtaPhiM(tree.MET_Pt, 0.0, tree.MET_Phi, 0.0)
                else:
                    MET.SetPtEtaPhiM(tree.MET_T1_pt, 0.0, tree.MET_T1_phi, 0.0)
                if self.sample.isMC():
                    for syst in self.METsystematics:
                        for UD in self._variations(syst):
                            Vphi_syst[syst+UD] = self._r('MET_T1_phi_'+syst+UD)
                V = MET
            else:
                raise Exception("Not the V type you are looking for")




            # nominal


            #Hbb_fjidx_old = self.HighestTaggerValueFatJet(tree, ptCut=250.0, msdCut=50.0, etaCut=2.5, taggerName='FatJet_deepTagMD_bbvsLight', systList=None, Vphi=V.Phi())
            Hbb_fjidx = self.HighestTaggerValueFatJet(tree, ptCut=250.0, msdCut=50.0, etaCut=2.5, taggerName='FatJet_particleNet_HbbvsQCD', systList=None, Vphi=V.Phi())
            

            self._b("Hbb_fjidx")[0] = Hbb_fjidx
            
            #print("new value", self._b("Hbb_fjidx")[0])
            
            # systematics
            if self.sample.isMC():
                systList     = ["".join(x) for x in itertools.product(self.systematicsBoosted, ["Up", "Down"])]
                nSystematics = len(systList)

                # return list of jet indices for systematic variations
                Hbb_fjidx_syst = self.HighestTaggerValueFatJet(tree, ptCut=250.0, msdCut=50.0, etaCut=2.5, taggerName='FatJet_particleNet_HbbvsQCD', systList=systList, Vphi=V.Phi(), Vphi_syst=Vphi_syst)
                for j in range(nSystematics):
                    fJidxName_syst = "Hbb_fjidx_{syst}".format(syst=systList[j])
                    self._b(fJidxName_syst)[0] = Hbb_fjidx_syst[j]


        return True



    def HighestTaggerValueFatJet(self, tree, ptCut, msdCut, etaCut, taggerName, systList=None, Vphi=None, Vphi_syst=None):
        fatJetMaxTagger = []
        Hbb_fjidx       = []
        Pt              = []
        Msd             = []
        systematics     = systList if systList is not None else [None]
        nSystematics    = len(systematics)
        #print(systematics)
            
        Pt_nom  = tree.FatJet_Pt
        Msd_nom = tree.FatJet_Msoftdrop
        eta     = tree.FatJet_eta
        phi     = tree.FatJet_phi
        lepFilter = tree.FatJet_lepFilter
        tagger  = getattr(tree, taggerName) 

        for syst in systematics: 
            Hbb_fjidx.append(-1)
            fatJetMaxTagger.append(-999.9)

            if syst is None:
                Pt.append(Pt_nom)
                Msd.append(Msd_nom)
            elif syst in ['jmrUp','jmrDown','jmsUp','jmsDown']:
                # for jms,jmr the branches contain the mass itself
                Pt.append(Pt_nom)
                Msd.append(getattr(tree, "FatJet_msoftdrop_"+syst))
            else:
                # for all other variations, the branches contain a multiplicative factor... duh
                Pt_scale  = getattr(tree, "FatJet_pt_{syst}".format(syst=syst)) 
                Pt.append([Pt_scale[i] for i in range(len(Pt_nom))])
                Msd_scale = getattr(tree, "FatJet_msoftdrop_{syst}".format(syst=syst)) 
                Msd.append([Msd_scale[i] for i in range(len(Msd_nom))])

        for i in range(tree.nFatJet):
            for j in range(nSystematics):
                selectedVphi = Vphi_syst[systematics[j]] if (Vphi_syst is not None and systematics[j] in Vphi_syst) else Vphi
                #if Pt[j][i] > ptCut and abs(eta[i]) < etaCut and Msd[j][i] > msdCut and lepFilter[i]:
                if Pt[j][i] > ptCut and abs(eta[i]) < etaCut and Msd[j][i] > msdCut:
                    if selectedVphi is None or abs(ROOT.TVector2.Phi_mpi_pi(selectedVphi-phi[i]))>1.57:
                        if tagger[i] > fatJetMaxTagger[j]:
                            fatJetMaxTagger[j] = tagger[i]
                            Hbb_fjidx[j] = i


        # return list for systematic variations given and scalar otherwise
        if systList is not None:
            return Hbb_fjidx
        else:
            return Hbb_fjidx[0]

