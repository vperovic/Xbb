#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class BTagWeights1lepLFCR(AddCollectionsModule):

    def __init__(self, year):
        super(BTagWeights1lepLFCR, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(BTagWeights1lepLFCR, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(BTagWeights1lepLFCR, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(BTagWeights1lepLFCR, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']


        #self.BTagWeights_Vpt = { 
        #        "isWenu":
        #            {
        #                '150250':{'bins1':[0.1241, 0.197675, 0.27125, 0.344825, 0.4184], 'bins2':[0.0, 0.05, 0.10, 0.15, 0.20], 'central':[[0.9916415810585022, 0.9584412574768066, 0.9594776630401611, 0.8701931834220886], [1.1699104309082031, 1.1641074419021606, 1.1529453992843628, 1.222765564918518], [1.3418360948562622, 1.2071154117584229, 1.7858020067214966, 1.3059296607971191], [1.3193720579147339, 1.2896544933319092, 1.5073931217193604, 1.156261920928955]] ,'uncertainty':[[0.015481258411659676, 0.021959775201441687, 0.029146397678441866, 0.031434926870391115], [0.0297030533029247, 0.04405765361908122, 0.05710833107315906, 0.07383114031098374], [0.06147262743128833, 0.07134233435258436, 0.887553726043514, 0.12645417029295156], [0.14127541030136653, 0.10774662779921003, 0.19062012291230784, 0.13835755073444653]]
        #                },
        #                '250Inf':{'bins1':[0.1241, 0.2222, 0.3203, 0.4184],'bins2':[0.0, 0.05, 0.10, 0.15],'central':[[0.9668123126029968, 0.9638965129852295, 0.9588664770126343], [1.189828872680664, 1.1365201473236084, 1.2213891744613647], [1.33958899974823, 1.1676898002624512, 1.7508957386016846]],'uncertainty':[[0.037686726812283966, 0.05857909036482687, 0.07790256278975781], [0.06546607386019454, 0.09242128509345554, 0.14583677634412293], [0.11843631460509806, 0.13640040691891173, 0.3831142684453897]]
        #                },
        #            },
        #          "isWmunu":
        #            {
        #                '150250':{'bins1':[0.1241, 0.197675, 0.27125, 0.344825, 0.4184], 'bins2':[0.0, 0.05, 0.10, 0.15, 0.20],'central':[[0.96950763463974, 0.9257828593254089, 0.9772651791572571, 0.9154996871948242], [1.197971224784851, 1.1870622634887695, 1.1659283638000488, 1.1017138957977295], [1.347110629081726, 1.090820074081421, 1.2446589469909668, 1.1037976741790771], [1.3143367767333984, 1.4231793880462646, 1.4130051136016846, 1.3336989879608154]] ,'uncertainty':[[0.013430123415103044, 0.018408307072443536, 0.025637232392714558, 0.029911695602429487], [0.027426441619909335, 0.040666066955206394, 0.04926834815214409, 0.05883166427148658], [0.053423390363718924, 0.04959367291444821, 0.08004881904794806, 0.08163593840452325], [0.11788482736535857, 0.10324053271177447, 0.13729568714705642, 0.17324257876808716]] 
        #                },
        #                '250Inf':{'bins1':[0.1241, 0.2222, 0.3203, 0.4184],'bins2':[0.0, 0.05, 0.10, 0.15],'central':[[1.0375690460205078, 1.049804925918579, 0.9331026077270508], [1.065399169921875, 1.1412078142166138, 1.431854009628296], [1.3786367177963257, 1.2600542306900024, 1.2865369319915771]],'uncertainty':[[0.03718670637573164, 0.05934540968823929, 0.06724456441118089], [0.04883411843997521, 0.08313328010227261, 0.18021919094356365], [0.10761317828460915, 0.1504036204643616, 0.20560683709190128]]
        #                },
        #            },                  
        #        }

        #self.BTagWeights_Vpt_set2 = {
        #    "isWenu":{
        #            '150250':{'central':[1.1956027746200562],'uncertainty':[0.08233978496082038]},
        #            '250Inf':{'central':[1.1352285146713257],'uncertainty':[0.10007264630140848]},
        #            },
        #    "isWmunu":{
        #            '150250':{'central':[1.3787345886230469],'uncertainty':[0.08310044206742796]},
        #            '250Inf':{'central':[1.2094062566757202],'uncertainty':[0.09234843949707723]},
        #            }
        #    }
        self.BTagWeights_Vpt = { 
                "isWenu":
                    {
                        '150250':{'bins1':[0.1241, 0.197675, 0.27125, 0.344825, 0.4184], 'bins2':[0.0, 0.05, 0.10, 0.15, 0.20], 'central':[[1.032393455505371, 0.9790868163108826, 0.9885106682777405, 0.9608139395713806], [1.1897361278533936, 1.1485553979873657, 1.111438512802124, 1.2556897401809692], [1.32746422290802, 1.2482514381408691, 1.294268012046814, 1.2709890604019165], [1.396206021308899, 1.3088346719741821, 1.3056814670562744, 1.239037036895752]] ,'uncertainty':[[0.008903485054578815, 0.012303765782621517, 0.01650676462256313, 0.02060599965505388], [0.016042303703838492, 0.022404911256513938, 0.0291224344953998, 0.04030979490451138], [0.030493420553324397, 0.0373419287648008, 0.05550929038654673, 0.07105679499533159], [0.08284879432745647, 0.05853899997576606, 0.07889014589045572, 0.08694892839804351]]
                        },
                        '250Inf':{'bins1':[0.1241, 0.2222, 0.3203, 0.4184],'bins2':[0.0, 0.05, 0.10, 0.15],'central':[[0.9153174161911011, 0.9159970283508301, 0.9729877710342407], [1.1063414812088013, 1.107075810432434, 1.128747582435608], [1.2806406021118164, 1.2665798664093018, 1.36318039894104]],'uncertainty':[[0.01577090887004394, 0.024507539934497384, 0.03653222168713467], [0.02456556707167221, 0.03858628719591998, 0.05779385104254366], [0.04465008503550442, 0.06687700294389474, 0.10117495301894036]]
                        },
                    },
                  "isWmunu":
                    {
                        '150250':{'bins1':[0.1241, 0.197675, 0.27125, 0.344825, 0.4184], 'bins2':[0.0, 0.05, 0.10, 0.15, 0.20],'central':[[0.9952577352523804, 0.9586493968963623, 0.9868407845497131, 0.9692205786705017], [1.2005494832992554, 1.1363950967788696, 1.2040292024612427, 1.1023597717285156], [1.3293867111206055, 1.2035988569259644, 1.268398404121399, 1.2267717123031616], [1.3532756567001343, 1.3478511571884155, 1.2754406929016113, 1.1867433786392212]] ,'uncertainty':[[0.007544620249150835, 0.01063633903928857, 0.014232926074985476, 0.01854630874821089], [0.014020495769940241, 0.01993332756123099, 0.026788496765468107, 0.031920598356033586], [0.026672664755536184, 0.032304169109274146, 0.045857788115029566, 0.055357931644216], [0.06765451046077696, 0.04977876186475055, 0.06180973309593239, 0.08051671352515215]] 
                        },
                        '250Inf':{'bins1':[0.1241, 0.2222, 0.3203, 0.4184],'bins2':[0.0, 0.05, 0.10, 0.15],'central':[[0.9631709456443787, 0.9335956573486328, 0.949765145778656], [1.1056307554244995, 1.120156168937683, 1.1143031120300293], [1.2962968349456787, 1.2111239433288574, 1.2706496715545654]] ,'uncertainty':[[0.014145394572878982, 0.02177678874897236, 0.031586274549974716], [0.021463941741209167, 0.034404932971556314, 0.05007309400452489], [0.0384816368726588, 0.056567791372198466, 0.08778691038410676]]
                        },
                    },                  
                }

        self.BTagWeights_Vpt_set2 = {
            "isWenu":{
                    '150250':{'central':[1.2400062084197998],'uncertainty':[0.04661834397042152]},
                    '250Inf':{'central':[1.094275951385498],'uncertainty':[0.04637505262825383]},
                    },
            "isWmunu":{
                    '150250':{'central':[1.3130080699920654],'uncertainty':[0.044211673597440306]},
                    '250Inf':{'central':[1.1986638307571411],'uncertainty':[0.04101569251931881]},
                    }
            }
        
        self.get_BTagWeights_Vptdict = {'150':'150250','250':'250Inf'}
        self.BTagWeights_Vpt_branches = ["WJet_btagDeepBVHbb2D"]

        if self.sample.isMC():
            self.maxNjet   = 256

            for var in ["Jet_btagDeepB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isWmunu","isWenu"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["nGenBpt25eta2p6","nGenDpt25eta2p6"]:
                self.existingBranches[var] = array.array('i', [100])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.BTagWeights_Vpt_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                    
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies(self, attr):
        isWJbtag_udsgc = False
        #print(attr["hJidx"][0])
        #print(attr["hJidx"][1])
        #print(attr["Jet_btagDeepB"][attr["hJidx"][0]])
        #print(attr["Jet_btagDeepB"][attr["hJidx"][1]])
        #print(attr["nGenBpt25eta2p6"][0])
        #print(attr["nGenDpt25eta2p6"][0])
        if (any([x in self.sample.identifier for x in ['WJetsToLNu']]) and any([s in self.sample.identifier for s in ['amcnloFXFX','amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if ((attr["Jet_btagDeepB"][attr["hJidx"][0]] > 0.1241 and attr["Jet_btagDeepB"][attr["hJidx"][0]] < 0.4184  and attr["Jet_btagDeepB"][attr["hJidx"][1]] < 0.4184) and ((attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1) or (attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]>0))):
                isWJbtag_udsgc = True
                #print(attr["hJidx"][0])
                #print(attr["hJidx"][1])
                #print(attr["Jet_btagDeepB"][attr["hJidx"][0]])
                #print(attr["Jet_btagDeepB"][attr["hJidx"][1]])
                #print(attr["nGenBpt25eta2p6"][0])
                #print(attr["nGenDpt25eta2p6"][0])
 
        #print("final: ",isWJbtag_udsgc)
        #print("----------------------------------------------------------------")
        return isWJbtag_udsgc

    def get_lowerBound(self,value,bins):
        j = 0
        #print("value is",value)
        #print("bins are ",bins)
        while not (value<bins[j] or value>=250):
            #print("value is ", value, "and bins are ",bins[j])
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        dRbbWeight_Vpt  = []
        dRbbWeight_Incl = []
 
        btag_hJidx0        = attr["Jet_btagDeepB"][attr["hJidx"][0]]
        btag_hJidx1        = attr["Jet_btagDeepB"][attr["hJidx"][1]]
        BTagWeights_Vptbin = self.get_BTagWeights_Vptdict[self.get_lowerBound(attr["V_pt"][0],[150,250])]

        #print(attr["V_pt"])
       
 
        j=0
        channel = None
        if attr["isWenu"][0] == 1: channel = "isWenu"
        if attr["isWmunu"][0] == 1: channel = "isWmunu"

        if (btag_hJidx0 < 0.0 or btag_hJidx1 < 0.0):
            return 1.0,1.0,1.0

        if (btag_hJidx1 >= self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins2"][-1]):
            nom = self.BTagWeights_Vpt_set2[channel][BTagWeights_Vptbin]['central'][0]
            unc = self.BTagWeights_Vpt_set2[channel][BTagWeights_Vptbin]['uncertainty'][0]
            #print("set2 ",nom,nom+unc,nom-unc)
            return nom,nom+unc,nom-unc

        while not (btag_hJidx0 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins1"][j]):
            #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][j])
            #print(btag_hJidx0)
            j+=1
            #print("j is now amining for ",j)
        k=0
        while not (btag_hJidx1 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins2"][k]):
            k+=1
        #print(j-1)
        #print(k-1)
        #print("channel ",channel," BTagWeights_Vptbin ",BTagWeights_Vptbin," j-1 ",j-1," k-1 ",k-1)
        #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1])
        nom = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1]
        up = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] + self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        down = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] - self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        #print("set1 ",nom,up,down)        
        return  nom,up,down 

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)
            attr = {}
            #for var in ["V_pt","hJidx","isWenu","isWmunu","Jet_btagDeepB","nGenBpt25eta2p6","nGenDpt25eta2p6","isZnn"]: 
            for var in ["V_pt","hJidx","isWenu","isWmunu","Jet_btagDeepB","nGenBpt25eta2p6","nGenDpt25eta2p6"]: 
                attr[var] = self.existingBranches[var]
            #print(attr["isWmunu"])        
            if self.applies(attr):
                nom,up,down = self.get_eventWeight(attr)
                self._b("WJet_btagDeepBVHbb2D")[0]         = nom
                self._b("WJet_btagDeepBVHbb2D"+"Up")[0]    = up
                self._b("WJet_btagDeepBVHbb2D"+"Down")[0]  = down
                #print("-------------------------------------------------")
            else:
                self._b("WJet_btagDeepBVHbb2D")[0]         = 1.0
                self._b("WJet_btagDeepBVHbb2D"+"Up")[0]    = 1.0
                self._b("WJet_btagDeepBVHbb2D"+"Down")[0]  = 1.0
                #print("-------------------------------------------------")


if __name__=='__main__':

    config = XbbConfigReader.read('Wlv2018')
    info = ParseInfo(config=config)
    sample = [x for x in info if x.identifier == 'WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8'][0]
    print(sample)

    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_94b42e05db46cb9fd44285b60e0ba8b57d34232afdeef2661e673e5f_000000_000000_0000_0_5c5fb21d0e20a7f021911ce80109ccd1fa39014fff8bca8fc463de97.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Wlv/mva/18oct20_all_2DbtagdRbbDYWeightsfromLFCR/WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_bb21dac9dc44e46adf956d61d5b485c39341b737ba249606a80eb2f6_000000_000000_0000_0_09ffe41113d97125a40885075110dac51c0f173e112396ca28a14ce2.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYBJetsToLL_M-50_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8/tree_a6b49dbb202048bd17cdf3f392ef3c2813bf6388d3275d8708169f7b_000000_000000_0000_0_f11fc63b7542561838a2fc3df60d8f8f1f67dd0c6fd24dd343324e76.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    w = BTagWeights1lepLFCR("2018")
    w.customInit({'sampleTree': sampleTree, 'sample': sample, 'config': config})
    sampleTree.addOutputBranches(w.getBranches())
    #histograms={}
    #for jec in w.JEC_reduced:
        
    #for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
    #    histograms[var] = {}
    #    for syst in w.JEC_reduced:
    #        histograms[var][syst] = {}
    #        for Q in ['Up','Down']:
    #            histograms[var][syst][Q]=ROOT.TH1F(var+syst+Q, var+syst+Q, 400, -2.0, 2.0 )

    n=0 
    #var = "MET_phi"
    for event in sampleTree:
        n=n+1
        w.processEvent(event)
        if n==30: break

   # with open('jec_validate_'+var+'.csv', 'w') as file:
   #     writer = csv.writer(file)
   #     writer.writerow(["event","run","Q",'true_jesAbsolute','jesAbsolute','AT','diff','true_jesAbsolute_2018','jesAbsolute_2018','AT','diff','true_jesBBEC1','jesBBEC1','AT','diff','true_jesBBEC1_2018','jesBBEC1_2018','AT','diff','true_jesEC2','jesEC2','AT','diff','true_jesEC2_2018','jesEC2_2018','AT','diff','true_jesHF','jesHF','AT','diff','true_jesHF_2018','jesHF_2018','AT','diff','true_jesRelativeSample_2018','jesRelativeSample_2018','AT','diff'])
   #     for event in sampleTree:
   #         n=n+1
   #         event,run,true_attr, attr = w.processEvent(event)
   #         for Q in ['Up','Down']:
   #             njet = len(true_attr[var][w.JEC_reduced[0]][Q])
   #             if njet>1:
   #                 for i in range(njet):
   #                     writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][i],attr[var]['jesAbsolute'][Q][i],'','',true_attr[var]['jesAbsolute_2018'][Q][i],attr[var]['jesAbsolute_2018'][Q][i],'','',true_attr[var]['jesBBEC1'][Q][i],attr[var]['jesBBEC1'][Q][i],'','',true_attr[var]['jesBBEC1_2018'][Q][i],attr[var]['jesBBEC1_2018'][Q][i],'','',true_attr[var]['jesEC2'][Q][i],attr[var]['jesEC2'][Q][i],'','',true_attr[var]['jesEC2_2018'][Q][i],attr[var]['jesEC2_2018'][Q][i],'','',true_attr[var]['jesHF'][Q][i],attr[var]['jesHF'][Q][i],'','',true_attr[var]['jesHF_2018'][Q][i],attr[var]['jesHF_2018'][Q][i],'','',true_attr[var]['jesRelativeSample_2018'][Q][i],attr[var]['jesRelativeSample_2018'][Q][i],'',''])
   #             else:
   #                 writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][0],attr[var]['jesAbsolute'][Q][0],'','',true_attr[var]['jesAbsolute_2018'][Q][0],attr[var]['jesAbsolute_2018'][Q][0],'','',true_attr[var]['jesBBEC1'][Q][0],attr[var]['jesBBEC1'][Q][0],'','',true_attr[var]['jesBBEC1_2018'][Q][0],attr[var]['jesBBEC1_2018'][Q][0],'','',true_attr[var]['jesEC2'][Q][0],attr[var]['jesEC2'][Q][0],'','',true_attr[var]['jesEC2_2018'][Q][0],attr[var]['jesEC2_2018'][Q][0],'','',true_attr[var]['jesHF'][Q][0],attr[var]['jesHF'][Q][0],'','',true_attr[var]['jesHF_2018'][Q][0],attr[var]['jesHF_2018'][Q][0],'','',true_attr[var]['jesRelativeSample_2018'][Q][0],attr[var]['jesRelativeSample_2018'][Q][0],'',''])

   #         #print("----------events over------------")
   #         if n==5: break

   # f = TFile("delete.root","RECREATE")

   # for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
   #     for syst in w.JEC_reduced:
   #         for Q in ['Up','Down']:
   #             histograms[var][syst][Q].Write()
   # f.Close()
