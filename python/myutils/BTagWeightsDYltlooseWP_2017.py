#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class BTagWeightsDYltlooseWP_2017(AddCollectionsModule):

    def __init__(self, year):
        super(BTagWeightsDYltlooseWP_2017, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(BTagWeightsDYltlooseWP_2017, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(BTagWeightsDYltlooseWP_2017, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(BTagWeightsDYltlooseWP_2017, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']


        self.BTagWeights_Vpt = { 
                "isZee":
                    {
                        '75150':{'bins':[0.0, 0.05073333, 0.10146667, 0.1522], 'central':[[0.8323129415512085, 0.8622650504112244, 0.8424711227416992], [0.0, 0.9233845472335815, 0.9671481847763062], [0.0, 0.0, 1.0082050561904907]]   ,'uncertainty':[[0.010581191953755162, 0.014037640986097857, 0.024367892024794127], [0.0, 0.03517897639522461, 0.046671339847980116], [0.0, 0.0, 0.11151087729824409]] 
                        },
                        '150250':{'bins':[0.0, 0.05073333, 0.10146667, 0.1522] , 'central': [[0.8250012397766113, 0.8742640614509583, 0.8379480242729187], [0.0, 1.0829013586044312, 0.9009799957275391], [0.0, 0.0, 0.9503080248832703]],'uncertainty':[[0.02061493733363416, 0.02683016186808812, 0.044042322180129316], [0.0, 0.07135696871821508, 0.07023767006791524], [0.0, 0.0, 0.16690265200660914]]
                        },
                        '250Inf':{'bins': [0.0, 0.05073333, 0.10146667, 0.1522],'central':[[0.8471426367759705, 0.8739399909973145, 0.8891540765762329], [0.0, 0.9584171175956726, 0.934523344039917], [0.0, 0.0, 1.0919986963272095]] ,'uncertainty':[[0.052054584658899984, 0.06063241010354099, 0.10664231940184121], [0.0, 0.1199091548047893, 0.14870563449809487], [0.0, 0.0, 0.3876561420397519]]
                        },
                    },
                  "isZmm":
                    {
                        '75150':{'bins':[0.0, 0.05073333, 0.10146667, 0.1522],'central':[[0.892299234867096, 0.9670608639717102, 0.9926850199699402], [0.0, 1.0784859657287598, 1.0587868690490723], [0.0, 0.0, 1.3376352787017822]], 'uncertainty':[[0.009733991935459452, 0.013758968487230117, 0.024393767320687588], [0.0, 0.03600900087675146, 0.04267100734465407], [0.0, 0.0, 0.1462016562253055]]
                        },
                        '150250':{'bins':[0.0, 0.05073333, 0.10146667, 0.1522] ,'central':[[0.8591960072517395, 0.9091895818710327, 0.960020899772644], [0.0, 1.0005894899368286, 0.9867532253265381], [0.0, 0.0, 1.0940577983856201]] ,'uncertainty':[[0.019184743712594685, 0.024444248636714933, 0.044816871518908896], [0.0, 0.05519784482632895, 0.06592318720429802], [0.0, 0.0, 0.16096067243983708]] 
                        },
                        '250Inf':{'bins':[0.0, 0.05073333, 0.10146667, 0.1522],'central':[[0.7410356998443604, 0.8860520124435425, 0.8185514807701111], [0.0, 0.896731436252594, 1.0882515907287598], [0.0, 0.0, 0.9835688471794128]] ,'uncertainty':[[0.04371668242415216, 0.05527180556811743, 0.08926458354963636], [0.0, 0.10414173107063088, 0.14304583884747601], [0.0, 0.0, 0.31584259888623045]] 
                        },
                    },                  
                }
        
        self.get_BTagWeights_Vptdict = {'75':'75150', '150':'150250','250':'250Inf'}
        self.BTagWeights_Vpt_branches = ["Jet_btagDeepBVHbb2D"]

        if self.sample.isMC():
            self.maxNjet   = 256

            for var in ["Jet_btagDeepB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isZmm","isZee"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["nGenBpt25eta2p6","nGenDpt25eta2p6"]:
                self.existingBranches[var] = array.array('i', [100])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.BTagWeights_Vpt_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                    
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies(self, attr):
        isDYbtag_udsg = False
        #print(attr["hJidx"][0])
        #print(attr["hJidx"][1])
        #print(attr["Jet_btagDeepB"][attr["hJidx"][0]])
        #print(attr["Jet_btagDeepB"][attr["hJidx"][1]])
        #print(attr["nGenBpt25eta2p6"][0])
        #print(attr["nGenDpt25eta2p6"][0])
        if (any([x in self.sample.identifier for x in ['DYJets','DY1Jets','DY2Jets']]) and any([s in self.sample.identifier for s in ['amcnloFXFX','amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if ((attr["Jet_btagDeepB"][attr["hJidx"][0]] < 0.1522 and attr["Jet_btagDeepB"][attr["hJidx"][1]] < 0.1522) and (attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1)):
                isDYbtag_udsg = True
                #print(attr["hJidx"][0])
                #print(attr["hJidx"][1])
                #print(attr["Jet_btagDeepB"][attr["hJidx"][0]])
                #print(attr["Jet_btagDeepB"][attr["hJidx"][1]])
                #print(attr["nGenBpt25eta2p6"][0])
                #print(attr["nGenDpt25eta2p6"][0])
 
        #print("final: ",isDYbtag_udsg)
        #print("----------------------------------------------------------------")
        return isDYbtag_udsg

    def get_lowerBound(self,value,bins):
        j = 0
        #print("value is",value)
        #print("bins are ",bins)
        while not (value<bins[j] or value>=250):
            #print("value is ", value, "and bins are ",bins[j])
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        dRbbWeight_Vpt  = []
        dRbbWeight_Incl = []
 
        btag_hJidx0        = attr["Jet_btagDeepB"][attr["hJidx"][0]]
        btag_hJidx1        = attr["Jet_btagDeepB"][attr["hJidx"][1]]
        BTagWeights_Vptbin = self.get_BTagWeights_Vptdict[self.get_lowerBound(attr["V_pt"][0],[75,150,250])]
 
        j=0
        channel = None
        if attr["isZee"][0] == 1: channel = "isZee"
        if attr["isZmm"][0] == 1: channel = "isZmm"

        if (btag_hJidx0 < 0.0 or btag_hJidx1 < 0.0):
            return 1.0,1.0,1.0

        while not (btag_hJidx0 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][j]):
            #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][j])
            #print(btag_hJidx0)
            j+=1
            #print("j is now amining for ",j)
        k=0
        while not (btag_hJidx1 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][k]):
            k+=1
        #print(j-1)
        #print(k-1)
        #print("channel ",channel," BTagWeights_Vptbin ",BTagWeights_Vptbin," j-1 ",j-1," k-1 ",k-1)
        #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1])
        nom = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1]
        up = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] + self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        down = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] - self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        #print(nom,up,down)        
        return  nom,up,down 

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)
            attr = {}
            for var in ["V_pt","hJidx","isZee","isZmm","Jet_btagDeepB","nGenBpt25eta2p6","nGenDpt25eta2p6"]: 
                attr[var] = self.existingBranches[var]
            if self.applies(attr):
                nom,up,down = self.get_eventWeight(attr)
                self._b("Jet_btagDeepBVHbb2D")[0]         = nom
                self._b("Jet_btagDeepBVHbb2D"+"Up")[0]    = up
                self._b("Jet_btagDeepBVHbb2D"+"Down")[0]  = down
            else:
                self._b("Jet_btagDeepBVHbb2D")[0]         = 1.0
                self._b("Jet_btagDeepBVHbb2D"+"Up")[0]    = 1.0
                self._b("Jet_btagDeepBVHbb2D"+"Down")[0]  = 1.0

if __name__=='__main__':

    config = XbbConfigReader.read('Zll2018')
    info = ParseInfo(config=config)
    sample = [x for x in info if x.identifier == 'DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8'][0]
    print(sample)

    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_94b42e05db46cb9fd44285b60e0ba8b57d34232afdeef2661e673e5f_000000_000000_0000_0_5c5fb21d0e20a7f021911ce80109ccd1fa39014fff8bca8fc463de97.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8/tree_0b9e5ec9f1d4328fc7ef364ebb5dd7364200ce37948e3753c76e6133_000000_000000_0000_5_fadbb45f88e40b3f3f44b0585861b2c4f87d08c5be7feda31913e251.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYBJetsToLL_M-50_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8/tree_a6b49dbb202048bd17cdf3f392ef3c2813bf6388d3275d8708169f7b_000000_000000_0000_0_f11fc63b7542561838a2fc3df60d8f8f1f67dd0c6fd24dd343324e76.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    w = BTagWeightsDYltlooseWP("2018")
    w.customInit({'sampleTree': sampleTree, 'sample': sample, 'config': config})
    sampleTree.addOutputBranches(w.getBranches())
    #histograms={}
    #for jec in w.JEC_reduced:
        
    #for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
    #    histograms[var] = {}
    #    for syst in w.JEC_reduced:
    #        histograms[var][syst] = {}
    #        for Q in ['Up','Down']:
    #            histograms[var][syst][Q]=ROOT.TH1F(var+syst+Q, var+syst+Q, 400, -2.0, 2.0 )

    n=0 
    #var = "MET_phi"
    for event in sampleTree:
        n=n+1
        w.processEvent(event)
        if n==10: break

   # with open('jec_validate_'+var+'.csv', 'w') as file:
   #     writer = csv.writer(file)
   #     writer.writerow(["event","run","Q",'true_jesAbsolute','jesAbsolute','AT','diff','true_jesAbsolute_2018','jesAbsolute_2018','AT','diff','true_jesBBEC1','jesBBEC1','AT','diff','true_jesBBEC1_2018','jesBBEC1_2018','AT','diff','true_jesEC2','jesEC2','AT','diff','true_jesEC2_2018','jesEC2_2018','AT','diff','true_jesHF','jesHF','AT','diff','true_jesHF_2018','jesHF_2018','AT','diff','true_jesRelativeSample_2018','jesRelativeSample_2018','AT','diff'])
   #     for event in sampleTree:
   #         n=n+1
   #         event,run,true_attr, attr = w.processEvent(event)
   #         for Q in ['Up','Down']:
   #             njet = len(true_attr[var][w.JEC_reduced[0]][Q])
   #             if njet>1:
   #                 for i in range(njet):
   #                     writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][i],attr[var]['jesAbsolute'][Q][i],'','',true_attr[var]['jesAbsolute_2018'][Q][i],attr[var]['jesAbsolute_2018'][Q][i],'','',true_attr[var]['jesBBEC1'][Q][i],attr[var]['jesBBEC1'][Q][i],'','',true_attr[var]['jesBBEC1_2018'][Q][i],attr[var]['jesBBEC1_2018'][Q][i],'','',true_attr[var]['jesEC2'][Q][i],attr[var]['jesEC2'][Q][i],'','',true_attr[var]['jesEC2_2018'][Q][i],attr[var]['jesEC2_2018'][Q][i],'','',true_attr[var]['jesHF'][Q][i],attr[var]['jesHF'][Q][i],'','',true_attr[var]['jesHF_2018'][Q][i],attr[var]['jesHF_2018'][Q][i],'','',true_attr[var]['jesRelativeSample_2018'][Q][i],attr[var]['jesRelativeSample_2018'][Q][i],'',''])
   #             else:
   #                 writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][0],attr[var]['jesAbsolute'][Q][0],'','',true_attr[var]['jesAbsolute_2018'][Q][0],attr[var]['jesAbsolute_2018'][Q][0],'','',true_attr[var]['jesBBEC1'][Q][0],attr[var]['jesBBEC1'][Q][0],'','',true_attr[var]['jesBBEC1_2018'][Q][0],attr[var]['jesBBEC1_2018'][Q][0],'','',true_attr[var]['jesEC2'][Q][0],attr[var]['jesEC2'][Q][0],'','',true_attr[var]['jesEC2_2018'][Q][0],attr[var]['jesEC2_2018'][Q][0],'','',true_attr[var]['jesHF'][Q][0],attr[var]['jesHF'][Q][0],'','',true_attr[var]['jesHF_2018'][Q][0],attr[var]['jesHF_2018'][Q][0],'','',true_attr[var]['jesRelativeSample_2018'][Q][0],attr[var]['jesRelativeSample_2018'][Q][0],'',''])

   #         #print("----------events over------------")
   #         if n==5: break

   # f = TFile("delete.root","RECREATE")

   # for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
   #     for syst in w.JEC_reduced:
   #         for Q in ['Up','Down']:
   #             histograms[var][syst][Q].Write()
   # f.Close()
