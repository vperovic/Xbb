#!/usr/bin/env python
import ROOT
import numpy as np
import array
import os
from BranchTools import Collection
from BranchTools import AddCollectionsModule

class newBranchesZvv(AddCollectionsModule):

    def __init__(self,year):
        super(newBranchesZvv, self).__init__()
        self.year = int(year)

    def customInit(self, initVars):
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.addBranch('hJets_pt_reg_max')
        self.addBranch('hJets_pt_reg_min')
        self.addBranch('hJets_pt_reg_0')
        self.addBranch('hJets_pt_reg_1')
        self.addBranch('hJets_btag_0')
        self.addBranch('hJets_btag_1')
        self.addBranch('nAddJetQCD')
        self.addBranch('nAddJet30')
        self.addBranch('minDphiJetMet')
        self.addBranch('dPhiMetTkMet')
        self.addBranch('dPhiVH')
        self.addBranch('dPhiMetH')
        self.addBranch('nVetoElectrons')
        self.addBranch('nVetoMuons')

    def processEvent(self, tree):
        # if current entry has not been processed yet
        if not self.hasBeenProcessed(tree):
            self.markProcessed(tree)

            self._b('hJets_pt_reg_max')[0] = 0.0
            self._b('hJets_pt_reg_min')[0] = 0.0
            self._b('hJets_pt_reg_0')[0] = 0.0
            self._b('hJets_pt_reg_1')[0] = 0.0
            self._b('hJets_btag_0')[0] = 0.0
            self._b('hJets_btag_1')[0] = 0.0
            self._b('nAddJetQCD')[0] = 0.0
            self._b('nAddJet30')[0] = 0.0
            self._b('minDphiJetMet')[0] = 0.0
            self._b('dPhiMetTkMet')[0] = 0.0
            self._b('dPhiVH')[0] = 0.0
            self._b('dPhiMetH')[0] = 0.0
            self._b('nVetoElectrons')[0] = 0.0
            self._b('nVetoMuons')[0] = 0.0

            if -1 not in [tree.hJidx[0],tree.hJidx[1]]  and len(tree.hJidx)>0:
                if self.year in [2017,2018]:
                    self._b('hJets_pt_reg_max')[0] = max(tree.Jet_PtReg[tree.hJidx[0]],tree.Jet_PtReg[tree.hJidx[1]])
                    self._b('hJets_pt_reg_min')[0] = min(tree.Jet_PtReg[tree.hJidx[0]],tree.Jet_PtReg[tree.hJidx[1]])
                    self._b('hJets_pt_reg_0')[0]   = tree.Jet_PtReg[tree.hJidx[0]] 
                    self._b('hJets_pt_reg_1')[0]   = tree.Jet_PtReg[tree.hJidx[1]]
                    self._b('hJets_btag_0')[0]   = tree.Jet_btagDeepB[tree.hJidx[0]]
                    self._b('hJets_btag_1')[0]   = tree.Jet_btagDeepB[tree.hJidx[1]]
                    self._b('nAddJetQCD')[0]   = len([tree.Jet_pt[i] for i in range(len(tree.Jet_pt)) if (abs(ROOT.TVector2.Phi_mpi_pi(tree.Jet_phi[i]-tree.V_phi))<0.5) and tree.Jet_Pt[i]>30 and (tree.Jet_puId[i]>6 or tree.Jet_Pt[i]>50.0) and tree.Jet_lepFilter[i]])
                    self._b('nAddJet30')[0]   = len([tree.Jet_pt[i] for i in range(len(tree.Jet_pt)) if tree.Jet_Pt[i]>30 and (abs(tree.Jet_eta[i])<2.4) and (tree.Jet_puId[i]>6 or tree.Jet_Pt[i]>50.0) and tree.Jet_lepFilter[i] and (i not in [tree.hJidx[0],tree.hJidx[1]])])
                    self._b('minDphiJetMet')[0]   = min([abs(ROOT.TVector2.Phi_mpi_pi(tree.Jet_phi[tree.hJidx[i]]-tree.MET_Phi)) for i in range(len(tree.hJidx))])
                    self._b('dPhiMetTkMet')[0]   = abs(ROOT.TVector2.Phi_mpi_pi(tree.MET_Phi-tree.TkMET_phi))
                    self._b('dPhiVH')[0]   = abs(ROOT.TVector2.Phi_mpi_pi(tree.V_phi-tree.H_phi))
                    self._b('dPhiMetH')[0]   = abs(ROOT.TVector2.Phi_mpi_pi(tree.MET_Phi-tree.H_phi))
                    self._b('nVetoElectrons')[0]   = len([tree.Electron_pt[i] for i in range(len(tree.Electron_pt)) if tree.Electron_pt[i]>6.5 and abs(tree.Electron_eta[i])<2.5 and tree.Electron_pfRelIso03_all[i]<0.4 and abs(tree.Electron_dz[i])<0.2 and abs(tree.Electron_dxy[i])<0.05 and tree.Electron_lostHits[i]<1.0])
                    self._b('nVetoMuons')[0]   = len([tree.Muon_pt[i] for i in range(len(tree.Muon_pt)) if tree.Muon_pt[i]>4.5 and abs(tree.Muon_eta[i])<2.4 and tree.Muon_pfRelIso04_all[i]<0.4 and abs(tree.Muon_dz[i])<0.2 and abs(tree.Muon_dxy[i])<0.05])

                   

                   #Actual formula:
                   #'nAddJetQCD':       {'formula': 'Sum$(abs(TVector2::Phi_mpi_pi(Jet_phi-V_phi))<0.5&&Jet_Pt>30&&Jet_puId>0&&Jet_lepFilter)', 'type': 'i'},                                                                          'nAddJet30':        {'formula': 'Sum$(Jet_Pt>30&&abs(Jet_eta)<2.5&&Jet_puId>0&&Jet_lepFilter&&Iteration$!=Alt$(hJidx[0],0)&&Iteration$!=Alt$(hJidx[1],0))', 'type': 'i'},  

