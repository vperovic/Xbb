#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class NLORbblt1Weights_2017(AddCollectionsModule):

    def __init__(self, year):
        super(NLORbblt1Weights_2017, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights_2017, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights_2017, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(NLORbblt1Weights_2017, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']

        # Weights derived using V+jets in HF CR
        #self.dRbbWeight_VPt = { 
        #        "isZee":
        #            {
        #                '75150':{'bins':[0.4,0.6,0.8,1.0] ,'central':[1.0558, 1.1573, 1.0902],'uncertainty':[0.3168, 0.2279, 0.1579]
        #                },
        #                '150250':{'bins':[0.4,0.6,0.8,1.0],'central':[1.4507, 1.2895, 1.1952],'uncertainty':[0.2456, 0.1509, 0.1783]
        #                },
        #                '250Inf':{'bins':[0.4,0.6,1.0],'central':[1.2356, 1.7847],'uncertainty':[0.1898, 0.2128]
        #                },
        #            },
        #          "isZmm":
        #            {
        #                '75150':{'bins':[0.4,0.6,0.8,1.0] ,'central':[1.2059, 1.2989, 1.3342],'uncertainty':[0.2916, 0.1856, 0.2012]
        #                },
        #                '150250':{'bins':[0.4,0.6,0.8,1.0],'central':[1.5755, 1.66, 1.782],'uncertainty':[0.2266, 0.1874, 0.2922]
        #                },
        #                '250Inf':{'bins':[0.4,0.6,0.8,1.0],'central':[1.3067, 1.2607, 1.0961],'uncertainty':[0.1695, 0.1906, 0.2277]
        #                },
        #            },                  
        #        }
        #self.dRbbWeight_Incl = {
        #          "isZee":
        #            {
        #                'InclPt':{'bins':[0.4,0.6,0.8,1.0] ,'central':[1.2642, 1.2682, 1.1561],'uncertainty':[0.1645, 0.1363, 0.1231]
        #                },
        #            },
        #          "isZmm":
        #            {
        #                'InclPt':{'bins':[0.4,0.6,0.8,1.0] ,'central':[1.3839, 1.4272, 1.419],'uncertainty':[0.1476, 0.1254, 0.1637]
        #                },
        #            },                  
        #        }
        
        # Weights derived from V+jets in LF CR
        self.dRbbWeight_VPt = {
                "isDY":
                    {
                    "isZee":
                        {
                            '75150':{'bins': [0.4,0.8,1.0],'central':[1.2041, 1.5204] ,'uncertainty':[0.3107, 0.2366]
                            },
                            '150250':{'bins':[0.4,0.8,1.0] ,'central':[1.1663, 1.2399] ,'uncertainty':[0.0897, 0.0638]
                            },
                            '250Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central':[0.9606, 1.2511, 1.2056] ,'uncertainty':[0.0831, 0.0666, 0.066]
                            },
                        },
                      "isZmm":
                        {
                            '75150':{'bins':[0.4,0.8,1.0] ,'central': [1.0706, 1.1756] ,'uncertainty':[0.1927, 0.1269]
                            },
                            '150250':{'bins':[0.4,0.6,0.8,1.0] ,'central':[1.1069, 1.4392, 1.3762] ,'uncertainty':[0.2375, 0.1026, 0.0623]
                            },
                            '250Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central':[1.3744, 1.3899, 1.3458] ,'uncertainty':[0.09, 0.0646, 0.0655]
                            },
                        },
                    },
                "isWJets":
                    {
                      "isWenu":
                        {
                            '150250':{'bins': [0.4,0.6,0.8,1.0] ,'central': [1.0953, 1.1222, 1.1169], 'uncertainty':[0.0346, 0.027, 0.0297]
                            },
                            '250Inf':{'bins': [0.4,0.6,0.8,1.0], 'central': [1.0533, 1.1563, 1.0749], 'uncertainty':[0.0306, 0.0336, 0.0367]
                            },
                        },
                      "isWmunu":
                        {   
                            '150250':{'bins': [0.4,0.6,0.8,1.0], 'central': [1.1614, 1.2678, 1.2502], 'uncertainty':[0.0319, 0.0272, 0.0301]
                            },
                            '250Inf':{'bins': [0.4,0.6,0.8,1.0], 'central': [1.0947, 1.1467, 1.1123], 'uncertainty': [0.0283, 0.0295, 0.0339]
                            },
                        },
                      "isZnn":
                        {   
                            '150250':{'bins': [0.4,0.6,0.8,1.0], 'central': [1.1324, 1.2018, 1.1898], 'uncertainty':[0.0235, 0.0192, 0.0213]
                            },
                            '250Inf':{'bins': [0.4,0.6,0.8,1.0], 'central': [1.0767, 1.1508, 1.0957], 'uncertainty':[0.0208, 0.0222, 0.0249]
                            },
                        },
                    },    
                "isZJets":
                    {
                    "isZnn":
                        {
                            '150250':{'bins':[0.4,0.6,0.8,1.0] ,'central': [1.1998, 1.541, 1.2748],'uncertainty':[0.1796, 0.2161, 0.209]
                            },
                            '250Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central': [1.3107, 1.6236, 1.9085],'uncertainty':[0.1466, 0.1846, 0.2532]
                            },
                        },
                    },
                }
                     # "isWenu":
                     #   {
                     #       '150250':{'bins': [0.4,0.6,0.8,1.0] ,'central': [0.9943, 1.099, 1.1479], 'uncertainty':[0.0312, 0.0259, 0.0288]
                     #       },
                     #       '250Inf':{'bins': [0.4,0.6,0.8,1.0], 'central':[1.046, 1.0615, 1.1645], 'uncertainty':[0.0448, 0.0415, 0.0567] 
                     #       },
                     #   },
                     # "isWmunu":
                     #   {   
                     #       '150250':{'bins': [0.4,0.6,0.8,1.0], 'central': [1.0409, 1.1758, 1.164], 'uncertainty':[0.0289, 0.0246, 0.0264]
                     #       },
                     #       '250Inf':{'bins': [0.4,0.6,0.8,1.0], 'central':[1.0608, 1.1448, 1.1597], 'uncertainty':[0.0406, 0.0417, 0.0496]  
                     #       },
                     #   },
                     # "isZnn":
                     #   {   
                     #       '150250':{'bins': [0.4,0.6,0.8,1.0], 'central': [1.0185, 1.1384, 1.1563], 'uncertainty':[0.015, 0.0126, 0.0138]
                     #       },
                     #       '250Inf':{'bins': [0.4,0.6,0.8,1.0], 'central': [1.0538, 1.103, 1.1619], 'uncertainty':[0.0213, 0.0208, 0.0265]
                     #       },
                     #   },

        self.dRbbWeight_VPt["isDY"]["isWenu"] = self.dRbbWeight_VPt["isDY"]["isZee"]
        self.dRbbWeight_VPt["isDY"]["isWmunu"] = self.dRbbWeight_VPt["isDY"]["isZmm"]

        #print("self.dRbbWeight_VPt ",self.dRbbWeight_VPt)

        '''        
        self.dRbbWeight_Incl = {
                  "isZee":
                    {
                        'InclPt':{'bins':[0.4,0.6,0.8,1.0], 'central':[0.9505, 1.1034, 1.2398] ,'uncertainty':[0.0712, 0.053, 0.0522]
                        },
                    },
                  "isZmm":
                    {
                        'InclPt':{'bins':[0.4,0.6,0.8,1.0], 'central':[1.237, 1.2726, 1.1743] ,'uncertainty':[0.102, 0.0535, 0.0413]
                        },
                    },                  
                  "isWenu":
                    {
                        'InclPt':{'bins':[0.4,0.6,0.8,1.0], 'central':[1.0116, 1.0896, 1.1514] ,'uncertainty':[0.0256, 0.022, 0.0257]
                        },
                    },
                  "isWmunu":
                    {
                        'InclPt':{'bins':[0.4,0.6,0.8,1.0], 'central':[1.0478, 1.1682, 1.1631] ,'uncertainty':[0.0235, 0.0212, 0.0233]
                        },
                    },
                  "isZnn":
                    {
                        'InclPt':{'bins':[0.4,0.6,0.8,1.0], 'central':[1.0305, 1.1296, 1.1575] ,'uncertainty':[0.0123, 0.0108, 0.0122]
                        },
                    }
                }
        '''        
        self.get_dRbbdict = {'75':'75150', '150':'150250','250':'250Inf'}
        #self.dRbb_branches = ["dRbbWeight_Vpt","dRbbWeight_Incl"]
        self.dRbb_branches = ["dRbbWeight_Vpt"]

        if self.sample.isMC():
            self.maxNjet   = 256

            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isZmm","isZee","isWenu","isWmunu","isZnn"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.dRbb_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                    
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies(self, attr):
        isNLO_dRbb_lt1 = False

        #print(attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)
        #print(self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]))
        #print("---------------------")
 
        if (any([s in self.sample.identifier for s in ['amcnloFXFX','amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
        #if (any([s in self.sample.identifier for s in ['amcnloFXFX','amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):       
            #print("isDYnlo_udsg interm",)
            if (self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0):
                #print("should work here ",attr["channel"]," ",self.sample.identifier)
                if (attr["channel"] == "isZee" or attr["channel"] == "isZmm"):
                    if (any([x in self.sample.identifier for x in ['DYJets','DY1Jets','DY2Jets']])):
                        isNLO_dRbb_lt1 = True
                elif (attr["channel"] == "isWenu" or attr["channel"] == "isWmunu"):
                    #print("I entered here")
                    if (any([x in self.sample.identifier for x in ['DYJets','DY1Jets','DY2Jets','WJetsToLNu','W1JetsToLNu','W2JetsToLNu']])):
                    #if (any([x in self.sample.identifier for x in ['DYJets','DY1Jets','DY2Jets']])):
                        #print("Final value change")
                        isNLO_dRbb_lt1 = True
                elif (attr["channel"] == "isZnn"):
                    if (any([x in self.sample.identifier for x in ['WJetsToLNu','Z1JetsToNuNu',"Z2JetsToNuNu",'W1JetsToLNu','W2JetsToLNu']])):
                        isNLO_dRbb_lt1 = True
                else:
                    isNLO_dRbb_lt1 = False
                        
        #print("final applies",isNLO_dRbb_lt1 )
        return isNLO_dRbb_lt1

    def get_lowerBound(self,value,bins):
        j = 0
        #print("value is",value)
        #print("bins are ",bins)
        while not (value<bins[j] or value>=250):
            #print("value is ", value, "and bins are ",bins[j])
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        dRbbWeight_Vpt  = []
        dRbbWeight_Incl = []
 
        dRbb_event     = self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0])
        #dRbbWeight_bin_Vpt= self.get_dRbbdict[self.get_lowerBound(attr["V_pt"][0],[75,150,250])]
        #dRbbWeight_bin_incl = 'InclPt'
        sample = attr["NLOsample"]
        channel = attr["channel"]
        #if sample=="isDY":print(sample, sample, dRbb_event) 
        j=0
        #channel = None
        VptBins = [150,250]
        #print("dRbb_event: ",dRbb_event)
        if any(channel==x for x in ["isZee","isZmm"]): VptBins=[75,150,250]
        dRbbWeight_bin_Vpt = self.get_dRbbdict[self.get_lowerBound(attr["V_pt"][0],VptBins)]
        #print(sample,channel)
        #print(attr)
        #print(self.dRbbWeight_VPt[sample].keys())
        while not (dRbb_event < self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["bins"][j]):
            j+=1
        #k=0
        #while not (dRbb_event < self.dRbbWeight_Incl[channel][dRbbWeight_bin_incl]["bins"][k]):
        #    k+=1
        #print("channel ",channel)
        #print("dRbbWeight_bin_Vpt",dRbbWeight_bin_Vpt)
        #print("Vpt ",attr["V_pt"][0])
        dRbbWeight_Vpt = [self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1] + self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["uncertainty"][j-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1] - self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["uncertainty"][j-1]]
        #dRbbWeight_Incl = [self.dRbbWeight_Incl[channel][dRbbWeight_bin_incl]["central"][j-1], self.dRbbWeight_Incl[channel][dRbbWeight_bin_incl]["central"][j-1] + self.dRbbWeight_Incl[channel][dRbbWeight_bin_incl]["uncertainty"][j-1], self.dRbbWeight_Incl[channel][dRbbWeight_bin_incl]["central"][j-1] - self.dRbbWeight_Incl[channel][dRbbWeight_bin_incl]["uncertainty"][j-1]]
        #print(dRbbWeight_Vpt,dRbbWeight_Incl)
        #return  dRbbWeight_Vpt,dRbbWeight_Incl 
        return  dRbbWeight_Vpt

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)
            attr = {}
            attr["NLOsample"] = None
            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt","hJidx","isZee","isZmm","isWenu","isWmunu","isZnn"]: 
                attr[var] = self.existingBranches[var]
            if any([x in self.sample.identifier for x in ['DYJets','DY1Jets','DY2Jets']]):
                attr["NLOsample"] = "isDY"
            elif any([x in self.sample.identifier for x in ['WJetsToLNu','W1JetsToLNu','W2JetsToLNu']]):
                attr["NLOsample"] = "isWJets" 
            elif any([x in self.sample.identifier for x in ['Z1JetsToNuNu',"Z2JetsToNuNu"]]):
                attr["NLOsample"] = "isZJets" 
            attr["channel"] = None   
            #print(attr)
            if attr["isZee"][0] == 1: attr["channel"] = "isZee"
            if attr["isZmm"][0] == 1: attr["channel"] = "isZmm"
            if attr["isWenu"][0] == 1: attr["channel"] = "isWenu"
            if attr["isWmunu"][0] == 1: attr["channel"] = "isWmunu"
            if attr["isZnn"][0] == 1: attr["channel"] = "isZnn"

            #print("channel is ",attr["channel"])

            if self.applies(attr):
                #dRbbWeight_Vpt,dRbbWeight_Incl = self.get_eventWeight(attr)
                dRbbWeight_Vpt = self.get_eventWeight(attr)
                    
                self._b("dRbbWeight_Vpt")[0]         = dRbbWeight_Vpt[0]
                self._b("dRbbWeight_Vpt"+"Up")[0]    = dRbbWeight_Vpt[1]
                self._b("dRbbWeight_Vpt"+"Down")[0]  = dRbbWeight_Vpt[2]
                #self._b("dRbbWeight_Incl")[0]        = dRbbWeight_Incl[0]
                #self._b("dRbbWeight_Incl"+"Up")[0]   = dRbbWeight_Incl[1]
                #self._b("dRbbWeight_Incl"+"Down")[0] = dRbbWeight_Incl[2]
                #print("--------------------------------------------------------------")
            else:
                self._b("dRbbWeight_Vpt")[0]         = 1.0
                self._b("dRbbWeight_Vpt"+"Up")[0]    = 1.0
                self._b("dRbbWeight_Vpt"+"Down")[0]  = 1.0
                #self._b("dRbbWeight_Incl")[0]        = 1.0
                #self._b("dRbbWeight_Incl"+"Up")[0]   = 1.0
                #self._b("dRbbWeight_Incl"+"Down")[0] = 1.0
        #print("------------------------------------------------------------------------------")            

if __name__=='__main__':

    config = XbbConfigReader.read('Wlv2018')
    info = ParseInfo(config=config)
    sample = [x for x in info if x.identifier == 'DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8'][0]
    print(sample)

    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_94b42e05db46cb9fd44285b60e0ba8b57d34232afdeef2661e673e5f_000000_000000_0000_0_5c5fb21d0e20a7f021911ce80109ccd1fa39014fff8bca8fc463de97.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Wlv/mva/18oct20_all_2DbtagdRbbDYWeightsfromLFCR/WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_bb21dac9dc44e46adf956d61d5b485c39341b737ba249606a80eb2f6_000000_000000_0000_0_09ffe41113d97125a40885075110dac51c0f173e112396ca28a14ce2.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")

    sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Wlv/mva/18oct20_all_2DbtagdRbbDYWeightsfromLFCR/DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8/tree_08cac0723ba4794e7272189a973d561e0218e3f66b8b85882f9a12b6_000000_000000_0000_1_9bb8647b504b59ef4532098963a8bc895564e9e8e148b08b61fb15cd.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/") 
  
    #config = XbbConfigReader.read('Zll2018')
    #info = ParseInfo(config=config)
    #sample = [x for x in info if x.identifier == 'DYBJetsToLL_M-50_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8'][0]
    #print(sample)

    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_94b42e05db46cb9fd44285b60e0ba8b57d34232afdeef2661e673e5f_000000_000000_0000_0_5c5fb21d0e20a7f021911ce80109ccd1fa39014fff8bca8fc463de97.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8/tree_0b9e5ec9f1d4328fc7ef364ebb5dd7364200ce37948e3753c76e6133_000000_000000_0000_5_fadbb45f88e40b3f3f44b0585861b2c4f87d08c5be7feda31913e251.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYBJetsToLL_M-50_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8/tree_a6b49dbb202048bd17cdf3f392ef3c2813bf6388d3275d8708169f7b_000000_000000_0000_0_f11fc63b7542561838a2fc3df60d8f8f1f67dd0c6fd24dd343324e76.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    w = NLORbblt1Weights("2018")
    w.customInit({'sampleTree': sampleTree, 'sample': sample, 'config': config})
    sampleTree.addOutputBranches(w.getBranches())
    #histograms={}
    #for jec in w.JEC_reduced:
        
    #for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
    #    histograms[var] = {}
    #    for syst in w.JEC_reduced:
    #        histograms[var][syst] = {}
    #        for Q in ['Up','Down']:
    #            histograms[var][syst][Q]=ROOT.TH1F(var+syst+Q, var+syst+Q, 400, -2.0, 2.0 )

    n=0 
    #var = "MET_phi"
    for event in sampleTree:
        n=n+1
        w.processEvent(event)
        #if n==50: break

   # with open('jec_validate_'+var+'.csv', 'w') as file:
   #     writer = csv.writer(file)
   #     writer.writerow(["event","run","Q",'true_jesAbsolute','jesAbsolute','AT','diff','true_jesAbsolute_2018','jesAbsolute_2018','AT','diff','true_jesBBEC1','jesBBEC1','AT','diff','true_jesBBEC1_2018','jesBBEC1_2018','AT','diff','true_jesEC2','jesEC2','AT','diff','true_jesEC2_2018','jesEC2_2018','AT','diff','true_jesHF','jesHF','AT','diff','true_jesHF_2018','jesHF_2018','AT','diff','true_jesRelativeSample_2018','jesRelativeSample_2018','AT','diff'])
   #     for event in sampleTree:
   #         n=n+1
   #         event,run,true_attr, attr = w.processEvent(event)
   #         for Q in ['Up','Down']:
   #             njet = len(true_attr[var][w.JEC_reduced[0]][Q])
   #             if njet>1:
   #                 for i in range(njet):
   #                     writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][i],attr[var]['jesAbsolute'][Q][i],'','',true_attr[var]['jesAbsolute_2018'][Q][i],attr[var]['jesAbsolute_2018'][Q][i],'','',true_attr[var]['jesBBEC1'][Q][i],attr[var]['jesBBEC1'][Q][i],'','',true_attr[var]['jesBBEC1_2018'][Q][i],attr[var]['jesBBEC1_2018'][Q][i],'','',true_attr[var]['jesEC2'][Q][i],attr[var]['jesEC2'][Q][i],'','',true_attr[var]['jesEC2_2018'][Q][i],attr[var]['jesEC2_2018'][Q][i],'','',true_attr[var]['jesHF'][Q][i],attr[var]['jesHF'][Q][i],'','',true_attr[var]['jesHF_2018'][Q][i],attr[var]['jesHF_2018'][Q][i],'','',true_attr[var]['jesRelativeSample_2018'][Q][i],attr[var]['jesRelativeSample_2018'][Q][i],'',''])
   #             else:
   #                 writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][0],attr[var]['jesAbsolute'][Q][0],'','',true_attr[var]['jesAbsolute_2018'][Q][0],attr[var]['jesAbsolute_2018'][Q][0],'','',true_attr[var]['jesBBEC1'][Q][0],attr[var]['jesBBEC1'][Q][0],'','',true_attr[var]['jesBBEC1_2018'][Q][0],attr[var]['jesBBEC1_2018'][Q][0],'','',true_attr[var]['jesEC2'][Q][0],attr[var]['jesEC2'][Q][0],'','',true_attr[var]['jesEC2_2018'][Q][0],attr[var]['jesEC2_2018'][Q][0],'','',true_attr[var]['jesHF'][Q][0],attr[var]['jesHF'][Q][0],'','',true_attr[var]['jesHF_2018'][Q][0],attr[var]['jesHF_2018'][Q][0],'','',true_attr[var]['jesRelativeSample_2018'][Q][0],attr[var]['jesRelativeSample_2018'][Q][0],'',''])

   #         #print("----------events over------------")
   #         if n==5: break

   # f = TFile("delete.root","RECREATE")

   # for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
   #     for syst in w.JEC_reduced:
   #         for Q in ['Up','Down']:
   #             histograms[var][syst][Q].Write()
   # f.Close()
