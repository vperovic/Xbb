#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class NLORbblt1Weights_0lep(AddCollectionsModule):

    def __init__(self, year):
        super(NLORbblt1Weights_0lep, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights_0lep, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights_0lep, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(NLORbblt1Weights_0lep, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.btagWPloose = float(self.config.get('General','btagWP_Loose'))
        self.btagWPmedium = float(self.config.get('General','btagWP_Medium'))
        print(">>>>>>>>>>>> year", self.year)
        print("Loose BTAG ", self.btagWPloose)
        print("Medium BTAG ", self.btagWPmedium)




        if self.year == "2018":


            #Careful about the overall normalization problem

            self.overallNormalization_LF_med = 1.2788
            self.overallNormalization_LF_high = 1.2788

            self.dRbbWeight_VPt = {
                        "isZnn":
                            {
                                '150250':{'bins': [0.4,0.6,0.8,1.0],'central':[1.4052, 1.3992, 1.5697],'uncertainty':[0.0679, 0.0657, 0.0876]
                                },
                                '250Inf':{'bins': [0.4,0.6,0.8,1.0],'central':[1.1694, 1.4449, 1.5944],'uncertainty':[0.0556, 0.0654, 0.0841]
                                },
                            },
                    }



        elif self.year == "2017":

            self.overallNormalization_LF_med = 1.2824
            self.overallNormalization_LF_high = 1.2179

            self.dRbbWeight_VPt = {
                        "isZnn":
                            {
                                '150250':{'bins': [0.4,0.6,0.8,1.0],'central':[1.2768, 1.5445, 1.6086]   ,'uncertainty': [0.0723, 0.0801, 0.1008]   
                                },
                                '250Inf':{'bins': [0.4,0.6,0.8,1.0],'central':[1.5767, 1.5607, 1.5288]   ,'uncertainty': [0.162, 0.1414, 0.1135] 
                                },
                            },
                    }


#central values are:  [1.2768, 1.5445, 1.6086, 1.2824]
#uncertainties are:  [0.0723, 0.0801, 0.1008, 0.0317]
#
#central values are:  [1.5767, 1.5607, 1.5288, 1.2179]
#uncertainties are:  [0.162, 0.1414, 0.1135, 0.0414]


        elif self.year == "2016":


            #Careful about the overall normalization problem

            self.overallNormalization_LF_med = 1.2437
            self.overallNormalization_LF_high = 1.2744

            self.dRbbWeight_VPt = {
                        "isZnn":
                            {
                                '150250':{'bins': [0.4,0.6,0.8,1.0],'central':[1.373, 1.6573, 1.6541],'uncertainty':[0.1054, 0.1176, 0.1387]
                                },
                                '250Inf':{'bins': [0.4,0.6,0.8,1.0],'central':[1.1542, 1.216, 1.2886],'uncertainty':[0.0899, 0.1009, 0.1246]
                                },
                            },
                    }


        elif self.year == "2016preVFP":


            #Careful about the overall normalization problem

            self.overallNormalization_LF_med = 1.0647
            self.overallNormalization_LF_high = 1.0284

            self.dRbbWeight_VPt = {
                        "isZnn":
                            {
                                '150250':{'bins': [0.4,0.6,0.8,1.0],'central':[1.2925, 1.4166, 1.2796]  ,'uncertainty': [0.1, 0.1041, 0.1217] 
                                },
                                '250Inf':{'bins': [0.4,0.6,0.8,1.0],'central':[1.0161, 1.2408, 1.2522]  ,'uncertainty':  [0.0793, 0.1001, 0.1101]
                                },
                            },
                    }

        else:
            raise Exception("Wrong year")

#central values are:  [1.2925, 1.4166, 1.2796, 1.0647]
#uncertainties are:  [0.1, 0.1041, 0.1217, 0.0385]

#central values are:  [1.0161, 1.2408, 1.2522, 1.0284]
#uncertainties are:  [0.0793, 0.1001, 0.1101, 0.0466]




        self.get_dRbbdict = {'75':'75150', '150':'150250','250':'250Inf'}
        self.dRbb_branches = ["VJets_dRbbWeight_Incl"]

        if self.sample.isMC():
            self.maxNjet   = 256


            for var in ["Jet_btagDeepFlavB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isZmm","isZee","isWenu","isWmunu","isZnn"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.dRbb_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                    
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies(self, attr):
        isNLO_dRbb_lt1 = False

        #if(attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1):
        #    print("res")  
        #print(self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0,"drBBb")
        #print("---------------------")
 
        if ((attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):
   
            if (self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0):
                if (attr["channel"] == "isZnn"):
                    if (any([x in self.sample.identifier for x in ['WJets','DYJets','ZJets']])):
                        isNLO_dRbb_lt1 = True

                        
        return isNLO_dRbb_lt1

    def get_lowerBound(self,value,bins):
        j = 0
        while not (value<bins[j] or value>=250):
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        
        #dRbbWeight_Vpt  = []
        
        dRbbWeight_Incl = []
 
        dRbb_event     = self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0])

        #dRbbWeight_bin_incl = '150Inf'
        dRbbWeight_bin_incl = self.get_dRbbdict[self.get_lowerBound(attr["V_pt"][0],[150,250])]
        channel = attr["channel"]
       
        #print("deltaRbb : ", dRbb_event)
 
        ### Inclusive
        k=0
        while not (dRbb_event < self.dRbbWeight_VPt[channel][dRbbWeight_bin_incl]["bins"][k]):
            k+=1
        #print("channel ",channel)
        #print("Vpt ",attr["V_pt"][0])
        dRbbWeight_Incl = [self.dRbbWeight_VPt[channel][dRbbWeight_bin_incl]["central"][k-1], self.dRbbWeight_VPt[channel][dRbbWeight_bin_incl]["central"][k-1] + self.dRbbWeight_VPt[channel][dRbbWeight_bin_incl]["uncertainty"][k-1], self.dRbbWeight_VPt[channel][dRbbWeight_bin_incl]["central"][k-1] - self.dRbbWeight_VPt[channel][dRbbWeight_bin_incl]["uncertainty"][k-1]]
        
        #return  dRbbWeight_Vpt
        return  dRbbWeight_Incl

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)
            attr = {}
            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt","hJidx","isZee","isZmm","isWenu","isWmunu","isZnn","Jet_btagDeepFlavB"]: 
                attr[var] = self.existingBranches[var]
            


            attr["channel"] = None   

            if attr["isZee"][0] == 1: attr["channel"] = "isZee"
            if attr["isZmm"][0] == 1: attr["channel"] = "isZmm"
            if attr["isWenu"][0] == 1: attr["channel"] = "isWenu"
            if attr["isWmunu"][0] == 1: attr["channel"] = "isWmunu"
            if attr["isZnn"][0] == 1: attr["channel"] = "isZnn"
            if attr["V_pt"] < 250: 
                self.overallNormalization_LF = self.overallNormalization_LF_med
            else:
                self.overallNormalization_LF = self.overallNormalization_LF_high

            if self.applies(attr):
                
                dRbbWeight_Incl = self.get_eventWeight(attr)
                
                #print("Checking pass boundary conditions")
                #print(dRbbWeight_Incl)
                        
                
                if attr["Jet_btagDeepFlavB"][attr["hJidx"][0]] > self.btagWPmedium: 
                   

                    #print("HF")
                    #print(attr["Jet_btagDeepFlavB"][attr["hJidx"][0]]) 
                    #print(dRbbWeight_Incl[0])
                    #print(dRbbWeight_Incl[1])
                    #print(dRbbWeight_Incl[2])

                    self._b("VJets_dRbbWeight_Incl")[0]        = dRbbWeight_Incl[0] 
                    self._b("VJets_dRbbWeight_Incl"+"Up")[0]   = dRbbWeight_Incl[1]
                    self._b("VJets_dRbbWeight_Incl"+"Down")[0] = dRbbWeight_Incl[2]
    
                else:
 
                    #print("LF")
                    #print(attr["Jet_btagDeepFlavB"][attr["hJidx"][0]]) 
                    #print(dRbbWeight_Incl[0]/self.overallNormalization_LF)
                    #print(dRbbWeight_Incl[1]/self.overallNormalization_LF)
                    #print(dRbbWeight_Incl[2]/self.overallNormalization_LF)
                    
                    self._b("VJets_dRbbWeight_Incl")[0]        = dRbbWeight_Incl[0]/self.overallNormalization_LF
                    self._b("VJets_dRbbWeight_Incl"+"Up")[0]   = dRbbWeight_Incl[1]/self.overallNormalization_LF
                    self._b("VJets_dRbbWeight_Incl"+"Down")[0] = dRbbWeight_Incl[2]/self.overallNormalization_LF
            else:



                self._b("VJets_dRbbWeight_Incl")[0]        = 1.0
                self._b("VJets_dRbbWeight_Incl"+"Up")[0]   = 1.0
                self._b("VJets_dRbbWeight_Incl"+"Down")[0] = 1.0

if __name__=='__main__':

    print("This module computes the DeltaRbb weights for WJets in 1 lep channel")


