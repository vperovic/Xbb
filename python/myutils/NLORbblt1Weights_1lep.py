#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class NLORbblt1Weights_1lep(AddCollectionsModule):

    def __init__(self, year):
        super(NLORbblt1Weights_1lep, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights_1lep, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights_1lep, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(NLORbblt1Weights_1lep, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.btagWPloose = float(self.config.get('General','btagWP_Loose'))
        self.btagWPmedium = float(self.config.get('General','btagWP_Medium'))
        print(">>>>>>>>>>>> year", self.year)
        print("Loose BTAG ", self.btagWPloose)
        print("Medium BTAG ", self.btagWPmedium)





        if self.year == "2018":

            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isWJets":
                        {
                        "isWenu":
                            {
                                '150Inf':{'bins': [0.4,0.6,0.8,1.0],'central':np.array([1.0028, 1.1057, 1.1514])/0.996,'uncertainty':[0.0128, 0.012, 0.0139]
                                },
                            },
                          "isWmunu":
                            {
                                '150Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central':np.array([0.951, 1.0391, 1.0536])/0.9178,'uncertainty':[0.0111, 0.0102, 0.0116]
                                },
                            },
                        },
                    }




        elif self.year == "2017":

            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isWJets":
                        {
                        "isWenu":
                            {
                                '150Inf':{'bins': [0.4,0.6,0.8,1.0],'central': np.array([1.1906, 1.2285, 1.2138])/1.0378   ,'uncertainty': [0.0211, 0.0173, 0.0174]
                                },
                            },
                          "isWmunu":
                            {
                                '150Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central': np.array([1.1119, 1.1817, 1.1662])/0.9894   ,'uncertainty': [0.018, 0.0149, 0.0153]
                                },
                            },
                        },
                    }


#central values are:  [0.8668, 1.1906, 1.2285, 1.2138, 1.0378]
#uncertainties are:  [0.6831, 0.0211, 0.0173, 0.0174, 0.0057]
#
#central values are:  [1.0535, 1.1119, 1.1817, 1.1662, 0.9894]
#uncertainties are:  [0.6791, 0.018, 0.0149, 0.0153, 0.0049]


        elif self.year == "2016":

            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isWJets":
                        {
                        "isWenu":
                            {
                                '150Inf':{'bins': [0.4,0.6,0.8,1.0],'central':np.array([1.3152, 1.4422, 1.4789])/1.2797, 'uncertainty': [0.0333, 0.0314, 0.0368]
                                },
                            },
                          "isWmunu":
                            {
                                '150Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central':np.array([1.1464, 1.394, 1.4019])/1.2232 ,'uncertainty': [0.0271, 0.027, 0.0301]
                                },
                            },
                        },
                    }


        elif self.year == "2016preVFP":

            self.dRbbWeight_VPt = {
                    "isWJets":
                        {
                        "isWenu":
                            {
                                '150Inf':{'bins': [0.4,0.6,0.8,1.0],'central': np.array([0.8665, 0.953, 0.9512])/0.8347 , 'uncertainty': [0.0223, 0.0214, 0.0238]   
                                },
                            },
                          "isWmunu":
                            {
                                '150Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central': np.array([0.8937, 0.9212, 0.9473])/0.7916  ,'uncertainty': [0.0201, 0.0179, 0.0202]
                                },
                            },
                        },
                    }


        else:
            raise Exception("Wrong year")




        self.get_dRbbdict = {'75':'75150', '150':'150250','250':'250Inf'}
        self.dRbb_branches = ["WJ_dRbbWeight_Incl"]

        if self.sample.isMC():
            self.maxNjet   = 256


            for var in ["Jet_btagDeepFlavB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isZmm","isZee","isWenu","isWmunu","isZnn"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.dRbb_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                    
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies(self, attr):
        isNLO_dRbb_lt1 = False

        #if(attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1):
        #    print("res")  
        #print(self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0,"drBBb")
        #print("---------------------")
 
        if (any([s in self.sample.identifier for s in ['amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):
   
            if (self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0):
                if (attr["channel"] == "isWenu" or attr["channel"] == "isWmunu"):
                    if (any([x in self.sample.identifier for x in ['WJets']])):
                        isNLO_dRbb_lt1 = True

                        
        return isNLO_dRbb_lt1

    def get_lowerBound(self,value,bins):
        j = 0
        while not (value<bins[j] or value>=250):
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        
        #dRbbWeight_Vpt  = []
        
        dRbbWeight_Incl = []
 
        dRbb_event     = self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0])

        dRbbWeight_bin_incl = '150Inf'
        sample = attr["NLOsample"]
        channel = attr["channel"]
       
        #print("deltaRbb : ", dRbb_event)
        #print(sample)
        #print(channel)
 
        ### Inclusive
        k=0
        while not (dRbb_event < self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["bins"][k]):
            k+=1
        #print("channel ",channel)
        #print("Vpt ",attr["V_pt"][0])
        dRbbWeight_Incl = [self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1] + self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["uncertainty"][k-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1] - self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["uncertainty"][k-1]]
        
        #return  dRbbWeight_Vpt
        return  dRbbWeight_Incl

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)
            attr = {}
            attr["NLOsample"] = None
            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt","hJidx","isZee","isZmm","isWenu","isWmunu","isZnn","Jet_btagDeepFlavB"]: 
                attr[var] = self.existingBranches[var]
            if any([x in self.sample.identifier for x in ['DYJets']]):
                attr["NLOsample"] = "isDY"
            elif any([x in self.sample.identifier for x in ['WJetsToLNu']]):
                attr["NLOsample"] = "isWJets" 
            elif any([x in self.sample.identifier for x in ['ZJetsToNuNu']]):
                attr["NLOsample"] = "isZJets" 
            attr["channel"] = None   
            


            if attr["isZee"][0] == 1: attr["channel"] = "isZee"
            if attr["isZmm"][0] == 1: attr["channel"] = "isZmm"
            if attr["isWenu"][0] == 1: attr["channel"] = "isWenu"
            if attr["isWmunu"][0] == 1: attr["channel"] = "isWmunu"
            if attr["isZnn"][0] == 1: attr["channel"] = "isZnn"


            if self.applies(attr):
                
                dRbbWeight_Incl = self.get_eventWeight(attr)
                
                #print("Checking pass boundary conditions")
                #print(dRbbWeight_Incl)
                
 
                self._b("WJ_dRbbWeight_Incl")[0]        = dRbbWeight_Incl[0]
                self._b("WJ_dRbbWeight_Incl"+"Up")[0]   = dRbbWeight_Incl[1]
                self._b("WJ_dRbbWeight_Incl"+"Down")[0] = dRbbWeight_Incl[2]
            else:


                self._b("WJ_dRbbWeight_Incl")[0]        = 1.0
                self._b("WJ_dRbbWeight_Incl"+"Up")[0]   = 1.0
                self._b("WJ_dRbbWeight_Incl"+"Down")[0] = 1.0

if __name__=='__main__':

    print("This module computes the DeltaRbb weights for WJets in 1 lep channel")


