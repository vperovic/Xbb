#!/usr/bin/env python
from __future__ import print_function
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools

# dPhiVH computation
class 2set0lepbasiccuts(AddCollectionsModule):

    def __init__(self, year):
        super(2set0lepbasiccuts, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.year = year if type(year) == str else str(year)
        self.quickloadWarningShown = False
        self.existingBranches = {}

    # only add as new branch if they don't exists already
    def addBranch(self, branchName, default=0.0):
        if branchName not in self.existingBranches:
            super(dPhiVH, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(dPhiVH, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):
        self.sampleTree  = initVars['sampleTree']
        self.sample      = initVars['sample']
        self.config      = initVars['config']
        self.xbbConfig   = XbbConfigTools(self.config)
        self.jetSystematicsResolved = self.xbbConfig.getJECuncertainties(step='Higgs')
        self.jetSystematics         = self.jetSystematicsResolved[:]
        
        # load needed information
        for var in ["H_phi","V_phi"]:
            self.existingBranches[var] = array.array('f', [0.0])
            self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

        
        for syst in self.jetSystematics:
            for Q in self._variations(syst):
                for v in [self._v('H_phi', syst, Q)]:
                    self.existingBranches[v] = array.array('f', [0.0])
                    self.sampleTree.tree.SetBranchAddress(v, self.existingBranches[v])
        
        # new dPhiVH branches
        self.addBranch('dPhiVH')
        for var in ['dPhiVH']: 
            for syst in self.jetSystematics:
                for Q in self._variations(syst):
                    self.addBranch(self._v(var, syst, Q))


    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree):
            self.markProcessed(tree)
    
        self._b('dPhiVH')[0] = abs(ROOT.TVector2.Phi_mpi_pi(self.existingBranches["H_phi"][0]-self.existingBranches["V_phi"][0])) 

        for syst in self.jetSystematics:
            for Q in self._variations(syst):
                self._b(self._v('dPhiVH', syst, Q))[0] = abs(ROOT.TVector2.Phi_mpi_pi(self.existingBranches[self._v('H_phi', syst, Q)][0]-self.existingBranches['V_phi'][0]))
             
