#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class BTagWeightsDYltlooseWP(AddCollectionsModule):

    def __init__(self, year):
        super(BTagWeightsDYltlooseWP, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(BTagWeightsDYltlooseWP, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(BTagWeightsDYltlooseWP, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(BTagWeightsDYltlooseWP, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.btagWPloose = float(self.config.get('General','btagWP_Loose'))
        print(">>>>>>>>>>>> year", self.year)
        print("Loose BTAG ", self.btagWPloose)

        self.Nevent = 0




        if self.year == "2018":




            self.BTagWeights_Vpt = { 
                    "isZee":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.7907933592796326, 0.8312709927558899, 0.8884780406951904], [0.0, 0.9234657287597656, 0.9863179326057434], [0.0, 0.0, 1.1009843349456787]],'uncertainty':[[0.009153088890761848, 0.007892420955429484, 0.01179090125909505], [0.0, 0.013732753996937224, 0.01435510424708321], [0.0, 0.0, 0.02881505561538729]]                        },
                            '150250':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.7856392860412598, 0.858334481716156, 0.8299710154533386], [0.0, 0.93546062707901, 1.0115679502487183], [0.0, 0.0, 1.031952142715454]],'uncertainty':[[0.019110465493358163, 0.017307792018060267, 0.024364618280042032], [0.0, 0.026848610461386452, 0.02844637707833244], [0.0, 0.0, 0.049697005580853916]]
                            },
                            '250Inf':{'bins':[-0.01, 0.01, 0.02333, 0.05],'central':[[0.8899331092834473, 0.8684532642364502, 0.9387526512145996], [0.0, 0.9084364175796509, 0.9397804141044617], [0.0, 0.0, 0.9378113150596619]],'uncertainty':[[0.04607136307034223, 0.03663484905272188, 0.05546372550254372], [0.0, 0.04834604879635611, 0.04704980665250763], [0.0, 0.0, 0.07884871690579497]]
                            },
                        },

                    "isZmm":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.7763943672180176, 0.8218029141426086, 0.8711157441139221], [0.0, 0.923093855381012, 0.9822417497634888], [0.0, 0.0, 1.0493618249893188]],'uncertainty':[[0.00790566142436296, 0.0068054282064372845, 0.010084347579713514], [0.0, 0.011871009122524917, 0.01229229172688405], [0.0, 0.0, 0.023358564362000536]]                        },
                            '150250':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.7382879853248596, 0.8094378709793091, 0.8675294518470764], [0.0, 0.9590796232223511, 0.9586070775985718], [0.0, 0.0, 1.1095141172409058]],'uncertainty':[[0.016141923480912007, 0.0147050219100583, 0.02206848244118352], [0.0, 0.024407748071742723, 0.023644649444900134], [0.0, 0.0, 0.0460459384382935]]
                            },
                            '250Inf':{'bins':[-0.01, 0.01, 0.02333, 0.05],'central':[[0.787730872631073, 0.7743799090385437, 0.8684601187705994], [0.0, 0.7613738179206848, 0.9070900082588196], [0.0, 0.0, 1.0061542987823486]],'uncertainty':[[0.03852495145532674, 0.030726481184429846, 0.04689226014407907], [0.0, 0.03901314771235718, 0.04091922066368205], [0.0, 0.0, 0.07102372870018187]]
                            },
                        },
                        }





        elif self.year == "2017":

            self.BTagWeights_Vpt = {
                    "isZee":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.054], 'central':  [[0.8312230706214905, 0.9135924577713013, 0.9233297109603882], [0.0, 1.0241429805755615, 1.0191556215286255], [0.0, 0.0, 1.0945454835891724]]    ,'uncertainty':   [[0.011131506632782747, 0.009799104989287051, 0.013317244111568073], [0.0, 0.016769687169289355, 0.015845840473683807], [0.0, 0.0, 0.02916499717760166]]        },
                            '150250':{'bins':[-0.01, 0.01, 0.02333, 0.054], 'central':  [[0.8241419792175293, 0.8985962867736816, 0.9318183660507202], [0.0, 1.0126670598983765, 1.0514856576919556], [0.0, 0.0, 1.1722595691680908]]    ,'uncertainty':   [[0.023800013353982068, 0.021168938494182617, 0.02978086421920199], [0.0, 0.033453277589771305, 0.03274180538907002], [0.0, 0.0, 0.056400809924687635]]         },
                            '250Inf':{'bins':[-0.01, 0.01, 0.02333, 0.054], 'central':   [[0.8680168390274048, 0.8491811156272888, 0.8473232984542847], [0.0, 0.9974240660667419, 1.017728567123413], [0.0, 0.0, 1.0748435258865356]]    ,'uncertainty':   [[0.06770969214809246, 0.051030371843803546, 0.06843885538962423], [0.0, 0.06803820959299445, 0.062060525470360096], [0.0, 0.0, 0.1000707604728649]]          },
                        },

                    "isZmm":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.054], 'central':  [[0.84031742811203, 0.9341896772384644, 0.9356573224067688], [0.0, 1.0539754629135132, 1.0803675651550293], [0.0, 0.0, 1.1283838748931885]]    ,'uncertainty':    [[0.00986113832464545, 0.008562190229439028, 0.01162673128862449], [0.0, 0.014805261383660786, 0.01412274225020723], [0.0, 0.0, 0.025000030097507186]]          },
                            '150250':{'bins':[-0.01, 0.01, 0.02333, 0.054], 'central':  [[0.7898350954055786, 0.8953565359115601, 0.8931418061256409], [0.0, 1.009871482849121, 1.0237219333648682], [0.0, 0.0, 1.0635793209075928]]     ,'uncertainty':  [[0.020479095511595928, 0.01893224886350884, 0.02550461732648788], [0.0, 0.02950125470690389, 0.02769188500268418], [0.0, 0.0, 0.04678824640980814]]            },
                            '250Inf':{'bins':[-0.01, 0.01, 0.02333, 0.054], 'central':   [[0.8322939872741699, 0.8533262014389038, 0.9900612235069275], [0.0, 0.9868396520614624, 0.8834596276283264], [0.0, 0.0, 1.1903059482574463]]    ,'uncertainty':   [[0.05859111730940865, 0.04548241145548369, 0.06606540454752427], [0.0, 0.06056414126893524, 0.05125140233240416], [0.0, 0.0, 0.09377201008578528]]             },
                        },
                        }


#>>>>>>>>>>>>> Central values >>>>>>>>>>>
#[[0.8312230706214905, 0.9135924577713013, 0.9233297109603882], [0.0, 1.0241429805755615, 1.0191556215286255], [0.0, 0.0, 1.0945454835891724]]
#>>>>>>>>>>>>> Uncertainties >>>>>>>>>>>
#[[0.011131506632782747, 0.009799104989287051, 0.013317244111568073], [0.0, 0.016769687169289355, 0.015845840473683807], [0.0, 0.0, 0.02916499717760166]]
#
#>>>>>>>>>>>>> Central values >>>>>>>>>>>
#[[0.8241419792175293, 0.8985962867736816, 0.9318183660507202], [0.0, 1.0126670598983765, 1.0514856576919556], [0.0, 0.0, 1.1722595691680908]]
#>>>>>>>>>>>>> Uncertainties >>>>>>>>>>>
#[[0.023800013353982068, 0.021168938494182617, 0.02978086421920199], [0.0, 0.033453277589771305, 0.03274180538907002], [0.0, 0.0, 0.056400809924687635]]
#
#>>>>>>>>>>>>> Central values >>>>>>>>>>>
#[[0.8680168390274048, 0.8491811156272888, 0.8473232984542847], [0.0, 0.9974240660667419, 1.017728567123413], [0.0, 0.0, 1.0748435258865356]]
#>>>>>>>>>>>>> Uncertainties >>>>>>>>>>>
#[[0.06770969214809246, 0.051030371843803546, 0.06843885538962423], [0.0, 0.06803820959299445, 0.062060525470360096], [0.0, 0.0, 0.1000707604728649]]
#
#>>>>>>>>>>>>> Central values >>>>>>>>>>>
#[[0.84031742811203, 0.9341896772384644, 0.9356573224067688], [0.0, 1.0539754629135132, 1.0803675651550293], [0.0, 0.0, 1.1283838748931885]]
#>>>>>>>>>>>>> Uncertainties >>>>>>>>>>>
#[[0.00986113832464545, 0.008562190229439028, 0.01162673128862449], [0.0, 0.014805261383660786, 0.01412274225020723], [0.0, 0.0, 0.025000030097507186]]
#
#>>>>>>>>>>>>> Central values >>>>>>>>>>>
#[[0.7898350954055786, 0.8953565359115601, 0.8931418061256409], [0.0, 1.009871482849121, 1.0237219333648682], [0.0, 0.0, 1.0635793209075928]]
#>>>>>>>>>>>>> Uncertainties >>>>>>>>>>>
#[[0.020479095511595928, 0.01893224886350884, 0.02550461732648788], [0.0, 0.02950125470690389, 0.02769188500268418], [0.0, 0.0, 0.04678824640980814]]
#
#>>>>>>>>>>>>> Central values >>>>>>>>>>>
#[[0.8322939872741699, 0.8533262014389038, 0.9900612235069275], [0.0, 0.9868396520614624, 0.8834596276283264], [0.0, 0.0, 1.1903059482574463]]
#>>>>>>>>>>>>> Uncertainties >>>>>>>>>>>
#[[0.05859111730940865, 0.04548241145548369, 0.06606540454752427], [0.0, 0.06056414126893524, 0.05125140233240416], [0.0, 0.0, 0.09377201008578528]]
#




        elif self.year == "2016":


            self.BTagWeights_Vpt = { 
                    "isZee":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.6716846823692322, 0.7562083601951599, 0.9039464592933655], [0.0, 0.8666558861732483, 0.9585658311843872], [0.0, 0.0, 1.2006434202194214]],'uncertainty':[[0.015794701151587185, 0.01324634348732657, 0.0212633283612298], [0.0, 0.022143529165887795, 0.023028612311304892], [0.0, 0.0, 0.048206095521604715]]                        },
                            '150250':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.7165776491165161, 0.7865867614746094, 0.9777619242668152], [0.0, 0.8959620594978333, 0.9950823187828064], [0.0, 0.0, 1.162878394126892]],'uncertainty':[[0.035219783927371774, 0.029832373178262886, 0.04895885044862157], [0.0, 0.04549588704332102, 0.04925399385795527], [0.0, 0.0, 0.09369405186544663]]
                            },
                            '250Inf':{'bins':[-0.01, 0.01, 0.02333, 0.05],'central':[[0.8650078177452087, 0.8285285234451294, 0.8720456957817078], [0.0, 0.8789269328117371, 0.9653627872467041], [0.0, 0.0, 1.0204286575317383]],'uncertainty':[[0.09102111176305985, 0.06886736459987856, 0.10322409053404379], [0.0, 0.08476947685675554, 0.08983478653584911], [0.0, 0.0, 0.14765073584934418]]
                            },
                        },

                    "isZmm":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.6760329008102417, 0.7584741115570068, 0.9133480787277222], [0.0, 0.8651329278945923, 1.0148346424102783], [0.0, 0.0, 1.2661274671554565]],'uncertainty':[[0.01335538721271371, 0.011096470933786563, 0.017912051918499306], [0.0, 0.018337465886852124, 0.019971078031894386], [0.0, 0.0, 0.04250606413235985]]                        },
                            '150250':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.677862286567688, 0.7661527991294861, 0.9369007349014282], [0.0, 0.8548046946525574, 0.9465891718864441], [0.0, 0.0, 1.2267094850540161]],'uncertainty':[[0.028727381411310517, 0.02538994806075282, 0.040291884442317356], [0.0, 0.04017781009345747, 0.04025589112700436], [0.0, 0.0, 0.08270551286261982]]
                            },
                            '250Inf':{'bins':[-0.01, 0.01, 0.02333, 0.05],'central':[[0.7416589856147766, 0.7618622779846191, 0.7124812006950378], [0.0, 0.8263094425201416, 0.9795730710029602], [0.0, 0.0, 1.1240142583847046]],'uncertainty':[[0.07331608397635242, 0.05768021858879866, 0.08199303791446583], [0.0, 0.07066338294329463, 0.07554572240106444], [0.0, 0.0, 0.13705749813618928]]
                            },
                        },
                        }


        elif self.year == "2016preVFP":


            self.BTagWeights_Vpt = { 
                    "isZee":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.051], 'central': [[0.7300159335136414, 0.8183323740959167, 0.8114995360374451], [0.0, 0.895467221736908, 0.9402682185173035], [0.0, 0.0, 0.9654498100280762]]  ,'uncertainty':   [[0.015690522021622494, 0.01341170198091692, 0.018071198460814675], [0.0, 0.021801637257057024, 0.02127349564251332], [0.0, 0.0, 0.037577880119857236]]                    },
                            '150250':{'bins':[-0.01, 0.01, 0.02333, 0.051], 'central': [[0.7248505353927612, 0.812743067741394, 0.8472251296043396], [0.0, 0.8689001798629761, 0.8655561208724976], [0.0, 0.0, 1.0444238185882568]]   ,'uncertainty':   [[0.03318754447941318, 0.02919740326059231, 0.04100537831734846], [0.0, 0.043606123312891876, 0.03978767245901204], [0.0, 0.0, 0.07606973760692222]]                    },
                            '250Inf':{'bins':[-0.01, 0.01, 0.02333, 0.051],'central':  [[0.8868972659111023, 0.7423298358917236, 0.6779660582542419], [0.0, 0.7254372835159302, 0.8499118685722351], [0.0, 0.0, 0.8759458661079407]]    ,'uncertainty':   [[0.09082646163258484, 0.06307778040017911, 0.08203662381408747], [0.0, 0.07788851917036413, 0.07607391723852831], [0.0, 0.0, 0.11975274690891685]]                    },
                        },

                    "isZmm":
                        {
                            '75150':{'bins':[-0.01, 0.01, 0.02333, 0.051], 'central':  [[0.7758827805519104, 0.8465625643730164, 0.8521142601966858], [0.0, 0.9064610600471497, 0.9349685907363892], [0.0, 0.0, 0.9392269253730774]]   ,'uncertainty':   [[0.013853410441892597, 0.011545118489113333, 0.015527925837992222], [0.0, 0.018174989495936964, 0.017485257628950212], [0.0, 0.0, 0.030124297866830872]]                   },
                            '150250':{'bins':[-0.01, 0.01, 0.02333, 0.051], 'central':  [[0.7383275628089905, 0.7236684560775757, 0.7459730505943298], [0.0, 0.8107945322990417, 0.7962382435798645], [0.0, 0.0, 0.8997228145599365]]  ,'uncertainty':   [[0.029055639292328673, 0.023191347572376023, 0.03286626482455601], [0.0, 0.03541879202640702, 0.03296512389368149], [0.0, 0.0, 0.05689439000629332]]                  },
                            '250Inf':{'bins':[-0.01, 0.01, 0.02333, 0.051],'central':  [[0.7368578314781189, 0.5854780673980713, 0.6567859053611755], [0.0, 0.773861289024353, 0.7625460624694824], [0.0, 0.0, 0.9293072819709778]]   ,'uncertainty':   [[0.06932363917665159, 0.048680583150394946, 0.07137137414105793], [0.0, 0.06603315956142203, 0.06190243859250356], [0.0, 0.0, 0.10433579334981134]]                   },
                        },
                        }



            #self.BTagWeights_Vpt = { 
            #        "isZee":
            #            {
            #                '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.6716846823692322, 0.7562083601951599, 0.9039464592933655], [0.0, 0.8666558861732483, 0.9585658311843872], [0.0, 0.0, 1.2006434202194214]],'uncertainty':[[0.015794701151587185, 0.01324634348732657, 0.0212633283612298], [0.0, 0.022143529165887795, 0.023028612311304892], [0.0, 0.0, 0.048206095521604715]]                        },
            #                '150250':{'bins':[-0.01, 0.01, 0.05], 'central':[[0.7165776491165161, 0.8482401967048645], [0.0, 0.9763705730438232]],'uncertainty':[[0.035219783927371774, 0.025634643984663984], [0.0, 0.0316642205449076]]
            #                },
            #                '250Inf':{'bins':[-0.01, 0.016666, 0.05],'central':[[0.9121828079223633, 0.8852620124816895], [0.0, 0.8506835699081421]],'uncertainty':[[0.05833592476906671, 0.0576924363138733], [0.0, 0.08608647181749181]]
            #                },
            #            },

            #        "isZmm":
            #            {
            #                '75150':{'bins':[-0.01, 0.01, 0.02333, 0.05], 'central':[[0.6760329008102417, 0.7584741115570068, 0.9133480787277222], [0.0, 0.8651329278945923, 1.0148346424102783], [0.0, 0.0, 1.2661274671554565]],'uncertainty':[[0.01335538721271371, 0.011096470933786563, 0.017912051918499306], [0.0, 0.018337465886852124, 0.019971078031894386], [0.0, 0.0, 0.04250606413235985]]                        },
            #                '150250':{'bins':[-0.01, 0.01, 0.05], 'central':[[0.677862286567688, 0.8227474689483643], [0.0, 0.9461235404014587]],'uncertainty':[[0.028727381411310517, 0.02159299821567743], [0.0, 0.027174801678031588]]
            #                },
            #                '250Inf':{'bins':[-0.01, 0.016666, 0.05],'central':[[0.7349904775619507, 0.8479240536689758], [0.0, 1.0100414752960205]],'uncertainty':[[0.04588778663576455, 0.048542612737693815], [0.0, 0.07911409540004521]]
            #                },
            #            },
            #            }


        else:
            raise Exception("Wrong year")


        self.get_BTagWeights_Vptdict = {'75':'75150', '150':'150250','250':'250Inf'}
        self.BTagWeights_Vpt_branches = ["DY_btagDeepFlavB_2D"]

        if self.sample.isMC():
            self.maxNjet   = 256

            for var in ["Jet_btagDeepFlavB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isZmm","isZee"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["nGenBpt25eta2p6","nGenDpt25eta2p6"]:
                self.existingBranches[var] = array.array('i', [100])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.BTagWeights_Vpt_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                


 
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies(self, attr):
        isDYbtag_udsg = False
        #print(">>>>>>>>>> before")
        #print(attr["hJidx"][0])
        #print(attr["hJidx"][1])
        #print("leading btag",attr["Jet_btagDeepFlavB"][attr["hJidx"][0]])
        #print("truth?",  attr["Jet_btagDeepFlavB"][attr["hJidx"][0]] < self.btagWPloose)
        #print("subleading btag",attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])
        #print(attr["nGenBpt25eta2p6"][0])
        #print(attr["nGenDpt25eta2p6"][0])
        #print(self.btagWPloose)
        if (any([x in self.sample.identifier for x in ['DYJets']]) and any([s in self.sample.identifier for s in ['amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if ((attr["Jet_btagDeepFlavB"][attr["hJidx"][0]] < self.btagWPloose and attr["Jet_btagDeepFlavB"][attr["hJidx"][1]] < self.btagWPloose) and (attr["nGenBpt25eta2p6"][0]<1 and attr["nGenDpt25eta2p6"][0]<1)):
                isDYbtag_udsg = True
        #        print(">>>>>>>>>> in")
        #        print(attr["hJidx"][0])
        #        print(attr["hJidx"][1])
        #        print(attr["Jet_btagDeepFlavB"][attr["hJidx"][0]])
        #        print(attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])
        #        print(attr["nGenBpt25eta2p6"][0])
        #        print(attr["nGenDpt25eta2p6"][0])
        #        print(">>>>>>>>>> after")
 
        #print("final: ",isDYbtag_udsg)
        #print("----------------------------------------------------------------")
        return isDYbtag_udsg

    def get_lowerBound(self,value,bins):
        j = 0
        #print("value is",value)
        #print("bins are ",bins)
        while not (value<bins[j] or value>=250):
            #print("value is ", value, "and bins are ",bins[j])
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        dRbbWeight_Vpt  = []
        dRbbWeight_Incl = []
 
        btag_hJidx0        = attr["Jet_btagDeepFlavB"][attr["hJidx"][0]]
        btag_hJidx1        = attr["Jet_btagDeepFlavB"][attr["hJidx"][1]]
        BTagWeights_Vptbin = self.get_BTagWeights_Vptdict[self.get_lowerBound(attr["V_pt"][0],[75,150,250])]
 
        j=0
        channel = None
        if attr["isZee"][0] == 1: channel = "isZee"
        if attr["isZmm"][0] == 1: channel = "isZmm"

        if (btag_hJidx0 < 0.0 or btag_hJidx1 < 0.0):
            return 1.0,1.0,1.0

        while not (btag_hJidx0 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][j]):
            #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][j])
            #print(btag_hJidx0)
            j+=1
            #print("j is now amining for ",j)
        k=0
        while not (btag_hJidx1 < self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["bins"][k]):
            k+=1
        #print(j-1)
        #print(k-1)
        #print("channel ",channel," BTagWeights_Vptbin ",BTagWeights_Vptbin," j-1 ",j-1," k-1 ",k-1)
        #print(self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1])
        nom = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1]
        up = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] + self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        down = self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["central"][k-1][j-1] - self.BTagWeights_Vpt[channel][BTagWeights_Vptbin]["uncertainty"][k-1][j-1]
        #print(nom,up,down)        
        return  nom,up,down 

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)

            #print("nevent ", self.Nevent, tree.GetEntries())
            self.Nevent+=1

            attr = {}
            for var in ["V_pt","hJidx","isZee","isZmm","Jet_btagDeepFlavB","nGenBpt25eta2p6","nGenDpt25eta2p6"]: 
                attr[var] = self.existingBranches[var]
            if self.applies(attr):
                nom,up,down = self.get_eventWeight(attr)
                self._b("DY_btagDeepFlavB_2D")[0]         = nom
                self._b("DY_btagDeepFlavB_2D"+"Up")[0]    = up
                self._b("DY_btagDeepFlavB_2D"+"Down")[0]  = down
            else:
                self._b("DY_btagDeepFlavB_2D")[0]         = 1.0
                self._b("DY_btagDeepFlavB_2D"+"Up")[0]    = 1.0
                self._b("DY_btagDeepFlavB_2D"+"Down")[0]  = 1.0

if __name__=='__main__':

    config = XbbConfigReader.read('Zll2018')
    info = ParseInfo(config=config)
    sample = [x for x in info if x.identifier == 'DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8'][0]
    print(sample)

    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_94b42e05db46cb9fd44285b60e0ba8b57d34232afdeef2661e673e5f_000000_000000_0000_0_5c5fb21d0e20a7f021911ce80109ccd1fa39014fff8bca8fc463de97.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8/tree_0b9e5ec9f1d4328fc7ef364ebb5dd7364200ce37948e3753c76e6133_000000_000000_0000_5_fadbb45f88e40b3f3f44b0585861b2c4f87d08c5be7feda31913e251.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYBJetsToLL_M-50_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8/tree_a6b49dbb202048bd17cdf3f392ef3c2813bf6388d3275d8708169f7b_000000_000000_0000_0_f11fc63b7542561838a2fc3df60d8f8f1f67dd0c6fd24dd343324e76.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    w = BTagWeightsDYltlooseWP("2018")
    w.customInit({'sampleTree': sampleTree, 'sample': sample, 'config': config})
    sampleTree.addOutputBranches(w.getBranches())
    #histograms={}
    #for jec in w.JEC_reduced:
        
    #for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
    #    histograms[var] = {}
    #    for syst in w.JEC_reduced:
    #        histograms[var][syst] = {}
    #        for Q in ['Up','Down']:
    #            histograms[var][syst][Q]=ROOT.TH1F(var+syst+Q, var+syst+Q, 400, -2.0, 2.0 )

    n=0 
    #var = "MET_phi"
    for event in sampleTree:
        n=n+1
        w.processEvent(event)
        if n==10: break

   # with open('jec_validate_'+var+'.csv', 'w') as file:
   #     writer = csv.writer(file)
   #     writer.writerow(["event","run","Q",'true_jesAbsolute','jesAbsolute','AT','diff','true_jesAbsolute_2018','jesAbsolute_2018','AT','diff','true_jesBBEC1','jesBBEC1','AT','diff','true_jesBBEC1_2018','jesBBEC1_2018','AT','diff','true_jesEC2','jesEC2','AT','diff','true_jesEC2_2018','jesEC2_2018','AT','diff','true_jesHF','jesHF','AT','diff','true_jesHF_2018','jesHF_2018','AT','diff','true_jesRelativeSample_2018','jesRelativeSample_2018','AT','diff'])
   #     for event in sampleTree:
   #         n=n+1
   #         event,run,true_attr, attr = w.processEvent(event)
   #         for Q in ['Up','Down']:
   #             njet = len(true_attr[var][w.JEC_reduced[0]][Q])
   #             if njet>1:
   #                 for i in range(njet):
   #                     writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][i],attr[var]['jesAbsolute'][Q][i],'','',true_attr[var]['jesAbsolute_2018'][Q][i],attr[var]['jesAbsolute_2018'][Q][i],'','',true_attr[var]['jesBBEC1'][Q][i],attr[var]['jesBBEC1'][Q][i],'','',true_attr[var]['jesBBEC1_2018'][Q][i],attr[var]['jesBBEC1_2018'][Q][i],'','',true_attr[var]['jesEC2'][Q][i],attr[var]['jesEC2'][Q][i],'','',true_attr[var]['jesEC2_2018'][Q][i],attr[var]['jesEC2_2018'][Q][i],'','',true_attr[var]['jesHF'][Q][i],attr[var]['jesHF'][Q][i],'','',true_attr[var]['jesHF_2018'][Q][i],attr[var]['jesHF_2018'][Q][i],'','',true_attr[var]['jesRelativeSample_2018'][Q][i],attr[var]['jesRelativeSample_2018'][Q][i],'',''])
   #             else:
   #                 writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][0],attr[var]['jesAbsolute'][Q][0],'','',true_attr[var]['jesAbsolute_2018'][Q][0],attr[var]['jesAbsolute_2018'][Q][0],'','',true_attr[var]['jesBBEC1'][Q][0],attr[var]['jesBBEC1'][Q][0],'','',true_attr[var]['jesBBEC1_2018'][Q][0],attr[var]['jesBBEC1_2018'][Q][0],'','',true_attr[var]['jesEC2'][Q][0],attr[var]['jesEC2'][Q][0],'','',true_attr[var]['jesEC2_2018'][Q][0],attr[var]['jesEC2_2018'][Q][0],'','',true_attr[var]['jesHF'][Q][0],attr[var]['jesHF'][Q][0],'','',true_attr[var]['jesHF_2018'][Q][0],attr[var]['jesHF_2018'][Q][0],'','',true_attr[var]['jesRelativeSample_2018'][Q][0],attr[var]['jesRelativeSample_2018'][Q][0],'',''])

   #         #print("----------events over------------")
   #         if n==5: break

   # f = TFile("delete.root","RECREATE")

   # for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
   #     for syst in w.JEC_reduced:
   #         for Q in ['Up','Down']:
   #             histograms[var][syst][Q].Write()
   # f.Close()
