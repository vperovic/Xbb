#!/usr/bin/env python
from __future__ import print_function
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools

#Computes jmr, jms from PUPPI corrections and applies to FatJet mass and Pt
class mSD_scale_res_shift_UL(AddCollectionsModule):

    def __init__(self):
        super(mSD_scale_res_shift_UL, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ


    def customInit(self, initVars):
        self.sampleTree = initVars['sampleTree']
        self.isData = initVars['sample'].isData()
        self.sample = initVars['sample']
        self.config      = initVars['config']
        self.xbbConfig   = XbbConfigTools(self.config)
        self.systematics = self.xbbConfig.getJECuncertainties()
        self.systematicsBoosted = [x for x in self.systematics if 'jerReg' not in x] + ['jms', 'jmr']

        self.maxnFatJet   = 256

        self.Nevent = 0


        if self.sample.isMC():

            self.FatJet_Msoftdrop         = array.array('f', [0.0]*self.maxnFatJet)
            self.FatJet_msoftdrop_nom     = array.array('f', [0.0]*self.maxnFatJet)

            self.sampleTree.tree.SetBranchAddress("FatJet_Msoftdrop", self.FatJet_Msoftdrop)
            self.sampleTree.tree.SetBranchAddress("FatJet_msoftdrop_nom", self.FatJet_msoftdrop_nom)




            #create backup branches
            self.addVectorBranch("FatJet_MsoftdropOld",     default=0.0, branchType='f', length=self.maxnFatJet, leaflist="FatJet_MsoftdropOld[nFatJet]/F")
            self.addVectorBranch("FatJet_msoftdrop_nomOld",     default=0.0, branchType='f', length=self.maxnFatJet, leaflist="FatJet_msoftdrop_nomOld[nFatJet]/F")


            self.FatJet_msoftdrop_syst  = {}
            for syst in self.systematicsBoosted:
                self.FatJet_msoftdrop_syst[syst] = {}
                for Q in self._variations(syst):
                    
                    self.FatJet_msoftdrop_syst[syst][Q]  = array.array('f', [0.0]*self.maxnFatJet)
                    self.sampleTree.tree.SetBranchAddress("FatJet_msoftdrop_"+syst+Q, self.FatJet_msoftdrop_syst[syst][Q])
                    
                    
                    #backup branches
                    self.addVectorBranch('FatJet_msoftdrop_'+syst+Q+'Old', default=0.0, branchType='f', length=self.maxnFatJet,leaflist='FatJet_msoftdrop_'+syst+Q+'Old[nFatJet]/F')






    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC():
            self.markProcessed(tree)
            
            nFatJet = tree.nFatJet

            #print("Nevent ", self.Nevent, tree.GetEntries())
            self.Nevent+=1

            if self.sample.isMC():
                

                PUPPI_scales = tree.FatJet_msoftdrop_corr_PUPPI

                for i in range(nFatJet):

                    #fill backup branches
                    self._b("FatJet_MsoftdropOld")[i]     = self.FatJet_Msoftdrop[i]
                    self._b("FatJet_msoftdrop_nomOld")[i]   = self.FatJet_msoftdrop_nom[i]

                    PUPPI_scale = PUPPI_scales[i] 


                    #overwrite branches
                    self.FatJet_Msoftdrop[i]     = self._b('FatJet_MsoftdropOld')[i]/PUPPI_scale
                    self.FatJet_msoftdrop_nom[i] = self._b("FatJet_msoftdrop_nomOld")[i]/PUPPI_scale

                    for syst in self.systematicsBoosted:
                        for Q in self._variations(syst):
                            
                            # overwrite branches
                            if syst=='jms':

                                if Q=='Up':
                                    if tree.FatJet_msoftdrop_tau21DDT_nom[i] > 0:
                                        tau_ratio = (tree.FatJet_msoftdrop_tau21DDT_jmsUp[i] - tree.FatJet_msoftdrop_tau21DDT_nom[i])/tree.FatJet_msoftdrop_tau21DDT_nom[i]  
                                    else: 
                                        tau_ratio = 0                                   


                                else:
                                    if tree.FatJet_msoftdrop_tau21DDT_nom[i] > 0:
                                        tau_ratio = (tree.FatJet_msoftdrop_tau21DDT_jmsDown[i] - tree.FatJet_msoftdrop_tau21DDT_nom[i])/tree.FatJet_msoftdrop_tau21DDT_nom[i]  
                                    else: 
                                        tau_ratio = 0                                   
                                   

                                jms_var = self._b("FatJet_msoftdrop_nomOld")[i]*(1+tau_ratio)/PUPPI_scale 
                                self.FatJet_msoftdrop_syst[syst][Q][i] = jms_var  
                                                               
    

                            elif syst=='jmr':

                                if Q=='Up':
                                    if tree.FatJet_msoftdrop_tau21DDT_nom[i] > 0:
                                        tau_ratio = (tree.FatJet_msoftdrop_tau21DDT_jmrUp[i] - tree.FatJet_msoftdrop_tau21DDT_nom[i])/tree.FatJet_msoftdrop_tau21DDT_nom[i]  
                                    else: 
                                        tau_ratio = 0                                   
                                   
                                else:
                                    if tree.FatJet_msoftdrop_tau21DDT_nom[i] > 0:
                                        tau_ratio = (tree.FatJet_msoftdrop_tau21DDT_jmrDown[i] - tree.FatJet_msoftdrop_tau21DDT_nom[i])/tree.FatJet_msoftdrop_tau21DDT_nom[i]  
                                    else: 
                                        tau_ratio = 0                                   
                                   


                                jmr_var = self._b("FatJet_msoftdrop_nomOld")[i]*(1+tau_ratio)/PUPPI_scale 
                                self.FatJet_msoftdrop_syst[syst][Q][i] = jmr_var  
                                
                                


                            else:    
                                # fill backup branches
                                self._b("FatJet_msoftdrop_"+syst+Q+'Old')[i] = self.FatJet_msoftdrop_syst[syst][Q][i]
                                self.FatJet_msoftdrop_syst[syst][Q][i]  = self._b("FatJet_msoftdrop_"+syst+Q+'Old')[i]/PUPPI_scale













