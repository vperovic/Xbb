#!/usr/bin/env python
from __future__ import print_function
from __future__ import division
import ROOT
from BranchTools import Collection
from BranchTools import AddCollectionsModule
import array
import os
import math
import numpy as np
from XbbConfig import XbbConfigTools
import time
from XbbConfig import XbbConfigReader, XbbConfigTools
from sample_parser import ParseInfo
from BranchList import BranchList
from FileLocator import FileLocator
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
from sampleTree import SampleTree
import copy
import csv

# correlates the JECs according to new JEC correlation scheme (V11 -> V13) 
class NLORbblt1Weights(AddCollectionsModule):

    def __init__(self, year):
        super(NLORbblt1Weights, self).__init__()
        self.debug = 'XBBDEBUG' in os.environ
        self.quickloadWarningShown = False
        self.existingBranches = {}

        self.year = year if type(year) == str else str(year)

    # only add as new branch if they don't exists already
    def addVectorBranch(self, branchName, default=0, branchType='d', length=1, leaflist=None):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights, self).addVectorBranch(branchName, default, branchType, length, leaflist)
        else:
            print("DEBUG: skip adding branch:", branchName)

    def addBranch(self, branchName, default=1.0):
        if branchName not in self.existingBranches:
            super(NLORbblt1Weights, self).addBranch(branchName, default)
        else:
            print("DEBUG: skip adding branch:", branchName)

    # can be used to overwrite branch if it already exists
    def _b(self, branchName):
        if branchName not in self.existingBranches:
            return super(NLORbblt1Weights, self)._b(branchName)
        else:
            return self.existingBranches[branchName]

    def customInit(self, initVars):

        self.sampleTree = initVars['sampleTree']
        self.sample = initVars['sample']
        self.config = initVars['config']
        self.btagWPloose = float(self.config.get('General','btagWP_Loose'))
        self.btagWPmedium = float(self.config.get('General','btagWP_Medium'))
        print(">>>>>>>>>>>> year", self.year)
        print("Loose BTAG ", self.btagWPloose)
        print("Medium BTAG ", self.btagWPmedium)

        # Weights derived using V+jets in HF CR




        if self.year == "2018":

            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isDY":
                        {
                        "isZee":
                            {
                                '75Inf':{'bins': [0.4,0.6,0.8,1.0],'central':[1.3525, 1.3845, 1.3709],'uncertainty':[0.0978, 0.0757, 0.0738]
                                },
                            },
                          "isZmm":
                            {
                                '75Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central': [1.2998, 1.2827, 1.326,],'uncertainty':[0.0863, 0.0648, 0.0631]
                                },
                            },
                        },
                    }

        elif self.year == "2017":

            self.dRbbWeight_VPt = {
                    #Vpt inclusive for DY
                    "isDY":
                        {
                        "isZee":
                            {
                                '75Inf':{'bins': [0.4,0.6,0.8,1.0],'central': [1.3867, 1.4508, 1.4314] ,'uncertainty': [0.1201, 0.0943, 0.0889]
                                },
                            },
                          "isZmm":
                            {
                                '75Inf':{'bins':[0.4,0.6,0.8,1.0] ,'central': [1.3359, 1.396, 1.3727]  ,'uncertainty': [0.102, 0.0806, 0.0721]
                                },
                            },
                        },
                    }

		#central values are:  [1.3867, 1.4508, 1.4314, 1.0978, 0.9413]
		#uncertainties are:  [0.1201, 0.0943, 0.0889, 0.0191, 0.1754]
		
		#central values are:  [1.3359, 1.396, 1.3727, 1.0509, 1.1556]
		#uncertainties are:  [0.102, 0.0806, 0.0721, 0.0159, 0.1641]



        else:
            raise Exception("Wrong year")


        #self.dRbbWeight_VPt["isDY"]["isWenu"] = self.dRbbWeight_VPt["isDY"]["isZee"]
        #self.dRbbWeight_VPt["isDY"]["isWmunu"] = self.dRbbWeight_VPt["isDY"]["isZmm"]

        #print("self.dRbbWeight_VPt ",self.dRbbWeight_VPt)

        


        self.get_dRbbdict = {'75':'75150', '150':'150250','250':'250Inf'}
        #self.dRbb_branches = ["dRbbWeight_Vpt","dRbbWeight_Incl"]
        #self.dRbb_branches = ["dRbbWeight_Vpt"]
        self.dRbb_branches = ["dRbbWeight_Incl"]

        if self.sample.isMC():
            self.maxNjet   = 256


            for var in ["Jet_btagDeepFlavB"]:
                self.existingBranches[var] = array.array('f', [-1.0]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt"]:
                self.existingBranches[var] = array.array('f', [0.0])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["isZmm","isZee","isWenu","isWmunu","isZnn"]:
                self.existingBranches[var] = array.array('i', [-1])
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])

            for var in ["hJidx"]:
                self.existingBranches[var] = array.array('i', [-1]*self.maxNjet)
                self.sampleTree.tree.SetBranchAddress(var, self.existingBranches[var])
                
            for var in self.dRbb_branches:
                self.addBranch(var)
                for Q in self._variations(var):
                    self.addBranch(var+Q)
                    
    def get_dRbb(self,dEta,dPhi):
        return round(math.sqrt(dEta*dEta+dPhi*dPhi),4)

    def applies(self, attr):
        isNLO_dRbb_lt1 = False

        #if(attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1):
        #    print("res")  
        #print(self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0,"drBBb")
        #print("---------------------")
 
        if (any([s in self.sample.identifier for s in ['amcatnloFXFX']]) and (attr["hJidx"][0] > -1 and attr["hJidx"][1]>-1)):   
            if (self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]) < 1.0):
                if (attr["channel"] == "isZee" or attr["channel"] == "isZmm"):
                    if (any([x in self.sample.identifier for x in ['DYJets']])):
                        #Additional condition: needs to be in Zhf
                        if ((attr["Jet_btagDeepFlavB"][attr["hJidx"][0]] > self.btagWPmedium and attr["Jet_btagDeepFlavB"][attr["hJidx"][1]] > self.btagWPloose)):
                            isNLO_dRbb_lt1 = True
                            #print("dRbb:")
                            #print(self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0]))

                elif (attr["channel"] == "isWenu" or attr["channel"] == "isWmunu"):
                    if (any([x in self.sample.identifier for x in ['DYJets','WJetsToLNu']])):
                        raise Exception("Did you check the btag cuts ? ")
                        isNLO_dRbb_lt1 = True
                elif (attr["channel"] == "isZnn"):
                    if (any([x in self.sample.identifier for x in ['WJetsToLNu','ZJetsToNuNu']])):
                        isNLO_dRbb_lt1 = True
                        raise Exception("Did you check the btag cuts ? ")
                else:
                    isNLO_dRbb_lt1 = False
                        
        return isNLO_dRbb_lt1

    def get_lowerBound(self,value,bins):
        j = 0
        while not (value<bins[j] or value>=250):
            j+=1
        if (value>=250):    
            return str(bins[-1])
        else:
            return str(bins[j-1])

    def get_eventWeight(self, attr):
        
        #dRbbWeight_Vpt  = []
        
        dRbbWeight_Incl = []
 
        dRbb_event     = self.get_dRbb(attr["hJets_FSRrecovered_dEta"][0],attr["hJets_FSRrecovered_dPhi"][0])
        dRbbWeight_bin_incl = '75Inf'
        sample = attr["NLOsample"]
        channel = attr["channel"]
        
        ### Vpt split
        #dRbbWeight_bin_Vpt= self.get_dRbbdict[self.get_lowerBound(attr["V_pt"][0],[75,150,250])]
        #j=0
        #VptBins = [150,250]
        #if any(channel==x for x in ["isZee","isZmm"]): VptBins=[75,150,250]
        #dRbbWeight_bin_Vpt = self.get_dRbbdict[self.get_lowerBound(attr["V_pt"][0],VptBins)]
        #while not (dRbb_event < self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["bins"][j]):
        #    j+=1
        #dRbbWeight_Vpt = [self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1] + self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["uncertainty"][j-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["central"][j-1] - self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_Vpt]["uncertainty"][j-1]]
        

        ### Inclusive
        k=0
        while not (dRbb_event < self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["bins"][k]):
            k+=1
        #print("channel ",channel)
        #print("Vpt ",attr["V_pt"][0])
        dRbbWeight_Incl = [self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1] + self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["uncertainty"][k-1], self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["central"][k-1] - self.dRbbWeight_VPt[sample][channel][dRbbWeight_bin_incl]["uncertainty"][k-1]]
        
        #return  dRbbWeight_Vpt
        return  dRbbWeight_Incl

    def processEvent(self, tree):
        if not self.hasBeenProcessed(tree) and self.sample.isMC(): 
            self.markProcessed(tree)
            attr = {}
            attr["NLOsample"] = None
            for var in ["hJets_FSRrecovered_dEta","hJets_FSRrecovered_dPhi","V_pt","hJidx","isZee","isZmm","isWenu","isWmunu","isZnn","Jet_btagDeepFlavB"]: 
                attr[var] = self.existingBranches[var]
            if any([x in self.sample.identifier for x in ['DYJets']]):
                attr["NLOsample"] = "isDY"
            elif any([x in self.sample.identifier for x in ['WJetsToLNu']]):
                attr["NLOsample"] = "isWJets" 
            elif any([x in self.sample.identifier for x in ['ZJetsToNuNu']]):
                attr["NLOsample"] = "isZJets" 
            attr["channel"] = None   
            

            if attr["isZee"][0] == 1: attr["channel"] = "isZee"
            if attr["isZmm"][0] == 1: attr["channel"] = "isZmm"
            if attr["isWenu"][0] == 1: attr["channel"] = "isWenu"
            if attr["isWmunu"][0] == 1: attr["channel"] = "isWmunu"
            if attr["isZnn"][0] == 1: attr["channel"] = "isZnn"

            #print("channel is ",attr["channel"])

            if self.applies(attr):
                #dRbbWeight_Vpt,dRbbWeight_Incl = self.get_eventWeight(attr)
                
                dRbbWeight_Incl = self.get_eventWeight(attr)
                
                #print("Checking pass boundary conditions")
                #print("Leading", attr["Jet_btagDeepFlavB"][attr["hJidx"][0]])                   
                #print("Subleading", attr["Jet_btagDeepFlavB"][attr["hJidx"][1]])                   
                #print(dRbbWeight_Incl)
                
 
                #self._b("dRbbWeight_Vpt")[0]         = dRbbWeight_Vpt[0]
                #self._b("dRbbWeight_Vpt"+"Up")[0]    = dRbbWeight_Vpt[1]
                #self._b("dRbbWeight_Vpt"+"Down")[0]  = dRbbWeight_Vpt[2]
                self._b("dRbbWeight_Incl")[0]        = dRbbWeight_Incl[0]
                self._b("dRbbWeight_Incl"+"Up")[0]   = dRbbWeight_Incl[1]
                self._b("dRbbWeight_Incl"+"Down")[0] = dRbbWeight_Incl[2]
            else:


                #self._b("dRbbWeight_Vpt")[0]         = 1.0
                #self._b("dRbbWeight_Vpt"+"Up")[0]    = 1.0
                #self._b("dRbbWeight_Vpt"+"Down")[0]  = 1.0
                self._b("dRbbWeight_Incl")[0]        = 1.0
                self._b("dRbbWeight_Incl"+"Up")[0]   = 1.0
                self._b("dRbbWeight_Incl"+"Down")[0] = 1.0

if __name__=='__main__':

    config = XbbConfigReader.read('Wlv2018')
    info = ParseInfo(config=config)
    sample = [x for x in info if x.identifier == 'DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8'][0]
    print(sample)

    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_94b42e05db46cb9fd44285b60e0ba8b57d34232afdeef2661e673e5f_000000_000000_0000_0_5c5fb21d0e20a7f021911ce80109ccd1fa39014fff8bca8fc463de97.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Wlv/mva/18oct20_all_2DbtagdRbbDYWeightsfromLFCR/WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_bb21dac9dc44e46adf956d61d5b485c39341b737ba249606a80eb2f6_000000_000000_0000_0_09ffe41113d97125a40885075110dac51c0f173e112396ca28a14ce2.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")

    sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/VHbbPostNano2018/V12/Wlv/mva/18oct20_all_2DbtagdRbbDYWeightsfromLFCR/DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8/tree_08cac0723ba4794e7272189a973d561e0218e3f66b8b85882f9a12b6_000000_000000_0000_1_9bb8647b504b59ef4532098963a8bc895564e9e8e148b08b61fb15cd.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/") 
  
    #config = XbbConfigReader.read('Zll2018')
    #info = ParseInfo(config=config)
    #sample = [x for x in info if x.identifier == 'DYBJetsToLL_M-50_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8'][0]
    #print(sample)

    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8/tree_94b42e05db46cb9fd44285b60e0ba8b57d34232afdeef2661e673e5f_000000_000000_0000_0_5c5fb21d0e20a7f021911ce80109ccd1fa39014fff8bca8fc463de97.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DY2JetsToLL_M-50_LHEZpT_150-250_TuneCP5_13TeV-amcnloFXFX-pythia8/tree_0b9e5ec9f1d4328fc7ef364ebb5dd7364200ce37948e3753c76e6133_000000_000000_0000_5_fadbb45f88e40b3f3f44b0585861b2c4f87d08c5be7feda31913e251.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    #sampleTree = SampleTree(['/pnfs/psi.ch/cms/trivcat/store/user/krgedia/VHbb/Zll/VHbbPostNano2018/mva/18oct20_all_NLOforbenr/DYBJetsToLL_M-50_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8/tree_a6b49dbb202048bd17cdf3f392ef3c2813bf6388d3275d8708169f7b_000000_000000_0000_0_f11fc63b7542561838a2fc3df60d8f8f1f67dd0c6fd24dd343324e76.root'], treeName='Events', xrootdRedirector="root://t3dcachedb03.psi.ch/")
    w = NLORbblt1Weights("2018")
    w.customInit({'sampleTree': sampleTree, 'sample': sample, 'config': config})
    sampleTree.addOutputBranches(w.getBranches())
    #histograms={}
    #for jec in w.JEC_reduced:
        
    #for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
    #    histograms[var] = {}
    #    for syst in w.JEC_reduced:
    #        histograms[var][syst] = {}
    #        for Q in ['Up','Down']:
    #            histograms[var][syst][Q]=ROOT.TH1F(var+syst+Q, var+syst+Q, 400, -2.0, 2.0 )

    n=0 
    #var = "MET_phi"
    for event in sampleTree:
        n=n+1
        w.processEvent(event)
        #if n==50: break

   # with open('jec_validate_'+var+'.csv', 'w') as file:
   #     writer = csv.writer(file)
   #     writer.writerow(["event","run","Q",'true_jesAbsolute','jesAbsolute','AT','diff','true_jesAbsolute_2018','jesAbsolute_2018','AT','diff','true_jesBBEC1','jesBBEC1','AT','diff','true_jesBBEC1_2018','jesBBEC1_2018','AT','diff','true_jesEC2','jesEC2','AT','diff','true_jesEC2_2018','jesEC2_2018','AT','diff','true_jesHF','jesHF','AT','diff','true_jesHF_2018','jesHF_2018','AT','diff','true_jesRelativeSample_2018','jesRelativeSample_2018','AT','diff'])
   #     for event in sampleTree:
   #         n=n+1
   #         event,run,true_attr, attr = w.processEvent(event)
   #         for Q in ['Up','Down']:
   #             njet = len(true_attr[var][w.JEC_reduced[0]][Q])
   #             if njet>1:
   #                 for i in range(njet):
   #                     writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][i],attr[var]['jesAbsolute'][Q][i],'','',true_attr[var]['jesAbsolute_2018'][Q][i],attr[var]['jesAbsolute_2018'][Q][i],'','',true_attr[var]['jesBBEC1'][Q][i],attr[var]['jesBBEC1'][Q][i],'','',true_attr[var]['jesBBEC1_2018'][Q][i],attr[var]['jesBBEC1_2018'][Q][i],'','',true_attr[var]['jesEC2'][Q][i],attr[var]['jesEC2'][Q][i],'','',true_attr[var]['jesEC2_2018'][Q][i],attr[var]['jesEC2_2018'][Q][i],'','',true_attr[var]['jesHF'][Q][i],attr[var]['jesHF'][Q][i],'','',true_attr[var]['jesHF_2018'][Q][i],attr[var]['jesHF_2018'][Q][i],'','',true_attr[var]['jesRelativeSample_2018'][Q][i],attr[var]['jesRelativeSample_2018'][Q][i],'',''])
   #             else:
   #                 writer.writerow([event,run,Q,true_attr[var]['jesAbsolute'][Q][0],attr[var]['jesAbsolute'][Q][0],'','',true_attr[var]['jesAbsolute_2018'][Q][0],attr[var]['jesAbsolute_2018'][Q][0],'','',true_attr[var]['jesBBEC1'][Q][0],attr[var]['jesBBEC1'][Q][0],'','',true_attr[var]['jesBBEC1_2018'][Q][0],attr[var]['jesBBEC1_2018'][Q][0],'','',true_attr[var]['jesEC2'][Q][0],attr[var]['jesEC2'][Q][0],'','',true_attr[var]['jesEC2_2018'][Q][0],attr[var]['jesEC2_2018'][Q][0],'','',true_attr[var]['jesHF'][Q][0],attr[var]['jesHF'][Q][0],'','',true_attr[var]['jesHF_2018'][Q][0],attr[var]['jesHF_2018'][Q][0],'','',true_attr[var]['jesRelativeSample_2018'][Q][0],attr[var]['jesRelativeSample_2018'][Q][0],'',''])

   #         #print("----------events over------------")
   #         if n==5: break

   # f = TFile("delete.root","RECREATE")

   # for var in ["Jet_pt", "Jet_mass", "MET_pt", "MET_phi", "FatJet_pt", "FatJet_msoftdrop"]:
   #     for syst in w.JEC_reduced:
   #         for Q in ['Up','Down']:
   #             histograms[var][syst][Q].Write()
   # f.Close()
