for i in "jer jerReg jesAbsoluteFlavMap jesAbsoluteMPFBias jesAbsoluteScale jesAbsoluteStat jesFlavorQCD jesFragmentation jesPileUpDataMC jesPileUpEnvelope jesPileUpMuZero jesPileUpPtBB jesPileUpPtEC1 jesPileUpPtEC2 jesPileUpPtHF jesPileUpPtRef jesRelativeBal jesRelativeFSR jesRelativeJEREC1 jesRelativeJEREC2 jesRelativeJERHF jesRelativePtBB jesRelativePtEC1 jesRelativePtEC2 jesRelativePtHF jesRelativeStatEC jesRelativeStatFSR jesRelativeStatHF jesSinglePionECAL jesSinglePionHCAL jesTotal".split(" "):
    s = "jer_Up: kinFit_H_mass_fit_jer_Up H_mass_jer_Up kinFit_H_pt_fit_jer_Up H_pt_jer_Up kinFit_HVdPhi_fit_jer_Up abs(VHbb::deltaPhi(H_phi_jer_Up,V_phi)) (Jet_btagDeepB[hJidx[0]]>0.1241)+(Jet_btagDeepB[hJidx[0]]>0.4184)+(Jet_btagDeepB[hJidx[0]]>0.7527) (Jet_btagDeepB[hJidx[1]]>0.1241)+(Jet_btagDeepB[hJidx[1]]>0.4184)+(Jet_btagDeepB[hJidx[1]]>0.7527) kinFit_hJets_pt_0_fit_jer_Up Jet_PtReg[hJidx[0]]*(Jet_pt_jerUp[hJidx[0]]/Jet_Pt[hJidx[0]]) kinFit_hJets_pt_1_fit_jer_Up Jet_PtReg[hJidx[1]]*(Jet_pt_jerUp[hJidx[1]]/Jet_Pt[hJidx[1]]) kinFit_V_mass_fit_jer_Up V_mass Sum$(Jet_pt_jerUp>30&&abs(Jet_eta)<2.4&&(Jet_puId>6||Jet_Pt>50)&&Jet_jetId>0&&Jet_lepFilter>0&&Iteration$!=hJidx[0]&&Iteration$!=hJidx[1]) kinFit_V_pt_fit_jer_Up V_pt kinFit_jjVPtRatio_fit_jer_Up (H_pt_jer_Up/V_pt) abs(Jet_eta[hJidx[0]]-Jet_eta[hJidx[1]]) SA5 VHbb::deltaR(kinFit_H_eta_fit_jer_Up,kinFit_H_phi_fit_jer_Up,kinFit_V_eta_fit_jer_Up,kinFit_V_phi_fit_jer_Up) VHbb::deltaR(H_eta_jer_Up,H_phi_jer_Up,V_eta,V_phi) MET_pt_jerUp kinFit_H_mass_sigma_fit_jer_Up kinFit_n_recoil_jets_fit_jer_Up VHbb::deltaR(Jet_eta[hJidx[0]],Jet_phi[hJidx[0]],Jet_eta[hJidx[1]],Jet_phi[hJidx[1]])" 

    varUp = s.replace('jer',i)
    #varUp = varUp.replace('H_mass_jet_Up','replaced_h_mass')
    ##varUp = varUp.replace('Jet_pt_jerRegUp','Jet_Pt')
    #varUp = varUp.replace('MET_pt_jerRegUp','MET_Pt')
    print varUp

    #print(i)
    varDown = s.replace('jer',i)
    varDown = varDown.replace('Up','Down')
    #varDown = varDown.replace('Jet_PtReg[hJidx[0]]*(Jet_pt_jerRegDown[hJidx[0]]/Jet_Pt[hJidx[0]])','Jet_PtRegDown[hJidx[0]]').replace('Jet_PtReg[hJidx[1]]*(Jet_pt_jerRegDown[hJidx[1]]/Jet_Pt[hJidx[1]])','Jet_PtRegDown[hJidx[1]]')
    #varDown = varDown.replace('Jet_pt_jerRegDown','Jet_Pt')
    #varDown = varDown.replace('MET_pt_jerRegDown','MET_Pt')
    print varDown

    print ''
