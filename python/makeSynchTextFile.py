import sys
import os
import ROOT as r
from myutils.XbbConfig import XbbConfigReader, XbbConfigTools
from myutils.sampleTree import SampleTree
from myutils import ParseInfo
from myutils.BranchList import BranchList
from myutils.FileLocator import FileLocator



r.gROOT.SetBatch(True)                                                                                                                                  
r.gSystem.Load("../interface/VHbbNameSpace_h.so")   


config = XbbConfigTools(config=XbbConfigReader.read("/mnt/t3nfs01/data01/shome/krgedia/CMSSW_10_1_0/src/Xbb/python/Wlv2018"))
sel_formula = str(config.get("Cuts","ttbar_med_Wln"))

fileForSync = open("Xbb20181lepttbarCR.txt", "w")\

#formula="(V_pt >= 150.0 && V_pt < 250.0) && (((hJidx[0]>-1&&hJidx[1]>-1) && (!((Hbb_fjidx>-1) && ((((FatJet_Pt[Hbb_fjidx]>250 && abs(FatJet_eta[Hbb_fjidx])<2.5 && lepMetDPhi < 2 && nAddLep15_2p5 == 0 && V_pt>250 && ((isData && isWenu && (run<319077 || !(Electron_phi[vLidx[0]]>-2.0 && Electron_phi[vLidx[0]]<-0.75 && Electron_eta[vLidx[0]]<-1.5 ) )) || (isData != 1) || (isWmunu))) && Sum$(VHbb::deltaR(FatJet_eta[Hbb_fjidx],FatJet_phi[Hbb_fjidx],Jet_eta,Jet_phi)> 0.8 && Jet_btagDeepB > 0.4184 && Jet_lepFilter>0 && Jet_PtReg>25 && fabs(Jet_eta)<2.5) == 0 && FatJet_deepTagMD_bbvsLight[Hbb_fjidx]>0.8) && FatJet_Msoftdrop[Hbb_fjidx] > 90 && FatJet_Msoftdrop[Hbb_fjidx] < 150) || ((FatJet_Pt[Hbb_fjidx]>250 && abs(FatJet_eta[Hbb_fjidx])<2.5 && lepMetDPhi < 2 && nAddLep15_2p5 == 0 && V_pt>250 && ((isData && isWenu && (run<319077 || !(Electron_phi[vLidx[0]]>-2.0 && Electron_phi[vLidx[0]]<-0.75 && Electron_eta[vLidx[0]]<-1.5 ) )) || (isData != 1) || (isWmunu))) && Sum$(VHbb::deltaR(FatJet_eta[Hbb_fjidx],FatJet_phi[Hbb_fjidx],Jet_eta,Jet_phi)> 0.8 && Jet_btagDeepB > 0.4184 && Jet_lepFilter>0 && Jet_PtReg>25 && fabs(Jet_eta)<2.5) > 0 && FatJet_deepTagMD_bbvsLight[Hbb_fjidx]>0.8 && FatJet_Msoftdrop[Hbb_fjidx] > 50) || (((FatJet_Pt[Hbb_fjidx]>250 && abs(FatJet_eta[Hbb_fjidx])<2.5 && lepMetDPhi < 2 && nAddLep15_2p5 == 0 && V_pt>250 && ((isData && isWenu && (run<319077 || !(Electron_phi[vLidx[0]]>-2.0 && Electron_phi[vLidx[0]]<-0.75 && Electron_eta[vLidx[0]]<-1.5 ) )) || (isData != 1) || (isWmunu))) && Sum$(VHbb::deltaR(FatJet_eta[Hbb_fjidx],FatJet_phi[Hbb_fjidx],Jet_eta,Jet_phi)> 0.8 && Jet_btagDeepB > 0.4184 && Jet_lepFilter>0 && Jet_PtReg>25 && fabs(Jet_eta)<2.5) == 0 && FatJet_deepTagMD_bbvsLight[Hbb_fjidx]>0.8) && ((FatJet_Msoftdrop[Hbb_fjidx] > 50 && FatJet_Msoftdrop[Hbb_fjidx] < 90) || (FatJet_Msoftdrop[Hbb_fjidx] > 150 && FatJet_Msoftdrop[Hbb_fjidx] < 250))) || ((FatJet_Pt[Hbb_fjidx]>250 && abs(FatJet_eta[Hbb_fjidx])<2.5 && lepMetDPhi < 2 && nAddLep15_2p5 == 0 && V_pt>250 && ((isData && isWenu && (run<319077 || !(Electron_phi[vLidx[0]]>-2.0 && Electron_phi[vLidx[0]]<-0.75 && Electron_eta[vLidx[0]]<-1.5 ) )) || (isData != 1) || (isWmunu))) && Sum$(VHbb::deltaR(FatJet_eta[Hbb_fjidx],FatJet_phi[Hbb_fjidx],Jet_eta,Jet_phi)> 0.8 && Jet_btagDeepB > 0.4184 && Jet_lepFilter>0 && Jet_PtReg>25 && fabs(Jet_eta)<2.5) == 0 && FatJet_deepTagMD_bbvsLight[Hbb_fjidx]<0.8 && FatJet_Msoftdrop[Hbb_fjidx] > 50))))) && (((isWenu||isWmunu) && H_pt > 100 && nAddLep15_2p5==0 && dPhiLepMet < 2.0 && H_mass>50) && Jet_btagDeepB[hJidx[0]] > 0.7527 && H_mass<250 && Sum$(Jet_Pt>30&&abs(Jet_eta)<2.5&&(Jet_puId>6||Jet_Pt>50)&&Jet_jetId>4&&Jet_lepFilter&&Iteration$!=hJidx[0]&&Iteration$!=hJidx[1]) > 1 && ((isData && isWenu && (run<319077 || !(Electron_phi[vLidx[0]]>-2.0 && Electron_phi[vLidx[0]]<-0.75 && Electron_eta[vLidx[0]]<-1.5 ) )) || (isData != 1) || (isWmunu)))) && ((isWenu && Electron_mvaFall17V2Iso_WP80[vLidx[0]]) || (isWmunu && Muon_tightId[vLidx[0]]))"

#formula="V_pt>150"

formula = sel_formula


formulaVars=["V_pt","hJidx","Hbb_fjidx","FatJet_Pt","FatJet_eta","lepMetDPhi","nAddLep15_2p5","isData","isWenu","run","dPhiLepMet","Electron_phi","vLidx","Electron_eta","isWmunu","FatJet_phi","Jet_eta","Jet_phi","Jet_btagDeepB","Jet_lepFilter","Jet_PtReg","FatJet_deepTagMD_bbvsLight","FatJet_Msoftdrop","H_pt","H_mass","Jet_Pt","Jet_puId","Jet_jetId","hJidx","Electron_mvaFall17V2Iso_WP80","Muon_tightId","HTXS_Higgs_pt","HTXS_Higgs_y","HTXS_stage1_1_cat_pTjet25GeV","HTXS_stage1_1_cat_pTjet30GeV","HTXS_stage1_1_fine_cat_pTjet25GeV","HTXS_stage1_1_fine_cat_pTjet30GeV","HTXS_stage_0","HTXS_stage_1_pTjet25","HTXS_stage_1_pTjet30","HTXS_njets25","HTXS_njets30","luminosityBlock","run","event"]



varsToCheck=["run","event","H_mass","H_pt","V_mt","V_pt","V_pt/H_pt","abs(TVector2::Phi_mpi_pi(V_phi-H_phi))","(Jet_btagDeepB[hJidx[0]]>0.1241)+(Jet_btagDeepB[hJidx[0]]>0.41148)+(Jet_btagDeepB[hJidx[0]]>0.7527)","(Jet_btagDeepB[hJidx[1]]>0.1241)+(Jet_btagDeepB[hJidx[1]]>0.41148)+(Jet_btagDeepB[hJidx[1]]>0.7527)","max(hJets_0_pt_FSRrecovered,hJets_1_pt_FSRrecovered)","min(hJets_0_pt_FSRrecovered,hJets_1_pt_FSRrecovered)","hJets_FSRrecovered_dEta","MET_Pt","dPhiLepMet","top_mass2_05","SA5","Sum$(Jet_Pt>30&&abs(Jet_eta)<2.5&&(Jet_puId>6||Jet_Pt>50.0)&&Jet_jetId>4&&Jet_lepFilter&&Iteration$!=hJidx[0]&&Iteration$!=hJidx[1])","weight"]
varsToCheck=["run","event",'VH_Whf_med_Wln_WP_resonly_splitc_30june']
#varsToCheck=["event"]


treeName="Events"
for var in varsToCheck:
    fileForSync.write(str(var)+",")   
fileForSync.write("\n")

print sys.argv
fileNames=sys.argv[-1]
print "fileNames: ",fileNames

filenames = []
filenames.append(fileNames)

for fileName in filenames:
    print "fileName: ",fileName
    tFile=r.TFile.Open(fileName)

    tree=tFile.Get(treeName)

    #tree.SetBranchStatus("*", 0)
    #for branchName in formulaVars:
    #    tree.SetBranchStatus(branchName, 1)
        
    nEntries=tree.GetEntries()
    print(nEntries)

    for iEntry in range(0,20):
        tree.GetEntry(iEntry)
        if tree.Query("",formula, "", 1, iEntry).GetRowCount():
            print "Status of selection cut: ",tree.Query("",formula, "", 1, iEntry).GetRowCount()
            print "iEntry: ", iEntry
            for var in varsToCheck:
                print "value of %s is %d"%(var, tree.GetLeaf(var).GetValue(0))
            #    fileForSync.write(str(tree.GetLeaf(var).GetValue(0))+",")
            #    fileForSync.write("\n")
                #tFile.Close()
                #fileForSync.close()
