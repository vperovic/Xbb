#!/usr/bin/env python 
from __future__ import division
import ROOT
import pickle
import array
import copy
import glob
from myutils.sampleTree import SampleTree
import sys
import os
from myutils.XbbConfig import XbbConfigReader, XbbConfigTools
from myutils import ParseInfo
from myutils.BranchList import BranchList
from myutils.FileLocator import FileLocator
import csv
ROOT.gROOT.SetBatch(True) 
ROOT.gSystem.Load("../interface/VHbbNameSpace_h.so")
from ROOT import TCanvas, TFile, TProfile, TNtuple, TH1F, TH2F
from ROOT import gROOT, gBenchmark, gRandom, gSystem, Double
import argparse

#with open('jacknife_2017/Zee2017.pickle', 'rb') as handle:
#    zee = pickle.load(handle)
#with open('jacknife_2017_26oct21/Zee2017.pickle', 'rb') as handle:
#    zee = pickle.load(handle)
#with open('jacknife_2017_3jan22/2017NLO_jacknife_Zee.pickle', 'rb') as handle:
#    zee = pickle.load(handle)
with open('jacknife_2017_NLO_31jan22/jacknife_2017_Zee_31jan22.pickle', 'rb') as handle:
    zee = pickle.load(handle)
with open('jacknife_2017_NLO_31jan22/jacknife_2017_Zmm_31jan22.pickle', 'rb') as handle:
    zmm = pickle.load(handle)
with open('jacknife_2017_NLO_31jan22/jacknife_2017_Wen_31jan22.pickle', 'rb') as handle:
    wen = pickle.load(handle)
with open('jacknife_2017_NLO_31jan22/jacknife_2017_Wmn_31jan22.pickle', 'rb') as handle:
    wmn = pickle.load(handle)
with open('jacknife_2017_NLO_31jan22/jacknife_2017_Znn_31jan22.pickle', 'rb') as handle:
    znn = pickle.load(handle)

dict_merge = {}
dict_merge.update(zee)
dict_merge.update(zmm)
dict_merge.update(wen)
dict_merge.update(wmn)
dict_merge.update(znn)

with open('jacknife_2017_NLO_31jan22/Xbbdict_2017'+'.pickle', 'wb') as handle:
    pickle.dump(dict_merge, handle, protocol=pickle.HIGHEST_PROTOCOL)


#print dictionary == b
