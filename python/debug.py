from __future__ import division

file = open(r'./debug.txt', 'r')
WZ = 0
WW = 0
ZZ = 0
ZH = 0
ggZH = 0
WH = 0 
Zjets = 0
Wjets = 0
DYjets = 0
TT = 0
ST = 0

for line in file:
    
    num = line.split("->")[1].strip()    

    if "WZTo" in line:
        print "line WZ ", line
        WZ = WZ + float(num.split()[0])

    if "WW" in line:
        print "line WW ", line
        WW = WW + float(num.split()[0])

    if "ZZ" in line:
        print "line ZZ ", line
        ZZ = ZZ + float(num.split()[0])

    if any(x in line for x in [" ZllH_"," ZnnH_"]):
        print "line ZH ", line
        ZH = ZH + float(num.split()[0])

    if any(x in line for x in ["ggZnnH_","ggZllH_"]):
        print "line ggZH ", line
        ggZH = ggZH + float(num.split()[0])

    if any(x in line for x in ["WplusH","WminusH"]):
        print "line WH ", line
        WH = WH + float(num.split()[0])
#
    if any(x in line for x in ["ZJetsHT","ZBGen","ZBJe"]):
        print "line ZJets ", line
        Zjets = Zjets + float(num.split()[0])
#
    if any(x in line for x in ["WJetsHT","WBGen","WBJe"]):
        print "line WJets ", line
        Wjets = Wjets + float(num.split()[0])

    if any(x in line for x in ["M4HT","ZJets_","DYBJets","DYJets"]):
        print "line DYjets ", line
        DYjets = DYjets + float(num.split()[0])

    if "TT" in line:
        TT = TT + float(num.split()[0])

    if "ST" in line:
        ST = ST + float(num.split()[0])



print "WZ: {} ".format(WZ/float(2))
print "WW: {} ".format(WW/float(2))
print "ZZ: {} ".format(ZZ/float(2))
print "ZH: {} ".format(ZH/float(2))
print "ggZH: {} ".format(ggZH/float(2))
print "WH: {} ".format(WH/float(2))
print "Zjets: {} ".format(Zjets/float(2))
print "Wjets: {} ".format(Wjets/float(2))
print "DYjets: {} ".format(DYjets/float(2))
print "TT: {} ".format(TT/float(2))
print "ST: {} ".format(ST/float(2))

