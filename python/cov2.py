import csv
import pandas as pd
import numpy
import re
	
var=[]
count = 0
start_counter = 0
stop_counter  = 0
start_vars = 0
filename="vz1lepCRonly"
with open('cov.csv', "wb") as csv_file:
    f = open(filename+'.txt','r')
    line=f.readline()
    try:
	while line:
    	    count = count +1 
            if "external parameters:" in line.strip():
		start_vars += 1
	        if start_vars == 2:	
		    line = f.readline()
		    line = f.readline()
		    line = f.readline()
		    while len(line.strip().replace('||','').split())>3:
                        var.append(line.strip().replace('||','').split()[1])
		        line = f.readline()	
                    writer = csv.writer(csv_file, delimiter=',')
	            writer.writerow(var)		
		    start_vars+=1
            if "MnUserCovariance Parameter correlations:" in line.strip():
		start_counter += 1
	    	if start_counter == 2:
		    line = f.readline()
		    line = f.readline()
		    while len(line.split())>5:
		        writer.writerow([i for i in line.split()])
		        line = f.readline()
		    start_counter+=1
	    line = f.readline()
    except Exception as e:
	print e

df = pd.read_csv('cov.csv',delimiter=',')
df.insert(loc=0,column='var',value=var)
df.to_csv('cov.csv',sep=',')

df = pd.read_csv('cov.csv',delimiter=',')
retain =  [x for x in df.columns if x.startswith('SF_') or x=='var']
df = df.drop(df.columns[[0]], axis=1)
df = df.loc[:,retain]

#df.to_csv('covv.csv')
df = df.loc[df['var'].isin(retain)]

v = len(df.columns) -1
for i in range(0,v):
    df.iloc[i,0] = str(df.iloc[i,0]).replace("SF_","").replace("_2018","").replace("_2017","").replace("_BOOST","")
    df.iloc[i] = df.iloc[i].apply(lambda x: round(x, 8) if type(x) is numpy.float64 else x)
    
mapping = {}
for i in range(0,v+1):
	mapping[df.columns[i]] = df.columns[i].replace("SF_","").replace("_2018","").replace("_2017","").replace("_BOOST","")


df = df.rename(columns=mapping)

cols =  [x for x in df.columns] 


dict  = {}
zero = []
one = []
two = []
other = []
for i in range (0,len(cols)):
    dict[cols[i]]=i
    if re.search('Z[+e,+m,+l]',cols[i]): two.append(cols[i])
    elif re.search('W[e,m,l][n]',cols[i]): one.append(cols[i])
    elif re.search('Znn',cols[i]): zero.append(cols[i])
    else: other.append(cols[i])

rearrange = other+zero+one+two
rearrange_index = [dict[key] for key in rearrange]
df = df.reindex(columns=rearrange)
df = df.set_index('var')
rearrange.remove('var')
df = df.reindex(rearrange)
df.to_csv(filename+'.csv')



