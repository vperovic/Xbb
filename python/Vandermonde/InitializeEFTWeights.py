import numpy as np
from scipy.linalg import inv
from sklearn.preprocessing import PolynomialFeatures
from WeightInfo import *



class SimulatedWC:
    def __init__( self, filename ):

	self.weightinfo = WeightInfo(filename) 
	self.invVandermonde = None 	
	self.polyfeatures = None
	self.transformedWC = None	

    def ComputeVandermonde(self):

	self.polyfeatures = PolynomialFeatures(2)		
	self.transformedWC = self.polyfeatures.fit_transform(self.weightinfo.basepoint_coordinates) 
	self.invVandermonde = inv(self.transformedWC)


    def DumpVandermonde(self, output):

	np.save(output, self.invVandermonde)


if __name__ == "__main__":


	SimWC = SimulatedWC("pickle/SMEFTsim_VH_reweight_card.pkl")
	SimWC.ComputeVandermonde()
	print(SimWC.invVandermonde)
	print(SimWC.weightinfo.variables)
	SimWC.DumpVandermonde("test")

	weights = np.arange(153).reshape(153,1)
	print(np.matmul(SimWC.invVandermonde, weights).shape)
	coeff = np.matmul(SimWC.invVandermonde, weights)


	WC = np.zeros((4, 16)) #nb de points, nb de variables
	WC[0, 3] = 1
	WC[1, 4] = 1
	WC[2, 5] = 1
	WC[3, 3] = 1

        print(np.diag(np.ones(16)))


	WCpoly = SimWC.polyfeatures.fit_transform(WC)
	print(WCpoly.shape)
	print(np.matmul(WCpoly,coeff ).shape)
	print(np.matmul(WCpoly, coeff).flatten())

	"""
	#A = WeightInfo("SMEFTsim_VH_reweight_card.pkl")
	SimWC = SimulatedWC("SMEFTsim_VH_reweight_card.pkl")
	
	A = SimWC.weightinfo
	
	print(A.basepoint_coordinates.shape)
	print(A.variables)
	poly = PolynomialFeatures(2)
	B = poly.fit_transform(A.basepoint_coordinates)
	print(B.shape)
	C = inv(B)

	print(C)
	"""
