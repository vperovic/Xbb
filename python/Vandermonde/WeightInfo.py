# General
import pickle
import scipy.special
import scipy.linalg
import itertools
from math import *

import numpy as np

# Logger
import logging
logger = logging.getLogger(__name__)

class WeightInfo:
    def __init__( self, filename ):
        data = pickle.load(file(filename))

        if 'rw_dict' in data.keys(): self.data = data['rw_dict']
        else: self.data = data

        if 'order' in data.keys(): self.pkl_order = data['order']['order']
        else: self.pkl_order = None

        if 'ref_point' in data.keys(): self.ref_point = data['ref_point']
        else: 
            self.ref_point = None
            logger.warning( "No reference point found in pkl file!" )

        # store all variables (Wilson coefficients)
        self.variables = self.data.keys()[0].split('_')[::2]
        self.nvar      = len(self.variables)

        # compute reference point coordinates
        self.ref_point_coordinates = { var: float( self.ref_point[var] ) if ( self.ref_point is not None and var in self.ref_point.keys() ) else 0 for var in self.variables }

        # Sort wrt to position in ntuple
        self.id = self.data.keys()
        self.id.sort(key=lambda w: self.data[w])
        self.nid = len(self.id)

        logger.debug( "Found %i variables: %s. Found %i weights." %(self.nvar, ",".join( self.variables ), self.nid) )
        
        weightInfo_data = list(self.data.iteritems())
        weightInfo_data.sort( key = lambda w: w[1] )
        self.basepoint_coordinates = map( lambda d: [d[v] for v in self.variables] , map( lambda w: self.interpret_weight(w[0]), weightInfo_data) )

        self.basepoint_coordinates = np.array(self.basepoint_coordinates)

    def interpret_weight(self, weight_id):
        str_s = weight_id.split('_')
        res={}
        for i in range(len(str_s)/2):
            res[str_s[2*i]] = float(str_s[2*i+1].replace('m','-').replace('p','.'))
        return res

if __name__ == "__main__":

    filename = "SMEFTsim_VH_reweight_card.pkl"

    A = WeightInfo(filename)
    print(A.basepoint_coordinates.shape)
    print(A.variables)
    
    
    
    
    


