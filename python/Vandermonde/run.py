import argparse, sys, os, math
from optparse import OptionParser
import InitializeEFTWeights as EFTWeights

def main():
    parser = OptionParser()
    parser.add_option("-i", "--inputfile",
                      action="store", dest="inputfile", default="",
                      help="Pickle file containing the simulated WC values")
    parser.add_option("-o", "--output",
                      action="store", dest="output", default="",
                      help="Numpy array output name for the inverse Vandermonde matrix")

    
    (options, args) = parser.parse_args()
    

    SimWC = EFTWeights.SimulatedWC(options.inputfile)
    SimWC.ComputeVandermonde()
    SimWC.DumpVandermonde("output/" + options.output)
    


if __name__ == '__main__':
    main()
