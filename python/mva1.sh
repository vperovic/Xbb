#!/bin/bash

# example: VHbb2018commonconfig/mva_bins.sh "arctans(15)"
# arctan shape binning with S(number of signal events) bins, max 15

REBINMETHOD=$1

echo "bin boundaries for method ${REBINMETHOD}"
echo ""
echo "dc:SR_low_Zmm.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_low_Zmm --vars DNN --set="plotDef:DNN.binList=<!dc:SR_low_Zmm|rebin_list!>;plotDef:DNN.relPath=<!SR_low_Zll|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_low_Zmm" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_low_Zee.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_low_Zee --vars DNN --set="plotDef:DNN.binList=<!dc:SR_low_Zee|rebin_list!>;plotDef:DNN.relPath=<!SR_low_Zll|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_low_Zee" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_med_Zmm_0j.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_med_Zmm_0j --vars DNN --set="plotDef:DNN.binList=<!dc:SR_med_Zmm_0j|rebin_list!>;plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_med_Zmm_0j" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_med_Zee_0j.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_med_Zee_0j --vars DNN --set="plotDef:DNN.binList=<!dc:SR_med_Zee_0j|rebin_list!>;plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_med_Zee_0j" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_med_Zmm_ge1j.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_med_Zmm_ge1j --vars DNN --set="plotDef:DNN.binList=<!dc:SR_med_Zmm_ge1j|rebin_list!>;plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_med_Zmm_ge1j" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_med_Zee_ge1j.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_med_Zee_ge1j --vars DNN --set="plotDef:DNN.binList=<!dc:SR_med_Zee_ge1j|rebin_list!>;plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_med_Zee_ge1j" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high_Zmm.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_high_Zmm --vars DNN --set="plotDef:DNN.binList=<!dc:SR_high_Zmm|rebin_list!>;plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_high_Zmm" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high_Zee.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_high_Zee --vars DNN --set="plotDef:DNN.binList=<!dc:SR_high_Zee|rebin_list!>;plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_high_Zee" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_high_Zmm_BOOST.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_high_Zmm_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!Eval|VH_BDT_branchName!>.Nominal;plotDef:DNN.xAxis=DNN_SR_high_Zmm_BOOST" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_high_Zee_BOOST.rebin_list="`./submit.py -J runplot -T Zll2018 -F tmp --regions SR_high_Zee_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:BDT.relPath=<!Eval|VH_BDT_branchName!>.Nominal;plotDef:BDT.xAxis=DNN_SR_high_Zee_BOOST" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
