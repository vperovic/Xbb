#!/usr/bin/env python
import ROOT
import sys
ROOT.gROOT.SetBatch(True)
from myutils import BetterConfigParser, ParseInfo
import glob
from multiprocessing import Process
from myutils.sampleTree import SampleTree as SampleTree
import argparse
import itertools
from myutils.BranchList import BranchList
from myutils.XbbConfig import XbbConfigReader, XbbConfigTools

parser = argparse.ArgumentParser(description='getExtWeightsNew: compute stitching coefficients for samples with overlap')
parser.add_argument('--samples', dest='samples', help='list of sample identifiers')
parser.add_argument('--output', dest='output', help='name of output config file')
parser.add_argument('--cuts', dest='cuts', help='cuts', default='')
parser.add_argument('--from', dest='fromFolder', help='folder name to use, e.g. PREPout', default='SYSout')
parser.add_argument('--fc', dest='fc', help='fc', default='')
parser.add_argument('--prune', dest='prune', help='remove contributions with fraction lower than this', default='0.0')
parser.add_argument('-T', dest='tag', help='config tag')
args = parser.parse_args()

# 1) ./getExtWeights.py configname
# 2) ./getExtWeight.py configname verify

#samples_cuts = {
#    "DYJetsToLL_Pt-50To100_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=50&&LHE_Vpt<100)",
#    "DYJetsToLL_Pt-100To250_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=100&&LHE_Vpt<250)",
#    "DYJetsToLL_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=250&&LHE_Vpt<400)",
#    "DYJetsToLL_Pt-400To650_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=400&&LHE_Vpt<650)",
#    "DYJetsToLL_Pt-650ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=650)",
#    "WJetsToLNu_Wpt-0To50_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=0&&LHE_Vpt<50)",
#    "WJetsToLNu_Pt-50To100_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=50&&LHE_Vpt<100)",
#    "WJetsToLNu_Pt-100To250_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=100&&LHE_Vpt<250)",
#    "WJetsToLNu_Pt-250To400_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=250&&LHE_Vpt<400)",
#    "WJetsToLNu_Pt-400To600_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=400&&LHE_Vpt<600)",
#    "WJetsToLNu_Pt-600ToInf_TuneCP5_13TeV-amcatnloFXFX-pythia8":"(LHE_Vpt>=600)"
#}




def getEventCount(config, sampleIdentifier, cut="1", sampleTree=None, sample=None):
    print(sampleIdentifier)
    #print(type(samples_cuts))
    #if sampleIdentifier in samples_cuts:
    #    if cut is not "1":
    #        cut = cut+"&&"+samples_cuts[sampleIdentifier]
    if not sampleTree:
        sampleTree = SampleTree({
                'name': sampleIdentifier, 
                'folder': config.get('Directories',args.fromFolder).strip()
            }, config=config)
    h1 = ROOT.TH1D("h1", "h1", 1, 0, 2)
    ##scaleToXs = sampleTree.getScale(sample)
    #nEvents = sampleTree.tree.Draw("1>>h1", "(" + cut + ")*genWeight*%1.6f"%scaleToXs, "goff")
    print(cut)
    nEvents = sampleTree.tree.Draw("1>>h1", cut, "goff")
    print(h1.GetBinContent(0))
    print(h1.GetBinContent(1))
    print(h1.GetBinContent(2))

    nEventsWeighted = h1.GetBinContent(1)
    #print("DEBUG:", sampleIdentifier, cut, " MC events:", nEvents, " (weighted:", nEventsWeighted, ")")
    h1.Delete()
    return nEvents


# load config
config = XbbConfigReader.read(args.tag)
sampleInfo = ParseInfo(samples_path=config.get('Directories', args.fromFolder), config=config)
mcSamples = sampleInfo.get_samples(XbbConfigTools(config).getMC())


#print([x.name for x in mcSamples])


pruneThreshold = float(args.prune)

sampleGroups = []
for x in args.samples.split(','):
    sampleGroups.append(x.split('+'))

#print("Group", sampleGroups)
#print("Group", [x.group for x in mcSamples])


sampleCuts = args.cuts.strip().split(',')
if args.fc != '':
    cutGroups = [x.strip(',').split(',') for x in args.fc.strip(';').split(';')]
    # cartesian product
    sampleCuts = list(set(['&&'.join(x) for x in itertools.product(*cutGroups)]))
    print sampleCuts
    print "=> len=",len(sampleCuts)

countDict = {}
for sampleGroup in sampleGroups:
    count = 0
    for sampleIdentifier in sampleGroup:
        print "\x1b[32m",sampleIdentifier,"\x1b[0m"
        countDict[sampleIdentifier] = {}

        samples_matching = [x for x in mcSamples if x.identifier == sampleIdentifier]
        if len(samples_matching) > 0:
            sample = samples_matching[0]

            sampleTree = SampleTree({'sample': sample, 'folder': config.get('Directories',args.fromFolder).strip()}, config=config)
            print "CUT=",sampleCuts,":"
            for sampleCut in sampleCuts:

                #Special case because LO sample has a different naming convention for branches
                if sampleIdentifier == "DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8":
                    print("LO sample")
                    replaceCut = sampleCut.replace("NpNLO","Nb") 
                    print "CUT=",sampleCuts," replaced by ", replaceCut 
                    sampleCount = getEventCount(config, sampleIdentifier, replaceCut, sampleTree=sampleTree, sample=sample)

                else:
                    sampleCount = getEventCount(config, sampleIdentifier, sampleCut, sampleTree=sampleTree, sample=sample)
                print sampleIdentifier,sampleCut,"\x1b[34m=>",sampleCount,"\x1b[0m"
                if sampleCut in countDict[sampleIdentifier]:
                    print "duplicate!!", sampleIdentifier, sampleCut, countDict[sampleIdentifier][sampleCut]
                    raise Exception("duplicate")
                countDict[sampleIdentifier][sampleCut] = sampleCount
        else:
            print sampleIdentifier
            raise Exception("SampleMissing")

# pruning
for sampleIdentifier,counts in countDict.iteritems():
    specialweight = ''
    for cut,cutCount in counts.iteritems():
        totalCount = 0
        for sample,sampleCounts in countDict.iteritems():
            if cut in sampleCounts:
                totalCount += sampleCounts[cut]

        if cutCount>0 and totalCount>0:
            fraction = 1.0*cutCount/totalCount
            if fraction < pruneThreshold:
                print "\x1b[31mfor "+sampleIdentifier + " set count("+cut+") from %d"%counts[cut] + " to 0\x1b[0m"
                counts[cut] = 0

print "result:"
print countDict
print "-"*80
for i,cut in enumerate(sampleCuts,1):
    print i, cut
print "-"*80
for i,sampleGroup in enumerate(sampleGroups,1):
    print i,sampleGroup
print "-"*80
for sampleGroup in sampleGroups:
    cutLine = ""
    for cut in sampleCuts:
        f = 0.0
        for sampleIdentifier in sampleGroup:
            if sampleIdentifier in countDict and cut in countDict[sampleIdentifier]:
                f += countDict[sampleIdentifier][cut]
        cutLine += "%1.4f "%f
    print cutLine

print "-"*80

for sampleIdentifier,counts in countDict.iteritems():
    specialweight = ''
    for cut,cutCount in counts.iteritems():
        totalCount = 0
        for sample,sampleCounts in countDict.iteritems():
            if cut in sampleCounts:
                totalCount += sampleCounts[cut]

        if cutCount>0:
            if cutCount==totalCount:
                if len(specialweight) > 0:
                    specialweight += ' + '
                specialweight += '(' + cut + ')'
            else:
                if len(specialweight) > 0:
                    specialweight += ' + '
                specialweight += '(' + cut + ')*%1.5f'%(1.0*cutCount/totalCount)
    print sampleIdentifier, specialweight

print "-"*80


if True:

    #Dump to .ini file
    DY_samples_naming = { "DYJetsToLL_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"DYnloINCL0j", 
                   "DYJetsToLL_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"DYnloINCL1j",
                   "DYJetsToLL_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"DYnloINCL2j",
                   "DYJetsToLL_LHEFilterPtZ-0To50_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"DYnlo0",
                   "DYJetsToLL_LHEFilterPtZ-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"DYnlo100",
                   "DYJetsToLL_LHEFilterPtZ-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"DYnlo250",
                   "DYJetsToLL_LHEFilterPtZ-400To650_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8": "DYnlo400",
                   "DYJetsToLL_LHEFilterPtZ-50To100_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8":"DYnlo50",
                   "DYJetsToLL_LHEFilterPtZ-650ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8":"DYnlo650",
                   #LO sample, need to add it also
                   "DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8":"M10HTincl",
                   "DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8" : "DYnloINCL"
                    }

    WJ_samples_naming = { "WJetsToLNu_0J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"WJetsnloINCL0j",
                   "WJetsToLNu_1J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"WJetsnloINCL1j",
                   "WJetsToLNu_2J_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"WJetsnloINCL2j",
                   "WJetsToLNu_Pt-100To250_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"WJetsnlo100",
                   "WJetsToLNu_Pt-250To400_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"WJetsnlo250",
                   "WJetsToLNu_Pt-400To600_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" :"WJetsnlo400",
                   "WJetsToLNu_Pt-600ToInf_MatchEWPDG20_TuneCP5_13TeV-amcatnloFXFX-pythia8" : "WJetsnlo600",
                   "WJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-pythia8" : "WJetsnloINCL" 
                }

    ZJ_samples_naming = { "ZJetsToNuNu_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8" :"ZJetsHT100",
                   "ZJetsToNuNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8" :"ZJetsHT1200",
                   "ZJetsToNuNu_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8" :"ZJetsHT200",
                   "ZJetsToNuNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8" :"ZJetsHT2500",
                   "ZJetsToNuNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8" :"ZJetsHT400",
                   "ZJetsToNuNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8" :"ZJetsHT600",
                   "ZJetsToNuNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8" :"ZJetsHT800"
                }



    prefix = "ZJ"*("ZJ" in args.samples.split(",")[0]) +  "WJ"*("WJ" in args.samples.split(",")[0]) + "DY"*("DY" in args.samples.split(",")[0])


    output = str(args.tag) + "config/" + prefix + "_new_" + str(args.output)

    with open("./" + output, "w") as of:
        of.write("[Stitching]\n")
        #for sampleIdentifier,counts in countDict.iteritems():
        #    for cut,cutCount in counts.iteritems():
        #        of.write("{s},{c},{v}\n".format(s=sampleIdentifier,c=cut,v=cutCount))

        for sampleIdentifier,counts in countDict.iteritems():
            specialweight = ''
            specialtag = '' 

            if "ZJ" in sampleIdentifier:
                specialtag = ZJ_samples_naming[sampleIdentifier] 
            elif "WJ" in sampleIdentifier:
                specialtag = WJ_samples_naming[sampleIdentifier] 
            elif "DY" in sampleIdentifier:
                specialtag = DY_samples_naming[sampleIdentifier] 
            else:
                raise Exception("Check the config")


            for cut,cutCount in counts.iteritems():
                totalCount = 0
                for sample,sampleCounts in countDict.iteritems():
                    if cut in sampleCounts:
                        totalCount += sampleCounts[cut]

                if cutCount>0:
                    if cutCount==totalCount:
                        if len(specialweight) > 0:
                            specialweight += ' + '
                        specialweight += '(' + cut + ')'
                    else:
                        if len(specialweight) > 0:
                            specialweight += ' + '
                        specialweight += '(' + cut + ')*%1.5f'%(1.0*cutCount/totalCount)

            if len(specialweight) > 0:
                of.write(specialtag + " = " + specialweight + "\n")
            else:
                of.write(specialtag + " = 1.0\n")







"""
Old example



./getExtWeightsNew.py --samples 'ZJetsToNuNu_HT-100To200_13TeV-madgraph,ZJetsToNuNu_HT-200To400_13TeV-madgraph,ZJetsToNuNu_HT-400To600_13TeV-madgraph,ZJetsToNuNu_HT-600To800_13TeV-madgraph,ZJetsToNuNu_HT-800To1200_13TeV-madgraph,ZJetsToNuNu_HT-1200To2500_13TeV-madgraph,ZJetsToNuNu_HT-2500ToInf_13TeV-madgraph,ZBJetsToNuNu_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8,ZBJetsToNuNu_Zpt-200toInf_TuneCP5_13TeV-madgraphMLM-pythia8,ZJetsToNuNu_BGenFilter_Zpt-100to200_TuneCP5_13TeV-madgraphMLM-pythia8,ZJetsToNuNu_BGenFilter_Zpt-200toInf_TuneCP5_13TeV-madgraphMLM-pythia8'  --fc 'LHE_Nb==0&&nGenStatus2bHad==0,LHE_Nb==0&&nGenStatus2bHad>0,LHE_Nb>0;LHE_Vpt<100,LHE_Vpt>=100&&LHE_Vpt<200,LHE_Vpt>=200;LHE_HT>=0&&LHE_HT<100,LHE_HT>=100&&LHE_HT<200,LHE_HT>=200&&LHE_HT<400,LHE_HT>=400&&LHE_HT<600,LHE_HT>=600&&LHE_HT<800,LHE_HT>=800&&LHE_HT<1200,LHE_HT>=1200&&LHE_HT<2500,LHE_HT>=2500' --from HADDout -T Zvv2018 --prune 0.05 | tee V13_stitching_0p05_V3_ZNuNu.txt


"""





