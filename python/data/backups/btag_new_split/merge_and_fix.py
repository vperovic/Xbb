#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 15:57:42 2022

@author: vasilije
"""

import numpy as np
import pandas as pd
import os

def fix_csv_file(filename):
    #filename = "../btag/reshaping_deepJet_106XUL18_v2.csv"
    data = pd.read_csv(filename)
    data['OperatingPoint'] = data['OperatingPoint'].replace('L',0)
    data['OperatingPoint'] = data['OperatingPoint'].replace('M',1)
    data['OperatingPoint'] = data['OperatingPoint'].replace('T',2)
    data['OperatingPoint'] = data['OperatingPoint'].replace('shape',3)
    
    data['jetFlavor'] = data['jetFlavor'].replace(0,2)
    data['jetFlavor'] = data['jetFlavor'].replace(4,1)
    data['jetFlavor'] = data['jetFlavor'].replace(5,0)
    
    data.to_csv(filename,index=False)

os.system("rm ../btag/reshaping_deepJet_106XUL16postVFP_v3.csv")
os.system("rm ../btag/reshaping_deepJet_106XUL16preVFP_v2.csv")
os.system("rm ../btag/reshaping_deepJet_106XUL17_v3.csv")
os.system("rm ../btag/reshaping_deepJet_106XUL18_v2.csv")


os.system("awk '(NR == 1) || (FNR > 1)' wp_deepJet_106XUL16postVFP_v3.csv reshaping_deepJet_106XUL16postVFP_v3.csv > ../btag/reshaping_deepJet_106XUL16postVFP_v3.csv")
os.system("awk '(NR == 1) || (FNR > 1)' wp_deepJet_106XUL16preVFP_v2.csv reshaping_deepJet_106XUL16preVFP_v2.csv > ../btag/reshaping_deepJet_106XUL16preVFP_v2.csv")
os.system("awk '(NR == 1) || (FNR > 1)' wp_deepJet_106XUL17_v3.csv reshaping_deepJet_106XUL17_v3.csv > ../btag/reshaping_deepJet_106XUL17_v3.csv")
os.system("awk '(NR == 1) || (FNR > 1)' wp_deepJet_106XUL18_v2.csv reshaping_deepJet_106XUL18_v2.csv > ../btag/reshaping_deepJet_106XUL18_v2.csv")

fix_csv_file("../btag/reshaping_deepJet_106XUL16postVFP_v3.csv")
fix_csv_file("../btag/reshaping_deepJet_106XUL16preVFP_v2.csv")
fix_csv_file("../btag/reshaping_deepJet_106XUL17_v3.csv")
fix_csv_file("../btag/reshaping_deepJet_106XUL18_v2.csv")

