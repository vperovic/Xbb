/*****************************************************************************
 * Project: RooFit                                                           *
 *                                                                           *
 * This code was autogenerated by RooClassFactory                            *
 *****************************************************************************/

#ifndef CRYSTALBALLFLATEFFICIENCY
#define CRYSTALBALLFLATEFFICIENCY

#include "RooAbsCategory.h"
#include "RooAbsReal.h"
#include "RooAbsReal.h"
#include "RooCategoryProxy.h"
#include "RooRealProxy.h"

class CrystalBallFlatEfficiency : public RooAbsReal {
 public:
  CrystalBallFlatEfficiency(){};
  CrystalBallFlatEfficiency(const char* name, const char* title, RooAbsReal& _m,
                        RooAbsReal& _m0, RooAbsReal& _sigma, RooAbsReal& _alpha,
                        RooAbsReal& _n, RooAbsReal& _norm, RooAbsReal& _switchpt);
  CrystalBallFlatEfficiency(const CrystalBallFlatEfficiency& other,
                        const char* name = 0);
  virtual TObject* clone(const char* newname) const {
    return new CrystalBallFlatEfficiency(*this, newname);
  }
  inline virtual ~CrystalBallFlatEfficiency() {}

 protected:
  RooRealProxy m;
  RooRealProxy m0;
  RooRealProxy sigma;
  RooRealProxy alpha;
  RooRealProxy n;
  RooRealProxy norm;
  RooRealProxy switchpt;

  Double_t evaluate() const;

 private:
  ClassDef(CrystalBallFlatEfficiency, 1)
};

#endif
