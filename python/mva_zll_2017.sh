#!/bin/bash

# example: VHbb2017commonconfig/mva_bins.sh "arctans(15)"
# arctan shape binning with S(number of signal events) bins, max 15

REBINMETHOD=$1

echo "bin boundaries for method ${REBINMETHOD}"
echo ""

echo "dc:SR_low_Zmm.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_low_Zmm --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_low_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_low_Zee.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_low_Zee --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_low_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_medhigh_Zmm.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_medhigh_Zmm --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_medhigh_Zee.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_medhigh_Zee --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_med_Zmm_0j.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_med_Zmm_0j --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_med_Zee_0j.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_med_Zee_0j --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_med_Zmm_ge1j.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_med_Zmm_ge1j --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_med_Zee_ge1j.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_med_Zee_ge1j --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_high_Zmm.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high_Zmm --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_high_Zee.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high_Zee --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high1_Zmm.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high1_Zmm --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high1_Zee.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high1_Zee --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high2_Zmm.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high2_Zmm --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high2_Zee.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high2_Zee --vars DNN --set="plotDef:DNN.rebinMethod=${REBINMETHOD};plotDef:DNN.relPath=<!SR_medhigh_Zll|branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_high_Zmm_BOOST.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high_Zmm_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:BDT.relPath=BDT_Zll_BOOSTFinal_wdB.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
#echo "dc:SR_high_Zee_BOOST.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high_Zee_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:BDT.relPath=BDT_Zll_BOOSTFinal_wdB.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high1_Zmm_BOOST.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high1_Zmm_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:BDT.relPath=<!Eval|VH_BDT_branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high1_Zee_BOOST.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high1_Zee_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:BDT.relPath=<!Eval|VH_BDT_branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high2_Zmm_BOOST.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high2_Zmm_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:BDT.relPath=<!Eval|VH_BDT_branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`
echo "dc:SR_high2_Zee_BOOST.rebin_list="`./submit.py -J runplot -T Zll2017 -F tmp --regions SR_high2_Zee_BOOST --vars BDT --set="plotDef:BDT.rebinMethod=${REBINMETHOD};plotDef:BDT.relPath=<!Eval|VH_BDT_branchName!>.Nominal" --local 2>1 | grep -A1 "new bin boundaries" | tail -n1 | sed 's/INFO:  //g'`

