from __future__ import division

file = open(r'./logs_Zvv2018/runplot/Logs/runplot_2020_04_17-11_02_33_plot_run_VV_SR_med_Znn_ge1j_0_Zvv2018_.out', 'r')
WZ = 0
WW = 0
ZZ = 0
ZH = 0
ggZH = 0
WH = 0 
Zjets = 0
Wjets = 0
DYjets = 0
TT = 0
ST = 0

for line in file:
    
    num = line.split()[2].strip()
    print num.rstrip()

    if "WZTo" in line:
        print "line WZ ", line.strip()
        WZ = WZ + float(num)

    if "WW" in line:
        print "line WW ", line
        WW = WW + float(num)
#
    if "ZZ" in line:
        print "line ZZ ", line
        ZZ = ZZ + float(num)
#
    if any(x == line[:4] for x in ["ZllH","ZnnH"]):
        print "line ZH ", line
        ZH = ZH + float(num)
#
    if any(x == line[:7] for x in ["ggZnnH_","ggZllH_"]):
        print "line ggZH ", line
        ggZH = ggZH + float(num)
#
    if any(x in line for x in ["WplusH","WminusH"]):
        print "line WH ", line
        WH = WH + float(num)
##
    if any(x in line for x in ["ZJetsHT","ZBGen","ZBJe"]):
        print "line ZJets ", line
        Zjets = Zjets + float(num)
##
    if any(x in line for x in ["WJetsHT","WBGen","WBJe"]):
        print "line WJets ", line
        Wjets = Wjets + float(num)
#
    if any(x in line for x in ["M4HT","ZJets_","DYBJets","DYJets"]):
        print "line DYjets ", line
        DYjets = DYjets + float(num)
#
    if "TT" in line:
        TT = TT + float(num)
#
    if "ST" in line:
        ST = ST + float(num)
#


print "WZ: {} ".format(WZ)
print "WW: {} ".format(WW)
print "ZZ: {} ".format(ZZ)
print "ZH: {} ".format(ZH)
print "ggZH: {} ".format(ggZH)
print "WH: {} ".format(WH)
print "Zjets: {} ".format(Zjets)
print "Wjets: {} ".format(Wjets)
print "DYjets: {} ".format(DYjets)
print "TT: {} ".format(TT)
print "ST: {} ".format(ST)

